package com.bsea.test;

import javax.annotation.Resource;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.test.context.web.WebAppConfiguration;

import com.zz.bsea.controller.TestController;
@WebAppConfiguration
public class TestJunit {
	@Resource
	TestController testController;
	
	@Test
	public void testcontroller(){
		
		Assert.assertEquals("hello SpringBoot",testController.helloSpringBoot());
	}
}
