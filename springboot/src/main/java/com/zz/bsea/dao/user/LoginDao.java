package com.zz.bsea.dao.user;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.zz.bsea.dao.BaseDao;
import com.zz.bsea.mode.User;

@Service("lDao")
public class LoginDao {
	@Autowired
	BaseDao baseDao;
	public User getUserByNamePWD(String name, String pwd){
		User u=null;
		
		Connection con=baseDao.getConnection();
		String sql="select u_id,pwd,name from user where pwd=? and name=?";
		try {
			PreparedStatement ps=con.prepareStatement(sql);
			ps.setString(1, pwd);
			ps.setString(2, name);
			ResultSet rs=ps.executeQuery();
			while(rs.next()){
				u=new User();
				u.setId(rs.getInt("u_id"));
				u.setName(rs.getString("name"));
				u.setPwd(rs.getString("pwd"));
			}
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		return u;
	}

}
