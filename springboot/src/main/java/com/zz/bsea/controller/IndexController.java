package com.zz.bsea.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class IndexController {
	
	@RequestMapping("toIndex")
	public String toIndex(){
		
		return "/bsea/index2.html";
		
	}

}
