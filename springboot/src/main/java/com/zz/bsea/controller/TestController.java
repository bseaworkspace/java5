package com.zz.bsea.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TestController {
	
	
	@RequestMapping("hello")
	public String helloSpringBoot(){
		
		return  "hello SpringBoot";
	}
	@RequestMapping(value="helloGet",method=RequestMethod.GET)
	public String helloSpringBoot2(){
		
		return  "hello SpringBoot";
	}
	@RequestMapping(value="helloPost",method=RequestMethod.POST)
	public String helloSpringBoot3(){
		
		return  "hello SpringBoot";
	}

}
