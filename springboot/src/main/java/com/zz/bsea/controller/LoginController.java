package com.zz.bsea.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.zz.bsea.dao.user.LoginDao;
import com.zz.bsea.mode.User;

@RestController
public class LoginController {
	@Autowired
	LoginDao lDao;
	
	@RequestMapping(value="login",method=RequestMethod.POST)
	public String login(@RequestParam("name") String userName,@RequestParam("password") String pwd,HttpServletRequest request){
		System.out.println("userName"+userName+" ---pwd---"+pwd);
		User u=lDao.getUserByNamePWD(userName, pwd);
		String result="N";
		if(u!=null){
			result="Y";
			HttpSession session=request.getSession();
			session.setAttribute("user", u);
		}
		
		
		return result;
	}
	//@RequestMapping(value="login",method=RequestMethod.POST) 和
	//@PostMapping("login") 效果是一样的。只接收post请求
	@PostMapping("login")
	public String login2(){
		
		return "登录成功";
		
	}

}
