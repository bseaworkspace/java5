package com.zz.bsea.controller;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/test")
public class URLPathParamController {
	
	@RequestMapping("show/{page}/{uid}/ttt")
	public String test(@PathVariable("page") String p,@PathVariable("uid") String u){
		
		return "当前请求的页码是:"+p+"  用户id是："+u;
	}

}
