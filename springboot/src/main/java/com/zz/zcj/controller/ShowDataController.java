package com.zz.zcj.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.zz.zcj.mode.Product;

@RestController
public class ShowDataController {

	@RequestMapping("getData")
	public List<Product> getDate(HttpServletRequest request){
	List <Product> ls=new ArrayList();
	String name=(String)request.getSession().getAttribute("username");
	System.out.println("ShowDataController---->userName="+name);
	Product p1=new Product();
	p1.setName("p1");
	p1.setPayDate(new Date());
	p1.setStatuc("待发货");
	Product p2=new Product();
	p2.setName("p2");
	p2.setPayDate(new Date());
	p2.setStatuc("已发货");
	Product p3=new Product();
	p3.setName("p3");
	p3.setPayDate(new Date());
	p3.setStatuc("待确认");
	
	ls.add(p1);
	ls.add(p2);
	ls.add(p3);
	return ls;
}
}
