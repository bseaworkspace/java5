package com.zz.zcj.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMethod;
@RestController
public class TestController {
	@RequestMapping("hello")
	public String hellowSpringBoot(){
		return "hello SpringBoot";
	}
	@RequestMapping(value="hellowGet",method=RequestMethod.GET)
	public String hellowSpringBoot2(){
		return "hello SpringBoot";
	}
	@RequestMapping(value="helloPost",method=RequestMethod.POST)
	public String helloSpringBoot3(){
		
		return  "hello SpringBoot";
	}
}
