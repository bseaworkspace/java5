package com.zz.hc.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class Indexcontroller {

	@RequestMapping("toIndex")
	public String toindex(){
		 
		return "/hc/index.html";
	}
	
}
