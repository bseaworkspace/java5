package com.zz.hc.controller;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.zz.hc.dao.user.LoginDao;
import com.zz.hc.dao.user.RegisterDao;
import com.zz.hc.mode.User;

@RestController
public class RegisterController {

	
	@RequestMapping(value="register",method=RequestMethod.POST)
	public String register(@RequestParam("username") String userName,@RequestParam("pwd1") String pwd1,@RequestParam("pwd2") String pwd2,@RequestParam("email") String email,@RequestParam("phonenumber") String phonenumber){
		String result="";
		LoginDao ld=new LoginDao();
		
		Pattern pattern = Pattern.compile("^13\\d{9}||15[8,9]\\d{8}$");
        Matcher matcher = pattern.matcher(phonenumber);
        
        String check = "^([a-z0-9A-Z]+[-|\\.]?)+[a-z0-9A-Z]@([a-z0-9A-Z]+(-[a-z0-9A-Z]+)?\\.)+[a-zA-Z]{2,}$";    
		 Pattern regex = Pattern.compile(check);    
		 Matcher matcher1 = regex.matcher(email);      
        System.out.println(userName);
		if((userName == null ||"".equals(userName))||(pwd1 == null ||"".equals(pwd1))||(pwd2 == null ||"".equals(pwd2))||(email == null ||"".equals(email))||(phonenumber == null ||"".equals(phonenumber))){
			result="null";
		}
		else if(!matcher.matches()){
                result="phoneerror";
		}else if(!matcher1.matches()){
			result="emailerror";
		} 
		else if(!pwd1.equals(pwd2)){
			result="pwderror";
		}else if(userName.indexOf(" ")!=-1){
			
			result="usernameerror";
			
		}else{
			if(ld.getUserByName(userName)!=null){
				result="haveuser";
			}else{
				RegisterDao rd=new RegisterDao();
				User u1=new User();
				u1.setName(userName);
				u1.setPwd(pwd1);
				u1.setEmail(email);
				u1.setPhoneNumber(phonenumber);
				if(rd.addUser(u1)){
					result="yes";
				}
				
			}
		}

		return result;
	}
}
