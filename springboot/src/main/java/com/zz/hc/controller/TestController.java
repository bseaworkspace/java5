package com.zz.hc.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

//只接受传输数据RestController
@RestController
public class TestController {

	@RequestMapping("hello")
	public String helloSpringBoot(){
		  

		return "hello springBoot";
	}
	@RequestMapping(value="hello",method=RequestMethod.GET)
	public String helloSpringBoot2(){
		
		
		return "hello springBoot";
	}
	@RequestMapping(value="hello",method=RequestMethod.POST)
	public String helloSpringBoot3(){
		
		
		return "hello springBoot";
	}
}
