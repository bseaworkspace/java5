package com.zz.lsw.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.zz.lsw.model.User;

@Service("loginDao")
public class LoginDao {
	@Autowired
	BaseDao bd;
	public User getUserByNamePWD(String name,String pwd){
		User u=null;
		Connection con=bd.getConnection();
		String sql="SELECT u_id, u_name, u_pwd, u_nickname, u_email, u_phone, u_sex FROM zztask0.`user`where u_name=? and u_pwd=?";
		try {
			PreparedStatement ps=con.prepareStatement(sql);
			ps.setString(1, name);
			ps.setString(2, pwd);
			ResultSet rs=ps.executeQuery();
			while(rs.next()){
				 u=new User();
				u.setId(rs.getInt("u_id"));
				u.setName(rs.getString("u_name"));
				u.setPwd(rs.getString("u_pwd"));
				
			}bd.closeQuery(rs, ps, con);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return u;
				
	}
}	
