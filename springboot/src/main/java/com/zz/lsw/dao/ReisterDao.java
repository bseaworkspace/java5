package com.zz.lsw.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;



public class ReisterDao {
	BaseDao bd=new BaseDao();
	public boolean addUser(String userName,String pwd){
		boolean flag=false;
		Connection con=bd.getConnection();
		String sql="INSERT INTO zztask0.`user` ( u_name, u_pwd) VALUES(?,?);";
		try {
			PreparedStatement ps=con.prepareStatement(sql);
			ps.setString(1,userName );
			ps.setString(2, pwd);
			if(ps.executeUpdate()>0){
				flag=true;
			}bd.closeUpdate(ps, con);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return flag;
	}

}
