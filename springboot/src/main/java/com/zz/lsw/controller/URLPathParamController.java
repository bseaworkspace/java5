package com.zz.lsw.controller;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/test")
public class URLPathParamController {
	@RequestMapping("show/{page}")
	public String test(@PathVariable("page") String p){
		
		return "当前页面请求的页码是"+p+"用户id是";
	}

}
