package com.zz.lsw.controller;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.zz.lsw.dao.LoginDao;
import com.zz.lsw.model.User;

@RestController
public class LoginController {
	@Autowired
	LoginDao loginDao;
	
	Pattern patPunc=Pattern.compile("[`~!@#$^&*()=|{}':;',\\[\\].<>/?~！@#￥……&*（）——|{}【】‘；：”“'。，、？]$");
	@RequestMapping(value="login",method=RequestMethod.POST)
	public String login(@RequestParam("name")String userName,@RequestParam("pwd")String pwd,HttpServletRequest request){
		System.out.println("userName>>"+userName+"pwd>>"+pwd);
		
		User u=loginDao.getUserByNamePWD(userName, pwd);
		String result="N";
		Matcher matcher=patPunc.matcher(userName);
		Matcher matcher1=patPunc.matcher(pwd);
		if(u!=null){
			if(!matcher.find()&&!matcher1.find()){
			result="Y";
			
		}}else{
			result="X";
		}
		return result;
	}
}
