package com.zz.EDGE.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.zz.EDGE.model.Product;

@RestController
public class ShowdataController {
	@RequestMapping("getData")
	public List<Product> getData(HttpServletRequest request){
		List<Product> ls=new ArrayList();
		String name=(String) request.getSession().getAttribute("userName");
		System.out.println("ShowDataController-->userName="+name);
		Product p1=new Product();
		p1.setName("p1");
		p1.setPayDate("payDate");
		p1.setStatuc("待发货");
		Product p2=new Product();
		p2.setName("p2");
		p2.setPayDate("payDate");
		p2.setStatuc("发货中");
		Product p3=new Product();
		p3.setName("p3");
		p3.setPayDate("payDate");
		p3.setStatuc("待确认");
		ls.add(p1);
		ls.add(p2);
		ls.add(p3);
		return ls;
	}
}
