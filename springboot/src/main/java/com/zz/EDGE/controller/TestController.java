package com.zz.EDGE.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TestController {
//必须在头文件下面，就是在com.zz.EDGE下面
	//专门传值
	@RequestMapping("hello")
	public String helloSpringBoot(){
		return "hello SpringBoot";
	}
	
	@RequestMapping(value="hello",method=RequestMethod.GET)
	public String helloSpringBoot2(){
		return "hello SpringBoot";
	}
	@RequestMapping(value="hello",method=RequestMethod.POST)
	public String helloSpringBoot3(){
		return "hello SpringBoot";
	}
}
