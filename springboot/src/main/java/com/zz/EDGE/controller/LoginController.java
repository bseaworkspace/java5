package com.zz.EDGE.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class LoginController {
	
	@RequestMapping(value="login",method=RequestMethod.POST)
	public String login(@RequestParam("name") String userName,@RequestParam("password") String pwd,HttpServletRequest request){
		System.out.println("userName"+userName+"--pwd--"+pwd);
		HttpSession session=request.getSession();
		session.setAttribute("userName", userName);
		return "Y";
		}
	}
