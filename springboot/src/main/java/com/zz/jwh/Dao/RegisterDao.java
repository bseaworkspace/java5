package com.zz.jwh.Dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import com.zz.hc.dao.BaseDao;
import com.zz.hc.mode.User;

public class RegisterDao {
	
	
	
	
	public boolean addUser(User u){
		BaseDao bd=new BaseDao();
		boolean flag=false;
		Connection con=bd.getConnection();
		String sql="INSERT INTO zztask1.`user`( pwd, phoneNumber, name, email)VALUES(?,?,?,?);	";
		try {
			PreparedStatement ps=con.prepareStatement(sql);
			ps.setString(1, u.getPwd());
			ps.setString(2, u.getPhoneNumber());
			ps.setString(3, u.getName());
			ps.setString(4, u.getEmail());
			if(ps.execute()){
				flag=true;
			}bd.closeUpdate(ps, con);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		
		return flag;
		
	}
	

}
