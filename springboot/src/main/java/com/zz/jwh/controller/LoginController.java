package com.zz.jwh.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.zz.jwh.BaseDao.BaseDao;
import com.zz.jwh.Dao.LoginDao;

@RestController
public class LoginController {
 @Autowired 
 LoginDao loginDao;

	
	
	@RequestMapping(value="login",method=RequestMethod.POST)
	public String login(@RequestParam("name")String userName,@RequestParam("pwd")String pwd,HttpServletRequest request){
		System.out.println("userName>>"+userName+"pwd>>"+pwd);
		HttpSession session=request.getSession();
		session.setAttribute(userName, userName);
		return "Y";
	}
}
