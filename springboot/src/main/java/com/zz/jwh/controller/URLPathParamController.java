package com.zz.jwh.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/URLPathParam")
public class URLPathParamController {

	
	
	@RequestMapping("/show/{page}/{num}/test")
	public String test(@PathVariable("page") String p,@PathVariable("num") int num){
		return "当前页面是"+p+"在第"+num+"个字";
	}
	
	@PostMapping("loginPost")
	public String test2(){
		return "";
	}
}
