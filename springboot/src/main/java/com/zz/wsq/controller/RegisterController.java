package com.zz.wsq.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.zz.wsq.dao.user.LoginDao;
import com.zz.wsq.model.User;

@RestController
public class RegisterController {
	@Autowired
	LoginDao loginDao;
	@RequestMapping(value = "register", method = RequestMethod.POST)

	public String register(@RequestParam("userName") String userName, @RequestParam("pwd") String pwd,
			@RequestParam("pwd1") String pwd1, @RequestParam("email") String email,
			@RequestParam("phoneNumber") String phoneNumber) {
		//LoginDao ld = new LoginDao();
		User u = loginDao.getUserByNamePwd(userName, pwd);
		User u1 = new User();
		u1.setName(userName);
		u1.setPwd(pwd);
		u1.setPhoneNumber(phoneNumber);
		u1.setEmail(email);
		String result = "N";
		if(userName!=null&&userName!=""&&pwd!=null&&pwd!=""&&email!=null&&email!=""&&phoneNumber!=null&&phoneNumber!=""){
		if (pwd.equals(pwd1)) {
		    if (u == null) {
			
				if (loginDao.add(u1)) {
					result = "Y";
				}
			}
		 }
		}else{
			result="S";
		}
		return result;
	}

}
