package com.zz.wsq.controller;

import javax.websocket.server.PathParam;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/test")
public class URLPallParamController {
	@RequestMapping("show/{page}/{uid}") //show是拦截地址
	public String test(@PathVariable("page") String p,@PathVariable("uid") String u){
		return "当前请求的页面:"+p + "用户id:"+u;
	}
}
