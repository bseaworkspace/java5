package com.zz.simpleSpade.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.zz.simpleSpade.model.User;

@Service("userDao")
public class UserDao {
	
	@Autowired
	BaseDao baseDao;

	public User getUserByName(String name, String pwd) {
		User u = null;
		Connection con = baseDao.getConnection();
		String sql = "select u_id,u_pwd,u_name from user where u_pwd=? and u_name=?";
		try {
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setString(1, pwd);
			ps.setString(2, name);
			ResultSet rs = ps.executeQuery();
			if (rs.next()) {
				u = new User();
				u.setU_id(rs.getInt("u_id"));
				u.setU_name(rs.getString("u_name"));
				u.setU_pwd(rs.getString("u_pwd"));
			}
			baseDao.closeQuery(rs, ps, con);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return u;
	}

	public boolean insertUser(User user) {
		boolean flag = false;
		Connection con = baseDao.getConnection();
		String sql = "insert into zztask0.`user` (u_name,u_nickname,u_pwd)values(?,?,?)";
		try {
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setString(1, user.getU_name());
			ps.setString(2, user.getU_nickname());
			ps.setString(3, user.getU_pwd());
			if(ps.executeUpdate()>0){
				flag = true;
			}
			baseDao.closeUpdate(ps, con);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return flag;
	}
}
