package com.zz.simpleSpade.controller;

import java.util.Date;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.zz.simpleSpade.model.Product;

@RestController
public class ShowDataController {
	
	@RequestMapping("getData")
	public List<Product> getData(HttpServletRequest request){
		List<Product> ls = null;
		
		ls = new ArrayList<Product>();
		System.out.println(request.getSession().getAttribute("userName"));
		
		Product p1 = new Product();
		p1.setName("p1");
		p1.setParDate(new Date());
		p1.setStatus("待发货");
		
		Product p2 = new Product();
		p2.setName("p2");
		p2.setParDate(new Date());
		p2.setStatus("发货中");
		
		Product p3 = new Product();
		p3.setName("p3");
		p3.setParDate(new Date());
		p3.setStatus("待确定");
		
		ls.add(p1);
		ls.add(p2);
		ls.add(p3);
		
		return ls;
	}
}
