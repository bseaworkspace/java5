package com.zz.simpleSpade.controller;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class URLPathParamController {
	
	@RequestMapping("show/{page}/{uid}")
	public String test(@PathVariable("page")String p,@PathVariable("uid")String u){
		
		return "当前请求的页码是："+p+"   用户id是："+u;
		
	}
}
