package com.zz.simpleSpade.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.zz.simpleSpade.dao.UserDao;
import com.zz.simpleSpade.model.User;


@RestController
public class RegisterController {

	@RequestMapping(value="register",method=RequestMethod.POST)
	public String register(@RequestParam("u_name")String u_name,@RequestParam("u_nickname")String u_nickname,@RequestParam("pwd")String pwd,@RequestParam("repwd")String repwd){
		String result = "N";
		User user = new User();
		user.setU_name(u_name);
		user.setU_nickname(u_nickname);
		user.setU_pwd(pwd);
		UserDao ud = new UserDao();
		if(ud.insertUser(user)){
			result = "Y";
		}
		return result;
	}
}
