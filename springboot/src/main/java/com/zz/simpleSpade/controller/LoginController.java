package com.zz.simpleSpade.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.zz.simpleSpade.dao.LoginDao;

@RestController
public class LoginController {
	
	@Autowired
	LoginDao loginDao;
	
	@RequestMapping(value="login",method=RequestMethod.POST)
	public String login(@RequestParam("name")String userName,@RequestParam("pwd")String pwd,HttpServletRequest request){
		System.out.println("userName>>>>"+userName+"  pwd>>>"+pwd);
		HttpSession session = request.getSession();
		session.setAttribute("userName", userName);
		return "Y";
	}
	
/*	@PostMapping("login")
	public String login2(){
		
		return "post";
		
	}
	
	@GetMapping("login")
	public String login3(){
		
		return "get";
		
	}*/
}
