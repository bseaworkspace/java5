package com.zz.simpleSpade.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TestController {
	@RequestMapping("hello")
	public String helloSpring(){
		return "hello SpringBoot!";
	}
	@RequestMapping(value="helloGet",method=RequestMethod.GET)
	public String helloSpring2(){
		return "hello SpringBoot! Get";
	}
	@RequestMapping(value="helloPost",method=RequestMethod.POST)
	public String helloSpring3(){
		return "hello SpringBoot! Post";
	}
}
