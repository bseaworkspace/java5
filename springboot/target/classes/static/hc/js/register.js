$(document).ready(function(){
	
	$("#rstbut").click(function(){
		var username=$("#name").val();
		var pwd1=$("#pwd1").val();
		var pwd2=$("#pwd2").val();
		var email=$("#email").val();
		var phonenumber=$("#phonenumber").val();
		$.post("/register", { username: username, pwd1: pwd1,pwd2:pwd2,email:email, phonenumber:phonenumber},
		          function(data){
	          if(data=="null"){
	        	  $("#context").text("必填项不能为空");	        	  
	        	  $('#myModal').modal('show');
	          }else if(data=="phoneerror"){
	        	  $("#context").text("手机号格式不正确");	        	  
	        	  $('#myModal').modal('show');
	          }else if(data=="emailerror"){
	        	  $("#context").text("邮箱格式不正确");	        	  
	        	  $('#myModal').modal('show');
	          }else if(data=="pwderror"){
	        	  $("#context").text("两次密码不一致");	        	  
	        	  $('#myModal').modal('show');
	          }else if(data=="usernameerror"){
	        	  $("#context").text("用户名包含非法字符");	        	  
	        	  $('#myModal').modal('show');
	          }else if(data=="haveuser"){
	        	  $("#context").text("用户名已存在");	        	  
	        	  $('#myModal').modal('show');
	          }else if(data="yes"){
	        	  window.location.href="/hc/login.html";
	          }
	          
	          })
		
		
	})
	
	
})