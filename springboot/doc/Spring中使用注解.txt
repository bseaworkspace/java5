
可在Java代码中使用@Resource或者@Autowired注解进行装配，但需在XML中配置以下信息 
xmlns:context="http://www.springframework.org/schema/context" 
xsi:schemaLocation="http://www.springframework.org/schema/context 
http://www.springframework.org/schema/context/spring-context-2.5.xsd" 
然后显式的配置<context:annotation-config/> 
该配置隐式注册了多个对注解进行解析的处理器，如下列举 
AutowiredAnnotationBeanPostProcessor      CommonAnnotationBeanPostProcessor 
PersistenceAnnotationBeanPostProcessor    RequiredAnnotationBeanPostProcessor 
其实，注解本身做不了任何事情，和XML一样，只起到配置的作用，主要在于背后强大的处理器 

另外，比较建议使用@Resource注解，而不要使用@Autowired注解 
因为@Autowired注解是Spring提供的，而@Resource注解是J2EE提供的 
在JDK6中就已经包含@Resource注解了，所以它没有跟Spring紧密耦合 
并且在使用Spring时，若使用了JSR-250中的注解，如@Resource//@PostConstruct//@PreDestroy 
那么还需要Spring安装目录中的SPRING_HOME\\lib\\j2ee\\common-annotations.jar包的支持 
这里面的@Resource注解就是在SPRING_HOME\\lib\\j2ee\\common-annotations.jar中的 

@Resource注解 
@Resource注解和@Autowired一样，也可以标注在字段或属性的setter方法上 
@Resource默认按名称装配，名称可以通过name属性指定。当找不到与名称匹配的bean时，才会按类型装配 
若注解标注在字段上且未指定name属性，则默认取字段名作为bean名称寻找依赖对象 
若注解标注在setter上且未指定name属性，则默认取属性名作为bean名称寻找依赖对象 
如果没有指定name属性，并且按照默认的名称仍找不到依赖对象时，它就会按类型匹配 
但只要指定了name属性，就只能按名称装配了 

@Autowired注解 
@Autowired默认是按类型装配对象的，默认情况下它要求依赖对象必须存在 
如果允许null值，可以设置它的required属性为FALSE，如@Autowired(required=false) 
若想要按名称装配，可以结合@Qualifier注解一起使用，如@Autowired(required=false)  @Qualifier("personDaoBean") 

另外

在一个稍大的项目中通常会有上百个组件，如果都使用XML的bean定义来配置组件的话 
显然会增加配置文件的体积，查找及维护也不方便 
而Spring2.5就为我们引入了组件自动扫描机制 
它可以在classpath下寻找标注了@Service、@Repository、@Controller、@Component注解的类 
并把这些类纳入Spring容器中管理，它的作用和在XML中使用bean节点配置组件是一样的 
使用自动扫描机制，则需配置<context:component-scan base-package="com.jadyer"/>启动自动扫描 
其中base-package指定需要扫描的包，它会扫描指定包中的类和子包里面类 
@Service用于标注业务层组件 
@Repository用于标注数据访问组件，即DAO组件 
@Controller用于标注控制层组件，如Struts中的Action 
@Component泛指组件，当组件不要好归类时，可以使用这个注解进行标注 

1、可以使用诸如@Service("personDao")修改bean名称，而它默认的是将首字母小写的类名作为<bean>名称 
2、若要更改<bean>作用域的话，可以使用@Scope("prototype")注解来修改<bean>作用域 
3、若想让<bean>实例化之后去执行初始化方法，可以使用@PostConstruct标注在方法上 
4、同样@PreDestroy注解标注在方法上，可以用来指定<bean>销毁时执行的方法 
这里的@PostConstruct是EJB3里面用来初始化bean的注解，它也不是Spring中的注解 
并且<context:component-scan base-package=""/>的背后注册了很多用于解析注解的处理器 
其中就包括了<context:annotation-config/>配置项里面的注解所使用的处理器 
所以配置了<context:component-scan base-package="">之后，便无需再配置<context:annotation-config> 

