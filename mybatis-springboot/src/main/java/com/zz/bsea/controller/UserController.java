package com.zz.bsea.controller;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.zz.bsea.model.User;
import com.zz.bsea.service.PieDataService;
import com.zz.bsea.service.UserService;
import com.zz.bsea.util.PieDataUtil;

@RestController
public class UserController {
	@Autowired
	UserService userService;
	
	@Autowired
	PieDataService pieDataService;
	
	@Autowired
	PieDataUtil pieDataUtil;
	
	@RequestMapping("/showUser")
	public String getName(){
		User u=userService.selectById(1);
		return u.getName();
	}
	
	@RequestMapping("/showPieData")
	public Map<String,List> showPieData(){
		return pieDataUtil.getPieMap(pieDataService.selectPieData());
	}
	@RequestMapping("/addUser")
	public User addUser(@RequestParam("name") String name,@RequestParam("mobile") String mobile,@RequestParam("email") String email,@RequestParam("sex") String sex){
		User u=new User();
		u.setCredits(0);
		u.setLevel("1");
		u.setMobile(mobile);
		u.setName(name);
		u.setSex(sex);
		int r=userService.addUser(u);
		String result="N";
		if(r>0){
			
			result="Y";
		}
		return u;
	}
	
	 @RequestMapping("/upload")
	 public void uploadFile(@RequestParam(value = "file" , required = true) MultipartFile file) {
	        //deal with file
		 System.out.println("file.getName()===="+file.getName());
		 System.out.println("file.getName()===="+file.getOriginalFilename());
		 String fileName=file.getOriginalFilename().substring(file.getOriginalFilename().lastIndexOf("\\"));
		 try {
			InputStream inputStream=file.getInputStream();
			OutputStream os= new FileOutputStream(new File("C:\\bsea\\wp2\\java5\\java5\\mybatis-springboot\\src\\main\\resources\\static\\bsea\\img\\"+fileName));
			byte[] b=new byte[(int)file.getSize()];
			inputStream.read(b);
			os.write(b);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		 
	  }

}
