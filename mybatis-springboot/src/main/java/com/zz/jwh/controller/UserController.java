package com.zz.jwh.controller;

import javax.xml.ws.RequestWrapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.zz.jwh.service.UserService;

@RestController
public class UserController {
	@Autowired
	UserService userService;
	
	
	@RequestMapping("showUser")
	public String getName(){
		return userService.selectById(1).getName();
		
	}

}
