package com.zz.simpleSpade.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.zz.simpleSpade.dao.ProductMapper;
import com.zz.simpleSpade.model.Product;

@RestController
public class RecordController {

	@Autowired
	ProductMapper productMapper;

	@RequestMapping("returnProduct")
	public Product getProduct(@RequestParam("products") String products, @RequestParam("count") String count) {

		System.out.println(products);
		//JSONArray json = JSONArray.fromObject(products);

		System.out.println(count);
		return null;

	}

	@RequestMapping("allProduct")
	public List<Product> getAllProduct() {

		return productMapper.selectAll();

	}

}
