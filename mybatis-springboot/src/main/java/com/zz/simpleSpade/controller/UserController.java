package com.zz.simpleSpade.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.zz.simpleSpade.model.User;
import com.zz.simpleSpade.service.UserService;



@RestController
public class UserController {
	@Autowired
	UserService userService;
	
	@RequestMapping("showUser")
	public String getName(){
		
		return userService.selectById(1).getName();
	}
	
	@RequestMapping("addUser")
	public String addUser(@RequestParam("userName")String userName,@RequestParam("phone")String phone,@RequestParam("email")String email,@RequestParam("sex")String sex){
		User u = new User();
		
	
		int r =  userService.insertSelective(u);
		String result="N";
		if(r > 0){
			result="Y";
		}
		return result;
	}
	

}
