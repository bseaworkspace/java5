package com.zz.EDGE.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.zz.EDGE.model.User;
import com.zz.EDGE.service.UserService;

@RestController
public class UserController {
@Autowired 
UserService userService;
@RequestMapping("showUser")
public String getName(){
	
	return userService.selectById(1).getName();
}
@RequestMapping("addUser")
public User addUser(@RequestParam("name") String name,@RequestParam("mobile") String mobile,@RequestParam("email") String email,@RequestParam("sex") String sex){
	System.out.println(1213);
	User u=new User();
	u.setCredits(0);
	u.setLevel("1");
	u.setMobile(mobile);
	u.setName(name);
	u.setSex(sex);
	int r=userService.addUser(u);
	String result="N";
	if(r>0){
		result="Y";
	}
	return u;
}

}
