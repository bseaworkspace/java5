package com.zz.zcj.controller;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.zz.zcj.model.User;
import com.zz.zcj.service.PieDataService;
import com.zz.zcj.service.UserService;
import com.zz.zcj.util.PieDataUtil;

@RestController
public class UserController {
	@Autowired
	UserService userService;
	
	@Autowired
	PieDataService pieDataService;
	
	@Autowired
	PieDataUtil pieDataUtil;
	
	@RequestMapping("/showUser")
	public String getName(){
		User u=userService.selectById(1);
		return u.getName();
	}
	
	@RequestMapping("/showPieData")
	public Map<String,List> showPieData(){
		return pieDataUtil.getPieMap(pieDataService.selectPieData());
	}
	@RequestMapping("/addUser")
	public User addUser(@RequestParam("name") String name,@RequestParam("mobile") String mobile,@RequestParam("email") String email,@RequestParam("sex") String sex){
		User u=new User();
		u.setCredits(0);
		u.setLevel("1");
		u.setMobile(mobile);
		u.setName(name);
		u.setSex(sex);
		int r=userService.addUser(u);
		String result="N";
		if(r>0){
			
			result="Y";
		}
		return u;
	}

}
