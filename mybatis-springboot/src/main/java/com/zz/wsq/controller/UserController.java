package com.zz.wsq.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.zz.wsq.service.UserService;

 @RestController
public class UserController {
	@Autowired
	UserService userService;
	@RequestMapping("showUser")
	public String getName(){
		return userService.selectById(1).getName();
	}

}
