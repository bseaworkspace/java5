package com.zz.wsq.controller;



import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.zz.wsq.dao.UserDao;
import com.zz.wsq.model.User;
import com.zz.wsq.service.UserService;

 @RestController
public class UserController2 {
	@Autowired
	UserService userService;
	
	
	
	@RequestMapping("/addUser")
	public Map addUser(@RequestParam("name") String name,@RequestParam("mobile") String mobile,@RequestParam("level") String level,@RequestParam("birthday") String birthday,@RequestParam("userable")int userable,@RequestParam("history") int history,@RequestParam("email") String email,@RequestParam("sex") String sex){
		
		User u=new User();
		Map m=new HashMap();
		UserDao ud=new UserDao();
		u.setCredits(0);
		u.setLevel(level);
		u.setMobile(mobile);
		u.setName(name);
		u.setHistory(history);
		u.setAvailableIntegral(userable);
	    u.setBirthday(birthday);
		u.setSex(sex);
		String result="N";
		if(ud.getUserByName(name).size()==0){
			int r=userService.addUser(u);
			result="Y";
		}else{
			result="N";
		
		
		}
		
	
		m.put("rs", result);
			
	
		
		return m;
	
	}
}
