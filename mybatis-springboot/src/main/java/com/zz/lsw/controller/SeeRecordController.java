package com.zz.lsw.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.zz.lsw.service.RecordService;
import com.zz.lsw.view.model.AllRecord;
import com.zz.lsw.view.model.Record;

@RestController
public class SeeRecordController {
	@Autowired
	RecordService recordService;
	
	@RequestMapping("seeRecord")
	public List<AllRecord> getDate(){
		Map m=new HashMap();
		System.out.println("______________________");
		List<AllRecord> ls=recordService.selectAllRecord();
		m.put("rs", ls);
		System.out.println("______________________"+ls.get(0).getName());
		return ls;
	};
	
	
}
	