package com.zz.lsw.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.zz.lsw.model.User;
import com.zz.lsw.service.UserService;

@RestController
public class UserController {
	@Autowired
	UserService userService;
	
	@RequestMapping("showUser")
	public String getName(){
		
		return userService.selectById(1).getName();
	}
	
	@RequestMapping("addUser")
	public String addUser(@RequestParam("name") String username,@RequestParam("mobile") String mobile,@RequestParam("email") String email,@RequestParam("sex") String sex){
		User u=new User();
		u.setCredits(0);
		u.setLevel("1");
		u.setMobile(mobile);
		u.setName(username);
		u.setSex(sex);
		String result;
		int r=userService.insertSelective(u);
		if(r>0){
			 result="Y";
			
		}else{
			 result="N";
		}
		return result;
		
	}
}