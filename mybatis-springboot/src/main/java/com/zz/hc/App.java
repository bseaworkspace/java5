package com.zz.hc;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletComponentScan;

@SpringBootApplication
@ServletComponentScan
@MapperScan("com.zz.hc.dao")
public class App {

	public static void main(String[] args) { 
		// TODO Auto-generated method stub
		  SpringApplication.run(App.class, args);
	}

}
