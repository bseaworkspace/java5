package com.zz.hc.controller;
 
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.zz.hc.model.Product;
import com.zz.hc.service.ProductService;
import com.zz.hc.util.ReadExcel;
 

@RestController
public class ProductController {

	@Autowired
	ProductService productService;
	
	
	
	@RequestMapping("viewProduct")
	public Map viewProduct(){
		Map m=new HashMap();
		
		List<Product>ls=productService.selectAllProduct();
		
		m.put("rs", ls);
		
		return m;
	}
	
	@PostMapping("addProduct")
	public Map addProduct(@RequestParam("name") String name,@RequestParam("type") String type,@RequestParam("number") Integer number,@RequestParam("integral") Integer integral,@RequestParam("price") Double price){
		
		Map m=new HashMap();
		Product p=new Product();
		p.setName(name);
		p.setType(type);
		p.setPrice(price);
		p.setIntegral(integral);
		p.setStock(number);
		productService.insertSelective(p);
	
			m.put("rs", "Y");

	
		return m;
	}
	@PostMapping("updataProduct")
	public Map updataProduct(@RequestParam("id") Integer id,@RequestParam("name") String name,@RequestParam("type") String type,@RequestParam("number") Integer number,@RequestParam("integral") Integer integral,@RequestParam("price") Double price){
		
		Map m=new HashMap();
		Product p=new Product();
		p.setId(id);
		p.setName(name);
		p.setType(type);
		p.setPrice(price);
		p.setIntegral(integral);
		p.setStock(number);
//		productMapper.updateByPrimaryKey(p);
		productService.updateByPrimaryKeySelective(p);
		
		m.put("rs", "Y");
		
		
		return m;
	}
	@PostMapping("deletProduct")
	public Map deletProduct(@RequestParam("pid") Integer id){
		
		Map m=new HashMap();
	
		productService.deleteByPrimaryKey(id);
		
		
		m.put("rs", "Y");
		
		
		return m;
	}
	
	
	
	 @RequestMapping("/upload")
	 public Map uploadFile(@RequestParam(value = "file" , required = true) MultipartFile file,HttpServletRequest request) {
	        //deal with file
		 Map m=new HashMap();
		 List<Product> ls=new ArrayList();
		 System.out.println("file.getName()===="+file.getName());
		 System.out.println("file.getName()===="+file.getOriginalFilename());
		 System.out.println(request.getContextPath());
		 String fileName=file.getOriginalFilename().substring(file.getOriginalFilename().lastIndexOf("\\"));
		 try {
			InputStream inputStream=file.getInputStream();
			File f=new File(fileName);
			OutputStream os= new FileOutputStream(f);
			byte[] b=new byte[(int)file.getSize()];
			inputStream.read(b);
			os.write(b);
			os.close();
			inputStream.close();
			ReadExcel re=new ReadExcel();
			ls=re.readExcel(f);
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		m.put("rs", ls);
		 return m;
		 
	  }
	
	 @PostMapping("addAllProduct")
		public Map addAllProduct(@RequestParam("ls")  Object[] ls){
			Product p=new Product();
			List<Product> pls=productService.selectAllProduct();
			List<String> namels=new ArrayList<String>();
			for(Product p1:pls){
				namels.add(p1.getName());
			}
			for(int i=0;i<ls.length;i++){
				System.out.println(ls[i]);
			
				if(i%5==0){ p.setName((String)ls[i]);}
				if(i%5==1){p.setIntegral(Integer.parseInt((String) ls[i]));}
				if(i%5==2){p.setPrice(Double.parseDouble((String) ls[i]));}
				if(i%5==3){p.setStock(Integer.parseInt((String) ls[i]));}
				if(i%5==4){
					p.setType((String)ls[i]);
					if(namels.contains(p.getName())){
						for(Product p2:pls){
							if(p2.getName().equals(p.getName())){
								p.setId(p2.getId());
								p.setStock(p2.getStock()+p.getStock());
							}
						}
						productService.updateByPrimaryKeySelective(p);
					}else{
						productService.insertSelective(p);
					}
					
					
					 p=new Product();
				}
				

				
			}
			Map m=new HashMap();
			
m.put("rs", "Y");
		
			return m;
		}
	
}
