package com.zz.hc.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.zz.hc.dao.UserDao;
import com.zz.hc.model.Product;
import com.zz.hc.model.User;
import com.zz.hc.service.UserService;

@RestController
public class UserController {
	
	@Autowired
	UserService userService ;
	@RequestMapping("showUser")
	public  String getName(){
		
		
		return userService.selectById(1).getName();
	}
	
	@RequestMapping("/addUser")
	public Map addUser(@RequestParam("name") String name,@RequestParam("mobile") String mobile,@RequestParam("level") String level,@RequestParam("birthday") String birthday,@RequestParam("userable")int userable,@RequestParam("history") int history,@RequestParam("email") String email,@RequestParam("sex") String sex){
		
		User u=new User();
		Map m=new HashMap();
		UserDao ud=new UserDao();
		u.setCredits(0);
		u.setLevel(level);
		u.setMobile(mobile);
		u.setName(name);
		u.setHistory(history);
		u.setAvailableIntegral(userable);
	    u.setBirthday(birthday);
		u.setSex(sex);
		String result="N";
		if(ud.getUserByName(name).size()==0){
			int r=userService.addUser(u);
			result="Y";
		}else{
			result="N";
		
		
		}
		
	
		m.put("rs", result);
			
	
		
		return m;
	
	}
	
	@RequestMapping("checkUser")
	public  Map addUser(@RequestParam("page") int page){
		User u=new User();
		UserDao ud=new UserDao();
		List<User> ls=new ArrayList();
		Map m=new HashMap();
		ls=ud.ListUser(page);
		int count1=0;
        for(User u1:ls){
        	
    	if(u1.getCredits()>5000){
			ls.get(count1).setMembership("黄金会员");
		}else if(u1.getCredits()>1000){
			ls.get(count1).setMembership("白银会员");
		}else{
			ls.get(count1).setMembership("普通会员");
		}
    	count1++;
    }
		m.put("rs", ls);
		System.out.println(ls.get(1).getMembership());
		int num=ud.selectUserNum();
		int totalPages=num%10>0?(num/10)+1:num/10;
	
		Map pages=new HashMap();
		
		//count 用来标记， 当前显示了多少页（目前我们设计是，一次最多显示5页）
		int count=0;
		//totalPages 从后台传过来的，表示整个数据，经过计算以后，总的页数。
		for(int i=1;i<totalPages+1;i++){
			//这个表示，需要显示页码的起始位置。
			int startPage=page-2;
			//这个表示，需要显示页码的末尾位置。
			int endPage=page+2;
			if((startPage<3&&count<5)||(startPage<=i&&i<=endPage&&count<5)){
				count++;
				if(page==i){
					pages.put(i, "active");
					
				}else{
					pages.put(i, " ");

				}
			}
		}
		
		m.put("pages", pages);
		
		
		return m;
	}
	
	@RequestMapping("serchMember")
	public Map serchMember(@RequestParam("phonenumber") String phonenumber){
		System.out.println(phonenumber);
		Map m=new HashMap();
		List<User> ls=userService.ListuserByNumber(phonenumber);
		int count1=0;
        for(User u1:ls){
        	
    	if(u1.getCredits()>5000){
			ls.get(count1).setMembership("黄金会员");
		}else if(u1.getCredits()>1000){
			ls.get(count1).setMembership("白银会员");
		}else{
			ls.get(count1).setMembership("普通会员");
		}
    	count1++;
    }
		m.put("rs", ls);
		return m;
	}
	
	
	
	@RequestMapping("integralNumber")
	public Map integralNumber(@RequestParam(value="integralPhone",required=false) String integralPhone ){
		System.out.println(integralPhone);
		List<User> ls=new ArrayList<User>();
		if(integralPhone==null){
			ls=userService.ListuserByNumber("%1%");
		}else{
		 ls=userService.ListuserByNumber("%"+integralPhone+"%");
			
		}
		
		
		Map m=new HashMap();

		m.put("integralLs", ls);
		return m;
	}
	
	@RequestMapping("updatastock")
	public Map updatastock(@RequestParam(value="credits",required=false) Integer credits ,@RequestParam("integralPhone") String integralPhone){
		Map m=new HashMap();
		List<User> ls=userService.ListuserByNumber(integralPhone);
	
		if(credits>ls.get(0).getCredits()){
			m.put("rs", "fail");
			
		}else{
			ls.get(0).setCredits(ls.get(0).getCredits()-credits);
			
			userService.updateByPrimaryKeySelective(ls.get(0));
			m.put("rs", "Y");
		}
		
		
		
		
		return m;
	}

}
