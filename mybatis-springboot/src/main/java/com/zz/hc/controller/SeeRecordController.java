package com.zz.hc.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.zz.hc.service.RecordService;
import com.zz.hc.view.mode.AllRecord;


@RestController
public class SeeRecordController {
	@Autowired
	RecordService recordService;
	
	@RequestMapping("seeRecord")
	public List<AllRecord> getDate(){
		Map m=new HashMap();
		System.out.println("______________________");
		List<AllRecord> ls=recordService.selectAllRecord();
		m.put("rs", ls);
		System.out.println("______________________"+ls.get(0).getName());
		return ls;
	};
	
	@RequestMapping("seeUserRecord")
	public List<AllRecord> getDate2(@RequestParam("Phone") String Mobile){
		Map m=new HashMap();
		System.out.println("______________________");
		List<AllRecord> ls=recordService.selectUserRecord(Mobile);
		m.put("rs", ls);
		System.out.println("______________________"+ls.get(0).getName());
		return ls;
	};
	
}
	