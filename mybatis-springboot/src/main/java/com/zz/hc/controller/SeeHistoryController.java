package com.zz.hc.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.zz.hc.service.ProductService;
import com.zz.hc.view.mode.R_product;



@RestController
public class SeeHistoryController {
	@Autowired
	ProductService productService;
	@RequestMapping("seeHistory")
	public List<R_product> getDate(@RequestParam("id") int id){
		
		System.out.println("______________________");
		List<R_product> ls=productService.selectByR_id(id);
	
		System.out.println("______________________"+ls.get(0).getName());
		return ls;
	};
}
