var app= angular.module("myapp",["ngRoute"]);

app.config(function($routeProvider){
	$routeProvider
	.when(
			"/addUser",
			{
				templateUrl:'/rm/hc/addUser.html',
				controller:"addUserController"
			}
	
	
	
	).when(
			"/seeRecord",
			{
				templateUrl:'/rm/hc/seeRecord.html',
				controller:'seeRecordController'
			}
		).when(
			"/table",
			{
				templateUrl:'/rm/hc/table.html',
				controller:"checkUserController"
			}
	
	
	
	).when(
			"/viewProduct",
			{
				templateUrl:'/rm/hc/viewProduct.html',
				controller:"viewProductController"
			}).
			
	otherwise(
			{
				redirectTo:'/'
					
			}
	
	);
	
	
	
	
})