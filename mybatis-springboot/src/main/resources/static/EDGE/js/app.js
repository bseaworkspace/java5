var app=angular.module("myapp",["ngRoute"])

app.config(function($routeProvider){
	
	$routeProvider
	.when(
			"/toIndex",
			{
				templateUrl:'/rm/EDGE/index.html',
				controller:'IndexController'
			}
		)
	.when(
		"/addUser",
		{
			templateUrl:'/rm/EDGE/addUser.html',
			controller:'addUserController'
		}
	)
	.when(
	"/addRecord",
	{
		templateUrl:'/rm/EDGE/addRecord.html',
		controller:'addRecordController'
	}
)
.when(
		"/seeRecord",
		{
			templateUrl:'/rm/EDGE/seeRecord.html',
			controller:'seeRecordController'
		}
	).
	otherwise(
		{
			templateUrl:'/rm/EDGE/home.html',
			controller:'homeController'
		}
	)
}













)