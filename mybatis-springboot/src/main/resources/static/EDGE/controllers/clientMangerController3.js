app.controller("seeRecordController", function($scope, $http) {
	/**
	 * $scope.allrecord=function(){ $http({     method: 'POST',     url:
	 * '/rm/seeRecord' }).then(function successCallback(response) {         //
	 * 请求成功执行代码 var rs=response.data;
	 * 
	 *     }, function errorCallback(response) {         // 请求失败执行代码 }); }
	 */

	$scope.userrecord = function() {
		$http({
			method : 'POST',
			url : '/rm/seeRecord',
			params : {
				'Phone' : $scope.phone
			}
		}).then(function successCallback(response) {
			// 请求成功执行代码
			if (response.data.rs1 != 0) {				
				$scope.rs = response.data.rs;
				if (response.data.rs2 && response.data.rs2.length > 0) {					
				}else{					
					$scope.errorMsg = "该用户尚未有消费记录";
					$('#myModal').modal('show')
				}
			} else if (response.data.rs1 && response.data.rs1.length > 0) {
				$scope.errorMsg = "该用户不存在，请重新输入手机号";
				$('#myModal').modal('show')
			}
		}, function errorCallback(response) {
			// 请求失败执行代码
		});
	}

	$scope.show = function(R) {
		$('#limitSpeedTemplateDialog').modal('show')
		$http({
			method : 'POST',
			url : '/rm/showRecord',
			params : {
				'rid' : R.recordId
			}
		}).then(function successCallback(response) {
			// 请求成功执行代码
			$scope.ls = response.data.p;
		}, function errorCallback(response) {
			// 请求失败执行代码
		});

	}
	$scope.sure = function() {
		$('#limitSpeedTemplateDialog').modal('hide')

	}
	$scope.queding = function() {
		$('#myModal').modal('hide')

	}
})
