var app=angular.module("myapp",["ngRoute"])

app.config(function($routeProvider){
	
	$routeProvider
	.when(
		"/addUser",
		{
			templateUrl:'/rm/lsw/addUser.html',
			controller:'addUserController'
		}
	).when(
			"/seeRecord",
			{
				templateUrl:'/rm/lsw/seeRecord.html',
				controller:'seeRecordController'
			}
		).
	otherwise(
		{
			templateUrl:'/rm/lsw/home.html',
			controller:'homeController'
		}
	)
})