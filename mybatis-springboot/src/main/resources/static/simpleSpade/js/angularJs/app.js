var app = angular.module('myapp', ['ngRoute']);

app.config(function($routeProvider){
	
	$routeProvider
	.when(
		'/addUser',
		{
			templateUrl:'/rm/simpleSpade/addUser.html',
			controller:'addUserController'
		}
			
	)
	.when(
		'/addRecord',
		{
			templateUrl:'/rm/simpleSpade/addRecord.html',
			controller:'addRecordController'
		}
			
	)
	.otherwise(
		{
			templateUrl:'/rm/simpleSpade/home.html',
			controller:'homeController'
		}
	)
	
})
