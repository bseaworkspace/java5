app.controller("addUserController", function($scope,$http) {

	$scope.addUser = function() {
		$http({
			method : 'POST',
			url : '/rm/addUser',
			params : {
				'userName' : $scope.userName,
				'phone' : $scope.phone,
				'email' : $scope.email,
				'sex' : $scope.sex,
			}
		}).then(function successCallback(response) {
			// 请求成功执行代码
			var rs = response.data.rs;
			if ("Y" == rs) {
				window.location.href = "/ng-sp/bsea/showData.html";
			} else {
				$scope.errorMsg = "用户名或者密码错误，请重新输入";
				$('#myModal').modal('show')
			}
		}, function errorCallback(response) {
			// 请求失败执行代码
		});
	}

})

app.controller("homeController", function($scope) {
	$scope.test = "ss";

})

app.controller("addRecordController", function($scope) {
	

})
