app.controller('HomeController', function($scope, $route) {
	$scope.$route = $route;
});

app.controller('AboutController', function($scope, $route) {
	$scope.$route = $route;
});

function testController($scope) {
	alert("coming test");
	$scope.showTitle = "this is Controller Test";
}

userDataApp.controller('userDataCtrl', function($scope, $http) {

	$http({
		method : 'POST',
		url : "/ng-sp/getUserData"
	}).then(function successCallback(response) {
		// 请求成功执行代码
		var userData = response.data;
		alert(userData[1].u_nickname)
		// window.location.href = "/ng-sp/bsea/userData.html";
		$scope.userList = userData;
	}, function errorCallback(response) {
		// 请求失败执行代码
	});
})