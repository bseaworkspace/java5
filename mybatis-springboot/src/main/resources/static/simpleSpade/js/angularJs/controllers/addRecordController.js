app.controller("addRecordController", function($scope,$http) {
		
	$http({
		method : 'POST',
		url : '/rm/allProduct'
	}).then(
		function successCallback(response) {
			// 请求成功执行代码	
			$scope.allProduct = response.data;
			
			
				
		}, function errorCallback(response) {
			// 请求失败执行代码
		});
	
	$("#mobileInput").change();
	
	
	$scope.updateSelection = function($event, id){
		var temp="#"+id+"num";
		if($(temp).is(':hidden')){
			$(temp).show();
		}else{
			$(temp).hide();
		}
	}
	
	$scope.clickProduct=function(name,count){
		
		
		var products=[];
		var count = [] ;
//		alert($scope.allProduct.length);
		for(var i=0;i<$scope.allProduct.length;i++){
			var temp="#"+$scope.allProduct[i].id+"pro";
			var temp1="#"+$scope.allProduct[i].id+"num";
			
			if($(temp).is(':checked')){
				
				products.push($(temp).val());
				count.push($(temp1).val())
			}
		
			
			
		}
		var productsJson=JSON.stringify(products);
		var countJson=JSON.stringify(count);
		
		//Post
		$http({
			method : 'POST',
			url : '/rm/returnProduct',
			params : {
				'products' : productsJson,
				'count':countJson
			}
		}).then(function successCallback(response) {
			// 请求成功执行代码
			var product = response.data;
			//alert(rs)
			$("#tr0").before("<tr>	<td>"+product.name+"</td>	<td>"+product.type+"</td>	<td>"+product.price+"</td>	<td>"+product.integral+"</td>	<td>"+count+"</td></tr>");
		}, function errorCallback(response) {
			// 请求失败执行代码
		});
		//清空模态框
		$(function() {
		    $('#recordProduct').on('hide.bs.modal',
		    function () {
		    	$(':input').each(function() {
				    var type = this.type;
				    var tag = this.tagName.toLowerCase(); // normalize case
				    if (type == 'text' || type == 'password' || tag == 'textarea')
				      this.value = "";
				    // 跌代多选checkboxes
				    else if (type == 'checkbox' || type == 'radio')
				      this.checked = false;
				    // select 迭代下拉框
				    else if (tag == 'select')
				      this.selectedIndex = -1;
				  });
		    })
		});
		
		//页面关闭模态框
		$('#recordProduct').modal('hide');
		
		
	}
	
	
})