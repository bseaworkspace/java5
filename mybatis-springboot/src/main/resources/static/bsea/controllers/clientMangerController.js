
app.controller("addUserController",function($scope,$http){
    $scope.addUser=function(){
    	
    	
    	$http({
    		    method: 'POST',
    		    url: '/rm/addUser',
    		    params:{'name':$scope.username,'mobile':$scope.mobile,'email':$scope.email,'sex':$scope.sex}
    		}).then(function successCallback(response) {
    		        // 请求成功执行代码
    				var rs=response.data.name;
    				alert(rs);
    				if("Y"==rs){
    	          		window.location.href="/ng-sp/bsea/showData.html";
    	          	}else{
    	          		$scope.errorMsg="用户名或者密码错误，请重新输入";
    	          		$('#myModal').modal('show')
    	          	}
    		    }, function errorCallback(response) {
    		        // 请求失败执行代码
    		});
		
		
	}
	
	
})
app.controller("addRecordController",function($scope,$http){
	$scope.allProduct=[
		{
			id:'1',
			name:'牙签',
			selected:false,
			price:'100'
		},
		{
			id:'2',
			name:'洗发水',
			selected:false,
			price:'1100'
		},
		{
			id:'3',
			name:'毛巾',
			selected:false,
			price:'99900'
		}
		
	]
	$scope.addProd=function(){
		
		$('#addProdModal').modal('show');
	}
	
	
	$scope.addRecordpopup=function(){
		var inputEle = angular.element("input:checkbox[name='objtest']:checked");
		var temp = [];
		angular.forEach(inputEle,function(item){
		    //按需去对应的数据
			console.log(item);
		    var itemValue = angular.fromJson(item.value);
		    console.log(itemValue.name);
		    temp.push(item)
		})
		
		
	}
	
	
})

app.controller("indexController",function($scope,$http,staticName,ps,movie,moviefactory){
	//$scope.test="ss";
	
	
	$scope.uploadFile = function(){
        var form = new FormData();
        var file = document.getElementById("fileUpload").files[0];
        form.append('file', file);
        $http({
            method: 'POST',
            url: '/rm/upload',
            data: form,
            headers: {'Content-Type': undefined},
            transformRequest: angular.identity
        }).success(function (data) {
            console.log('upload success');
        }).error(function (data) {
             console.log('upload fail');
        })
    }
	 $scope.rs={};
	$http({
		    method: 'GET',
		    url: '/rm/showPieData'
		}).then(function successCallback(response) {
		        // 请求成功执行代码
		console.log(response)
		     $scope.rs=response.data;
			console.log($scope.rs)
			var pieyeardata2=$scope.rs.year;
			new Chart(document.getElementById("pieyear").getContext("2d")).Pie(pieyeardata2);
			new Chart(document.getElementById("piemonthly").getContext("2d")).Pie($scope.rs.month);
			
		    }, function errorCallback(response) {
		        // 请求失败执行代码
		});
	
	
	var pieDatatoday = [
		{
			value: 90,
			color:"rgba(233, 78, 2, 1)",
			label: "Product 1"
		},
		{
			value : 50,
			color : "rgba(242, 179, 63, 1)",
			label: "Product 2"
		},
		{
			value : 60,
			color : "rgba(88, 88, 88,1)",
			label: "Product 3"
		},
		{
			value : 40,
			color : "rgba(79, 82, 186, 1)",
			label: "Product 4"
		}
		
	];
var pieDatamonth = [
		{
			value: 90,
			color:"rgba(233, 78, 2, 1)",
			label: "Product 1"
		},
		{
			value : 50,
			color : "rgba(242, 179, 63, 1)",
			label: "Product 2"
		},
		{
			value : 60,
			color : "rgba(88, 88, 88,1)",
			label: "Product 3"
		},
		{
			value : 40,
			color : "rgba(79, 82, 186, 1)",
			label: "Product 4"
		}
		
	];
var pieDatayear = [
		{
			value: 90,
			color:"rgba(233, 78, 2, 1)",
			label: "Product 1"
		},
		{
			value : 50,
			color : "rgba(242, 179, 63, 1)",
			label: "Product 2"
		},
		{
			value : 60,
			color : "rgba(88, 88, 88,1)",
			label: "Product 3"
		},
		{
			value : 40,
			color : "rgba(79, 82, 186, 1)",
			label: "Product 4"
		}
		
	];


var lineChartData = {
		labels : ["Sun","Mon","Tue","Wed","Thr","Fri","Sat"],
		datasets : [
			{
				fillColor : "rgba(51, 51, 51, 0)",
				strokeColor : "#4F52BA",
				pointColor : "#4F52BA",
				pointStrokeColor : "#fff",
				data : [50,65,68,71,67,70,65]
			},
			{
				fillColor : "rgba(51, 51, 51, 0)",
				strokeColor : "#F2B33F",
				pointColor : "#F2B33F",
				pointStrokeColor : "#fff",
				data : [55,60,54,58,62,55,58]
			},
			{
				fillColor : "rgba(51, 51, 51, 0)",
				strokeColor : "#e94e02",
				pointColor : "#e94e02",
				pointStrokeColor : "#fff",
				data : [50,55,52,45,46,49,52]
			}
		]
		
	};

console.log("rs=="+$scope.rs);
var pieDatamonth1=$scope.rs.month;
new Chart(document.getElementById("pietoday").getContext("2d")).Pie(pieDatatoday);


new Chart(document.getElementById("line").getContext("2d")).Line(lineChartData);
	
	




})

