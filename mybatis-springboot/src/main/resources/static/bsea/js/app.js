var app=angular.module("myapp",["ngRoute"])

app.config(function($routeProvider){
	
	$routeProvider
	.when(
		"/addUser",
		{
			templateUrl:'/rm/bsea/addUser.html',
			controller:"addUserController"
		}
	)
	.when(
		"/toIndex",
		{
			templateUrl:'/rm/bsea/index.html',
			controller:"indexController"
		}
	)
	.when(
			"/addRecord",
			{
				templateUrl:'/rm/bsea/addRecord.html',
				controller:"addRecordController"
			}
	).
	otherwise(
		{
			templateUrl:'/rm/bsea/addRecord.html',
			controller:"addRecordController"
		}
	)
})