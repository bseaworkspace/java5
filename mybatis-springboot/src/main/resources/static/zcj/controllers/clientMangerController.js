
app.controller("addUserController",function($scope,$http){
    $scope.addUser=function(){
    	
    	
    	$http({
    		    method: 'POST',
    		    url: '/rm/addUser',
    		    params:{'name':$scope.username,'mobile':$scope.mobile,'email':$scope.email,'sex':$scope.sex}
    		}).then(function successCallback(response) {
    		        // 请求成功执行代码
    				var rs=response.data.name;
    				alert(rs);
    				if("Y"==rs){
    	          		window.location.href="/ng-sp/zcj/showData.html";
    	          	}else{
    	          		$scope.errorMsg="用户名或者密码错误，请重新输入";
    	          		$('#myModal').modal('show')
    	          	}
    		    }, function errorCallback(response) {
    		        // 请求失败执行代码
    		});
		
		
	}
	
	
})

app.controller("homeController",function($scope,$http){
	//$scope.test="ss";
	
	$scope.rs={};
	$http({
		    method: 'GET',
		    url: '/rm/showPieData'
		}).then(function successCallback(response) {
		        // 请求成功执行代码
		console.log(response)
		     $scope.rs=response.data;
			console.log($scope.rs)
			var pieyeardata2=$scope.rs.year;
			new Chart(document.getElementById("pieYear").getContext("2d")).Pie($scope.rs.year);
			new Chart(document.getElementById("pieMonth").getContext("2d")).Pie($scope.rs.month);
			new Chart(document.getElementById("pieDate").getContext("2d")).Pie($scope.rs.day);
			
		    }, function errorCallback(response) {
		        // 请求失败执行代码
		});
	
	var pieYear = [
		{
			value: 90,
			color:"rgba(233, 78, 2, 1)",
			label: "Product 1"
		},
		{
			value : 50,
			color : "rgba(242, 179, 63, 1)",
			label: "Product 2"
		},
		{
			value : 60,
			color : "rgba(88, 88, 88,1)",
			label: "Product 3"
		},
		{
			value : 40,
			color : "rgba(79, 82, 186, 1)",
			label: "Product 4"
		}
		
	];

var pieMonth = [
	{
		value: 90,
		color:"rgba(233, 78, 2, 1)",
		label: "Product 1"
	},
	{
		value : 50,
		color : "rgba(242, 179, 63, 1)",
		label: "Product 2"
	},
	{
		value : 60,
		color : "rgba(88, 88, 88,1)",
		label: "Product 3"
	},
	{
		value : 40,
		color : "rgba(79, 82, 186, 1)",
		label: "Product 4"
	}
	
];

var pieData = [
	{
		value: 90,
		color:"rgba(233, 78, 2, 1)",
		label: "Product 1"
	},
	{
		value : 50,
		color : "rgba(242, 179, 63, 1)",
		label: "Product 2"
	},
	{
		value : 60,
		color : "rgba(88, 88, 88,1)",
		label: "Product 3"
	},
	{
		value : 40,
		color : "rgba(79, 82, 186, 1)",
		label: "Product 4"
	}
	
];
var lineChartData = {
		labels : ["1","2","3","4","5","6","7","8","9","10","11","12"],
		datasets : [
			{
				fillColor : "rgba(51, 51, 51, 0)",
				strokeColor : "#4F52BA",
				pointColor : "#4F52BA",
				pointStrokeColor : "#fff",
				data : [50,65,68,81,67,70,65]
			},
			{
				fillColor : "rgba(51, 51, 51, 0)",
				strokeColor : "#F2B33F",
				pointColor : "#F2B33F",
				pointStrokeColor : "#fff",
				data : [55,60,54,58,62,55,58]
			},
			{
				fillColor : "rgba(51, 51, 51, 0)",
				strokeColor : "#e94e02",
				pointColor : "#e94e02",
				pointStrokeColor : "#fff",
				data : [50,55,52,45,46,49,52]
			}
		]
		
	};


})