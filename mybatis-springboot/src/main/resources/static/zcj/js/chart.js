var pieYear = [
										{
											value: 90,
											color:"rgba(233, 78, 2, 1)",
											label: "Product 1"
										},
										{
											value : 50,
											color : "rgba(242, 179, 63, 1)",
											label: "Product 2"
										},
										{
											value : 60,
											color : "rgba(88, 88, 88,1)",
											label: "Product 3"
										},
										{
											value : 40,
											color : "rgba(79, 82, 186, 1)",
											label: "Product 4"
										}
										
									];
								
								var pieMonth = [
									{
										value: 90,
										color:"rgba(233, 78, 2, 1)",
										label: "Product 1"
									},
									{
										value : 50,
										color : "rgba(242, 179, 63, 1)",
										label: "Product 2"
									},
									{
										value : 60,
										color : "rgba(88, 88, 88,1)",
										label: "Product 3"
									},
									{
										value : 40,
										color : "rgba(79, 82, 186, 1)",
										label: "Product 4"
									}
									
								];
								
								var pieData = [
									{
										value: 90,
										color:"rgba(233, 78, 2, 1)",
										label: "Product 1"
									},
									{
										value : 50,
										color : "rgba(242, 179, 63, 1)",
										label: "Product 2"
									},
									{
										value : 60,
										color : "rgba(88, 88, 88,1)",
										label: "Product 3"
									},
									{
										value : 40,
										color : "rgba(79, 82, 186, 1)",
										label: "Product 4"
									}
									
								];
								var lineChartData = {
										labels : ["1","2","3","4","5","6","7","8","9","10","11","12"],
										datasets : [
											{
												fillColor : "rgba(51, 51, 51, 0)",
												strokeColor : "#4F52BA",
												pointColor : "#4F52BA",
												pointStrokeColor : "#fff",
												data : [50,65,68,81,67,70,65]
											},
											{
												fillColor : "rgba(51, 51, 51, 0)",
												strokeColor : "#F2B33F",
												pointColor : "#F2B33F",
												pointStrokeColor : "#fff",
												data : [55,60,54,58,62,55,58]
											},
											{
												fillColor : "rgba(51, 51, 51, 0)",
												strokeColor : "#e94e02",
												pointColor : "#e94e02",
												pointStrokeColor : "#fff",
												data : [50,55,52,45,46,49,52]
											}
										]
										
									};
							new Chart(document.getElementById("line").getContext("2d")).Line(lineChartData);
							new Chart(document.getElementById("pieYear").getContext("2d")).Pie(pieYear);
							new Chart(document.getElementById("pieMonth").getContext("2d")).Pie(pieMonth);
							new Chart(document.getElementById("pieDate").getContext("2d")).Pie(pieData);