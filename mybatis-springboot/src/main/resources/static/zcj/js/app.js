var app=angular.module("myapp",["ngRoute"])

app.config(function($routeProvider){
	
	$routeProvider
	.when(
		"/addUser",
		{
			templateUrl:'/rm/zcj/addUser.html',
			controller:"addUserController"
		}
	).
	when(
		"/home",	
		{
			templateUrl:'/rm/zcj/home.html',
			controller:"homeController"
		}
	).
	otherwise(
			{
				templateUrl:'/rm/zcj/home.html',
				controller:'homeController'
			}
		)
})