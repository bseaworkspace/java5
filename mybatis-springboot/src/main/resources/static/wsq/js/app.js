var app=angular.module("myapp",["ngRoute"])

app.config(function($routeProvider){
	
	$routeProvider
	.when(
		"/addUser",
		{
			templateUrl:'/rm/wsq/addUser.html',
			controller:'addUserController'
		}
	).
	otherwise(
		{
			templateUrl:'/rm/wsq/home.html',
			controller:'homeController'
		}
	)
})