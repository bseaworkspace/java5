package com.zz.hc.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import com.zz.hc.model.Product;


import jxl.Sheet;
import jxl.Workbook;
import jxl.read.biff.BiffException;

public class ReadExcel {
	public List<Product> readExcel(File file) {
		List<Product> ls = new ArrayList<Product>();
		try {
			//  创建输入流，读取Excel
			InputStream is = new FileInputStream(file.getAbsolutePath());
			//  jxl提供的Workbook类
			Workbook wb = Workbook.getWorkbook(is);
			//  Excel的页签数量
			int sheet_size = wb.getNumberOfSheets();
			for (int index = 0; index < sheet_size; index++) {
				//  每个页签创建一个Sheet对象
				Sheet sheet = wb.getSheet(index);
				//  sheet.getRows()返回该页的总行数
				for (int i = 1; i < sheet.getRows(); i++) {
					Product m = new Product();
					
					
					//  sheet.getColumns()返回该页的总列数
					for (int j = 0; j < sheet.getColumns(); j++) {
						
						String cellinfo = sheet.getCell(j, i).getContents();
						System.out.println("cellinfo-----------"+cellinfo);
						if (j == 0) {
							if (cellinfo.equals("")) {

							} else {
								m.setName(cellinfo);
							}
						} else if (j == 1) {
							if (cellinfo.equals("")) {

							} else {
								m.setStock(Integer.parseInt(cellinfo));
							}
						} else if (j == 2) {
							if (cellinfo.equals("")) {
								
							} else {
								m.setType(cellinfo);
							}
						} else if (j == 3) {
							if (cellinfo.equals("")) {

							} else {
								m.setPrice(Double.parseDouble(cellinfo));
								
							}
						} else{
							if (cellinfo.equals("")) {
								
							} else {
								m.setIntegral(Integer.parseInt(cellinfo));
							}
						
						} 

					}
				
				ls.add(m);
				}
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (BiffException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return ls;
	}
}
