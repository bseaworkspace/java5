package com.zz.bsea.util;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Component;

import com.zz.bsea.view.model.PieData;
import com.zz.bsea.view.model.PieData1;

@Component
public class PieDataUtil {
	  public Map<String,List> getPieMap(List<PieData1> ls){
		  Map rs=new HashMap();
		  double yearAMT=0;
		  double monthAMT=0;
		  double dayAMT=0;
		  List<String> types=getProdType(ls);
		  List<PieData> ls2=new ArrayList();
		  List<PieData> ls3=new ArrayList();
		  List<PieData> ls4=new ArrayList();
		  
		  
		  for(String t:types){
			  PieData y=new PieData();
			  y.setLabel(t);
			  y.setColor("rgba(233, 78, 2, 1)");
			  PieData m=new PieData();
			  m.setLabel(t);
			  m.setColor("rgba(242, 179, 63, 1)");
			  PieData d=new PieData();
			  d.setLabel(t);
			  d.setColor("rgba(88, 88, 88,1)");
			  for(PieData1 p:ls){
				  
				  int year=p.getCreate_dt().getYear();
				  int month=p.getCreate_dt().getMonth();
				  int day=p.getCreate_dt().getDay();
				  int curentYear=new Date().getYear();
				  int curentmonth=new Date().getMonth();
				  int curentday=new Date().getDay();
				  if(t.equals(p.getType())){
					  if(curentYear==year){
						  yearAMT=yearAMT++;
					  }
					  if(curentYear==year&&curentmonth==month){
						  monthAMT=monthAMT++;
					  }
					  if(curentYear==year&&curentmonth==month&&curentday==day){
						  dayAMT=dayAMT++;
					  }
				  }
			  }
			  y.setValue(yearAMT);
			  m.setValue(monthAMT);
			  d.setValue(dayAMT);
			  ls2.add(y);
			  ls3.add(m);
			  ls4.add(d);
		  }
		  
		  rs.put("year", ls2);
		  rs.put("month", ls3);
		  rs.put("day", ls4);
		  return rs;
	  }
	  
	  
	  public List<String> getProdType(List<PieData1> ls){
		  List<String> rs=new ArrayList();
		  for(PieData1 p:ls){
			 if(!(rs.contains(p.getType()))){
				 rs.add(p.getType());
			 }
		  }
		  
		  return rs;
	  }
	  
	  
	
	  public static void main( String[] args )
	    {
	        System.out.println( "Hello World!" );
	    }

}
