package com.zz.simpleSpade.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.zz.simpleSpade.jpa.model.MyjUser;
import com.zz.simpleSpade.jpa.repository.UserRepository;

@RestController
public class UserController {

	@Autowired
	private UserRepository userRepository;

	@ResponseBody
	@RequestMapping("myjlogin")
	public boolean login(@RequestParam("name") String name,@RequestParam("pwd") String pwd) {
		boolean flag = false;
		if(userRepository.findByName(name)!=null){
			MyjUser u = userRepository.findByName(name);
			if(u.getPwd().equals(pwd)){
				flag = true;
			}
		}
		return flag;
	}
	
	@RequestMapping("register")
	public boolean register(@RequestParam("name") String name,@RequestParam("pwd") String pwd) {
		boolean flag = false;
		MyjUser u = new MyjUser();
		u.setName(name);
		u.setPwd(pwd);
		if(userRepository.findByName(name)==null){
			
			userRepository.save(u);
			flag = true;
		}
		return flag;
	}

}
