package com.zz.jwh.controller;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.zz.jwh.jpa.model.UserJ;
import com.zz.jwh.jpa.service.UserRepository;

@RestController
public class LoginController {
 @Autowired 
 UserRepository userRepository;

	private final Logger log = LoggerFactory.getLogger(this.getClass());  
	
	@RequestMapping(value="login",method=RequestMethod.POST)
	public Map login(@RequestParam("name")String userName,@RequestParam("pwd")String pwd,HttpServletRequest request){
		  log.debug("debug level log");  
	        log.info("info level log");  
	        log.warn("warn level log");  
	        log.error("error level log");  
		
		
		System.out.println("userName>>"+userName+"pwd>>"+pwd);
		
		Map m=new HashMap();
		HttpSession session=request.getSession();
		
		session.setAttribute(userName, userName);
	//-----------
		/*
		 * 框架的数据库的取值比较 
		 * 
		 */
		UserJ u=userRepository.findByName(userName);
		if(u==null){
			m.put("re", "N1");
		}else{
			
			if(u.getPassword().equals(pwd)){
				 m.put("re", "Y");
			}else{
				 m.put("re", "N2");
			}
		}
			
		
		
	//---------
	
		

		
		return m;
	}
}
