package com.zz.hc.controller;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.zz.hc.jpa.dao.UserHDao;
import com.zz.hc.jpa.model.UserH;
 

@RestController
public class LoginController {
    @Autowired()
    UserHDao userHDao;
	
    private final Logger log = LoggerFactory.getLogger(this.getClass());  
    
    
	@RequestMapping(value="login",method=RequestMethod.POST)
	public Map login(@RequestParam("name") String userName,@RequestParam("password") String pwd,HttpServletRequest request){
		System.out.println("username==="+userName+"pwd==="+pwd);
		log.debug("debug level log");  
        log.info("info level log");  
        log.warn("warn level log");  
        log.error("error level log");  
		
		
		
		Map m=new HashMap();
		UserH u=userHDao.findByName(userName);
		if(u!=null){
			if(u.getPwd().equals(pwd)){
				m.put("rs", "Y");
			}else{
				m.put("rs", "N1");
			}
			
		}else{
			m.put("rs", "N");
		}
		
		  
		return m;
	}
	
	
	
	@RequestMapping(value="register",method=RequestMethod.POST)
	public Map register(@RequestParam("username") String userName,@RequestParam("pwd1") String pwd1,@RequestParam("pwd2") String pwd2,@RequestParam("email") String email,@RequestParam("phonenumber") String phonenumber){
		log.debug("debug level log");  
        log.info("info level log");  
        log.warn("warn level log");  
        log.error("error level log");  
		
		Map m=new HashMap();
		UserH u=new UserH();
		u.setName(userName);
		u.setPwd(pwd1);
		u.setPhonenumber(phonenumber);
		u.setEmail(email);
		
		UserH u1=userHDao.save(u);
		if(u1!=null){
			m.put("rs", "Y");
		}else{
			m.put("rs", "N");
		}
		

		return m;
	}
	//@RequestMapping(value="login",method=RequestMethod.POST)
	//@PostMapping("login")效果是一样的
//	@PostMapping("login")
//	public String login2(){
//		return "登录成功";
//	}
	
	@ResponseBody
	@RequestMapping(value="login2",method=RequestMethod.POST)
	public Map login2(@RequestBody UserH user){
		System.out.println("username==="+user.getName()+"pwd==="+user.getPwd());
		log.debug("debug level log");  
        log.info("info level log");  
        log.warn("warn level log");  
        log.error("error level log");  
        String  userName=user.getName();
        String pwd=user.getPwd();
		
		
		Map m=new HashMap();
		UserH u=userHDao.findByName(userName);
		if(u!=null){
			if(u.getPwd().equals(pwd)){
				m.put("rs", "Y");
			}else{
				m.put("rs", "N1");
			}
			
		}else{
			m.put("rs", "N");
		}
		
		  
		return m;
	}
	
}
