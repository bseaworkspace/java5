app.config(function($provide) {

	$provide.service('register', function($http, $q) {

		this.register = function(name,pwd) {
	
			var d = $q.defer();
			$http({
				method : "POST",
				url : "/jpa/jpaRegister",
				params : {
					name:name,
					pwd:pwd
					
				}
			}).success(function(response) {
				d.resolve(response);
			}).error(function() {
				d.reject("error");
			});
			return d.promise;
		}
	});
	$provide.service('login', function($http, $q) {
		
		this.login = function(name,pwd) {
			
			var d = $q.defer();
			$http({
				method : "POST",
				url : "/jpa/login",
				params : {
					name:name,
					pwd:pwd
					
				}
			}).success(function(response) {
				d.resolve(response);
			}).error(function() {
				d.reject("error");
			});
			return d.promise;
		}
	});
});
