registerapp.config(function($provide) {
	
	$provide.service('register', function($http, $q) {

		this.register = function(username,pwd1,pwd2,email,phonenumber) {
		
			var d = $q.defer();
			$http({
				method : "POST",
				url : "/jpa/register",
				params : {
					'username' : username,
					'pwd1':pwd1,
					'pwd2':pwd2,
					'email':email,
					'phonenumber':phonenumber
				}
			}).success(function(response) {
				d.resolve(response);
			}).error(function() {
				d.reject("error");
			});
			return d.promise;
		}
	});
	
	
})