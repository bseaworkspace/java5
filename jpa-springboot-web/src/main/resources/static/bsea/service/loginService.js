loginapp.config(function($provide) {
	
	$provide.service('login', function($http, $q) {

		this.login = function(name,pwd) {
		
			var d = $q.defer();
			$http({
				method : "POST",
				url : "/jpa/login",
				params : {
					'name' : name,
					'password':pwd
				}
			}).success(function(response) {
				d.resolve(response);
			}).error(function() {
				d.reject("error");
			});
			return d.promise;
		}
	});
	
	
	
})




