loginapp.factory('userService',function($http){
	var loginURL="/jpa/login2";
	var registURL="";
	var service={
			user:{},
			setName:function(name){
				service.user['name']=name;
			},
			setPassword:function(password){
				service.user['pwd']=password;
			},
			login:function() {
			     return $http.post(loginURL,{
			    	 name: service.user.name,
			    	 pwd:service.user.pwd
			       });
			 }
	};
	return service;
})