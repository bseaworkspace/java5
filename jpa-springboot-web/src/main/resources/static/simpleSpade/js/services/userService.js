app.factory('userService', function($http, $q) {
	var factory = {};
	factory.register = function(name,pwd) {
		var defer = $q.defer();
		$http({
			url : "/jpa/register",
			method : "POST",
			params : {
				'name' : name,
				'pwd' : pwd
			}
		}).success(function(data) {
			defer.resolve(data);
		}).error(function(data, status, headers, config) {
			// defer.resolve(data);
			defer.reject(data);
			
		});
		return defer.promise;
	};
	factory.login = function($scope) {
		var defer = $q.defer();
		$http({
			url : "/jpa/myjlogin",
			method : "POST",
			params : {
				'name' : $scope.name,
				'pwd' : $scope.pwd
			}
		}).success(function(data) {
			defer.resolve(data);
		}).error(function(data, status, headers, config) {
			// defer.resolve(data);
			defer.reject(data);
		});
		return defer.promise;
	};
	return factory;

});