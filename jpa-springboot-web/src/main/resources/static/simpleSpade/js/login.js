var app = angular.module("loginapp", []);
app.controller("loginController", function($scope, $http ,userService) {
	$scope.onClicklogin = function() {
		var name = $scope.name;
		var pwd = $scope.pwd;
		userService.login(name,pwd).then(function(data) {
			// success函数
			if (data) {
				window.location.href = "/jpa/simpleSpade/welcome.html";
			} else {
				alert("用户名或密码错误！");
			}
			
		}, function(data) {
			// error函数
		})
	}
})