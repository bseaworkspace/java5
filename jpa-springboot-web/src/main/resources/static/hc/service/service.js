app.service("userService", ["UserH", function(UserH){
    return {

        login: function (name, pwd) {
            return UserH("POST", "/jpa/login", {'name': name, 'password': pwd}, {}, function (data) {
                return data;
            }, function (err) {
                return err;
            });
        },
        register: function (username, pwd1, pwd2, email, phonenumber) {
            return UserH("POST", "/jpa/register", {
                'username': username,
                'pwd1': pwd1,
                'pwd2': pwd2,
                'email': email,
                'phonenumber': phonenumber
            }, {}, function (data) {
                return data;
            }, function (err) {
                return err;
            });
        }


    }

}]);