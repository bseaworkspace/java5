// main
app.controller("mainController",[
    "$rootScope",
    "$scope",
    "$state",
    function($rootScope, $scope, $state){
        $rootScope.$on("$stateChangeSuccess", function(event, toState, toParams, fromState, fromParams){
            if(!toParams.type){
                $state.go({type: 'std'})
            }else{
                $scope.viewMode = toParams.type;
            }
        });
    }
]);

// 标准计算和特殊计算的列表
app.controller("kpiListController",[
    "$rootScope",
    "$scope",
    "$timeout",
    "$uibModal",
    "$stateParams",
    "Popup",
    "kpiService",
    function($rootScope, $scope, $timeout, $uibModal, $stateParams, Popup, kpiService){
        var type = $scope.type = $stateParams.type;
        //翻页的设置
        $scope.pager = {
            size: 20,
            index: 1
        };
        $scope.list = {
            isDone: false,
            data: [],
            totalRecords: 0
        };
        var getListData = function(){
            var params = {
                pageNo: $scope.pager.index,
                pageSize: $scope.pager.size
            };
            $scope.list.isDone = false;
            $scope.list.data = [];
            kpiService[type+'Kpi']('list', params).then(function(data){
                $scope.list.data = (type == 'std' ? data.data.kpiInfoVos :  data.data.specialKpiVos);
                $scope.list.totalRecords= data.data.totalRecords;
                $scope.list.isDone = true;
            },function(err){
                $scope.list.isDone = true;
            });
        };
        getListData();
        $scope.pageChanged = function(){
            getListData();
        };

        function addOrEditKpi(type, data) {
            if(!type){
                Popup.notice("请选择要操作的类型", 'warning');
                return ;
            }
            data = data ? data : {};
            var tempInstance = $uibModal.open({
                templateUrl: 'createOrUpdate'+type.toUpperCase()+'.html',
                controller: type + 'Controller',
                backdrop:'static',
                //size: 'sm',
                //windowClass: "kpi-data-modify",
                resolve: {
                    data: function () {
                        return data;
                    }
                }
            });
            tempInstance.result.then(function() {
                getListData();
            }, function () {
                // TODO sth
            });
        }

        $scope.dataAction = function(action, data){
            switch (action){
                case 'new':
                case 'edit':
                    addOrEditKpi(type, data);
                    break;
                case 'del':
                    Popup.comfirm("确认删除该计算规则", function(){
                        // ok
                        kpiService[type+'Kpi']('delete', {
                            ids: data.id
                        }).then(function(data){
                            Popup.notice(data.message);
                            getListData();
                        });
                    }, function(){
                        // cancel
                    });
                    break;
                default:
                    break;
            }
        }
    }
]);

function modifyCommon(scope, service, $uibModal){
    var self = scope || this;

    self.chooseKpi = function(){
        var kpiInstance = $uibModal.open({
            templateUrl: 'chooseKpi.html',
            controller: 'chooseTreeDataController',
            backdrop:'static',
            //size: 'sm',
            //windowClass: "kpi-data-modify",
            resolve: {
                data: function () {
                    scope.data.treeType = 'kpi';
                    return scope.data;
                }
            }
        });
        kpiInstance.result.then(function(data) {
            scope.data.evaluateKpiCode = data.kpiCode;
            // 还有一些值是带出来的
            service.stdKpi('queryCascadeKpiName', {
                evaluateKpiCode: data.kpiCode
            }).then(function(res){
                if(res.type == 'success'){
                    scope.data.kpiLevelName = res.data[1];
                    scope.data.chainedEvaluateKpiName = res.data[0];
                }
            });
        }, function () {
            // TODO sth
        });
    };

    self.chooseDefect = function(){
        var kpiInstance = $uibModal.open({
            templateUrl: 'chooseDefect.html',
            controller: 'chooseTreeDataController',
            backdrop:'static',
            //size: 'sm',
            //windowClass: "kpi-data-modify",
            resolve: {
                data: function () {
                    scope.data.treeType = 'defect';
                    return scope.data;
                }
            }
        });
        kpiInstance.result.then(function(data) {
            scope.data.defectCategoryCode = data.defectCategoryCode;
            service.stdKpi('queryCascadeDefectCategoryName', {
                defectCategoryCode: data.defectCategoryCode
            }).then(function(res){
                if(res.type == 'success'){
                    scope.data.chainedDefectCategory = res.data;
                }
            });

        }, function () {
            // TODO sth
        });
    };
}

// 标准计算编辑
app.controller("stdController",[
    "$rootScope",
    "$scope",
    "$timeout",
    "$uibModal",
    "$uibModalInstance",
    "Popup",
    "kpiService",
    "data",
    function($rootScope, $scope, $timeout, $uibModal, $uibModalInstance, Popup, kpiService, data){
        $scope.data = data || {};
        modifyCommon($scope, kpiService, $uibModal);
        kpiService.stdKpi('toAddPage', {}).then(function(res){
            if(res.type == 'success'){
                // 什么初始值 不知道
                if(!data.id){
                    angular.extend($scope.data, res.data);
                }else{
                    $scope.data.kpiValueTypes = res.data.kpiValueTypes;
                    $scope.data.kpiFieldNames = res.data.kpiFieldNames;
                    $scope.data.kpiCalcTypes = res.data.kpiCalcTypes;
                    $scope.data.kpiCalcLimitSet = res.data.kpiCalcLimitSet;
                }
            }
        });

        kpiService.stdKpi('queryCascadeDefectCategoryName', {
            defectCategoryCode: data.defectCategoryCode
        }).then(function(res){
            if(res.type == 'success'){
                $scope.data.chainedDefectCategory = res.data;
            }
        });

        $scope.saveData = function(){
            var postData = {
                groupCode: $scope.data.groupCode,
                evaluateKpiLevelCode: $scope.data.evaluateKpiLevelCode,
                evaluateKpiCode: $scope.data.evaluateKpiCode,
                evaluateKpiValueTypeCode: $scope.data.evaluateKpiValueTypeCode,
                evaluateKpiFieldName: $scope.data.evaluateKpiFieldName,
                evaluateKpiCalcFormula: $scope.data.evaluateKpiCalcFormula,
                defectCategoryCode: $scope.data.defectCategoryCode,
                evaluateKpiCalcType: $scope.data.evaluateKpiCalcType,
                evaluateKpiCalcLimitSet: $scope.data.evaluateKpiCalcLimitSet,
                slotDbl1: $scope.data.slotDbl1,
                slotDbl2: $scope.data.slotDbl2,
                slotDbl3: $scope.data.slotDbl3,
                slotDbl4: $scope.data.slotDbl4,
                slotDbl5: $scope.data.slotDbl5,
                slotDbl6: $scope.data.slotDbl6,
                slotDbl7: $scope.data.slotDbl7,
                slotDbl8: $scope.data.slotDbl8,
                slotDbl9: $scope.data.slotDbl9,
                slotDbl10: $scope.data.slotDbl10
            };
            kpiService.stdKpi("saveOrUpate", postData).then(function(res){
                if(res.type == 'success'){
                    Popup.notice(res.message)
                    $uibModalInstance.close();
                }else{
                    Popup.notice(res.message, 'warning')
                }
            });
        };
    }
]);

// 特殊计算编辑
app.controller("specController",[
    "$rootScope",
    "$scope",
    "$timeout",
    "$uibModal",
    "$uibModalInstance",
    "Popup",
    "kpiService",
    "data",
    function($rootScope, $scope, $timeout, $uibModal, $uibModalInstance, Popup, kpiService, data){
        $scope.data = data || {};
        modifyCommon($scope, kpiService, $uibModal);
        kpiService.specKpi('toAddPage', {}).then(function(res){
            if(res.type == 'success'){
                // 什么初始值 不知道
                if(!data.id){
                    angular.extend($scope.data, res.data);
                }else{
                    // TODO sth
                }
            }
        });
        $scope.saveData = function(){
            var postData = {
                groupCode:$scope.data.groupCode,
                evaluateKpiLevelCode:$scope.data.evaluateKpiLevelCode,
                evaluateKpiCode:$scope.data.evaluateKpiCode,
                ctrlUrl:$scope.data.ctrlUrl,
                viewUrl:$scope.data.viewUrl
            };
            kpiService.specKpi("saveOrUpate", postData).then(function(res){
                if(res.type == 'success'){
                    Popup.notice(res.message)
                    $uibModalInstance.close();
                }else{
                    Popup.notice(res.message, 'warning')
                }
            });
        };
    }
]);

// 选择KPI数据
app.controller("chooseTreeDataController", [
    "$scope",
    "$timeout",
    "$uibModalInstance",
    "kpiService",
    "data",
    "Popup",
    function($scope, $timeout, $uibModalInstance, kpiService, data, Popup){
        // kpi tree
        function initKpiTree(){
            $scope.isDone = true;
            zTree = $.fn.zTree.init($("#kpiTree"), {
                view:{
                    selectedMulti: false,
                    dblClickExpand: false,
                    fontCss: function(treeId, treeNode){
                        if(treeNode.kpiCode.length < '230001001001'.length){
                            // foribid
                            return {"cursor":"not-allowed"}
                        }
                    }
                },
                check:{
                    enable:false
                },
                data: {
                    key: {
                        name: "kpiName"
                    },
                    simpleData: {
                        idKey: "kpiCode"
                    }
                },
                callback: {
                    // 记录选中的节点
                    onClick: function(event, treeId, treeNode){
                        //if(treeNode.kpiCode.length >= '230001001001'.length) {
                        //    $scope.selectData = treeNode;
                        //    $scope.$apply();
                        //}else{
                        //    $scope.selectData = {};
                        //}
                        if(treeNode.kpiCode.length < '230001001001'.length){
                            $scope.selectData = {};
                        }else{
                            // 异步检查
                            kpiService.judgeKpiExisted(treeNode.kpiCode).then(function(res){
                                if(res.type == 'success'){
                                    $scope.selectData = treeNode;
                                }else{
                                    $scope.selectData = {};
                                }
                            })
                        }
                    }
                }
            }, kpiService.kpiTreeData);
        }

        // defect tree
        function initDefectTree(){
            $scope.isDone = true;
            zTree = $.fn.zTree.init($("#defectTree"), {
                view:{
                    selectedMulti: false,
                    dblClickExpand: false,
                    fontCss: function(treeId, treeNode){
                        if(treeNode.defectCategoryCode.length < '121000000000'.length){
                            // foribid
                            return {"cursor":"not-allowed"}
                        }
                    }
                },
                check:{
                    enable:false
                },
                data: {
                    key: {
                        name: "defectCategoryName"
                    },
                    simpleData: {
                        idKey: "defectCategoryCode"
                    }
                },
                callback: {
                    // 记录选中的节点
                    onClick: function(event, treeId, treeNode){
                        if(treeNode.defectCategoryCode.length >= '121000000000'.length) {
                            $scope.selectData = treeNode;
                            $scope.$apply();
                        }else{
                            $scope.selectData = {};
                        }
                    }
                }
            }, kpiService.kpiDefectData);
        }
        $scope.selectData = {};
        // 区别类型
        if(data.treeType == 'kpi'){
            if(kpiService.kpiTreeData){
                $timeout(function(){
                    initKpiTree();
                });
            }else{
                kpiService.loadKpiTree('').then(function(data){
                    $scope.isDone = true;
                    if(data.type == 'success'){
                        kpiService.kpiTreeData = data.data;
                        initKpiTree();
                    }
                });
            }
        }else if( data.treeType = 'defect'){
            if(kpiService.kpiDefectData){
                $timeout(function(){
                    initDefectTree();
                });
            }else{
                kpiService.loadDefectCategoryTreeNodes().then(function(data){
                    if(data.type == 'success'){
                        kpiService.kpiDefectData = data.data;
                        initDefectTree();
                    }
                });
            }
        }else{
            Popup.alert("出错了");
        }

        $scope.saveData = function(){
            if($scope.selectData){
                $uibModalInstance.close($scope.selectData);
            }else{
                Popup.notice('请选择KPI');
            }
        };
    }
]);