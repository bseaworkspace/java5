/**
 * Created by genffy on 16/4/3.
 */
app.controller("mainController",[
    "$rootScope",
    "$scope",
    "$timeout",
    "$uibModal",
    "$stateParams",
    "kpiService",
    "Popup",
    function($rootScope, $scope, $timeout, $uibModal, $stateParams, kpiService, Popup){
        $scope.kpiTempList = {
            isDone: false,
            data: []
        };
        $scope.kpiList = {
            isDone: false,
            data: []
        };
        $scope.btnAva = true;
        function checkNewIsAva(){
            if(calcValue() >= 100){
                $scope.btnAva = false;
            }else{
                $scope.btnAva = true;
            }
        }

        function calcValue(){
            var _total = 0,
                data = $scope.kpiList.data;
            angular.forEach(data, function(d){
                angular.forEach(d.templateKpis, function(dd){
                    _total += parseFloat(dd.evaluateKpiVolume);
                });
            });
            return _total;
        }

        $scope.curEvaluateTemplateCode = "";
        function getTempList(cb){
            $scope.kpiTempList.isDone = false;
            kpiService.listTemp().then(function(data){
                $scope.kpiTempList.isDone = true;
                if(data.type == 'success'){
                    $scope.kpiTempList.data = data.data.evaluateTemplateInfo;
                }else{
                    $scope.kpiTempList.data = [];
                }
                cb && cb();
            })
        }

        function getTempKpiData(){
            if(!$scope.curEvaluateTemplateCode){
                return ;
            }
            $scope.kpiList.isDone = false;
            kpiService.listKpiByTemp($scope.curEvaluateTemplateCode).then(function(data){
                $scope.kpiList.isDone = true;
                if(data.type == 'success'){
                    $scope.kpiList.data = data.data;
                }else{
                    $scope.kpiList.data = [];
                }
                checkNewIsAva();
            });
        }
        getTempList(function(){
            var data = $scope.kpiTempList.data;
            if(data.length) {
                $scope.dSelect(data[0]);
            }
        });

        $scope.dSelect = function(d){
            $scope.kpiTempList.data.forEach(function(item){
                item.active = false;
            });
            d.active = true;
            $scope.curEvaluateTemplateCode = d.evaluateTemplateCode;
            getTempKpiData();
        };
        $scope.clickEditConf = {
            type: 'text',
            title: '请输入分值',
            url: function(params){
                var data = {
                    id: params.id,
                    evaluateKpiDisplayName: params.evaluateKpiDisplayName,
                    evaluateKpiVolume: parseInt(params.value)
                };
                var d = new $.Deferred();
                kpiService.updateKpi(data).then(function(data){
                    d.resolve(data);
                    $timeout(function(){
                        checkNewIsAva();
                    });
                }, function(err){
                    d.reject(err);
                });
                return d.promise();
            },
            validate: function(value){
                if(!value){
                    return '不能为空';
                }
                var reg = /^([1-9]\d?(\.\d{1,2})?|100)$/;
                if(!reg.test(value)){
                    return "输入的值不合法";
                }
                // 计算和不能超过100
                var oldVal = parseFloat($(this).text()),
                    newVal = parseFloat(value),
                    total = calcValue();
                if((total + newVal - oldVal) > 100){
                    return "计算综合超过100,请重新输入";
                }
            }
        };


        // 新增模版 temp
        // 新增API kpi
        // 删除KPI delKpi
        // 编辑KPI editKpi
        // 删除模版 delTemp
        $scope.dataAction = function(type, data){
            if(!data) data = {};
            switch (type){
                case 'temp':
                    var tempInstance = $uibModal.open({
                        templateUrl: "createOrUpdateTemp.html",
                        controller: 'createOrUpdateTempController',
                        backdrop:'static',
                        size: 'sm',
                        //windowClass: "emt-data-detail",
                        resolve: {
                            data: function () {
                                return data;
                            }
                        }
                    });
                    tempInstance.result.then(function() {
                        getTempList();
                    }, function () {
                        // TODO sth
                    });
                    break;
                case 'kpi':
                    var kpiInstance = $uibModal.open({
                        templateUrl: "createOrUpdateKpi.html",
                        controller: 'createOrUpdateKpiController',
                        backdrop:'static',
                        //size: 'lg',
                        //windowClass: "emt-data-detail",
                        resolve: {
                            data: function () {
                                data._evaluateTemplateCode = $scope.curEvaluateTemplateCode;
                                return data;
                            }
                        }
                    });
                    kpiInstance.result.then(function() {
                        getTempKpiData();
                    }, function () {
                        // TODO sth
                    });
                    break;
                case 'delKpi':
                    Popup.comfirm("确认删除该KPI数据", function(){
                        // ok
                        kpiService.delKpi(data.id).then(function(data){
                            Popup.notice(data.message);
                            getTempKpiData();
                        });
                    }, function(){
                        // cancel
                    })
                    break;
                default:
                    break;
            }
        };

    }
]);

app.controller("createOrUpdateKpiController", [
    "$scope",
    "$timeout",
    "$uibModalInstance",
    "kpiService",
    "data",
    "Popup",
    function($scope, $timeout, $uibModalInstance, kpiService, data, Popup){
        // 加载 kpi 树,过滤已经存在的KPI
        // 显示国网的就只能再选国网的了
        $scope.selectKpi = {};
        $scope.isDone = false;
        kpiService.getKpiTree(data._evaluateTemplateCode||'').then(function(data){
            $scope.isDone = true;
            if(data.type == 'success'){
                $scope.treeData = data.data;
                zTree = $.fn.zTree.init($("#kpiTree"), {
                    view:{
                        selectedMulti: false,
                        dblClickExpand: false,
                        fontCss: function(treeId, treeNode){
                            if(treeNode.kpiCode.length != '230001001001'.length){
                                // foribid
                                return {"cursor":"not-allowed"}
                            }
                        }
                    },
                    check:{
                        enable:false
                    },
                    data: {
                        key: {
                            name: "kpiName"
                        },
                        simpleData: {
                            idKey: "kpiCode"
                        }
                    },
                    callback: {
                        // 记录选中的节点
                        onClick: function(event, treeId, treeNode){
                            if(treeNode.kpiCode.length == '230001001001'.length) {
                                $scope.selectKpi = treeNode;
                                $scope.$apply();
                            }else{
                                $scope.selectKpi = {};
                            }
                        }
                    }
                }, $scope.treeData);

            }
        });

        $scope.chooseKpi = function(){
            if($scope.selectKpi){
                //$uibModalInstance.close();
                var postData = {
                    evaluateTemplateCode: data._evaluateTemplateCode,
                    evaluateKpiCode: $scope.selectKpi.kpiCode,
                    evaluateKpiDisplayName: $scope.selectKpi.kpiName,
                };
                kpiService.addKpi(postData).then(function(data){
                    if(data.type == 'success'){
                        $uibModalInstance.close();
                    }else{
                        Popup.notice(data.message, 'warning');
                    }
                });
            }
        };
    }
]);