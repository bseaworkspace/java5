/**
 * Created by genffy on 16/4/3.
 */
app.service("kpiService", ["HttpRequest", function(HttpRequest){
    return {
        //================== kpi 模版相关API
        // 获取kpi模版
        listTemp: function(data){
            params = {};
            return HttpRequest("", context_path+"/ebp/template/list", params, {}, function(data){
                // format success
                return data;
            }, function(err){
                // format error
                return err;
            });
        },
        // 跳转到添加模版页面
        addTemp: function(data){
            return HttpRequest("", context_path + "/ebp/template/add", {}, {}, function(data){
                return data;
            })
        },
        // 保存模版数据
        saveTemp: function(data){
          var postData = data;
            return HttpRequest("POST", context_path + "/ebp/template/saveTemplate", {}, postData, function(data){
                return data;
            })
        },
        // 获取模版详细数据(编辑模版数据)
        detailTemp:function(id){
          return HttpRequest("", context_path + "/ebp/template/edit", {id: id}, {}, function(data){
              return data;
          })
        },
        checkTempName: function(data){
            var postData = data;
            return HttpRequest("POST", context_path + "/ebp/template/judgeTemplateName", {}, postData, function(data){
                return data;
            })
        },
        // 根据模版的code获取kpi列表
        listKpiByTemp: function(tempCode){
            return HttpRequest("", context_path+"/ebp/templatekpi/list", {evaluateTemplateCode: tempCode}, {}, function(data){
                return data;
            }, function(err){
                return err;
            });
        },
        // 模版
        getKpiTree: function(tempCode){
            return HttpRequest("", context_path + "/ebp/templatekpi/loadKpiTreeNodes", {evaluateTemplateCode: tempCode}, {}, function(data){
               return data;
            });
        },
        // 计算
        loadKpiTree: function(){
            return HttpRequest("", context_path + "/ebp/kpi/loadKpiTreeNodes", {}, {}, function(data){
                return data;
            });
        },
        addKpi: function(data){
            var postData = data;
            return HttpRequest("POST", context_path + "/ebp/templatekpi/addKpi", {}, postData, function(data){
                return data;
            });
        },
        updateKpi: function(data){
            var postData = data;
            return HttpRequest("POST", context_path + "/ebp/templatekpi/modifyKpi", {}, postData, function(data){
                return data;
            })
        },
        delKpi: function(id){
            return HttpRequest("", context_path + "/ebp/templatekpi/deleteKpi", {id: id}, {}, function(data){
                return data;
            })
        },
        loadDefectCategoryTreeNodes: function(){
            return HttpRequest("", context_path + "/ebp/kpi/loadDefectCategoryTreeNodes", {}, {}, function(data){
                return data;
            })
        },
        judgeKpiExisted: function(code){
            return HttpRequest("", context_path + "/ebp/kpi/judgeKpiExisted", {
                evaluateKpiCode: code
            }, {}, function(data){
                return data;
            })
        },
        //================== kpi 计算相关API
        stdKpi: function(action, data){
            var types = {
                "list": "",
                "toAddPage": "",
                "queryCascadeKpiName": "", // evaluateKpiCode
                "queryCascadeDefectCategoryName":"", //defectCategoryCode
                /**
                 * groupCode;
                 * evaluateKpiLevelCode;
                 * evaluateKpiCode;
                 * evaluateKpiValueTypeCode;
                 * evaluateKpiFieldName;
                 * evaluateKpiCalcFormula;
                 * defectCategoryCode;
                 * slotDbl1;
                 * slotDbl2;
                 * slotDbl3;
                 * slotDbl4;
                 * slotDbl5;
                 * slotDbl6;
                 * slotDbl7;
                 * slotDbl8;
                 * slotDbl9;
                 * slotDbl10;
                 * createUserCode;
                 * createDate;
                 * lastModiUserCode;
                 * lastModiDate;
                 */
                "saveOrUpate": "POST",
                "delete": "" //ids
            }, params = {}, postData = {};
            if(types[action]== undefined){
                return ;
            }
            if(types[action] != ""){
                postData = data;
            }else{
                params = data;
            }
            return HttpRequest(types[action], context_path + "/ebp/kpi/"+action, params, postData, function(data){
                return data;
            });
        },
        specKpi: function(action, data){
            var types = {
                "list": "", // query
                "toAddPage": "",
                "toEditPage": "", // id
                /**
                 * groupCode;
                 * evaluateKpiLevelCode;
                 * evaluateKpiCode;
                 * ctrlUrl;
                 * viewUrl;
                 * createUserCode;
                 * createDate;
                 * lastModiUserCode;
                 * lastModiDate;
                 */
                "saveOrUpate": "POST",
                "delete": "" //ids
            }, params = {}, postData = {};
            if(types[action]== undefined){
                return ;
            }
            if(types[action] != ""){
                postData = data;
            }else{
                params = data;
            }
            return HttpRequest(types[action], context_path + "/ebp/specialkpi/"+action, params, postData, function(data){
                return data;
            })
        }
    }
}]);