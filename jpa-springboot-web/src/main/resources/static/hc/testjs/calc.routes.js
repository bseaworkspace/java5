/**
 * Created by genffy on 16/4/3.
 */
app.config(["$stateProvider","$urlRouterProvider", function($stateProvider, $urlRouterProvider){
    // defalut
    $urlRouterProvider.otherwise("/std");
    $stateProvider
        .state('list', {
            url: "/:type",
            templateUrl: function ($stateParams){
                //return $stateParams.type + 'List.html';
                return 'kpiList.html';
            },
            controller: "kpiListController"
            //controllerProvider: function($stateParams) {
            //    var ctrlName = $stateParams.type + "ListController";
            //    return ctrlName;
            //}
        });
}]);
