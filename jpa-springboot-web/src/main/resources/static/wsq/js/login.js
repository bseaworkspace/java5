var app = angular.module("loginApp", [])
app.controller("loginController", function($scope, $http) {
	$scope.onClicklogin = function() {
//		alert("用户名是：" + $scope.name)
//		alert("密码是：" + $scope.pwd)

		$http({
			method : 'POST',
			url : '/jpa/login',
			params : {
				name : $scope.name,
				password : $scope.pwd
			}
		}).then(function successCallback(response) {
			var result = response.data.rs;
//			alert(response.data.rs)
			if ("Y" == response.data.rs) {
				window.location.href = "/jpa/wsq/register.html";
			} else if (response.data.rs == "S") {
				$scope.errorMsg = "密码错误，请重新输入";
				$('#myModal').modal('show')
			}else if(response.data.rs == "N"){
				$scope.errorMsg = "用户名不存在，请重新输入";
				$('#myModal').modal('show')
			}
		}, function errorCallback(response) {

		});

	}
})