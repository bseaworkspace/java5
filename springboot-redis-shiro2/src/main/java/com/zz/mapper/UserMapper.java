package com.zz.mapper;

import com.zz.model.User;
import com.zz.util.MyMapper;

public interface UserMapper extends MyMapper<User> {
}