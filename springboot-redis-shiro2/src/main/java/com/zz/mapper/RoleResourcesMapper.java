package com.zz.mapper;

import com.zz.model.RoleResources;
import com.zz.util.MyMapper;

public interface RoleResourcesMapper extends MyMapper<RoleResources> {
}