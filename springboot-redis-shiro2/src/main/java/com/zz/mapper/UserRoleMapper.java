package com.zz.mapper;

import java.util.List;

import com.zz.model.UserRole;
import com.zz.util.MyMapper;

public interface UserRoleMapper extends MyMapper<UserRole> {
    public List<Integer> findUserIdByRoleId(Integer roleId);
}