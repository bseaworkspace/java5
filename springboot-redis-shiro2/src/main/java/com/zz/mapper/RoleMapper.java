package com.zz.mapper;

import java.util.List;

import com.zz.model.Role;
import com.zz.util.MyMapper;

public interface RoleMapper extends MyMapper<Role> {
    public List<Role> queryRoleListWithSelected(Integer id);
}