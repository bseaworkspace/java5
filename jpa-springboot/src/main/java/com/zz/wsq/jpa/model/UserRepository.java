package com.zz.wsq.jpa.model;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
public interface UserRepository extends JpaRepository<UserW1, Long> {

    UserW1 findByName(String name);

    UserW1 findByNameAndAge(String name, Integer age);

    @Query("from UserW1 u where u.name=:name")
    UserW1 findUser2(@Param("name") String name);


}
