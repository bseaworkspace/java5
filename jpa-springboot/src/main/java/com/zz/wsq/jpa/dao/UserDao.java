package com.zz.wsq.jpa.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.zz.wsq.jpa.model.UserW2;



public interface UserDao extends JpaRepository<UserW2, Long> {
	
	
	UserW2 findByName(String name);
	
//	 @Query("INSERT INTO java5.userh(id, email, name, phonenumber, pwd)VALUES(0, '', '', '', '');")
//	 UserH insertUserH(@Param("name") String name);
	 
	 
	 UserW2 save(UserW2 user);

}
