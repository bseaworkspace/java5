package com.zz.hc.jpa.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.zz.hc.jpa.model.UserH;

public interface UserHDao extends JpaRepository<UserH, Long> {
	
	
	UserH findByName(String name);
	
//	 @Query("INSERT INTO java5.userh(id, email, name, phonenumber, pwd)VALUES(0, '', '', '', '');")
//	 UserH insertUserH(@Param("name") String name);
	 
	 
	 UserH save(UserH user);

}
