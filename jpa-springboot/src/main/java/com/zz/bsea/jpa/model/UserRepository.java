package com.zz.bsea.jpa.model;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
public interface UserRepository extends JpaRepository<User2, Long> {

    User2 findByName(String name);

    User2 findByNameAndAge(String name, Integer age);

    @Query("from User2 u where u.name=:name")
    User2 findUser2(@Param("name") String name);


}
