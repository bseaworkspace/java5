package com.zz.simpleSpade.jpa.repository;

import javax.persistence.Table;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.zz.simpleSpade.jpa.model.MyjUser;

@Repository
@Table(name="myjuser")
public interface UserRepository extends JpaRepository<MyjUser, Long> {
	
	public MyjUser findOne(Long id);
	
	public MyjUser findByName(String name);

	public MyjUser save(MyjUser u);
	

}
