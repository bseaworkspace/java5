package com.zz.jwh.jpa.service;

import org.springframework.data.jpa.repository.JpaRepository;

import com.zz.jwh.jpa.model.UserJ;

public interface  UserRepository  extends JpaRepository<UserJ, Long>{
 UserJ findByName(String name);
 UserJ findByNameAndPassword(String name,String password);
}
