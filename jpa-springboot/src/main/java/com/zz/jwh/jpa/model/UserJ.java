package com.zz.jwh.jpa.model;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
@Entity
public class UserJ {

	
	
	//设置自增逐渐
    @Id  //列名为 Id
    @GeneratedValue //自增属性
    private Long id; //model 属性

    @Column(nullable = false) //数据库 属性不能为空
    private String name;

    @Column(nullable = false) //同上
    private String password;

    public UserJ(){}

    public UserJ(String name, String password) {
        this.name = name;
        this.password = password;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

  
}
