package com.zz.EDGE.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.zz.EDGE.jpa.model.UserJEZ;
public interface UserDao extends JpaRepository<UserJEZ, Long> {

	UserJEZ findByName(String name);
	
	UserJEZ save(UserJEZ u);

    //UserJEZ findByNameAndAge(String name, Integer age);

/*    @Query("from User2 u where u.name=:name")
    UserJEZ findUser2(@Param("name") String name);*/


}