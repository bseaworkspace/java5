package com.zz.EDGE.jpa.model;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
public interface UserRepository extends JpaRepository<UserJEZ, Long> {

	UserJEZ findByName(String name);
	
	UserJEZ save(UserJEZ u);

    //UserJEZ findByNameAndAge(String name, Integer age);

/*    @Query("from User2 u where u.name=:name")
    UserJEZ findUser2(@Param("name") String name);*/


}
