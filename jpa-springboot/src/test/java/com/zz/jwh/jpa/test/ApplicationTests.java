package com.zz.jwh.jpa.test;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.zz.jwh.jpa.model.UserJ;
import com.zz.jwh.jpa.service.UserRepository;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
public class ApplicationTests {
	
	@Autowired
	private UserRepository userRepository;


	
	@Test
	public void test() throws Exception {
		userRepository.save(new UserJ("AAA","10"));
		userRepository.save(new UserJ("BBB","10"));
		userRepository.save(new UserJ("CCC","10"));
		userRepository.save(new UserJ("DDD","10"));
		userRepository.save(new UserJ("EEE","10"));
		userRepository.save(new UserJ("FFF","10"));
	
	}
}
