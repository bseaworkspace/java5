package com.weichat.simpleSpade.tool;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;

import org.apache.http.client.ClientProtocolException;
import org.json.JSONException;
import org.json.JSONObject;

public class JsApiUtil {

	public static String getJsApi() {
		String url = "https://api.weixin.qq.com/cgi-bin/ticket/getticket?access_token=" + WeiChatConfig.accessToken
				+ "&type=jsapi";

		try {
			URL urlGet = new URL(url);
			HttpURLConnection http = (HttpURLConnection) urlGet.openConnection();
			http.setRequestMethod("GET");
			http.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
			http.setDoOutput(true);
			http.setDoInput(true);
			System.setProperty("sun.net.client.defaultConnectTimeout", "30000");// 连接超时30秒
			System.setProperty("sun.net.client.defaultReadTimeout", "30000"); // 读取超时30秒
			http.connect();

			InputStream is = http.getInputStream();
			int size = is.available();
			byte[] jsonBytes = new byte[size];
			is.read(jsonBytes);
			String lable = new String(jsonBytes, "UTF-8");
			is.close();
			return "获得" + lable;
		} catch (ProtocolException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return "getJsApi 失败";

	}
	
	public static String getjsApiTicket() throws ClientProtocolException, IOException {
		String ticket="";
		URL url2=new URL("https://api.weixin.qq.com/cgi-bin/ticket/getticket?access_token="+WeiChatConfig.accessToken+"&type=jsapi");
		HttpsURLConnection con=(HttpsURLConnection) url2.openConnection();
		con.setRequestMethod("GET");
		con.setRequestProperty("Content-Type","application/x-www-form-urlencoded"); 
		con.setDoOutput(true); 
		con.setDoInput(true);
		con.connect();
		InputStream input=con.getInputStream();
		byte[] c=new byte[input.available()];
		input.read(c);
		// String有一个构造方法，可以接受一个，字节的数组，来创建一个String.
		//这个过程，就实现了，字节数组转化为String的过程。
		String message=new String(c,"UTF-8");
		input.close();
		con.disconnect();
		
		JSONObject demoJson;
		try {
			demoJson = new JSONObject(message);
			ticket=demoJson.getString("ticket");
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return ticket;
		
	}

	public static void main(String[] args) throws IOException {

		// System.out.println(deleteMenu());
		System.out.println(getJsApi());
	}

}
