package com.weichat.simpleSpade.tool;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.MessageDigest;
import java.util.Iterator;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.SortedMap;

import javax.net.ssl.HttpsURLConnection;

import org.apache.http.client.ClientProtocolException;
import org.json.JSONException;
import org.json.JSONObject;

import com.weichat.hc.tool.MD5Util;

public class SignUtil {

	private static String noncestr;

	private static String jsapi_ticket;

	private static String timestamp;

	public static void setNoncestr(String noncestr) {
		SignUtil.noncestr = noncestr;
	}

	public static void setJsapi_ticket(String jsapi_ticket) {
		SignUtil.jsapi_ticket = jsapi_ticket;
	}

	public static void setTimestamp(String timestamp) {
		SignUtil.timestamp = timestamp;
	}

	private static String url = "http://37ff6e4e.ngrok.io/weichat/simpleSpade/weichatinit-simpleSpade.html/";

	public static String getNonceStr() {
		Random random = new Random();
		return MD5Util.MD5Encode(String.valueOf(random.nextInt(10000)), "UTF-8");
	}

	public static String getTimeStamp() {
		return String.valueOf(System.currentTimeMillis() / 1000);
	}

	public static String getjsApiTicket(String url, String assess_token) throws ClientProtocolException, IOException {
		String ticket = "";
		URL url2 = new URL(url + assess_token + "&type=jsapi");
		HttpsURLConnection con = (HttpsURLConnection) url2.openConnection();
		con.setRequestMethod("GET");
		con.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
		con.setDoOutput(true);
		con.setDoInput(true);
		con.connect();
		InputStream input = con.getInputStream();
		byte[] c = new byte[input.available()];
		input.read(c);
		// String有一个构造方法，可以接受一个，字节的数组，来创建一个String.
		// 这个过程，就实现了，字节数组转化为String的过程。
		String message = new String(c, "UTF-8");
		input.close();
		con.disconnect();

		JSONObject demoJson;
		try {
			demoJson = new JSONObject(message);
			ticket = demoJson.getString("ticket");
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return ticket;

	}

	private static String getString() {

		String s = "";

		s = "jsapi_ticket=" + jsapi_ticket + "&noncestr=" + noncestr + "&timestamp=" + timestamp + "&url=" + url;
		return s;
	}
	
	public static String getString(SortedMap<String, String> map) {

		String s = "";

		s = "jsapi_ticket=" + map.get("jsapi_ticket") + "&noncestr=" + map.get("nonceStr") + "&timestamp=" + map.get("timestamp") + "&url=" + map.get("url");
		System.out.println(s);
		return s;
		
	}

	// 创建签名SHA1
	public static String createSHA1Sign(SortedMap<String, String> signParams) throws Exception {
		StringBuffer sb = new StringBuffer();
		Set es = signParams.entrySet();
		Iterator it = es.iterator();
		while (it.hasNext()) {
			Map.Entry entry = (Map.Entry) it.next();
			String k = (String) entry.getKey();
			String v = (String) entry.getValue();
			sb.append(k + "=" + v + "&");
			// 要采用URLENCODER的原始值！
		}
		String params = sb.substring(0, sb.lastIndexOf("&"));
		return getSha1(params);
	}

	// Sha1签名
	public static String getSha1(String str) {
		if (str == null || str.length() == 0) {
			return null;
		}
		char hexDigits[] = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f' };

		try {
			MessageDigest mdTemp = MessageDigest.getInstance("SHA1");
			mdTemp.update(str.getBytes("UTF-8"));
			byte[] md = mdTemp.digest();
			int j = md.length;
			char buf[] = new char[j * 2];
			int k = 0;
			for (int i = 0; i < j; i++) {
				byte byte0 = md[i];
				buf[k++] = hexDigits[byte0 >>> 4 & 0xf];
				buf[k++] = hexDigits[byte0 & 0xf];
			}
			return new String(buf);
		} catch (Exception e) {
			return null;
		}
	}

	public static void main(String[] args) {
		SignUtil.setTimestamp(SignUtil.getTimeStamp());
		try {
			SignUtil.setJsapi_ticket(JsApiUtil.getjsApiTicket());
		} catch (ClientProtocolException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		SignUtil.setNoncestr(SignUtil.getNonceStr());
		System.out.println("noncestr=" + SignUtil.noncestr);
		System.out.println("timestamp=" + SignUtil.timestamp);
		System.out.println("jsapi_ticket=" + SignUtil.jsapi_ticket);
		System.out.println("sign=" + getSha1(getString()));
	}
}
