package com.weichat.jwh.tool;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

import org.json.JSONObject;


public class MenuUtil {
	private static String getAccess_token(){ 
		 String url = "https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid="+ WeiChatConfig.appid + "&secret=" +WeiChatConfig.secretid;
		 String accessToken = null;
		 try {
		 URL urlGet = new URL(url);
		 HttpURLConnection http = (HttpURLConnection) urlGet.openConnection(); 

		 http.setRequestMethod("GET"); //必须是get方式请求 
		 http.setRequestProperty("Content-Type","application/x-www-form-urlencoded"); 
		 http.setDoOutput(true); 
		 http.setDoInput(true);
		 System.setProperty("sun.net.client.defaultConnectTimeout", "30000");//连接超时30秒
		 System.setProperty("sun.net.client.defaultReadTimeout", "30000"); //读取超时30秒
		 http.connect();

		 InputStream is =http.getInputStream();
		 int size =is.available();
		 byte[] jsonBytes =new byte[size];
		 is.read(jsonBytes);
		 String message=new String(jsonBytes,"UTF-8");
System.out.println("mess2-------"+message);
		 JSONObject demoJson = new JSONObject(message);
		 accessToken = demoJson.getString("access_token");

		 System.out.println(message);
		 } catch (Exception e) {
		 e.printStackTrace();
		 }
		 return accessToken;
		 }

		/**
		 * @throws UnsupportedEncodingException 
		 * 创建Menu
		* @Title: createMenu
		* @Description: 创建Menu
		* @param @return
		* @param @throws IOException 设定文件
		* @return int 返回类型
		* @throws
		 */
		 public static String createMenu() throws UnsupportedEncodingException {
		
		//String menu = "{\"button\":[{\"type\":\"click\",\"name\":\"MENU01\",\"key\":\"1\"},{\"type\":\"click\",\"name\":\"天气查询\",\"key\":\"西安\"},{\"name\":\"日常工作\",\"sub_button\":[{\"type\":\"click\",\"name\":\"待办工单\",\"key\":\"01_WAITING\"},{\"type\":\"click\",\"name\":\"已办工单\",\"key\":\"02_FINISH\"},{\"type\":\"click\",\"name\":\"我的工单\",\"key\":\"03_MYJOB\"},{\"type\":\"click\",\"name\":\"公告消息箱\",\"key\":\"04_MESSAGEBOX\"},{\"type\":\"click\",\"name\":\"签到\",\"key\":\"05_SIGN\"}]}]}";
		/*
		 * "{\"

	button\":[

	    {\"name\":\"余额\",
	          \"sub_button\":[
			                  {\"type\":\"view\",\"name\":\"总计\",\"url\":\"http://5e764acc.ngrok.io/v11\"},
							  {\"type\":\"view\",\"name\":\"直投\",\"url\":\"http://5e764acc.ngrok.io/v21\"},
							  {\"type\":\"view\",\"name\":\"提现\",\"url\":\"http://5e764acc.ngrok.io/v31\"}
							  
							  ]
		},
	    {\"name\":\"股票\",
	          \"sub_button\":[
			                  {\"type\":\"view\",\"name\":\"股总计\",\"url\":\"http://5e764acc.ngrok.io/v41\"},
							  {\"type\":\"view\",\"name\":\"预投\",\"url\":\"http://5e764acc.ngrok.io/v42\"},
							  {\"type\":\"view\",\"name\":\"配投\",\"url\":\"http://5e764acc.ngrok.io/v43\"},
							  {\"type\":\"view\",\"name\":\"受让\",\"url\":\"http://5e764acc.ngrok.io/v44\"},
							   {\"type\":\"view\",\"name\":\"转让\",\"url\":\"http://5e764acc.ngrok.io/v45\"}
							  ]
		},
	   {\"name\":\"个人中心\",
	          \"sub_button\":[
	          				  {\"type\":\"view\",\"name\":\"注册\",\"url\":\"http://5e764acc.ngrok.io/v5\"},
			                  {\"type\":\"view\",\"name\":\"股池\",\"url\":\"http://5e764acc.ngrok.io/v1\"},
							  {\"type\":\"view\",\"name\":\"分红\",\"url\":\"http://5e764acc.ngrok.io/v2\"},
							  {\"type\":\"view\",\"name\":\"密码管理\",\"url\":\"http://5e764acc.ngrok.io/v3\"},
							  {\"type\":\"view\",\"name\":\"常见问题\",\"url\":\"http://5e764acc.ngrok.io/v4\"}
							 
							  ]
		}
	]}"
		 * 
		 */
			 
			 String url1="https://open.weixin.qq.com/connect/oauth2/authorize?appid="+WeiChatConfig.appid+"&redirect_uri=";
			 //String url2="&redirect_uri=";
			 String url2="&response_type=code&scope=snsapi_base&state=123#wechat_redirect";		
			 
			 String v11str="http://www.zizai.pro/zz-web/v11";		 
			 v11str=URLEncoder.encode(v11str, "utf-8");
			 
			 String v21str="http://www.zizai.pro/zz-web/v21";		 
			 v21str=URLEncoder.encode(v21str, "utf-8");
			 
			 String v31str="http://www.zizai.pro/zz-web/v31";		 
			 v31str=URLEncoder.encode(v31str, "utf-8");
			 
			 String v41str="http://www.zizai.pro/zz-web/v41";		 
			 v41str=URLEncoder.encode(v41str, "utf-8");
			 
			 String v42str="http://www.zizai.pro/zz-web/v42";		 
			 v42str=URLEncoder.encode(v42str, "utf-8");
			 
			 String v43str="http://www.zizai.pro/zz-web/v43";		 
			 v43str=URLEncoder.encode(v43str, "utf-8");
			 
			 String v44str="http://www.zizai.pro/zz-web/v44";		 
			 v44str=URLEncoder.encode(v44str, "utf-8");
			 
			 String v45str="http://www.zizai.pro/zz-web/v45";		 
			 v45str=URLEncoder.encode(v45str, "utf-8");
			 
			 String v5str="http://www.zizai.pro/zz-web/v5";		 
			 v5str=URLEncoder.encode(v5str, "utf-8");
			 
			 String v1str="http://www.zizai.pro/zz-web/v1";		 
			 v1str=URLEncoder.encode(v1str, "utf-8");
			 
			 String v2str="http://www.zizai.pro/zz-web/v2";		 
			 v2str=URLEncoder.encode(v2str, "utf-8");
			 
			 String v3str="http://www.zizai.pro/zz-web/v3";		 
			 v3str=URLEncoder.encode(v3str, "utf-8");
			 
			 String v4str="http://www.zizai.pro/zz-web/v4";		 
			 v4str=URLEncoder.encode(v4str, "utf-8");
			 
			 String menu ="{\"button\":"
			 		+ "[{\"type\":\"click\",\"name\":\"今日歌曲\",\"key\":\"V1001_TODAY_MUSIC\"},"
			 		+ "{\"name\":\"菜单\","
			 		+ "\"sub_button\":"
			 		+ "["
			 		+ "{\"type\":\"view\",\"name\":\"搜索\",\"url\":\"http://b58207dd.ngrok.io/weichat/jwh/weichatinit-jwh.html\"},"
			 		+ "{\"type\":\"view\",\"name\":\"存手机\",\"url\":\"http://b58207dd.ngrok.io/weichat/jwh/setPhone.html\"},"
			 		+ "{\"type\":\"view\",\"name\":\"存手机\",\"url\":\"http://b58207dd.ngrok.io/weichat/jwh/indoorFitness.html\"},"
			 		+ "]"
			 		+ "}"
			 		+ "]"
			 		+ "}";
			 //此处改为自己想要的结构体，替换即可
			 
		 String access_token= TokenSaveRead.getToken();
		 System.out.println("access_token=="+access_token);
		 String action = "https://api.weixin.qq.com/cgi-bin/menu/create?access_token="+access_token;
		 try {
		 URL url = new URL(action);
		 HttpURLConnection http = (HttpURLConnection) url.openConnection(); 

		 http.setRequestMethod("POST"); 
		 http.setRequestProperty("Content-Type","application/x-www-form-urlencoded"); 
		 http.setDoOutput(true); 
		 http.setDoInput(true);
		 System.setProperty("sun.net.client.defaultConnectTimeout", "30000");//连接超时30秒
		 System.setProperty("sun.net.client.defaultReadTimeout", "30000"); //读取超时30秒
		 http.connect();
		 OutputStream os= http.getOutputStream(); 
		 os.write(menu.getBytes("UTF-8"));//传入参数 
		 os.flush();
		 os.close();

		 InputStream is =http.getInputStream();
		 int size =is.available();
		 byte[] jsonBytes =new byte[size];
		 is.read(jsonBytes);
		 String message=new String(jsonBytes,"UTF-8");
		 return "返回信息"+message;
		 } catch (MalformedURLException e) {
		 e.printStackTrace();
		 } catch (IOException e) {
		 e.printStackTrace();
		 } 
		 return "createMenu 失败";
		 }
		 /**
		 * 删除当前Menu
		 * @Title: deleteMenu
		 * @Description: 删除当前Menu
		 * @param @return 设定文件
		 * @return String 返回类型
		 * @throws
		 */
		 public static String deleteMenu()
		 {
		 String access_token= TokenSaveRead.getToken();
		 String action = "https://api.weixin.qq.com/cgi-bin/menu/delete? access_token="+access_token;
		 try {
		 URL url = new URL(action);
		 HttpURLConnection http = (HttpURLConnection) url.openConnection(); 

		 http.setRequestMethod("GET"); 
		 http.setRequestProperty("Content-Type","application/x-www-form-urlencoded"); 
		 http.setDoOutput(true); 
		 http.setDoInput(true);
		 System.setProperty("sun.net.client.defaultConnectTimeout", "30000");//连接超时30秒
		 System.setProperty("sun.net.client.defaultReadTimeout", "30000"); //读取超时30秒
		 http.connect();
		 OutputStream os= http.getOutputStream(); 
		 os.flush();
		 os.close();

		 InputStream is =http.getInputStream();
		 int size =is.available();
		 byte[] jsonBytes =new byte[size];
		 is.read(jsonBytes);
		 String message=new String(jsonBytes,"UTF-8");
		 return "deleteMenu返回信息:"+message;
		 } catch (MalformedURLException e) {
		 e.printStackTrace();
		 } catch (IOException e) {
		 e.printStackTrace();
		 }
		 return "deleteMenu 失败"; 
		 }
		public static void main(String[] args) throws IOException {

		//System.out.println(deleteMenu());
			
		System.out.println(createMenu());
		
		}
}
