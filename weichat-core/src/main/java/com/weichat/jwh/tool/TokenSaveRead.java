package com.weichat.jwh.tool;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.URL;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;

import javax.net.ssl.HttpsURLConnection;

import org.json.JSONException;
import org.json.JSONObject;

 
public class TokenSaveRead {
	
	public static String appid=WeiChatConfig.appid;
	public static String secretid=WeiChatConfig.secretid;
	
	public static String getAccessToken() throws IOException{
		String accessToken="";
		String url="https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid="+appid+"&secret="+secretid;
		URL url2=new URL(url);
		HttpsURLConnection con=(HttpsURLConnection) url2.openConnection();
		con.setRequestMethod("GET");
		con.setRequestProperty("Content-Type","application/x-www-form-urlencoded"); 
		con.setDoOutput(true); 
		con.setDoInput(true);
		
		con.connect();
		InputStream input=con.getInputStream();
		byte[] c=new byte[input.available()];
		input.read(c);
		// String有一个构造方法，可以接受一个，字节的数组，来创建一个String.
		//这个过程，就实现了，字节数组转化为String的过程。
		String message=new String(c,"UTF-8");
		input.close();
		con.disconnect();
		
		JSONObject demoJson;
		try {
			demoJson = new JSONObject(message);
			accessToken=demoJson.getString("access_token");
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return accessToken;
		
	}
	
	public static void write(String X){
		File f=new File("D:"+File.separator+"token.txt");
		try {
			FileOutputStream fs=new FileOutputStream(f);
			//  \r\n 会换行
			Date data=new Date();
			String str= X;
			byte[] b=str.getBytes();
			fs.write(b);
			fs.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static BufferedReader read(){
		File f=new File("D:"+File.separator+"token.txt");
		BufferedReader br=null;
		try {
			FileInputStream fi=new FileInputStream(f);
			Reader r=new InputStreamReader(fi,"utf-8");
			br=new BufferedReader(r);

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return br;
	}
	
	public static String getToken(){
		Map m=new HashMap();
		Validate ve =new Validate();
		
		BufferedReader br=read();
		Date date=new Date();
		String  token=null;
		try {
			if(br.ready()){
				token=br.readLine();
				System.out.println("write-token------"+token);
				Map ip_list=ve.validate(token);
				System.out.println("errcode====="+ip_list.get("errcode"));
				Integer  a;
				if(ip_list.get("ip_list")!=null){
			 
				}
				if(ip_list.get("errcode")!=null){
					a=(Integer) ip_list.get("errcode");
					if (a==42001||a==40001){
						token=getAccessToken();
						 write(token);
							System.out.println("new-token------"+token);

						}
					}
					 
		 
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		 return token;
	}
public static void main (String[] args) throws IOException{
	write(getAccessToken());

//System.out.println(getToken());
/*	 Map<String,String>map=new HashMap<String,String>();
	  map.put("A","1");
	  map.put("12", "4");
	  map.put("11", "2");
	  for (Entry<String, String> entry: map.entrySet()) {
	   System.out.println("排序之前:"+entry.getKey()+" 值"+entry.getValue());
	
	  }
	  System.out.println("======================================================");
	  SortedMap<String,String> sort=new TreeMap<String,String>(map);
	  Set<Entry<String,String>> entry1=sort.entrySet();
	  Iterator<Entry<String,String>> it=entry1.iterator();
	  
	   	
	  while(it.hasNext())
	  {
	   Entry<String,String> entry=it.next();
	   System.out.println("排序之后:"+entry.getKey()+" 值"+entry.getValue());
	  }*/
	 }
	
}
