package com.weichat.jwh.tool;

import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import javax.net.ssl.HttpsURLConnection;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;

 

public class Validate {

	
	
	public static Map validate(String asstoken) throws IOException{
		Map m=new HashMap();
		JSONArray ip_list = null;
		int errcode=0;
		String msg;
		String url="https://api.weixin.qq.com/cgi-bin/getcallbackip?access_token="+asstoken;
		URL url2=new URL(url);
		HttpsURLConnection con=(HttpsURLConnection) url2.openConnection();
		con.setRequestMethod("GET");
		con.setRequestProperty("Content-Type","application/x-www-form-urlencoded"); 
		con.setDoOutput(true); 
		con.setDoInput(true);
		con.connect();
		InputStream input=con.getInputStream();
		byte[] c=new byte[input.available()];
		input.read(c);
		// String有一个构造方法，可以接受一个，字节的数组，来创建一个String.
		//这个过程，就实现了，字节数组转化为String的过程。
		String message=new String(c,"UTF-8");
		input.close();
		con.disconnect();
		System.out.println(message);
		JSONObject demoJson;
		try {
			demoJson = new JSONObject(message);
			msg=demoJson.toString();
			if(msg.indexOf("ip_list")!=-1){
				ip_list=demoJson.getJSONArray("ip_list");
				m.put("ip_list", ip_list);
			}else if(msg.indexOf("errcode")!=-1){
				errcode=demoJson.getInt("errcode");
				m.put("errcode", errcode);
			}

			
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return m;
		
	}
	

	public static void main(String[] args){
 
		
		
	}
	
}
