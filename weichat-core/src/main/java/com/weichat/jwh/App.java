package com.weichat.jwh;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletComponentScan;

@SpringBootApplication
@ServletComponentScan
public class App {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		  SpringApplication.run(App.class, args);
			
	}

}
//Forwarding                    http://a62f3034.ngrok.io/weichart.test -> localhost:80