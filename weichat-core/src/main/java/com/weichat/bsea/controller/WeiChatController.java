package com.weichat.bsea.controller;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.dom4j.DocumentException;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.weichat.bsea.tool.AesException;
import com.weichat.bsea.tool.WeiChatUtil;

/**
 * 微信开发的第一步骤。 接入
 * 
 * @author bsea
 *
 */
@RestController
public class WeiChatController {
	String token="JAVA5";
	
	@GetMapping("/int")
	public String intWeichat(HttpServletRequest req){
		String signature=req.getParameter("signature");
		String timestamp=req.getParameter("timestamp");
		String nonce=req.getParameter("nonce");
		String param4=req.getParameter("echostr"); 
		
		System.out.println("initWeiChat timestamp="+timestamp);
		System.out.println("initWeiChat nonce="+nonce);
		System.out.println("initWeiChat signature="+signature);
		System.out.println("initWeiChat echostr="+param4);
		boolean result=false;
		try {
			if(signature.equals(WeiChatUtil.getSHA1(token, timestamp, nonce))){
				result=true;
				
			};
		} catch (AesException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if(result){
			return param4;
			
		}else{
			
			return "error";
		}
	}
	
	
	/*
	 * 1. 接收用户给微信公众平台发的消息
	 * 
	 */
	
	@RequestMapping(value="int" ,method=RequestMethod.POST)
	public String message(HttpServletRequest req,HttpServletResponse response) throws IOException, DocumentException{
		System.out.println(req);
		String accessToken=WeiChatUtil.getAccessToken();
		//todo  获取素材列表
		
		InputStream inst=req.getInputStream();
		Map map=WeiChatUtil.xmlToMap(inst);
		System.out.println("FromUserName="+map.get("FromUserName"));
		System.out.println("Content="+map.get("Content"));
		System.out.println("MsgType="+map.get("MsgType"));
		System.out.println("PicUrl="+map.get("PicUrl"));
		System.out.println("MediaId="+map.get("MediaId"));
		Map mapres=new HashMap();
		mapres.put("FromUserName", map.get("ToUserName"));
		mapres.put("ToUserName", map.get("FromUserName"));
		mapres.put("Content", "欢迎来到java 5班");
		mapres.put("MsgType", "text");   
		mapres.put("CreateTime", 12455744); 
		return WeiChatUtil.MapToXml(mapres);
	}

}
