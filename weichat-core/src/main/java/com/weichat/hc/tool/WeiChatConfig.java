package com.weichat.hc.tool;

import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.beans.factory.annotation.Value;

@Configurable
public class WeiChatConfig {
	
//	public static String appid="wx4a682a3984b218d0";
//	public static String secretid="33653858f0f8501b7719bf9e4a439ddb";
	public static String appid="wx4c77158b3d5d5ca7";
	public static String secretid="2f41aced533b3f5b1e32bbd6534b97b8";
	//商户号
	public static String mchId="1480255942";
	//统一下单异步通知地址
	public static String notifyUrl="";
	//商户号设置密钥
	public static String key="yinzhouchenghuangguojin123456789";
	//商户交易类型
	public static String trade_type="JSAPI";
	//签字类型
	public static String signType = "MD5";	
	//统一下单地址
	public static String orderUrl="https://api.mch.weixin.qq.com/pay/unifiedorder";
	//查询订单地址
	public static String inquireOrderUrl="https://api.mch.weixin.qq.com/pay/orderquery";
	//关闭订单地址
	public static String cancelOrderUrl="https://api.mch.weixin.qq.com/pay/closeorder";
	//申请退款地址
	public static String refundsUrl="https://api.mch.weixin.qq.com/secapi/pay/refund";
	//查询退款地址
	public static String inquireRefundsUrl="https://api.mch.weixin.qq.com/pay/refundquery";
	//下载对账单地址
	public static String downloadBillUrl="https://api.mch.weixin.qq.com/pay/downloadbill";
	public static String getAppid() {
		return appid;
	}
	public static void setAppid(String appid) {
		WeiChatConfig.appid = appid;
	}
	public static String getSecretid() {
		return secretid;
	}
	public static void setSecretid(String secretid) {
		WeiChatConfig.secretid = secretid;
	}
	public static String getMchId() {
		return mchId;
	}
	public static void setMchId(String mchId) {
		WeiChatConfig.mchId = mchId;
	}
	public static String getNotifyUrl() {
		return notifyUrl;
	}
	public static void setNotifyUrl(String notifyUrl) {
		WeiChatConfig.notifyUrl = notifyUrl;
	}
	public static String getKey() {
		return key;
	}
	public static void setKey(String key) {
		WeiChatConfig.key = key;
	}
	public static String getOrderUrl() {
		return orderUrl;
	}
	public static void setOrderUrl(String orderUrl) {
		WeiChatConfig.orderUrl = orderUrl;
	}
	public static String getInquireOrderUrl() {
		return inquireOrderUrl;
	}
	public static void setInquireOrderUrl(String inquireOrderUrl) {
		WeiChatConfig.inquireOrderUrl = inquireOrderUrl;
	}
	public static String getCancelOrderUrl() {
		return cancelOrderUrl;
	}
	public static void setCancelOrderUrl(String cancelOrderUrl) {
		WeiChatConfig.cancelOrderUrl = cancelOrderUrl;
	}
	public static String getRefundsUrl() {
		return refundsUrl;
	}
	public static void setRefundsUrl(String refundsUrl) {
		WeiChatConfig.refundsUrl = refundsUrl;
	}
	public static String getInquireRefundsUrl() {
		return inquireRefundsUrl;
	}
	public static void setInquireRefundsUrl(String inquireRefundsUrl) {
		WeiChatConfig.inquireRefundsUrl = inquireRefundsUrl;
	}
	public static String getDownloadBillUrl() {
		return downloadBillUrl;
	}
	public static void setDownloadBillUrl(String downloadBillUrl) {
		WeiChatConfig.downloadBillUrl = downloadBillUrl;
	}
	public static String getTrade_type() {
		return trade_type;
	}
	public static void setTrade_type(String trade_type) {
		WeiChatConfig.trade_type = trade_type;
	}
	public static String getSignType() {
		return signType;
	}
	public static void setSignType(String signType) {
		WeiChatConfig.signType = signType;
	}

}
