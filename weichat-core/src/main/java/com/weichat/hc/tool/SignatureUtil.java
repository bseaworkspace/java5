package com.weichat.hc.tool;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.security.MessageDigest;
import java.util.Arrays;

import javax.net.ssl.HttpsURLConnection;

import org.json.JSONException;
import org.json.JSONObject;

//public class SignatureUtil {
//	
	
//	
//	public static String getsignature(){
//		String signature="";
//		String jsapi_ticket="";
//		WeiChatUtil wu=new WeiChatUtil();
//		try {
//			jsapi_ticket=getjsapi_ticket(wu.getAccessToken());
//		} catch (IOException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//		//String a="jsapi_ticket="+jsapi_ticket+"&timestamp=1509760123&url="+url+"?params=value";
//	
//		try {
//			String[] array = new String[] { "4198774850", jsapi_ticket, "1509760123", "http://mp.weixin.qq.com?params=value"};
//			StringBuffer sb = new StringBuffer();
//			// 字符串排序(字典排序)
//			Arrays.sort(array);
//			for (int i = 0; i < 3; i++) {
//				sb.append(array[i]);
//			}
//			String str = sb.toString();
//			// SHA1签名生成
//			MessageDigest md = MessageDigest.getInstance("SHA-1");
//			md.update(str.getBytes());
//			byte[] digest = md.digest();
//		
//			StringBuffer hexstr = new StringBuffer();
//			String shaHex = "";
//			for (int i = 0; i < digest.length; i++) {
//				shaHex = Integer.toHexString(digest[i] & 0xFF);
//				if (shaHex.length() < 2) {
//					hexstr.append(0);
//				}
//				hexstr.append(shaHex);
//			}
//			return hexstr.toString();
//		} catch (Exception e) {
//			e.printStackTrace();
//			try {
//				throw new AesException(AesException.ComputeSignatureError);
//			} catch (AesException e1) {
//				// TODO Auto-generated catch block
//				e1.printStackTrace();
//			}
//		}
//		
//		
//		
//		return signature;
//	}

import java.net.URLDecoder;
import java.security.MessageDigest;
import java.util.Iterator;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;




/*
'============================================================================
'api说明：
'createSHA1Sign创建签名SHA1
'getSha1()Sha1签名
'============================================================================
'*/
public class SignatureUtil {
	
	public static String getjsapi_ticket(String access_token){
		String ticket="";
		String url="https://api.weixin.qq.com/cgi-bin/ticket/getticket?access_token="+access_token+"&type=jsapi";
		URL urlGet;
		try {
			 urlGet = new URL(url);
			 HttpsURLConnection http = (HttpsURLConnection) urlGet.openConnection(); 
			 
			 http.setRequestMethod("GET"); //必须是get方式请求 
			 http.setRequestProperty("Content-Type","application/x-www-form-urlencoded"); 
			 http.setDoOutput(true); 
			 http.setDoInput(true);
			 System.setProperty("javax.net.ssl.keyStore", "");
			 System.setProperty("javax.net.ssl.keyStorePassword", "");
			 System.setProperty("javax.net.ssl.trustStore", "");
			 System.setProperty("javax.net.ssl.trustStorePassword","");
			 System.setProperty("sun.net.client.defaultConnectTimeout", "30000");//连接超时30秒
			 System.setProperty("sun.net.client.defaultReadTimeout", "30000"); //读取超时30秒
			 http.connect();
			 InputStream is =http.getInputStream();
			 int size =is.available();
			 byte[] jsonBytes =new byte[size];
			 is.read(jsonBytes);
			 String message=new String(jsonBytes,"UTF-8");
			 JSONObject demoJson;
			try {
				demoJson = new JSONObject(message);
				ticket = demoJson.getString("ticket");
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			 
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}		 
		return ticket;
	}	
	
	public static String getTimeStamp() {
		return String.valueOf(System.currentTimeMillis() / 1000);
	}
	
   //创建签名SHA1
	public static String createSHA1Sign(SortedMap<String, String> signParams) throws Exception {
		StringBuffer sb = new StringBuffer();
		Set es = signParams.entrySet();
		Iterator it = es.iterator();
		while (it.hasNext()) {
			Map.Entry entry = (Map.Entry) it.next();
			String k = (String) entry.getKey();
			String v = (String) entry.getValue();
			sb.append(k + "=" + v + "&");
			//要采用URLENCODER的原始值！
		}
		String params = sb.substring(0, sb.lastIndexOf("&"));
		return getSha1(params);
	}
	//Sha1签名
	public static String getSha1(String str) {
		if (str == null || str.length() == 0) {
			return null;
		}
		char hexDigits[] = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
				'a', 'b', 'c', 'd', 'e', 'f' };

		try {
			MessageDigest mdTemp = MessageDigest.getInstance("SHA1");
			mdTemp.update(str.getBytes("UTF-8"));
			byte[] md = mdTemp.digest();
			int j = md.length;
			char buf[] = new char[j * 2];
			int k = 0;
			for (int i = 0; i < j; i++) {
				byte byte0 = md[i];
				buf[k++] = hexDigits[byte0 >>> 4 & 0xf];
				buf[k++] = hexDigits[byte0 & 0xf];
			}
			return new String(buf);
		} catch (Exception e) {
			return null;
		}
	}
	public static void main(String[] args) throws Exception {
		SortedMap<String, String> finalpackage = new TreeMap<String, String>();
		String timestamp = SignatureUtil.getTimeStamp();
		WeiChatUtil wu= new WeiChatUtil();
		String jsapi_ticket=getjsapi_ticket(wu.getAccessToken());
		//finalpackage.put("appId", WeiChatConfig.appid);
		System.out.println(timestamp);
		finalpackage.put("timestamp", timestamp);
		finalpackage.put("noncestr", "4198774850");
		
		finalpackage.put("jsapi_ticket", jsapi_ticket);
		finalpackage.put("url", "http://06b37fa9.ngrok.io/weichat/hc/weichatinit-hc.html");
		//finalpackage.put("package", packages);
		//finalpackage.put("signType", WeiChatConfig.signType);
		String signature =SignatureUtil.createSHA1Sign(finalpackage);
		System.out.println("signature=="+signature);
		System.out.println(getSha1("jsapi_ticket=kgt8ON7yVITDhtdwci0qeXZ5dXwKA2vWKFzfgQLOoSepnY2WdtS2v8Ccr1DcVfOAVElmBE14sFHDJyBSS_B_gQ&noncestr=d130ca139ad84f479ed49406b220862b&timestamp=1496951550&url=http://www.zizai.pro/zz-web/weiChat/toPay"));
	}
}



