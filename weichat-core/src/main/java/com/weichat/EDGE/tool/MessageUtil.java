package com.weichat.EDGE.tool;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;

import org.json.JSONObject;

public class MessageUtil {
	
	
	/**
	 * 获得ACCESS_TOKEN
	* @Title: getAccess_token
	* @Description: 获得ACCESS_TOKEN
	* @param @return 设定文件
	* @return String 返回类型
	* @throws
	 */
	private static String getAccess_token(){ 
	 String url = "https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid="+ WeiChatConfig.appid + "&secret=" +WeiChatConfig.secretid;
	 String accessToken = null;
	 try {
	 URL urlGet = new URL(url);
	 HttpURLConnection http = (HttpURLConnection) urlGet.openConnection(); 

	 http.setRequestMethod("GET"); //必须是get方式请求 
	 http.setRequestProperty("Content-Type","application/x-www-form-urlencoded"); 
	 http.setDoOutput(true); 
	 http.setDoInput(true);
	 System.setProperty("sun.net.client.defaultConnectTimeout", "30000");//连接超时30秒
	 System.setProperty("sun.net.client.defaultReadTimeout", "30000"); //读取超时30秒
	 http.connect();

	 InputStream is =http.getInputStream();
	 int size =is.available();
	 byte[] jsonBytes =new byte[size];
	 is.read(jsonBytes);
	 String message=new String(jsonBytes,"UTF-8");

	 JSONObject demoJson = new JSONObject(message);
	 accessToken = demoJson.getString("access_token");

	 System.out.println(message);
	 is.close();
	 } catch (Exception e) {
	 e.printStackTrace();
	 }
	 return accessToken;
	 }
	
	
	/**
	 * @throws UnsupportedEncodingException 
	 * 创建Menu
	* @Title: createMenu
	* @Description: 创建Menu
	* @param @return
	* @param @throws IOException 设定文件
	* @return int 返回类型
	* @throws
	 */
	 public static String getImag() throws UnsupportedEncodingException {
	
		 
	 String access_token= getAccess_token();
	 System.out.println("access_token=="+access_token);
	 String action = "https://api.weixin.qq.com/cgi-bin/media/get?access_token="+access_token+"&media_id=V1N8KwtLVIW7DDex962CbGottMOyAp1Ry8GEJbT2oydyC6P_ctlZsHxnE5-VZD82";
	 try {
	 URL url = new URL(action);
	 HttpURLConnection http = (HttpURLConnection) url.openConnection(); 

	 http.setRequestMethod("GET"); 
	 http.setRequestProperty("Content-Type","application/x-www-form-urlencoded"); 
	 http.setDoOutput(true); 
	 http.setDoInput(true);
	 System.setProperty("sun.net.client.defaultConnectTimeout", "30000");//连接超时30秒
	 System.setProperty("sun.net.client.defaultReadTimeout", "30000"); //读取超时30秒
	 http.connect();

	 InputStream is =http.getInputStream();
	 int size =is.available();
	 byte[] jsonBytes =new byte[size];
	 is.read(jsonBytes);
	 String message=new String(jsonBytes,"UTF-8");
	 return "返回信息"+message;
	 } catch (MalformedURLException e) {
	 e.printStackTrace();
	 } catch (IOException e) {
	 e.printStackTrace();
	 } 
	 return "createMenu 失败";
	 }

	public static void main(String[] args) throws UnsupportedEncodingException {
		// TODO Auto-generated method stub
		System.out.println(getImag());
	}

}
