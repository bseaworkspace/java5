package com.weichat.EDGE.tool;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.MessageDigest;

import org.json.JSONObject;

public class SignUtil {

	private String noncestr = "2231496767";

	private String jsapi_ticket = "HoagFKDcsGMVCIY2vOjf9i5G635A8Ml7cCsp8qKNziXhyhqEZMHlcAsEsKBpFhtENK10kJgZkKLB0KQXSJEjqg";

	private String timestamp = "1509776996";

	private String url = "http://acd49839.ngrok.io/weichat/EDGE/weichat.html/";

	private static String getAccess_token() {
		String url = "https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=" + WeiChatConfig.appid
				+ "&secret=" + WeiChatConfig.secretid;
		String accessToken = null;
		try {
			URL urlGet = new URL(url);
			HttpURLConnection http = (HttpURLConnection) urlGet.openConnection();

			http.setRequestMethod("GET"); // 必须是get方式请求
			http.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
			http.setDoOutput(true);
			http.setDoInput(true);
			System.setProperty("sun.net.client.defaultConnectTimeout", "30000");// 连接超时30秒
			System.setProperty("sun.net.client.defaultReadTimeout", "30000"); // 读取超时30秒
			http.connect();

			InputStream is = http.getInputStream();
			int size = is.available();
			byte[] jsonBytes = new byte[size];
			is.read(jsonBytes);
			String message = new String(jsonBytes, "UTF-8");

			JSONObject demoJson = new JSONObject(message);
			accessToken = demoJson.getString("access_token");

			System.out.println(message);
			is.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return accessToken;
	}

	private static String getString() {
		String s = "";
		
		String noncestr = "2003480040";

		String jsapi_ticket = "HoagFKDcsGMVCIY2vOjf9lGiR0pr-q9lUP1bStYBGmbzSY9Fgl0oRvVFZyxe0QMZx3B5zFIpyCiM54R471xY_w";

		String timestamp = "1509767292";

		String url = "http://acd49839.ngrok.io/weichat/EDGE/weichat.html/";
		
		s = "jsapi_ticket=" + jsapi_ticket + "&noncestr=" + noncestr + "&timestamp=" + timestamp + "&url=" + url;
		return s;
	}

	// Sha1签名
	public static String getSha1(String str) {
		if (str == null || str.length() == 0) {
			return null;
		}
		char hexDigits[] = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f' };

		try {
			MessageDigest mdTemp = MessageDigest.getInstance("SHA1");
			mdTemp.update(str.getBytes("UTF-8"));
			byte[] md = mdTemp.digest();
			int j = md.length;
			char buf[] = new char[j * 2];
			int k = 0;
			for (int i = 0; i < j; i++) {
				byte byte0 = md[i];
				buf[k++] = hexDigits[byte0 >>> 4 & 0xf];
				buf[k++] = hexDigits[byte0 & 0xf];
			}
			return new String(buf);
		} catch (Exception e) {
			return null;
		}
	}

	public static void main(String[] args) {
		System.out.println("sign="+getSha1(getString()));
	}
}
