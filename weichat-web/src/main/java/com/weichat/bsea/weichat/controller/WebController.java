package com.weichat.bsea.weichat.controller;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import com.weichat.bsea.tool.WeiChatConfig;
import com.weichat.bsea.tool.WeiChatUtil;


@RestController
public class WebController {
	
	
	@PostMapping("chooseWXPayo")
	public Map<String,String> postchooseWXPay(HttpServletRequest request){
		
		Map<String,String> postWX=new HashMap<String,String>();
		postWX.put("appId",WeiChatConfig.appid);
		postWX.put("timeStamp",WeiChatUtil.getTimeStamp());// 支付签名时间戳，注意微信jssdk中的所有使用timestamp字段均为小写。但最新版的支付后台生成签名使用的timeStamp字段名需大写其中的S字符
		postWX.put("nonceStr",(String) request.getSession().getAttribute("nonce_str"));// 支付签名随机串，不长于 32 位
		postWX.put("package",(String) request.getSession().getAttribute("prepay_id"));// 统一支付接口返回的prepay_id参数值，提交格式如：prepay_id=***）■██████
		postWX.put("signType","MD5");// 签名方式，默认为'SHA1'，使用新版支付需传入'MD5'
		postWX.put("paySign",(String) request.getSession().getAttribute("pay_sign"));// 支付签名*/■██████

		return postWX;
	}

}
