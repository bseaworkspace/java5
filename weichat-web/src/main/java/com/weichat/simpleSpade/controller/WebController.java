package com.weichat.simpleSpade.controller;

import java.io.IOException;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.weichat.simpleSpade.tool.JsApiUtil;
import com.weichat.simpleSpade.tool.SignUtil;
import com.weichat.simpleSpade.tool.WeiChatConfig;
import com.weichat.simpleSpade.tool.WeiChatUtil;

@RestController
public class WebController {
	
	@RequestMapping("initConfig")
	public Map<String,String> getJSDKConfig() throws IOException{
		SortedMap<String,String> map = new TreeMap<String,String>();
		String timeStamp = SignUtil.getTimeStamp();
		String nonceStr = SignUtil.getNonceStr();
		WeiChatUtil wu = new WeiChatUtil();
		//WeiChatConfig.accessToken = wu.getAccessToken();
		map.put("accessToken", WeiChatConfig.accessToken);
		String ticket = JsApiUtil.getjsApiTicket();
		String url = "http://926fe30a.ngrok.io/weichat/simpleSpade/weichatinit-simpleSpade.html/";
		map.put("timestamp", timeStamp);
		map.put("nonceStr", nonceStr);
		map.put("jsapi_ticket", ticket);
		map.put("url", url);
		String signature = SignUtil.getSha1(SignUtil.getString(map));
		map.put("signature",signature);
		map.put("appId", WeiChatConfig.appid);
		
		return map;
	}
}
