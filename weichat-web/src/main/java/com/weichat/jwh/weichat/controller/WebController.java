package com.weichat.jwh.weichat.controller;

import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.weichat.jwh.tool.MessageUtil;
import com.weichat.jwh.tool.Send;
import com.weichat.jwh.tool.Signatrue;
import com.weichat.jwh.tool.TokenSaveRead;
import com.weichat.jwh.tool.WeiChatConfig;

@RestController
public class WebController {
@Autowired
MessageUtil messageUtil;
@Autowired
Send send;
	
	@RequestMapping("/iniConfig/{html}")
	public Map<String,String> getJSDKconfig(@PathVariable String html,@RequestParam("html2") String htmlnow ) throws Exception{
		Map<String,String>  configParam=new HashMap();
		
		 String url1="https://open.weixin.qq.com/connect/oauth2/authorize?appid="+WeiChatConfig.appid+"&redirect_uri=";
		 String url2="&response_type=code&scope=snsapi_base&state=123#wechat_redirect";		
		 //健康生活
		 String html2="http://b58207dd.ngrok.io/weichat/jwh/"+html+".html";		 
		 html2=URLEncoder.encode(html2, "utf-8");
		 String html3=url1+html2+url2;
		 
		System.out.println("htmlnow==========="+htmlnow);
		Signatrue signatrue=new Signatrue();
		SortedMap<String, String> finalpackage = new TreeMap<String, String>();
		String timestamp = Signatrue.getTimeStamp();
		String nonce=Signatrue.getNonceStr();
		String token=TokenSaveRead.getToken();
		System.out.println("token-------"+token);
		//finalpackage.put("appId", WeiChatConfig.appid);
		finalpackage.put("timestamp", timestamp);
		finalpackage.put("noncestr",nonce );
		finalpackage.put("jsapi_ticket", signatrue.getjsApiTicket(signatrue.ticketUrl,token));
		finalpackage.put("url", htmlnow);
		//finalpackage.put("package", packages);
		//finalpackage.put("signType", WeiChatConfig.signType);
		String signature =Signatrue.createSHA1Sign(finalpackage);
		
		
		configParam.put("appid", WeiChatConfig.appid);
		configParam.put("timestamp", timestamp);
		configParam.put("nonceStr", nonce);
		configParam.put("signature", signature);
		
		
		
		return configParam;
		
	}
	@RequestMapping("/test2")
	public Map<String,String> getJSDKconfigSP( HttpServletRequest req,@RequestParam("first") String first,@RequestParam("keyword") String keyword)  {
		System.out.println("test-----------");
		
		Map<String,String>  m=new HashMap();
		String openId=(String) req.getSession().getAttribute("openId");
		
		System.out.println("openId-----------"+openId);
		
		m.put("rs", send.sendMsg(openId,first,keyword));
	
		/**
		 从数据库里拿 状态id ---
		 			乘车id 
		 				判断时间    	 有乘车（8：00-8：30）	 没乘车（8：00-9：00）	 时间内Y	时间外N 
		 			存放手机id
		 				N
		 */
		

		System.out.println("test---end-------");

		
		return m;
		
	}
	@RequestMapping("/SetPhone")
	public Map<String,String> setPhone() throws Exception{
		Map<String,String>  m=new HashMap();
		m.put("rs", "setPhone去数据库修改");
		
	 
		
		return m;
		
	}
	@RequestMapping("/BreakUp")
	public Map<String,String> breakUp() throws Exception{
		Map<String,String>  m=new HashMap();
		m.put("rs", "breakUp去数据库修改");
		
		
		
		return m;
		
	}
	
	@RequestMapping("/uploadImg")
	public Map<String,String> uploadImg(@RequestParam String serverId) throws Exception{
		Map<String,String>  m=new HashMap();
		m.put("rs", "breakUp去数据库修改");
		
		System.out.println("serverId-------"+serverId);
		messageUtil.saveImageToLocal(serverId, TokenSaveRead.getToken(),"openid");
		return m;
		
	}
	
}
