package com.weichat.wsq.controller;

import java.util.HashMap;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.weichat.wsq.tool.Sha1Util;
import com.weichat.wsq.tool.WeiChatConfig;
import com.weichat.wsq.tool.WeiChatUtil;



@RestController
public class WebController {
	@RequestMapping("/configer1")

	public Map<String,String> getJSDKconfig() throws Exception{
		Map<String,String>  configParam=new HashMap();
		SortedMap<String, String> finalpackage = new TreeMap<String, String>();
		String timestamp = Sha1Util.getTimeStamp();
		String nonce=Sha1Util.getNonceStr();
		WeiChatUtil wu=new WeiChatUtil();
		String accesstoken=wu.getAccessToken();
		String Ticket=Sha1Util.getjsApiTicket("https://api.weixin.qq.com/cgi-bin/ticket/getticket?access_token=",accesstoken);
		
		finalpackage.put("timestamp", timestamp);
		finalpackage.put("noncestr", nonce);
		finalpackage.put("jsapi_ticket", Ticket);
		finalpackage.put("url", "http://339c2199.ngrok.io/weichat/wsq/weichat-wsq.html/");
		
		
		
		
		
		String signature =Sha1Util.createSHA1Sign(finalpackage);
		configParam.put("appid", WeiChatConfig.appid);
		configParam.put("timestamp", timestamp);
		configParam.put("nonceStr", nonce);
		configParam.put("signature", signature);
		return configParam;
		
   }
}