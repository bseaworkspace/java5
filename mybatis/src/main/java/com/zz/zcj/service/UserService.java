package com.zz.zcj.service;

import com.zz.zcj.model.User;

public interface UserService {
	
	 User selectById(Integer id);
	 
	 int addUser(User record);

}
