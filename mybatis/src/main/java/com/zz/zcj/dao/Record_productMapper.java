package com.zz.zcj.dao;

import java.util.List;

import com.zz.zcj.model.Record_product;
import com.zz.zcj.view.model.PieData1;

public interface Record_productMapper {
    int insert(Record_product record);

    int insertSelective(Record_product record);
    List<PieData1> selectPieData();
}