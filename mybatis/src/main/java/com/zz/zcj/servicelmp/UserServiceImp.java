package com.zz.zcj.servicelmp;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.zz.zcj.dao.UserMapper;
import com.zz.zcj.model.User;
import com.zz.zcj.service.UserService;

@Service
public class UserServiceImp implements UserService {
 
	@Autowired
	UserMapper userMapper;
	@Override
	public User selectById(Integer id) {
		// TODO Auto-generated method stub
		return userMapper.selectByPrimaryKey(id);
	}
	@Override
	public int addUser(User record) {
		// TODO Auto-generated method stub
		return userMapper.insertSelective(record);
	}

}
