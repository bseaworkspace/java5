package com.zz.zcj.servicelmp;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.zz.zcj.dao.Record_productMapper;
import com.zz.zcj.service.PieDataService;
import com.zz.zcj.view.model.PieData1;

@Service
public class PieDataServiceImp implements PieDataService{
    @Autowired
	Record_productMapper rp;
	@Override
	public List<PieData1> selectPieData() {
		// TODO Auto-generated method stub
		return rp.selectPieData();
	}

}
