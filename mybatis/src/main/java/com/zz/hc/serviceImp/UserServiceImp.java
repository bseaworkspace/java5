package com.zz.hc.serviceImp;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.zz.hc.dao.ProductMapper;
import com.zz.hc.dao.UserDao;
import com.zz.hc.dao.UserMapper;
import com.zz.hc.model.User;
import com.zz.hc.service.UserService;
@Service
public class UserServiceImp implements UserService{
	@Autowired
	UserMapper userMapper;
	UserDao userDao;
	ProductMapper productMapper;
	@Override
	public User selectById(Integer id) {
		// TODO Auto-generated method stub
		return userMapper.selectByPrimaryKey(id);
	}
	
	@Override
	public int addUser(User record) {
		// TODO Auto-generated method stub
		return userMapper.insertSelective(record);
	}
	@Override
	public int selectUserNum() {
		// TODO Auto-generated method stub
		return userDao.selectUserNum();
	}
	
	@Override
	public List<User> ListuserByNumber(String phonenumber){
		
		return userMapper.ListuserByNumber(phonenumber);
	}
	
	@Override
	public int updateByPrimaryKeySelective(User record){
		
		return userMapper.updateByPrimaryKeySelective(record);
	}
	
}
