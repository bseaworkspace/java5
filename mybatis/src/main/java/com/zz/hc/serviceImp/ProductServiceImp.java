package com.zz.hc.serviceImp;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.zz.hc.dao.ProductMapper;
import com.zz.hc.model.Product;
import com.zz.hc.service.ProductService;
import com.zz.hc.view.mode.R_product;
@Service
public class ProductServiceImp implements ProductService{

	@Autowired
	ProductMapper productMapper;
	@Override
	public int deleteByPrimaryKey(Integer id) {
		// TODO Auto-generated method stub
		return productMapper.deleteByPrimaryKey(id);
	}

	@Override
	public int insert(Product record) {
		// TODO Auto-generated method stub
		return productMapper.insert(record);
	}

	@Override
	public int insertSelective(Product record) {
		// TODO Auto-generated method stub
		return productMapper.insertSelective(record);
	}

	@Override
	public Product selectByPrimaryKey(Integer id) {
		// TODO Auto-generated method stub
		return productMapper.selectByPrimaryKey(id);
	}

	@Override
	public int updateByPrimaryKeySelective(Product record) {
		// TODO Auto-generated method stub
		return productMapper.updateByPrimaryKeySelective(record);
	}

	@Override
	public int updateByPrimaryKey(Product record) {
		// TODO Auto-generated method stub
		return productMapper.updateByPrimaryKey(record);
	}

	@Override
	public List<Product> selectAllProduct() {
		// TODO Auto-generated method stub
		return productMapper.selectAllProduct();
	}

	@Override
	public List<R_product> selectByR_id(Integer id) {
		// TODO Auto-generated method stub
		return productMapper.selectByR_id(id);
	}

}
