package com.zz.hc.serviceImp;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.zz.hc.dao.RecordMapper;
import com.zz.hc.model.Record;
import com.zz.hc.service.RecordService;
import com.zz.hc.view.mode.AllRecord;




@Service
public class RecordServiceImp implements RecordService{

	@Autowired
	RecordMapper recordMapper;
	@Override
	public int insertSelective(Record record) {
		// TODO Auto-generated method stub
		return recordMapper.insertSelective(record);
	}
	@Override
	public List<AllRecord> selectAllRecord() {
		// TODO Auto-generated method stub
		
		return recordMapper.selectAllRecord();
	}
	@Override
	public List<AllRecord> selectUserRecord(String phone) {
		// TODO Auto-generated method stub
		return recordMapper.selectUserRecord(phone);
	}

	
	
}
