package com.zz.hc.service;

import java.util.List;

import com.zz.hc.model.Record;
import com.zz.hc.view.mode.AllRecord;



public interface RecordService {
	int insertSelective(Record record);
	
	List<AllRecord> selectAllRecord();

	List<AllRecord> selectUserRecord(String phone);
	

}
