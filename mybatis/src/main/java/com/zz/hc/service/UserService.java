package com.zz.hc.service;

import java.util.List;

import com.zz.hc.model.User;

public interface UserService {

		User selectById(Integer id);
	
	 int addUser(User record);
	 
	 int  selectUserNum();
	 
	 List<User> ListuserByNumber(String phonenumber);
	 
	 int updateByPrimaryKeySelective(User record);

}
