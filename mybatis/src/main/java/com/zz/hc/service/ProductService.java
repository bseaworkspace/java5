package com.zz.hc.service;

import java.util.List;

import com.zz.hc.model.Product;
import com.zz.hc.view.mode.R_product;


public interface ProductService {
    int deleteByPrimaryKey(Integer id);

    int insert(Product record);

    int insertSelective(Product record);

    Product selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(Product record);

    int updateByPrimaryKey(Product record);
    
    List<Product> selectAllProduct();
    
    List<R_product> selectByR_id(Integer id);
}
