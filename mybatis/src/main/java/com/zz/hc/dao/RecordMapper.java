package com.zz.hc.dao;

import java.util.List;

import com.zz.hc.model.Record;
import com.zz.hc.view.mode.AllRecord;


public interface RecordMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(Record record);

    int insertSelective(Record record);

    Record selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(Record record);

    int updateByPrimaryKey(Record record);
    
    
    List<AllRecord> selectAllRecord();
    List<AllRecord> selectUserRecord(String phone);
}