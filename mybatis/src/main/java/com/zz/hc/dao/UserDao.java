package com.zz.hc.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.zz.hc.model.User;

public class UserDao {
	
	BaseDao bd=new BaseDao();
	public List<User> ListUser(int page){
		List ls=new ArrayList();
		Connection con=bd.getConnection();
		String sql="SELECT  name, credits, join_dt, `level`, mobile, sex FROM java5.`user` order by join_dt desc limit ?,10  ;";
		try {
			PreparedStatement ps=con.prepareStatement(sql);
			ps.setInt(1,(page-1)*10 );
			ResultSet rs =ps.executeQuery();
			while(rs.next()){
				User u=new User();
				u.setName(rs.getString("name"));
				u.setCredits(rs.getInt("credits"));
				u.setJoinDt(rs.getDate("join_dt"));
				u.setLevel(rs.getString("level"));
				u.setMobile(rs.getString("mobile"));
				u.setSex(rs.getString("sex"));
				
				
				ls.add(u);
			}bd.closeQuery(rs, ps, con);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		
		
		return ls;
	}
	
	public int selectUserNum(){
		int num=0;
	Connection con=bd.getConnection();
		String sql="SELECT count(*) as num FROM java5.`user` ;";
		try {
			PreparedStatement ps=con.prepareStatement(sql);
			ResultSet rs =ps.executeQuery();
			while(rs.next()){
			num=rs.getInt("num");
			}bd.closeQuery(rs, ps, con);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		
		
		return num;
	}

	public List<User> getUserByName(String name){
		List<User> ls=new ArrayList();
		Connection con=bd.getConnection();
		String sql="select*from user where name=?";
		try {
			PreparedStatement ps=con.prepareStatement(sql);
			ps.setString(1, name);
			ResultSet rs=ps.executeQuery();
			while(rs.next()){
				User u=new User();
				u.setName(rs.getString("name"));
				u.setJoinDt(rs.getDate("join_dt"));
				u.setLevel(rs.getString("level"));
				u.setMobile(rs.getString("mobile"));
				u.setSex(rs.getString("sex"));
				u.setCredits(rs.getInt("credits"));
				ls.add(u);
			}bd.closeQuery(rs, ps, con);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return ls;
		
	}

}
