package com.zz.jwh.serviceimp;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.zz.jwh.dao.UserMapper;
import com.zz.jwh.model.User;
import com.zz.jwh.service.UserService;
@Service
public class UserServiceImp implements UserService {
	@Autowired
	UserMapper userMapper;
	@Override
	public User selectById(Integer id) {
		// TODO Auto-generated method stub
		return userMapper.selectByPrimaryKey(id);
	}

}
