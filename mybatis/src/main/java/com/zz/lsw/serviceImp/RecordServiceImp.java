package com.zz.lsw.serviceImp;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.zz.lsw.dao.RecordMapper;
import com.zz.lsw.model.Record;
import com.zz.lsw.view.model.AllRecord;
import com.zz.lsw.service.RecordService;



@Service
public class RecordServiceImp implements RecordService{

	@Autowired
	RecordMapper recordMapper;
	@Override
	public int insertSelective(Record record) {
		// TODO Auto-generated method stub
		return recordMapper.insertSelective(record);
	}
	@Override
	public List<AllRecord> selectAllRecord() {
		// TODO Auto-generated method stub
		
		return recordMapper.selectAllRecord();
	}

	
	
}
