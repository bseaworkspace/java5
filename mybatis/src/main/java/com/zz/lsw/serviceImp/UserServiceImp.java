package com.zz.lsw.serviceImp;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.zz.lsw.dao.UserMapper;
import com.zz.lsw.model.User;
import com.zz.lsw.service.UserService;

@Service
public class UserServiceImp implements UserService {
 
	@Autowired
	UserMapper userMapper;
	@Override
	public User selectById(Integer id) {
		// TODO Auto-generated method stub
		return userMapper.selectByPrimaryKey(id);
	}
	@Override
	public int insertSelective(User record) {
		// TODO Auto-generated method stub
		return userMapper.insertSelective(record);
	}
	
	
		

}
