package com.zz.lsw.serviceImp;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.zz.lsw.dao.productMapper;
import com.zz.lsw.service.ProductService;
import com.zz.lsw.view.model.R_product;

@Service
public class ProductServiceImp implements ProductService{

	@Autowired
	productMapper p;
	@Override
	public List<R_product> selectByR_id(Integer id) {
		// TODO Auto-generated method stub
		return p.selectByR_id(id);
	}

}
