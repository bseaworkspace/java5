package com.zz.lsw.service;

import com.zz.lsw.model.User;

public interface UserService {
	
	 User selectById(Integer id);
	 
	 int insertSelective(User record);

}
