package com.zz.lsw.service;

import java.util.List;

import com.zz.lsw.model.Record;
import com.zz.lsw.view.model.AllRecord;

public interface RecordService {
	int insertSelective(Record record);
	
	List<AllRecord> selectAllRecord();

	
	

}
