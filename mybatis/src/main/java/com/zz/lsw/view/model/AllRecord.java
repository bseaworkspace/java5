package com.zz.lsw.view.model;

import java.sql.Timestamp;
import java.util.Date;




public class AllRecord {
	
	private Timestamp create_dt;
	private String name ;
	private int userId;
	private int id;
	private int total_integral;
	private double  total;
	public int getTotal_integral() {
		return total_integral;
	}
	public void setTotal_integral(int total_integral) {
		this.total_integral = total_integral;
	}
	public double getTotal() {
		return total;
	}
	public void setTotal(double total) {
		this.total = total;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	
	
	public Timestamp getCreate_dt() {
		return create_dt;
	}
	public void setCreate_dt(Timestamp create_dt) {
		this.create_dt = create_dt;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}

}
