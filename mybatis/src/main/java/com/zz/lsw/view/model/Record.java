package com.zz.lsw.view.model;

import java.util.Date;
import java.util.List;

import com.zz.lsw.model.product;

public class Record {
	private Date create_dt;
	private String name ;
	private int userId;
	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	public Date getCreate_dt() {
		return create_dt;
	}
	public void setCreate_dt(Date create_dt) {
		this.create_dt = create_dt;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}


}
