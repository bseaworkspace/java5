package com.zz.lsw.model;

import java.util.List;

import com.zz.hc.model.Product;

public class ViewModelRecord {
	public String getMobile() {
		return mobile;
	}
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	public List<Product> getProduct() {
		return product;
	}
	public void setProduct(List<Product> product) {
		this.product = product;
	}
	private String mobile;
	private List<Product> product;

}
