package com.zz.lsw.dao;

import com.zz.lsw.model.Record_product;

public interface Record_productMapper {
    int insert(Record_product record);

    int insertSelective(Record_product record);
}