package com.zz.lsw.dao;

import java.util.List;



import com.zz.lsw.model.Record;
import com.zz.lsw.view.model.AllRecord;


public interface RecordMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(Record record);

    int insertSelective(Record record);

    Record selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(Record record);

    int updateByPrimaryKey(Record record);
    
    
    List<AllRecord> selectAllRecord();
}