package com.zz.lsw.dao;

import java.util.List;

import com.zz.lsw.model.product;
import com.zz.lsw.view.model.R_product;

public interface productMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(product record);

    int insertSelective(product record);

    product selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(product record);

    int updateByPrimaryKey(product record);
    
    List<R_product> selectByR_id(Integer id);
}