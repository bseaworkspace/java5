package com.zz.EDGE.service;

import java.util.List;

import com.zz.EDGE.model.User;

public interface UserService {
	
	  User selectById(Integer id);
	  int addUser(User record);
	  List<User> selectMobile();
	  User selectUserByPhone(String phone);
}
