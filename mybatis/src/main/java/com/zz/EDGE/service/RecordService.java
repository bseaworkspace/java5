package com.zz.EDGE.service;



import java.util.List;

import com.zz.EDGE.model.Record;

public interface RecordService {
	Record selectByRid(Integer id);
	int addRecord(Record record);
	Record selectAllRecord();
	List<Record> selectRecordIdByphone(String number);
	int selectAllRecordByPhone(String number);
}
