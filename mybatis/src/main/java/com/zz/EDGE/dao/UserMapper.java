package com.zz.EDGE.dao;

import java.util.List;

import com.zz.EDGE.model.User;
import com.zz.EDGE.model.XiaofeiProduct;

public interface UserMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(User record);

    int insertSelective(User record);

    User selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(User record);

    int updateByPrimaryKey(User record);
    
    List<User> selectMobile();
    
    List<XiaofeiProduct> getProductByPhone(Integer id);
    
    User selectUserByPhone(String phone);
}