package com.zz.EDGE.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.zz.EDGE.model.Product;
import com.zz.EDGE.model.Record;
import com.zz.EDGE.model.Record_Product;

@Service("Rd")
public class RecordDao {
	@Autowired
	BaseDao baseDao;
	public boolean insertRecord_Product(int id,int number){
		boolean flag=false;
		Connection con=baseDao.getConnection();
		try {
		String sql="insert into record_product(product_id,product_count)values(?,?)";
			PreparedStatement ps=con.prepareStatement(sql);
			ps.setInt(1, id);
			ps.setInt(2, number);
			if(ps.executeUpdate()>0){
				flag=true;
			}
			baseDao.closeUpdate(ps, con);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return flag;
	}
	public Product getProductIdByName(String name){
		Product p=null;
		Connection con=baseDao.getConnection();
		try {
		String sql="select product.id from product where product.name=?";		
			PreparedStatement ps=con.prepareStatement(sql);
			ps.setString(1, name);
			ResultSet rs=ps.executeQuery();
			while(rs.next()){
				p=new Product();
				p.setId(rs.getInt("id"));
				p.setName(rs.getString("name"));
			}
			baseDao.closeQuery(rs, ps, con);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return p;
	}
	public Record_Product getRecord_ProductByMobile(String mobile){
		Record_Product rp=null;
		Connection con=baseDao.getConnection();
		try {
		String sql="select rd.record_id,rd.product_id,rd.product_count from user u left join record r on(u.id=r.user_id)left join record_product rd on(r.id=rd.record_id) where u.id in(select u.id from user where mobile=?)";		
			PreparedStatement ps=con.prepareStatement(sql);
			ps.setString(1, mobile);
			ResultSet rs=ps.executeQuery();
			while(rs.next()){
				rp.setRecordId(rs.getInt("recordId"));
				rp.setProductId(rs.getInt("productId"));
				rp.setProductCount(rs.getInt("productCount"));
			}
			baseDao.closeQuery(rs, ps, con);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return rp;
	}
}
