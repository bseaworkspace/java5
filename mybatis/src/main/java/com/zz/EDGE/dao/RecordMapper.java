package com.zz.EDGE.dao;

import java.util.List;



import com.zz.EDGE.model.Record;
import com.zz.EDGE.model.XiaoFei;

public interface RecordMapper {
	
    int deleteByPrimaryKey(Integer id);

    int insert(Record record);

    int insertSelective(Record record);

    Record selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(Record record);

    int updateByPrimaryKey(Record record);
    
    Record selectAllRecord();
    
    int getRecordCountByPhone(String number);
    
    List<XiaoFei> selectRecordByphone(String number);
    
    List<Record> selectRecordIdByphone(String number);
    
    int selectAllRecordByPhone(String number);
}