package com.zz.EDGE.dao;

import com.zz.EDGE.model.Record_Product;

public interface Record_ProductMapper {
    int insert(Record_Product record);

    int insertSelective(Record_Product record);
}