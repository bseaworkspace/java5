package com.zz.EDGE.dao;


import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.zz.EDGE.model.Product;

@Service("pd")
public class ProductDao {
	@Autowired
	BaseDao baseDao;
public List<Product> getProduct(){
	List<Product> ls=new ArrayList<Product>();
	Connection con=baseDao.getConnection();
	try {
	String sql="select p_name from product ";
		PreparedStatement ps=con.prepareStatement(sql);
		ResultSet rs=ps.executeQuery();
		while(rs.next()){
			Product p=new Product();
			
			ls.add(p);
		}
		baseDao.closeQuery(rs, ps, con);
	} catch (SQLException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	return ls;
}
}
