package com.zz.EDGE.servicelmp;



import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.zz.EDGE.dao.RecordMapper;
import com.zz.EDGE.model.Record;
import com.zz.EDGE.service.RecordService;

@Service
public class RecordServicelmp implements RecordService{
@Autowired
RecordMapper recordmapper;
@Override
public int addRecord(Record record){
	return recordmapper.insertSelective(record);
}
@Override
public Record selectAllRecord() {
	// TODO Auto-generated method stub
	return recordmapper.selectAllRecord();
}
public int getRecordCountByPhone(String number){
	return recordmapper.getRecordCountByPhone(number);
}
@Override
public Record selectByRid(Integer id) {
	// TODO Auto-generated method stub
	return null;
}
@Override
public List<Record> selectRecordIdByphone(String number){
	return recordmapper.selectRecordIdByphone(number);
}
public int selectAllRecordByPhone(String number){
	return recordmapper.selectAllRecordByPhone(number);
}
}
