package com.zz.EDGE.servicelmp;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.zz.EDGE.dao.UserMapper;
import com.zz.EDGE.model.XiaofeiProduct;
import com.zz.EDGE.service.XiaofeiProductService;
@Service
public class XiaofeiProductServicelmp implements XiaofeiProductService{
	@Autowired
	UserMapper usermapper;
	@Override
	public List<XiaofeiProduct> getProductByPhone(Integer id){
		return usermapper.getProductByPhone(id);
	}
}
