package com.zz.EDGE.servicelmp;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.zz.EDGE.dao.UserMapper;
import com.zz.EDGE.model.User;
import com.zz.EDGE.service.UserService;

@Service
public class UserServicelmp implements UserService{
	@Autowired
	UserMapper userMapper;
	@Override
	public User selectById(Integer id) {
		// TODO Auto-generated method stub
		return userMapper.selectByPrimaryKey(id);
	}
	@Override
	public int addUser(User record){
	return userMapper.insertSelective(record);
	}
	@Override
	public  List<User> selectMobile(){
		return userMapper.selectMobile();
	}
	@Override
	public User selectUserByPhone(String phone){
		return userMapper.selectUserByPhone(phone);
	}
}
