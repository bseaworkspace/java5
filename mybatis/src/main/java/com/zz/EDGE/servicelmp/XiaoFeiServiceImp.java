package com.zz.EDGE.servicelmp;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.zz.EDGE.dao.RecordMapper;
import com.zz.EDGE.model.Record;
import com.zz.EDGE.model.XiaoFei;
import com.zz.EDGE.service.XiaoFeiService;
@Service
public class XiaoFeiServiceImp implements XiaoFeiService{
	@Autowired
	RecordMapper recordmapper;
	@Override
	public List<XiaoFei> selectRecordByphone(String number){
		return recordmapper.selectRecordByphone(number);
	}
}
