package com.zz.EDGE.model;

import java.sql.Time;
import java.util.Date;

public class XiaoFei {
	private Integer recordId;
	 private Time createDt;
	 private String name;	 
	public Integer getRecordId() {
		return recordId;
	}
	public void setRecordId(Integer recordId) {
		this.recordId = recordId;
	}
	
	public Time getCreateDt() {
		return createDt;
	}
	public void setCreateDt(Time createDt) {
		this.createDt = createDt;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
}
