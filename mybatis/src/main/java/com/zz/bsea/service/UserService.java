package com.zz.bsea.service;

import com.zz.bsea.model.User;

public interface UserService {
	
	 User selectById(Integer id);
	 
	 int addUser(User record);

}
