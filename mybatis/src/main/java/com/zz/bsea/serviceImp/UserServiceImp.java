package com.zz.bsea.serviceImp;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.zz.bsea.dao.UserMapper;
import com.zz.bsea.model.User;
import com.zz.bsea.service.UserService;

@Service
public class UserServiceImp implements UserService {
 
	@Autowired
	UserMapper userMapper;
	@Override
	public User selectById(Integer id) {
		// TODO Auto-generated method stub
		return userMapper.selectByPrimaryKey(id);
	}
	@Override
	public int addUser(User record) {
		// TODO Auto-generated method stub
		return userMapper.insertSelective(record);
	}

}
