package com.zz.simpleSpade.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.zz.simpleSpade.model.Product;

@Service
public interface ProductService {
	List<Product> selectAll();
}
