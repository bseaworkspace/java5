package com.zz.simpleSpade.service;

import com.zz.simpleSpade.model.User;

public interface UserService {
	
	 User selectById(Integer id);
	 
	 int insertSelective(User record);
	 
	 User selectByMobile(String mobile);

}
