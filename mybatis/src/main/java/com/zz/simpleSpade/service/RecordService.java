package com.zz.simpleSpade.service;

import com.zz.simpleSpade.model.Product;


public interface RecordService {
	
	Product selectByName(String name);
	
}
