package com.zz.simpleSpade.dao;

import java.util.List;

import com.zz.simpleSpade.model.Product;

public interface ProductMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(Product record);

    int insertSelective(Product record);

    Product selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(Product record);

    int updateByPrimaryKey(Product record);
    
    Product selectByName(String name);
    
    List<Product> selectAll();
}