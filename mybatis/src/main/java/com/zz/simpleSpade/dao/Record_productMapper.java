package com.zz.simpleSpade.dao;

import com.zz.simpleSpade.model.Record_product;

public interface Record_productMapper {
    int insert(Record_product record);

    int insertSelective(Record_product record);
}