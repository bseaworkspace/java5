package com.zz.simpleSpade.serviceImp;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.zz.simpleSpade.dao.UserMapper;
import com.zz.simpleSpade.model.User;
import com.zz.simpleSpade.service.UserService;



@Service
public class UserServiceImp implements UserService {
 
	@Autowired
	UserMapper userMapper;
	
	@Override
	public User selectById(Integer id) {
		// TODO Auto-generated method stub
		return userMapper.selectByPrimaryKey(id);
	}
	
	@Override
	public int insertSelective(User record) {
		// TODO Auto-generated method stub
		return userMapper.insertSelective(record);
	}

	@Override
	public User selectByMobile(String mobile) {
		// TODO Auto-generated method stub
		return userMapper.selectByMobile(mobile);
	}

}
