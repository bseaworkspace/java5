package com.zz.simpleSpade.serviceImp;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.zz.simpleSpade.dao.ProductMapper;
import com.zz.simpleSpade.model.Product;
import com.zz.simpleSpade.service.RecordService;

@Service
public class RecordServiceImp implements RecordService{
	
	@Autowired
	ProductMapper productMapper;
	
	@Override
	public Product selectByName(String name) {
		
		return productMapper.selectByName(name);
		
	}
	
}
