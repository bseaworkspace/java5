package com.zz.simpleSpade.serviceImp;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.zz.simpleSpade.dao.ProductMapper;
import com.zz.simpleSpade.model.Product;
import com.zz.simpleSpade.service.ProductService;

public class ProductServiceImp implements ProductService {

	@Autowired
	ProductMapper productMapper;

	@Override
	public List<Product> selectAll() {
		// TODO Auto-generated method stub
		return productMapper.selectAll();
	}

}
