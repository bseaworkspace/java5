package com.zz.wsq.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.zz.wsq.model.User;

public class UserDao {
	BaseDao basedao=new BaseDao();
	public List<User> getUserByName(String name){
		List<User> ls=new ArrayList();
		Connection con=basedao.getConnection();
		String sql="select*from user where name=?";
		try {
			PreparedStatement ps=con.prepareStatement(sql);
			ps.setString(1, name);
			ResultSet rs=ps.executeQuery();
			while(rs.next()){
				User u=new User();
				u.setName(rs.getString("name"));
				u.setJoinDt(rs.getDate("join_dt"));
				u.setLevel(rs.getString("level"));
				u.setMobile(rs.getString("mobile"));
				u.setSex(rs.getString("sex"));
				u.setCredits(rs.getInt("credits"));
				u.setBirthday(rs.getString("birthday"));
				ls.add(u);
			}basedao.closeQuery(rs, ps, con);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return ls;
		
	}




}
