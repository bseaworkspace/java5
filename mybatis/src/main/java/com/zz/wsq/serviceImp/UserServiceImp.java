package com.zz.wsq.serviceImp;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.zz.wsq.dao.UserMapper;
import com.zz.wsq.model.User;
import com.zz.wsq.service.UserService;
@Service
public class UserServiceImp implements UserService{
	@Autowired
	UserMapper UserMaooer;

	@Override
	public User selectById(Integer id) {
		// TODO Auto-generated method stub
		return UserMaooer.selectByPrimaryKey(id);
	}
	@Override
	public int addUser(User u) {
		// TODO Auto-generated method stub
		return UserMaooer.insertSelective(u);
	}

}
