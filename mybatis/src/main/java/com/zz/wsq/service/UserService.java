package com.zz.wsq.service;

import com.zz.wsq.model.User;

public interface UserService {
	User selectById(Integer id);
	 int addUser(User record);

}
