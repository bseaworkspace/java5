# java5

-----------------------------------Bsea 开始-------------------------------------

1. java的8个基本数据类型： byte,short,int,long,float, double,char, boolean




-----------------------------------Bsea 结束-------------------------------------




-----------------------------------王生旗 开始-------------------------------------



-----------------------------------王生旗   结束-------------------------------------
-----------------------------------梁思维 开始-------------------------------------
0，sts的基本操作，引包jdk，utf-8

1，java的7种类型
char，int,string,double,long,boolean,float

2，if循环，for循环，if判断
if(i=0;i<10，i++)

3，list，map，数组的存值和遍历取值,和一些方法
（1）list add，map put，i[int]=

4，面向对象
（1）对象的属性，以及发放

5，string的一些方法
对字符串切割，判断

6，各种修饰符的区别和用法
访问权限   类   包  子类  其他包
public     ∨   ∨   ∨     ∨
protect    ∨   ∨   ∨      ×
default    ∨   ∨   ×     ×
private    ∨   ×   ×     ×

7，算术运算符
+-*/% 亦或&|非！

8，mysql的构建，已经用dao方法链接数据库
select delete insert UPDATE

9，对mysql进行查询修改dao方法的编写

10，html的基本属性

11，bootstrep是用法

12，jquery的使用
ajax

13，网页基本功能的实现
跳转，传值

14，doget和dopost的区别和用法
doGet：GET方法会把名值对追加在请求的URL后面。因为URL对字符数目有限制，进而限制了用在客户端请求的参数值的数目。并且请求中的参数值是可见的，因此，敏感信息不能用这种方式传递。
doPOST：POST方法通过把请求参数值放在请求体中来克服GET方法的限制，因此，可以发送的参数的数目是没有限制的。最后，通过POST请求传递的敏感信息对外部客户端是不可见的

15，session传值

16，局部刷新和整体刷新

17，io的用法

18，通过io读取文件和创建文件

19，用java代码实现发送邮件

20，通过java实现读取Excel和创建编写Excel

21，maven的子项目和母项目

22，用springboard框架编写登录注册
springboard各种sevlet的特殊功能

23，date的各种方法

24，码云项目的创建和上传下载

25，项目的上线及更新

26，mysql语句的编写

27，数据库的搭建


-----------------------------------梁思维   结束-------------------------------------
----------------黄琛 开始----------------
1.首先需要配置开发环境使用sts，将jre改为jdk配置CLASSPATH(JDK和JRE的区别：JDK： 开发环境 JRE: 运行环境)，修改编码格式将gbk改为utf-8，将java编译版本改为1.8
2.java有八大基本数据类型：byte,short,int, long,float,double,char,boolean, 一共8个。标示符： 数字不可以开头，不可以使用关键字
3.逻辑运算符。 
& | ^ ! && || 逻辑运算符除了 ! 外都是用于连接两个boolean类型表达式。 
&: 只有两边都为true结果是true。否则就是false。 
|:只要两边都为false结果是false，否则就是true 
^:异或：和或有点不一样。 两边结果一样，就为false。 两边结果不一样，就为true. 
& 和 &&区别： & ：无论左边结果是什么，右边都参与运算。 
&&:短路与，如果左边为false，那么右边不参数与运算。 
| 和|| 区别：|：两边都运算。 
||：短路或，如果左边为true，那么右边不参与运算。 
4.面向对象
    1封装：封装好的类和方法可以用public,private之类的修饰符来修饰，从而实现对封装对象的一些属性隐藏。
	2继承：代码重用，在不修改父类的情况下，在子类中去扩展新的功能,可以直接调用父类的方法 	   
    3多态： java有了给同一类型的方法，在运行时实现不同功能的能力。多态是通过方法的重写和重载实现的。而其 中重写是指父类A或接口有一个方法function(),子类B,C分别继承A并且重写function()，当创建一个对象A b = new B(); b.function()就调用B的funciotn,假如你new C(),那调用的就是C重写的function。其中function实现了不同的功能。重载则是在同一个类中有相同名字的方法，参数类型或者参数个数不相同这些方法实现了不同的意义。
5.成员变量和局部变量的区别： 
    	1：成员变量直接定义在类中。 
		局部变量定义在方法中，参数上，语句中。 
		2：成员变量在这个类中有效。 
		局部变量只在自己所属的大括号内有效，大括号结束，局部变量失去作用域。 
		3：成员变量存在于堆内存中，随着对象的产生而存在，消失而消失。 
		局部变量存在于栈内存中，随着所属区域的运行而存在，结束而释放。 
6.构造函数：用于给对象进行初始化，是给与之对应的对象进行初始化，它具有针对性，函数中的一种。 
		特点： 
		1：该函数的名称和所在类的名称相同。 
		2：不需要定义返回值类型。 
		3：该函数没有具体的返回值。 

7.冒泡排序：从大到小排序
		 * 1.两个for循环
		 * 2.第一个for循环的初始值是0，第二个for循环的初始值是i+1
		 * 3.只有i位置的值比j位置的值小的时候，才交换位置。
   
8.选择排序：从所有的元素中选择一个最小的元素，放在0号位置。
		比较排序和冒泡排序，比较次数是一样的，但是效率，比较排序比较高，因为交换次数少（不是绝对的，比如  数组本来顺序就是对的）
		冒泡排序交换次数是0，但是比较排序，会是2.

9.控制语句if-else
	 1)、if后的括号不能省略，括号里表达式的值最终必须返回的是布尔值 
     2)、如果条件体内只有一条语句需要执行，那么if后面的大括号可以省略，但这是一种极为不好的编程习惯。 
     3)、对于给定的if，else语句是可选的，else if 语句也是可选的 
     4)、else和else if同时出现时，else必须出现在else if 之后 
     5)、如果有多条else if语句同时出现，那么如果有一条else if语句的表达式测试成功，那么会忽略掉其他所有else if和else分支。 
     6)、如果出现多个if,只有一个else的情形，else子句归属于最内层的if语句
10.switch-case
switch(表达式) { 
case 常量表达式1:语句1; .... 
case 常量表达式2:语句2; default:语句; } 
1、switch-case语句完全可以与if-else语句互转，但通常来说，switch-case语句执行效率要高。 
2、default就是如果没有符合的case就执行它,default并不是必须的. 3、case后的语句可以不用大括号. 
4、switch语句的判断条件可以接受int,byte,char,short,不能接受其他类型. 
5、一旦case匹配,就会顺序执行后面的程序代码,而不管后面的case是否匹配,直到遇见break,利用这一特性可以让好几个case执行统一语句. 

11.for循环
1.第一种写法for(int i=0;i<a,i++){}
2.for循环的另外一种写法，for each写法。 第二个参数是数组，第一个参数是一个临时变量.每次都把数组里面的元素赋值给第一个参数。

12.数组，list，map
1).声明一个数组int[] a=new int[7];即下标从0开始可以存七个元素 数组的常用方法 1.length属性，表示数组长度，即数组包含多少个元素。
2).list 声明一个list List list= new ArrayList(); size()方法，返回list长度。 输入参数是Object,除了8个基本类型，什么都可以添加。add(index,object)如果index已经有值，就会替换。
list.contains("")判断某个对象是否含有这个元素含有返回true
3).声明一个Map map=new HashMap();<String,Object>泛型 预先规定，这个map的key必须是String类型的  值是object类型，也就是值可以随便改 map2.put放入元素 遍历for(String key:map2.keySet()){
	遍历map的四种方法：
	
	//第一种，也是最常用的遍历方法
		for(Map.Entry<String, Car>e:ma1.entrySet()){
			String key=e.getKey();
			Car c=e.getValue();
			System.out.println("遍历map方法一通过entryset--"+key);
		}
		
		//第二种。
		for(String str:ma1.keySet()){
			System.out.println("遍历map方法一通过entryset-key-"+str);
		}
		
		for(Car c6:ma1.values()){
			System.out.println("遍历map方法一通过entryset-value-"+c6);
		}
		
		
		//第三种
		Iterator<Map.Entry<String, Car>> entries=ma1.entrySet().iterator();
		
		while(entries.hasNext()){
			
			Map.Entry <String, Car>e1= entries.next();
			System.out.println("遍历map方法三通过entryset--"+e1.getKey());
			
			
		}
		
		//第四种(尽量避免使用)
		/**
		 * 作为方法一的替代，这个代码看上去更加干净；但实际上它相当慢且无效率。
		 * 因为从键取值是耗时的操作（与方法一相比，在不同的Map实现中该方法慢了20%~200%）。
		 * 如果你安装了FindBugs，它会做出检查并警告你关于哪些是低效率的遍历。
		 * 所以尽量避免使用。
		 * 
		 * 
		 * 
		 */
		for(String str:ma1.keySet()){
			System.out.println("遍历map方法一通过entryset--"+str);
			
			Car value=ma1.get(str);
			
		}
	
13.判断连接符
	1).==判断两个值是否是相等。适用范围：八个基本类型和String== 8个基本类型，比较的是栈里面的值。
	* String方法比较特殊，==也是比较值
	2).equals方法，判断引用类型的两个变量是不是相等。equals默认情况下，也是比较地址equals方法来自于object是通过继承过来的
	
14.数据库增删改查
-- select表示查询
-- *表示所有列
-- from 表示后面的是表的名字
select * from user;

-- like表示模糊查询其中%表示0或更多
select * from user where pwd like '4%';

-- where后面如果要多个过滤的查询条件，用and连接，结果会越来越少，因为取交集
select * from user where phoneNumber like '168%' and '%6' ;

-- where后面如果要多个过滤的查询条件，用or连接，结果会越来越多，因为取并集
select * from user where phoneNumber like '%5'or name='king';

-- 插入数据
insert into user(id,name,pwd,phoneNumber) values (8,'king1','13652','13262215729');

-- 删除数据
delete from user where name='king';

-- 修改数据
update user set name='king' where id=8;

-- mysql 自带的函数count（列的名字），表示总的条数 *表示任何列
select count(*)from user where name='文明';

-- avg（列的名字）取得该列的平均值
select avg(score)from user;

-- min（列的名字）取得该列的平最小值
select min(score)from user;

-- max（列的名字）取得该列的最大值
select max(score)from user;


-- distinct列名。表示在结果集中，去掉值是重复的，只保留一条。
-- 比如这里 文明的值有四条，这里只会有一条显示在查询结果中。
-- distinct 列名1，列名2，只有在列名1和列名2都相同的时候，才会认为是重复记录，重复记录只显示一条。

-- sum（列）表示计算该列值的总和
-- 列名as 别名  ，就是给列取了一个外号，在我们的结果显示这个外号。as也可以省略。效果一样
-- group by 列名 表示按照那个列名的值分组，如果该列的值是相同的，就分到一个组。
-- having 必须是在group by后面，表示分组完成以后再加过滤条件（和where相似）
-- <> 表示不等于
-- oeder by 列表示按照该列的值排序，默认情况下是按照升序，也就是从小到大


-- oeder by列desc 表示按照该列的值，降序，从大到小显示
-- oeder by列asc 表示按照该列的值，升序，从小到大显示和不写是一样的效果

-- 新建数据库
-- create database java5

-- 新建表
create table java5.class_details(
id int,
userId int,
is_avaiable varchar(1),
class_date date


);

-- 修改表
-- 增加列
alter table java5.book add printDate date;

-- 修改列的长度从30变成了31
alter table java5.book modify name varchar(31);

-- 修改列的名字change 后面第一个参数是原来的列名，接着跟了新的列名
alter table java5.book change name bookName varchar(31);


15.JDBC
	
	try {
			//加载驱动
			Class.forName("com.mysql.jdbc.Driver");
			
			//获取数据库连接
			Connection con=DriverManager.getConnection("jdbc:mysql://localhost:3306/java5", "root", "");
			
			//获取statement
			PreparedStatement ps=con.prepareStatement("select * from user");
			
			//执行sql以后，获取执行结果
			ResultSet rs=ps.executeQuery();
			
			while(rs.next()){
				int id=rs.getInt(1);
				String name =rs.getString(2);
				
				int id2=rs.getInt("id");
				
				System.out.println("id="+id+"  name="+name+"  id2="+id2);
				
			}
			
			//关闭连接。必须从里到外关。
			//也就是先关resulstset 最后关connection
			if(rs!=null){
				rs.close();
			}
			if(ps!=null){
				ps.close();
			}
			if(con!=null){
				con.close();
			}
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
16.dao controller model

DAO层：DAO层主要是做数据持久层的工作，负责与数据库进行联络的一些任务都封装在此，DAO层的设计首先是设计DAO的接口，然后在Spring的配置文件中定义此接口的实现类，然后就可在模块中调用此接口来进行数据业务的处理，而不用关心此接口的具体实现类是哪个类，显得结构非常清晰，DAO层的数据源配置，以及有关数据库连接的参数都在Spring的配置文件中进行配置。   
  
Service层：Service层主要负责业务模块的逻辑应用设计。同样是首先设计接口，再设计其实现的类，接着再Spring的配置文件中配置其实现的关联。这样我们就可以在应用中调用Service接口来进行业务处理。Service层的业务实现，具体要调用到已定义的DAO层的接口，封装Service层的业务逻辑有利于通用的业务逻辑的独立性和重复利用性，程序显得非常简洁。   
  
Controller层:Controller层负责具体的业务模块流程的控制，在此层里面要调用Serice层的接口来控制业务流程，控制的配置也同样是在Spring的配置文件里面进行，针对具体的业务流程，会有不同的控制器，我们具体的设计过程中可以将流程进行抽象归纳，设计出可以重复利用的子单元流程模块，这样不仅使程序结构变得清晰，也大大减少了代码量。   
  
View层 此层与控制层结合比较紧密，需要二者结合起来协同工发。View层主要负责前台jsp页面的表示，   
  
DAO层，Service层这两个层次都可以单独开发，互相的耦合度很低，完全可以独立进行，这样的一种模式在开发大项目的过程中尤其有优势，Controller，View层因为耦合度比较高，因而要结合在一起开发，但是也可以看作一个整体独立于前两个层进行开发。这样，在层与层之前我们只需要知道接口的定义，调用接口即可完成所需要的逻辑单元应用，一切显得非常清晰简单。   
  
DAO设计的总体规划需要和设计的表，和实现类之间一一对应。   
  
DAO层所定义的接口里的方法都大同小异，这是由我们在DAO层对数据库访问的操作来决定的，对数据库的操作，我们基本要用到的就是新增，更新，删除，查询等方法。因而DAO层里面基本上都应该要涵盖这些方法对应的操作。除此之外，可以定义一些自定义的特殊的对数据库访问的方法。   
  
Service逻辑层设计   
  
Service层是建立在DAO层之上的，建立了DAO层后才可以建立Service层，而Service层又是在Controller层之下的，因而Service层应该既调用DAO层的接口，又要提供接口给Controller层的类来进行调用，它刚好处于一个中间层的位置。每个模型都有一个Service接口，每个接口分别封装各自的业务处理方法。   
  
在DAO层定义的一些方法，在Service层并没有使用，那为什么还要在DAO层进行定义呢？这是由我们定义的需求逻辑所决定的。DAO层的操作 经过抽象后基本上都是通用的，因而我们在定义DAO层的时候可以将相关的方法定义完毕，这样的好处是在对Service进行扩展的时候不需要再对DAO层进行修改，提高了程序的可扩展性。 

17.发邮件
		
		public void sendEmail(String emailAdd,String subject,String body) throws Exception{
	 	System.out.println("send email begin");
	 	HtmlEmail email = new HtmlEmail();
	 	//email.set
        email.setHostName("smtp.163.com"); // 设定smtp服务器
        email.setAuthentication("18623214225@163.com", "WANGjibo1993520"); // 设定smtp服务器的认证资料信息
        email.addTo(emailAdd, "reciever"); // 设定收件人地址（可以添加多个收件人）
        email.setFrom("18623214225@163.com", "自在天原");//发件人+邮箱地址
        email.setSubject(subject);//设定主题
        email.setCharset("UTF-8");
        email.setHtmlMsg("<html>"+body+"</html>");//设定发送内容
        email.send();
        System.out.println("end");}

18.
-----------------黄琛 结束---------------
-----------------------------------金文浩   开始-------------------------------------


java的特点：封装 继承 多态 抽象

有方法的构成
public void返回值   testWay 方法名 (Object a参数){方法主体}       


方法的重载；方法名相同 其他必须有至少一个不同
方法的重写：除了方法主体 其他必须都相同

static方法 静态方法：无法引用非静态被static修饰的成员变量，或者方法，可以在不需要new的情况下，使用。


访问权限修饰符
					public 		protected   无修饰符（默认）	private 
同一个类			可以访问	可以访问	可以访问			可以访问
同一个包中的子类	可以访问	可以访问    可以访问            不可以访问
同一个包中的其他类	可以访问	可以访问    可以访问            不可以访问
不同包中的子类		可以访问	可以访问	不可以访问          不可以访问
不同包中的其他类	可以访问	不可以访问  不可以访问          不可以访问



final 被final修饰的变量，一旦赋值，就不能被改变。



java基本数据类型:
byte,short,int,long,float,double,boolean,char

运算符：
基本：  = ，+ ，- ，* ，/ ，%
自增：++ 自减--
关系运算符：<,>,=,<=,>=
逻辑运算符：&&(与),||（或）,!（非）
赋值运算符：=，+=，-=，*+，/=
例：int a=0;
	a+=1;
	(结果a=1)
条件运算符：  ?:
例： int a=0;
	a=(2>1)?2:1;即括号内为真取左边的值，反之去右边的值
	(结果a=2）
	
其他：
\r \n 换行
	
java几个常用函数；
if:
	if(boolean类型判断){
	}else if(boolean类型判断){
	}else{
	}
	(可以省略 else if () )
	冒泡排序 比较排序
switch case:
	switch (表达式){ 
		case 值1 : 
			主体1 
			break; 
		case 值2 : 
			主体2 
			break; 
		... 
		default : 语句n break; 
	} 
for；
	1:
		for(int temp=0;temp<参数;temp++){}
		
	2： for循环遍历一个数组等类型的对象ls
		for(Object  temp(内存储的类型):ls){}

while:
	whlie(判断){主体} 先判断 再执行
	或
	do{主体}while{判断} 先执行 在判断
	
equals方法，判断引用类型的两个变量是不是相等。
equals默认情况下，也是比较地址 equals方法来自于Object
		
		
另：object.contain(参数); 判断是都存在参数内元素

	String的函数
	.trim() 去空格
	.substring(beginIndex, endIndex);从第几位元素 截取 至
	.indexOf(参数);从头检索 是否存在参数字段 返回检索到字段再字符串中的位置
	.lastIndexOf(str);从尾检索 是都存在参数字段
	返回检索到字段再字符串中的位置

	Math.****;调运Math内置函数
	
软件框架基础
	MVC：
		model(抽象对象，建模)
			使用java类 创建抽象对象
		controller(控制前台传来的数据，并做运算)
			不用框架(springboot) 使用servlet 
		dao(控制与数据库的交互，不做数据操作)
			java类 针对model与数据库交互
		view (视图)
		
		
		dao层
		特殊方法
			public class BaseDao {
					Connection con;
					
					String driver="com.mysql.jdbc.Driver";
					
					String url="jdbc:mysql://数据库ip地址:3306/java5?useUnicode=true&characterEncoding=UTF-8";
					
					String name="用户名";
					
					String pwd="密码";
					
					
					public Connection getConnection(){
						//加载驱动
						try {
							Class.forName(driver);
							//获取数据库连接
							con=DriverManager.getConnection(url, name, pwd);
						} catch (ClassNotFoundException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						} catch (SQLException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						return con;
					}
					
		dao层中方法体构成
			
			1.创建连接(Connection对象)
			2.创建待执行sql语句对象(prepareStatement对象)
			3.执行sql对象
			4.关闭数据库连接
		
页面过滤
 当通过收藏等进入页面取值时,判断是否是能正常进入页面 如果不能进行拦截
	public class indexFillter implements Filter {
	
	@Override
	//进入项目 启动过滤器
	public void init(FilterConfig filterConfig) throws ServletException {
		// TODO Auto-generated method stub
		System.out.println("IndexFillter  ---init");
		Test t= new Test();
		t.diao();
		
	}

	@Override
	//执行过滤操作
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		// TODO Auto-generated method stub
		System.out.println("IndexFillter  ---doFilter");
		HttpServletRequest request1=(HttpServletRequest)request;
		HttpSession session=request1.getSession();
		User u=(User) session.getAttribute("user");
		 String a=request1.getRequestURI();
		 if(a.contains(".css") || a.contains(".js") || a.contains(".png")|| a.contains(".jpg")|| a.contains(".rar")){
				//如果请求中含有 参数内容 则不进行过滤
				chain.doFilter(request, response);
			}
		 else if(!("/UploadServlet".equals(request1.getServletPath()))&&!("/register.jsp".equals(request1.getServletPath()))&&!("/RegisterController".equals(request1.getServletPath()))&&!("/LoginController".equals(request1.getServletPath()))&&u==null){
			//如果叫User的session内(即用户信息为空) 则跳转登陆页面 其中登陆，注册页面不进行拦截
			request.getRequestDispatcher("/wsq/login.jsp").forward(request, response);
			
		}
		else{
			//其他条件符合 不拦截 
			response.setContentType("text/html;charset=utf-8");
			chain.doFilter(request, response);
		}
		
		
	}
	@Override
	public void destroy() {
		// TODO Auto-generated method stub
		System.out.println("IndexFillter  ---destroy");
		
	}

}
	
发送邮件

	架包：
	<dependency>
		<groupId>org.apache.commons</groupId>
		<artifactId>commons-email</artifactId>
		<version>1.4</version>
	</dependency>
	方法：
		public void sendEmail(String emailAdd,String subject,String body) throws Exception{
			System.out.println("send email begin");
			HtmlEmail email = new HtmlEmail();
			//email.set
			email.setHostName("smtp.163.com"); // 设定smtp服务器
			email.setAuthentication("18623214225@163.com", "WANGjibo1993520"); // 设定smtp服务器的认证资料信息
			email.addTo(emailAdd, "reciever"); // 设定收件人地址（可以添加多个收件人）
			email.setFrom("18623214225@163.com", "自在天原");//发件人+邮箱地址
			email.setSubject(subject);//设定主题
			email.setCharset("UTF-8");
			email.setHtmlMsg("<html>"+body+"</html>");//设定发送内容
			email.send();
			System.out.println("end");
	}



	
文件读取
	架包：
		<dependency>
				<groupId>commons-io</groupId>
				<artifactId>commons-io</artifactId>
				<version>2.4</version>
			</dependency>
	
	方法:
		字节流读取
		public static void read1(){
			File f=new File("C:"+File.separator+"bsea"+File.separator+"tmp"+File.separator+"test.txt");
			try {
				FileInputStream fs=new FileInputStream(f);
				byte[] b=new byte[(int)f.length()];
				fs.read(b);
				fs.close();
				System.out.println(new String(b));
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		字符流读取
		public static void read2(){
			File f=new File("C:"+File.separator+"bsea"+File.separator+"tmp"+File.separator+"test.txt");
			try {
				Reader r=new FileReader(f);
				char[] c=new char[(int)f.length()];
				r.read(c);
				r.close();
				System.out.println(new String(c));
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
		按行读取
		public static void read3(){
			File f=new File("C:"+File.separator+"bsea"+File.separator+"tmp"+File.separator+"test2.txt");
			try {
				FileInputStream fi=new FileInputStream(f);
				Reader r=new InputStreamReader(fi,"utf-8");
				BufferedReader br=new BufferedReader(r);
				while(br.ready()){
				String str=br.readLine();
				System.out.println(str);
				}
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
		doc/docx 读取
				架包：
				<dependency>
					<groupId>org.apache.poi</groupId>
					<artifactId>poi</artifactId>
					<version>3.9</version>
				</dependency>
				方法：
				public static StringBuffer readWord(String path) {
					String s = "";
					try {
						if(path.endsWith(".doc")) {
							InputStream is = new FileInputStream(new File(path));
							WordExtractor ex = new WordExtractor(is);
							s = ex.getText();
						}else if (path.endsWith("docx")) {
							OPCPackage opcPackage = POIXMLDocument.openPackage(path);
							POIXMLTextExtractor extractor = new XWPFWordExtractor(opcPackage);
							s = extractor.getText();
							// System.out.println("内容=="+s);
						}else {
							System.out.println("传入的word文件不正确:"+path);
						}
				
					} catch (Exception e) {
						e.printStackTrace();
					}
					StringBuffer bf = new StringBuffer(s);
					return bf;
				}
文件写入
	架包：
		<dependency>
				<groupId>commons-io</groupId>
				<artifactId>commons-io</artifactId>
				<version>2.4</version>
			</dependency>
	
	方法:
		/**
		* 字节流读取文件内容。
		* 
		* FileOutputStream
		* 
		* 
		*/
		public static void write1(){
			File f=new File("C:"+File.separator+"bsea"+File.separator+"tmp"+File.separator+"test.txt");
			try {
				FileOutputStream fs=new FileOutputStream(f);
				//  \r\n 会换行
				String str="hello java5 haha\r\nha1";
				byte[] b=str.getBytes();
				fs.write(b);
				fs.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		
		/**
		* 字符流，写入内容
		* Writer
		
		*/
		public static void write3(){
			File f=new File("C:"+File.separator+"bsea"+File.separator+"tmp"+File.separator+"test.txt");
			try {
				//如果第二个参数是true，就不会覆盖，新内容会在后面追加。
				Writer w=new FileWriter(f,true);
				String str="2017";
				w.write(str);
				w.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
		
		
Excel操作
	架包：
		<dependency>
			<groupId>org.apache.poi</groupId>
			<artifactId>poi-ooxml</artifactId>
			<version>3.9</version>
		</dependency>
	创建Excel方法
		/**
		* 只支持创建string类型的单元格（看情况更改方法）
		* @param wb
		* @param row
		* @param col  应该是你的cell单元格的位置也就是列号；
		* @param align 应该是你的对齐方式；
		* @param val 应该是你单元格里面要添加的值；
		*/
			private static void  cteateCell(HSSFWorkbook wb,HSSFRow row,int col,short align,String val){
				HSSFCell cell = row.createCell(col);
				//cell.setEncoding(HSSFCell.ENCODING_UTF_16);
				
				cell.setCellValue(val);
				HSSFCellStyle cellstyle = wb.createCellStyle();
				cellstyle.setAlignment(align);
				cell.setCellStyle(cellstyle);
			}
			
			HSSFWorkbook wb=new HSSFWorkbook();
			//创建一个excel 对象
			
			HSSFSheet s1=wb.createSheet();
			//创建excel中的一个sheet
			
			HSSFRow r4=s1.createRow(4);
			//创建sheet中的一行
			
			cteateCell(wb,r4,3,HSSFCellStyle.ALIGN_CENTER,"测试封装方法");
			//调用方法
			
			FileOutputStream out=new FileOutputStream("C:\\tmp\\t.xls");
			wb.write(out);
			out.close();
			//写入内容
			
	其他：
		冻结一行
			HSSFWorkbook wb=new HSSFWorkbook();
		
			HSSFSheet s1=wb.createSheet();
			
			
			//2表示冻结两列， 3表示冻结3行  ,如果要冻结第一行，（0，1）
			s1.createFreezePane(2, 3);
	
	
	
	
	
	
页面
1.
	<html> 与 </html> 之间的文本描述网页
	<head></head> 之间，设置编码和标题，
	也可以引用外部js,css等。
	<body> 与 </body> 之间的文本是可见的页面内容
	例子：
<a href="http://www.baidu.com" target="_blank">百度_blank</a><br>
	
	1. href
	URL
	规定链接指向的页面的 URL。
	
	
	2. target
	_blank
	_parent
	_self
	_top
	framename
	规定在何处打开链接文档
	
2.
	链接到同一个页面的不同位置
	本例演示如何使用链接跳转至文档的另一个部分
	
	Step 1. 在chapter 4的位置，通过name属性放一个锚
	<h2><a name="C4">Chapter 4</a></h2>
	
	Step 2.
	通过href属性去设置step1中设置好的锚
	<a href="#C4">查看 Chapter 4。</a> 
3.
	定义和用法
	disabled 属性规定禁用按钮。
	被禁用的按钮既不可用，也不可点击。
	可以设置 disabled 属性，直到满足某些条件
	（比如选择一个复选框），
	才恢复用户对该按钮的使用。
	然后，可以使用 JavaScript 
	来清除 disabled 属性，以使文本区变为可用状态。
4.
	div 和span的区别。
 
	都是表示一个语句块。
	
	但是div会自动在结束标签后面换行。
	span是不会自动换行的。
5.单选与多选
	多选： <input name="tec" value="1" type="checkbox">

	单选： java<input name="like" value="1" type="radio">
	
	
	所有的，多选和单选，必须设置
	name,value,的属性值
6. .html/.htm 
	首先来看 .htm 和 .html 文件的区别。答案居然是：
它们是相同的。
   事实上，这只是个人喜好问题，保持统一的后缀名即可。
   习惯上，windows 通常会用 .htm 的后缀名，
   而 linux（unix） 会用 .html 后缀
   （在 linux 中，如果打开 .htm 的文件，会直接展示源码）。这是因为很久以前，操作系统（DOS）的平台是 window 3.x.x，系统对于文件有个 8.3 约束（8.3 naming convention），即文件名只能是 8 个字符，后缀只能是 3 个字符，所以当时显然无法使用 .html 后缀。而现在，这些问题都已经不复存在了。（个人感觉 .html 更正规一点，
   一些人认为使用 .htm 是回到了 dos 时代）
 
7.iframe
	标签定义及使用说明
	<iframe> 标签规定一个内联框架。
	一个内联框架被用来在当前 HTML 文档中嵌入另一个文档。
	
	提示和注释
	提示：您可以把需要的文本放置在 <iframe> 和 </iframe> 之间，这样就可以应对不支持 <iframe> 的浏览器。
	提示：使用 CSS 为 <iframe> （包括滚动条）定义样式。
8.上传文件选择
	文件上传要点：
	1.必须是form         
	2.必须添加enctype="multipart/form-data 
	3.必须是post 
	4.有一个input的type是file
		前端页面必须给input标签 name,否则后台不读取标签)
		<input type="file" name="fileupload"/>  
		
		
		后台代码
		DiskFileItemFactory factory=newDiskFileItemFactory();
		ServletFileUpload upload=new ServletFileUpload(factory);
		upload.setHeaderEncoding("utf-8");
		try {
			List<FileItem> ls= upload.parseRequest(request);
			for(FileItem f:ls){
				f.isFormField();(是否是文件地址)
				String fieldName=f.getFieldName();
				(获取文件的整个地址)
				String fileName=fileName.substring(fileName.lastIndexOf("\\"));
				(从最后一个 \ 截取 即文件名)
			}
	
9.json 传值（调回值失败）

	json向后台传值 及 前台接受json
		前台代码
		   //用这种方式组装的json数组是正确的  
          var testArray = new Array();
          for(var i = 0; i < 5; i++) {
              var tempArray = new Array();  
              for(var j = 0; j < 10; j++){
                tempArray.push(i*j);
              }
              testArray.push(tempArray);
          }
          alert("tempArray=" + tempArray);
          
          //用这种方式组装的json数组是正确的
          var jsonArr = new Array();             
          for ( var j = 0; j < 3; j++) {                 
            var jsonObj = {};                  
            jsonObj["gradeId"]=1;                 
            jsonObj["gradeName"]=2;                 
            jsonObj["level"]=3;                 
            jsonObj["boundary"]=4;                 
            jsonObj["status"]=5;                 
            jsonArr.push(jsonObj)             
          }
          alert("jsonArr=" + jsonArr);            
          
          // 用这种方式组装的json数组是错误的，接收侧只能接收到整个的这个字符串            
          var employees='[';
          for ( var j = 0; j < 5; j++) {                          
            employees+= '{';                             
            employees+="name:";                            
            employees+="zhang";
            employees+=",";
            employees+="old:";                            
            employees+=30 + j;
            employees +='}';
            if(j!=4){                             
                employees+=','
            };                         
          } 
          employees+=']';
          alert("employees=" + employees);
          
           $.ajax({
                type:"POST", //请求方式  
                url:"./TestJson", //请求路径  
                cache: false,     
                data:{//传参  
                    "name":"zhang3",
                    "testArray":testArray,    
                    "students": //用这种方式传递多维数组也是正确的
                        [
                            {"name":"jackson","age":100},
                            {"name":"michael","age":51}
                        ],
                    "employees":employees,
                    "jsonArr":jsonArr    
                },
                dataType: 'json',   //返回值类型  
                success:function(json){        
                    alert(json[0].username+" " + json[0].password);    //弹出返回过来的List对象  
                    alert(json[1].username+" " + json[1].password);    //弹出返回过来的List对象
                }  
            });  
			
		后台代码
				protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		 response.setContentType("text/html");
	        System.out.println("into doPost");
	        
	        //用map来接收request发送过来的多维数组
	        Map map = request.getParameterMap();
	        Iterator<String> iter = map.keySet().iterator();
	        while (iter.hasNext()) {
	            String key = iter.next();
	            System.out.println("key=" + key );
	            String[] value =  (String[]) map.get(key);
	            System.out.print("value=");
	            for(String v:value){                
	                System.out.print(v + "  ");
	            }            
	            System.out.println();
	        }        
	        
	        String sName= request.getParameter("name");//得到ajax传递过来的paramater  
	        System.out.println(sName);

	        PrintWriter printWriter = response.getWriter();  
	        List list = new ArrayList();//传递List  
	        
	        Map m=new HashMap();//传递Map      
	        User u1=new User();  
	       u1.setName("n1");
	       u1.setPwd("123");
	        User u2=new User();  
	        u2.setName("n2");
		       u2.setPwd("1235555");   
	        list.add(u1); //添加User对象        
	        list.add(u2);     //添加User对象    
	        
	        m.put("u1", u1);  
	        m.put("u2", u2);     
	          
	       // JSONArray jsonArray2 = JSONArray.fromObject( list );  
	      // 把java数组转化成转化成json对象   
	        JSONObject jsonObject =JSONObject.fromObject(m);//转化Map对象  
	        String str=jsonObject.toString();
	      // printWriter.print(jsonArray2);//返给ajax请求  
	        printWriter.print(str);//返给ajax请求  
	        printWriter.flush();
	        printWriter.close();1155
	        System.out.println("finish");
	}

	
10.使用jquery
	$表示调用jquery
	其他看 jquery中文教程 使用

11.分页
	前端页面
	<div class="col-sm-6">
			<ul class="pagination">
			<!--  currentPage是从后台传过来的，表示当前用户看到的页码-->
				<%if(currentPage!=1){%>
				<li><a href="/zz_java5/UserProjectController?page=<%=currentPage-1%>">&laquo;</a></li>
				<%}
				//count 用来标记， 当前显示了多少页（目前我们设计是，一次最多显示5页）
				int count=0;
				//totalPages 从后台传过来的，表示整个数据，经过计算以后，总的页数。
				for(int i=1;i<totalPages+1;i++){
					//这个表示，需要显示页码的起始位置。
					int startPage=currentPage-2;
					//这个表示，需要显示页码的末尾位置。
					int endPage=currentPage+2;
					if((startPage<3&&count<5)||(startPage<=i&&i<=endPage&&count<5)){
						count++;
						if(currentPage==i){
							%>	
							<li class="active"><a href="/zz_java5/UserProjectController?page=<%=i%>"><%=i %></a></li>
							
						<%		
						}else{
							%>	
							<li><a href="/zz_java5/UserProjectController?page=<%=i%>"><%=i %></a></li>
							
							
						<%	
						}
					}
				}
				
				
				if(currentPage!=totalPages){
				%>
				
				<li><a href="/zz_java5/UserProjectController?page=<%=currentPage+1%>">&raquo;</a></li>
				<%} %>
			</ul>
		</div>
	后台代码
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String curentPage=request.getParameter("page");
		System.out.println("当前页="+curentPage);
		int curpage=1;
		if(curentPage!=null){
			//把String 转换成 int
			curpage=Integer.parseInt(curentPage);
		}
		UserDao udao=new UserDao();
		//获取页的记录
		 int startIndex=(curpage-1)*4;
		List<User> ls=udao.getPageRecords(startIndex, 4);
		request.setAttribute("records", ls);
		//获取总的页数
		int totalPages2=udao.getTotalRecords()%4>0?(udao.getTotalRecords()/4)+1:udao.getTotalRecords()/4;
		System.out.println("totalPages2--"+totalPages2);
		request.setAttribute("curpage", curpage);
		request.setAttribute("pageConut", totalPages2);
		request.getRequestDispatcher("/bsea/porjectUserList.jsp").forward(request, response);
	}

	
	
	
创建码云项目
cd wp/git

1.git clone https://git.oschina.net/bseaworkspace/zz_java3.git

2. copy file to wp/git
 
 cd wp/git/项目名
3. git add .
git config --global user.email "yinyouhai@aliyun.com"
git config --global user.name "bsea"

4. git commit -m'setup'

5.git push

文件中路径的拼写

1.绝对路径是“/”开头。
2.html-->最前面的 /代表： http://localhost:8080
3.web.xml--》最前面的 /代表： http://localhost:8080/zz_java3
4.webapp目录下面默认是： http://localhost:8080/zz_java3

文件,方法的命名:

网页：首字母小写 紧接着的有意义的单词大写
model层：首字母大写 ~
dao层：根据他针对的model层命名 首字母大写
control层：根据前端的不同需求命名 首字母大写
例：网页--->login.html (用户为User类)
	model-->User
	dao-->UserDao
	control--》LoginController
	
方法的命名：
model层--->使用内置的快捷方式
dao层--->根据对数据库操作的不同来命名
查找单条数据：select 例：selectUserByName()
查找多条数据：list 例：listUserByName()
插入数据：add 例：addUser()
删除数据：delete 例：deleteUserByName()
control层-->不能有方法

sql语句
。。。。。txt
	

-----------------------------------金文浩   结束-------------------------------------

------------------------------------江恩泽java基础总结开始-------------------------------------------
基本数据类型：byte、short、int、long、float、double、char、boolean 

引用类型：类class、 接口interface、 数组array ;

类class:Object(Objectk可以定义所有的类，再通过强制转换成其他类)、String(表示字符串)、void、Date;
接口interface:List<>、Map

自动类型转换：从低级别到高级别，系统自动转的；

强制类型转换：什么情况下使用?把一个高级别的数赋给一个别该数的级别低的变量；


访问权限修饰符
					public 		protected   	无修饰符（默认）	private 
同一个类			   	可以访问	             可以访问	  	可以访问			可以访问
同一个包中的子类	   	可以访问	             可以访问        	可以访问                   不可以访问
同一个包中的其他类	可以访问	  	    可以访问       	可以访问                   不可以访问
不同包中的子类		可以访问		    可以访问		不可以访问          	不可以访问
不同包中的其他类		可以访问		    不可以访问  		不可以访问         	不可以访问

面向对象：
封 装（面向对象特征之一）：是指隐藏对象的属性和实现细节，仅对外提供公共访问方式。
好处：将变化隔离；便于使用；提高重用性；安全性。 
封装原则：将不需要对外提供的内容都隐藏起来，把属性都隐藏，提供公共方法对其访问。

重载的定义是：在一个类中，如果出现了两个或者两个以上的同名函数，只要它们的参数的个数，或者参数的类型不同，即可称之为该函数重载了。
 (方法名相同，形参不同)

继承：子类继承父类的特征和行为，使得子类具有父类的各种属性和方法。或子类从父类继承方法，使得子类具有父类相同的行为。

特点：在继承关系中，父类更通用、子类更具体。父类具有更一般的特征和行为，而子类除了具有父类的特征和行为，还具有一些自己特殊的特征和行为。
使用继承可以有效实现代码复用，避免重复代码的出现。实现继承：B extends A。

重写：父类与子类之间的多态性，对父类的函数进行重新定义。如果在子类中定义某方法与其父类有相同的名称和参数和返回类型，我们说该方法被重写 (Overriding)。重写将父类的同名方法覆盖。

抽象：将一个事物提取出来，用代码表示。把事物称作类，将事物的属性抽象出来叫做成员变量，将事物的行为抽象出来叫做方法。

普通的成员变量，必须要先new出对象，才能访问对象的属性，被static修饰的变量，叫静态变量，可以直接通过类的名字点出来使用，不需要new
被final修饰的变量，不能再赋值

成员变量和局部变量的区别： 
1：成员变量直接定义在类中。 
局部变量定义在方法中，参数上，语句中。 
2：成员变量在这个类中有效。 
局部变量只在自己所属的大括号内有效，大括号结束，局部变量失去作用域。 
3：成员变量存在于堆内存中，随着对象的产生而存在，消失而消失。
 
局部变量存在于栈内存中，随着所属区域的运行而存在，结束而释放。

多态：(必要条件要有继承和重写)
通过继承实现了，同一个方法，在运行的时候 根据输入参数是不同的子类，产生不一样的效果。


for循环(常用来计数)：
for(int i=0;i<a.length;i++){
}

for(float x:f){
System.out.println(x);
}
f是一个数组,x是被定义为float类型的变量，将每一个f的元素赋值给x


冒泡排序：从大到小排序
		 * 1.两个for循环
		 * 2.第一个for循环的初始值是0，第二个for循环的初始值是i+1
		 * 3.只有i位置的值比j位置的值小的时候，才交换位置。
   
选择排序：从所有的元素中选择一个最小的元素，放在0号位置。
		比较排序和冒泡排序，比较次数是一样的，但是效率，比较排序比较高，因为交换次数少（不是绝对的，比如  数组本来顺序就是对的）
		冒泡排序交换次数是0，但是比较排序，会是2.






==和equals区别？
对于基本类型来说，没有equals方法，通过==比较的是值；

对引用类型来说，默认情况下，==和equals是一样的，比较的是地址的值。在equals方法被重写的情况下,equals可以不比较地址，而是比较值，当然这个需要在重写的equals方法里面自己去实现。String是一个特殊的类，在java源码中，就已经重写了equals了方法，所以String方法可以直接比较值。


数据库：
增删改查；

选择：select * from table1 where 范围
插入：insert into table1(field1,field2) values(value1,value2)
删除：delete from table1 where 范围
更新：update table1 set field1=value1 where 范围
查找：select * from table1 where field1 like ’%value1%’ ---
 SELECT * FROM [user] WHERE u_name LIKE '%三%' (把名字中带有三的都查出来)
 SELECT * FROM [user] WHERE u_name LIKE '_三_' (把名字中间为三的都查出来)

排序：select * from table1 order by field1,field2 [desc]
总数：select count as totalcount from table1
求和：select sum(field1) as sumvalue from table1
平均：select avg(field1) as avgvalue from table1
最大：select max(field1) as maxvalue from table1
最小：select min(field1) as minvalue from table1

-- 新建数据库。

-- create database java5

-- 新建表

create table java5.class_details(
id int,
userId int,
is_avaiable varchar(1),
class_date date
);

-- 修改表
-- 增加列
alter table java5.book add  printDate date;

-- 修改列的长度，从30该成了31
alter table java5.book modify name varchar(31);

-- 修改列的名字   change 后面第一个参数是原来的列名，接着跟新的列名
alter table java5.book change name bookName varchar(31);


联表查询：INNER JOIN（内连接）
SELECT Students.ID,Students.Name,Majors.Name AS MajorName
FROM Students INNER JOIN Majors
ON Students.MajorID = Majors.ID

外连接
SELECT Students.ID,Students.Name,Majors.Name AS MajorName
FROM Students LEFT JOIN Majors
ON Students.MajorID = Majors.ID

HTML基本知识

<!-- 
<html> 与 </html> 之间的文本描述网页
<head></head> 之间，设置编码和标题，
也可以引用外部js,css等。
<body> 与 </body> 之间的文本是可见的页面内容

 -->
 
 <!-- 
1. href
URL
规定链接指向的页面的 URL。


2. target
_blank
_parent
_self
_top
framename
规定在何处打开链接文档

 -->
 
 <!--  

链接到同一个页面的不同位置
本例演示如何使用链接跳转至文档的另一个部分

Step 1. 在chapter 4的位置，通过name属性放一个锚
<h2><a name="C4">Chapter 4</a></h2>

Step 2.
通过href属性去设置step1中设置好的锚
<a href="#C4">查看 Chapter 4。</a> 

-->

<!-- 

定义和用法
disabled 属性规定禁用按钮。
被禁用的按钮既不可用，也不可点击。
可以设置 disabled 属性，直到满足某些条件
（比如选择一个复选框），
才恢复用户对该按钮的使用。
然后，可以使用 JavaScript 
来清除 disabled 属性，以使文本区变为可用状态。

 -->
 
 <!-- 
 
 div 和span的区别。
 
 都是表示一个语句块。
 
 但是div会自动在结束标签后面换行。
 span是不会自动换行的。
 
  -->
 
<!-- 
HTML 表单
表单是一个包含表单元素的区域。
表单元素是允许用户在表单中输入内容,比如：
文本域(textarea)、下拉列表、
单选框(radio-buttons)、
复选框(checkboxes)等等。
所有的，多选和单选，必须设置
name,value,的属性值
表单使用表单标签 <form> 来设置:
 -->
 
 HTML和HTM的区别
<!-- 
首先来看 .htm 和 .html 文件的区别。答案居然是：
它们是相同的。
   事实上，这只是个人喜好问题，保持统一的后缀名即可。
   习惯上，windows 通常会用 .htm 的后缀名，
   而 linux（unix） 会用 .html 后缀
   （在 linux 中，如果打开 .htm 的文件，会直接展示源码）。这是因为很久以前，操作系统（DOS）的平台是 window 3.x.x，系统对于文件有个 8.3 约束（8.3 naming convention），即文件名只能是 8 个字符，后缀只能是 3 个字符，所以当时显然无法使用 .html 后缀。而现在，这些问题都已经不复存在了。（个人感觉 .html 更正规一点，
   一些人认为使用 .htm 是回到了 dos 时代）
   
   
   标签定义及使用说明
<iframe> 标签规定一个内联框架。
一个内联框架被用来在当前 HTML 文档中嵌入另一个文档。

提示和注释
提示：您可以把需要的文本放置在 <iframe> 和 </iframe> 之间，这样就可以应对不支持 <iframe> 的浏览器。
提示：使用 CSS 为 <iframe> （包括滚动条）定义样式。
 --> 


bootstrap前端页面制作

javascript触发一些想要的功能(ajax传值)

JDBC连接数据库

try {
			//加载驱动
			Class.forName("com.mysql.jdbc.Driver");
			
			//获取数据库连接
			Connection con=DriverManager.getConnection("jdbc:mysql://localhost:3306/java5", "root", "");
			
			//获取statement
			PreparedStatement ps=con.prepareStatement("select * from user");
			
			//执行sql以后，获取执行结果
			ResultSet rs=ps.executeQuery();
			
			while(rs.next()){
				int id=rs.getInt(1);
				String name =rs.getString(2);
				
				int id2=rs.getInt("id");
				
				System.out.println("id="+id+"  name="+name+"  id2="+id2);
				
			}
			
			//关闭连接。必须从里到外关。
			//也就是先关resulstset 最后关connection
			if(rs!=null){
				rs.close();
			}
			if(ps!=null){
				ps.close();
			}
			if(con!=null){
				con.close();
			}
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

运算符：

	运算符	描述	例子
==	检查如果两个操作数的值是否相等，如果相等则条件为真。	（A == B）为假(非真)。
!=	检查如果两个操作数的值是否相等，如果值不相等则条件为真。	(A != B) 为真。
> 	检查左操作数的值是否大于右操作数的值，如果是那么条件为真。	（A> B）非真。
< 	检查左操作数的值是否小于右操作数的值，如果是那么条件为真。	（A <B）为真。
> =	检查左操作数的值是否大于或等于右操作数的值，如果是那么条件为真。	（A> = B）为假。
<=	检查左操作数的值是否小于或等于右操作数的值，如果是那么条件为真。	（A <= B）为真。

	下表列出了逻辑运算符的基本运算，假设布尔变量A为真，变量B为假
操作符	描述	例子
&&	称为逻辑与运算符。当且仅当两个操作数都为真，条件才为真。	（A && B）为假。
| |	称为逻辑或操作符。如果任何两个操作数任何一个为真，条件为真。	（A | | B）为真。
！	称为逻辑非运算符。用来反转操作数的逻辑状态。如果条件为true，则逻辑非运算符将得到false。	！（A && B）为真。
D
操作符	描述	例子
=	简单的赋值运算符，将右操作数的值赋给左侧操作数	C = A + B将把A + B得到的值赋给C
+ =	加和赋值操作符，它把左操作数和右操作数相加赋值给左操作数	C + = A等价于C = C + A
- =	减和赋值操作符，它把左操作数和右操作数相减赋值给左操作数	C - = A等价于C = C -
 A
* =	乘和赋值操作符，它把左操作数和右操作数相乘赋值给左操作数	C * = A等价于C = C * A
/ =	除和赋值操作符，它把左操作数和右操作数相除赋值给左操作数	C / = A等价于C = C / A
（％）=	取模和赋值操作符，它把左操作数和右操作数取模后赋值给左操作数	C％= A等价于C = C％A
<< =	左移位赋值运算符	C << = 2等价于C = C << 2
>> =	右移位赋值运算符	C >> = 2等价于C = C >> 2
＆=	按位与赋值运算符	C＆= 2等价于C = C＆2
^ =	按位异或赋值操作符	C ^ = 2等价于C = C ^ 2
| =	按位或赋值操作符	C | = 2等价于C = C | 2


Map

Map map=new HashMap();
		//赋值
		map.put("name", "value");
		//取值
		map.get("name");
		
		//泛型： 提前规定，这个map的key必须是String 类型， value是Car类型
		Map<String,Car> map1=new HashMap();
		Car c1=new Car();
		map1.put("奔驰", c1);
		Car c2=new Car();
		map1.put("宝马", c2);
		Car c3=new Car();
		map1.put("奥迪", c3);
		
		//遍历map
		/**
		 * 总结
如果仅需要键(keys)或值(values)使用方法二。
如果你使用的语言版本低于java 5，
或是打算在遍历时删除entries，必须使用方法三。
否则使用方法一(键值都要)。
		 * 
		 * 
		 * 
		 */
		
		//第一种，也是最常用的遍历方法
		for(Map.Entry<String, Car> e:map1.entrySet()){
			  String key=e.getKey();
			  Car c=e.getValue();
			  
			  System.out.println("遍历map方法一 通过entrySet--"+key);
			
		}
		
		
		//第二种。
		
		for(String str:map1.keySet()){
			 System.out.println("遍历map方法二 通过keySet-key-"+str);
			
		}
		
		for(Car c6:map1.values()){
			System.out.println("遍历map方法二 通过keySet-value-"+c6);
			
		}
		
		//第三种
		Iterator<Map.Entry<String, Car>> entries=map1.entrySet().iterator();
		
		while(entries.hasNext()){
			
			Map.Entry<String, Car> e1=entries.next();
			  System.out.println("遍历map方法三 通过entrySet--"+e1.getKey());
		}
		
		//第四种(尽量避免使用)
		/**
		 * 
作为方法一的替代，这个代码看上去更加干净；但实际上它相当慢且无效率。
因为从键取值是耗时的操作（与方法一相比，在不同的Map实现中该方法慢了20%~200%）。
如果你安装了FindBugs，它会做出检查并警告你关于哪些是低效率的遍历。所以尽量避免使用。
		 * 
		 * 
		 */
		
		
		for(String str:map1.keySet()){
			 System.out.println("遍历map方法四 通过keySet-key-"+str);
			 
			 Car value=map1.get(str);
			
		}




绝对路径是“/”开头。
html-->最前面的 /代表： http://localhost:8080
web.xml--》最前面的 /代表： http://localhost:8080/zz_java3
webapp目录下面默认是： http://localhost:8080/zz_java3

servelt中， / 包含了项目的名字。
jsp中， / 没有包含项目名字。




用代码发送一封电子邮箱

public static void main(String[] args) {
		SendMessage sm = new SendMessage();
		try {
			sm.sendEmail("765340527@qq.com","java五班邮件测试","123456789");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void sendEmail(String emailAdd,String subject,String body) throws Exception{
	 	System.out.println("send email begin");
	 	HtmlEmail email = new HtmlEmail();
	 	//email.set
        email.setHostName("smtp.163.com"); // 设定smtp服务器
        email.setAuthentication("18623214225@163.com", "WANGjibo1993520"); // 设定smtp服务器的认证资料信息
        email.addTo(emailAdd, "reciever"); // 设定收件人地址（可以添加多个收件人）
        email.setFrom("18623214225@163.com", "自在天原");//发件人+邮箱地址
        email.setSubject(subject);//设定主题
        email.setCharset("UTF-8");
        email.setHtmlMsg("<html>"+body+"</html>");//设定发送内容
        email.send();
        System.out.println("end");
 }

创建一个excel
/**
	 * 只支持创建string类型的单元格
	 * @param wb
	 * @param row
	 * @param col  应该是你的cell单元格的位置也就是列号；
	 * @param align 应该是你的对齐方式；
	 * @param val 应该是你单元格里面要添加的值；
	 */
	 
	 	HSSFWorkbook wb = new HSSFWorkbook();    //建立新HSSFWorkbook对象
		HSSFSheet sheet = wb.createSheet("成绩");  //建立新的sheet对象
		HSSFRow row = sheet.createRow((short)0);
		//在sheet里创建一行，参数为行号（第一行，此处可想象成数组）
		HSSFCell cell = row.createCell((short)0);      
		//在row里建立新cell（单元格），参数为列号（第一列）
		cell.setCellValue("1"); 
		//cell.set
		//设置cell的整数类型的值
		row.createCell((short)1).setCellValue(1.2);     //设置cell浮点类型的值
		row.createCell((short)2).setCellValue("test");   //设置cell字符类型的值
		row.createCell((short)3).setCellValue(true);    //设置cell布尔类型的值 
		HSSFCellStyle cellStyle = wb.createCellStyle(); //建立新的cell样式
		cellStyle.setDataFormat(HSSFDataFormat. getBuiltinFormat("m/d/yy h:mm"));
		//设置cell样式为定制的日期格式
		HSSFCell dCell =row.createCell((short)4);
		dCell.setCellValue(new Date());            //设置cell为日期类型的值
		dCell.setCellStyle(cellStyle);              //设置该cell日期的显示格式
		HSSFCell csCell =row.createCell((short)5);
		//csCell.setEncoding(HSSFCell.ENCODING_UTF_16);
		//设置cell编码解决中文高位字节截断
		csCell.setCellValue("中文测试_Chinese Words Test");  //设置中西文结合字符串
		row.createCell((short)6).setCellType(HSSFCell.CELL_TYPE_ERROR);
		//建立错误cell
		
		//  开始新建第一行
		HSSFRow row1 = sheet.createRow((short)1);
		row1.createCell((short)0).setCellValue("姓名"); 
		row1.createCell((short)1).setCellValue("学号"); 
		row1.createCell((short)3).setCellValue("成绩"); 
		//  开始新建第二行
		HSSFRow row2 = sheet.createRow((short)2);
		row2.createCell((short)0).setCellValue("小明"); 
		row2.createCell((short)1).setCellValue("1"); 
		row2.createCell((short)3).setCellValue("98"); 
		
		try {
			FileOutputStream fileOut = new FileOutputStream("c:\\tmp\\workbook.xls");
			wb.write(fileOut);
			fileOut.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

分页

<ul class="pagination">
			<!--  currentPage是从后台传过来的，表示当前用户看到的页码-->
				<%if(currentPage!=1){%>
				<li><a href="/zz_java5/UserProjectController?page=<%=currentPage-1%>">&laquo;</a></li>
				<%}
				//count 用来标记， 当前显示了多少页（目前我们设计是，一次最多显示5页）
				int count=0;
				//totalPages 从后台传过来的，表示整个数据，经过计算以后，总的页数。
				for(int i=1;i<totalPages+1;i++){
					//这个表示，需要显示页码的起始位置。
					int startPage=currentPage-2;
					//这个表示，需要显示页码的末尾位置。
					int endPage=currentPage+2;
					if((startPage<3&&count<5)||(startPage<=i&&i<=endPage&&count<5)){
						count++;
						if(currentPage==i){
							%>	
							<li class="active"><a href="/zz_java5/UserProjectController?page=<%=i%>"><%=i %></a></li>
							
						<%		
						}else{
							%>	
							<li><a href="/zz_java5/UserProjectController?page=<%=i%>"><%=i %></a></li>
							
							
						<%	
						}
					}
				}
				
				
				if(currentPage!=totalPages){
				%>
				
				<li><a href="/zz_java5/UserProjectController?page=<%=currentPage+1%>">&raquo;</a></li>
				<%} %>
			</ul>
------------------------------------江恩泽java基础总结结束-------------------------------------------
-----------------------------------毛屹嘉 开始-------------------------------------
1.java八大基本数据类型

	byte,short,int, long,float,double,char,boolean

2.运算符

	算数运算符：+(加)  -(减)  *(乘)  /(除)  %(取余)  ++(自增)  --(自减)
	关系运算符：==  !=  >  >=  <  <=
	逻辑运算符：&&  ||  !  ^  &  |
	条件运算符：a?b:c
		例： int a=0;
		a=(2>1)?2:1;即括号内为真取左边的值，反之去右边的值
		(结果a=2)
	其他：\r \n 换行

3.方法的构成

	访问权限修饰符 返回值类型(void表示没有返回值) 方法名 (参数类型 参数名,参数类型 参数名){方法体}
	

4.重载与重写

	方法的重载；同一个类里，方法名相同，参数数量，位置，参数类型至少有一个不同
	方法的重写：子类对父类方法的重写，方法的方法名和参数都完全相同，方法体不同

5.访问权限修饰符

					public  		protected 	无修饰符（默认）	private 
	同一个类          		可以访问		可以访问  		可以访问			可以访问
	同一个包中的子类  	可以访问		可以访问  		可以访问			不可以访问
	同一个包中的其他类	可以访问		可以访问  		可以访问           		不可以访问
	不同包中的子类    		可以访问		可以访问  		不可以访问          		不可以访问
	不同包中的其他类  	可以访问		不可以访问		不可以访问          		不可以访问

6.面向对象

	面向对象的三大特点：封装，继承，多态

7.成员变量和局部变量的区别：

	成员变量直接定义在类中。 
	局部变量定义在方法中，参数上，语句中。 
	成员变量在这个类中有效。 
	局部变量只在自己所属的大括号内有效，大括号结束，局部变量失去作用域。 
	成员变量存在于堆内存中，随着对象的产生而存在，消失而消失。 
	局部变量存在于栈内存中，随着所属区域的运行而存在，结束而释放。 
8.构造函数

	构造器必须与类同名（如果一个源文件中有多个类，那么构造器必须与公共类同名）
	每个类可以有一个以上的构造器
	构造器可以有0个、1个或1个以上的参数
	构造器没有返回值
	构造器总是伴随着new操作一起调用
9.排序
	
	冒泡排序
	
	比较相邻的元素。如果第一个比第二个大，就交换他们两个。
	对每一对相邻元素作同样的工作，从开始第一对到结尾的最后一对。在这一点，最后的元素应该会是最大的数。
	针对所有的元素重复以上的步骤，除了最后一个。
	持续每次对越来越少的元素重复上面的步骤，直到没有任何一对数字需要比较。
	
	选择排序
	
	选择排序：比如在一个长度为N的无序数组中，在第一趟遍历N个数据，找出其中最小的数值与第一个元素交换，
	第二趟遍历剩下的N-1个数据，找出其中最小的数值与第二个元素交换......
	第N-1趟遍历剩下的2个数据，找出其中最小的数值与第N-1个元素交换，至此选择排序完成。
10.if-else

	1)、if后的括号不能省略，括号里表达式的值最终必须返回的是布尔值 
	2)、如果条件体内只有一条语句需要执行，那么if后面的大括号可以省略，但这是一种极为不好的编程习惯。 
	3)、对于给定的if，else语句是可选的，else if 语句也是可选的 
	4)、else和else if同时出现时，else必须出现在else if 之后 
	5)、如果有多条else if语句同时出现，那么如果有一条else if语句的表达式测试成功，那么会忽略掉其他所有else if和else分支。 
	6)、如果出现多个if,只有一个else的情形，else子句归属于最内层的if语句

11.switch-case

	switch(表达式) { 
	case 常量表达式1:语句1; .... 
	case 常量表达式2:语句2; default:语句; } 
	1、switch-case语句完全可以与if-else语句互转，但通常来说，switch-case语句执行效率要高。 
	2、default就是如果没有符合的case就执行它,default并不是必须的. 3、case后的语句可以不用大括号. 
	4、switch语句的判断条件可以接受int,byte,char,short,不能接受其他类型. 
	5、一旦case匹配,就会顺序执行后面的程序代码,而不管后面的case是否匹配,直到遇见break,利用这一特性可以让好几个case执行统一语句. 

12.for循环

	1)第一种写法for(int i=0;i<a,i++){}
	2)for循环的另外一种写法，for each写法。 第二个参数是数组，第一个参数是一个临时变量.每次都把数组里面的元素赋值给第一个参数。

12.数组，list，map

	1).声明一个数组int[] a=new int[7];即下标从0开始可以存七个元素 数组的常用方法 1.length属性，表示数组长度，即数组包含多少个元素。
	
	2).list 声明一个list List list= new ArrayList(); size()方法，返回list长度。 输入参数是Object,除了8个基本类型，什么都可以添加。add(index,object)如果index已经有值，就会替换。
	list.contains("")判断某个对象是否含有这个元素含有返回true
	
	3).声明一个Map map=new HashMap();<String,Object>泛型 预先规定，这个map的key必须是String类型的  值是object类型，也就是值可以随便改 map2.put放入元素 遍历for(String key:map2.keySet()){
	遍历map的四种方法：	
	//第一种，也是最常用的遍历方法
		for(Map.Entry<String, Car>e:ma1.entrySet()){
			String key=e.getKey();
			Car c=e.getValue();
			System.out.println("遍历map方法一通过entryset--"+key);
		}
		
	//第二种。
		for(String str:ma1.keySet()){
			System.out.println("遍历map方法一通过entryset-key-"+str);
		}
		
		for(Car c6:ma1.values()){
			System.out.println("遍历map方法一通过entryset-value-"+c6);
		}
		
		
	//第三种
		Iterator<Map.Entry<String, Car>> entries=ma1.entrySet().iterator();
		
		while(entries.hasNext()){
			
			Map.Entry <String, Car>e1= entries.next();
			System.out.println("遍历map方法三通过entryset--"+e1.getKey());
			
			
		}
		
	//第四种(尽量避免使用)
		/**
		 * 作为方法一的替代，这个代码看上去更加干净；但实际上它相当慢且无效率。
		 * 因为从键取值是耗时的操作（与方法一相比，在不同的Map实现中该方法慢了20%~200%）。
		 * 如果你安装了FindBugs，它会做出检查并警告你关于哪些是低效率的遍历。
		 * 所以尽量避免使用。
		 * 
		 * 
		 * 
		 */
		for(String str:ma1.keySet()){
			System.out.println("遍历map方法一通过entryset--"+str);
			
			Car value=ma1.get(str);
			
		}
	
14.==和equals的区别？

	对基本类型来说，没有equals方法，通过==比较的是值

	对引用类型来说，默认情况下，==和equals是一样的，都是比较地址的值，在equals方法被重写的情况下，equals可以不比较地址，而是比较值。
	当然这个需要在重写的equals方法里面自己去实现。
	String是有一个特殊的类，在java源代码中就重写了equals方法，所以String的equals方法可以直接比较值。

15.数据库增删改查
	
	选择：select * from table1 where 范围
	插入：insert into table1(field1,field2) values(value1,value2)
	删除：delete from table1 where 范围
	更新：update table1 set field1=value1 where 范围
	查找：select * from table1 where field1 like ’%value1%’ ---
	SELECT * FROM [user] WHERE u_name LIKE '%三%' (把名字中带有三的都查出来)
	SELECT * FROM [user] WHERE u_name LIKE '_三_' (把名字中间为三的都查出来)
	
	排序：select * from table1 order by field1,field2 [desc]
	总数：select count as totalcount from table1
	求和：select sum(field1) as sumvalue from table1
	平均：select avg(field1) as avgvalue from table1
	最大：select max(field1) as maxvalue from table1
	最小：select min(field1) as minvalue from table1

	-- 新建数据库。

	-- create database java5

	-- 新建表

	create table java5.class_details(
	id int,
	userId int,
	is_avaiable varchar(1),
	class_date date
	);

	-- 修改表
	-- 增加列
	alter table java5.book add  printDate date;
	
	-- 修改列的长度，从30该成了31
	alter table java5.book modify name varchar(31);
	
	-- 修改列的名字   change 后面第一个参数是原来的列名，接着跟新的列名
	alter table java5.book change name bookName varchar(31);
	
	
	联表查询：INNER JOIN（内连接）
	SELECT Students.ID,Students.Name,Majors.Name AS MajorName
	FROM Students INNER JOIN Majors
	ON Students.MajorID = Majors.ID
	
	外连接
	SELECT Students.ID,Students.Name,Majors.Name AS MajorName
	FROM Students LEFT JOIN Majors
	ON Students.MajorID = Majors.ID

16.doGet和doPost的区别和用法

	doGet：GET方法会把名值对追加在请求的URL后面。
	因为URL对字符数目有限制，进而限制了用在客户端请求的参数值的数目。
	并且请求中的参数值是可见的，因此，敏感信息不能用这种方式传递。
	doPOST：POST方法通过把请求参数值放在请求体中来克服GET方法的限制，因此，可以发送的参数的数目是没有限制的。
	最后，通过POST请求传递的敏感信息对外部客户端是不可见的



-----------------------------------毛屹嘉   结束-------------------------------------