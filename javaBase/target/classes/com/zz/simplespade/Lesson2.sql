use java5;

select  from user;

-- mysql自带的函数count()表示计数
select count() from user where name = '文明';

select count() from user where phoneNumber = '1776565323';

update user set score = 99.9;

update user set score = 59.9 where name = '文明';

-- 获取平均值 avg(列的名字) 取得该列的平均值
select avg(score) from user;

-- 取最小最大值min(列的名字),max(列的名字)
select min(score) from user;
select max(score) from user;

-- where优先于min(),max(),avg()

-- distinct 列名，表示在结果集中，去掉值是重复的，只保留一条
select distinct name from user;

select distinct name,score from user;

select  from user group by name having count()2;

update user set category = '英语';

-- 求三课成绩大于230的人
-- sum()表示计算该列值的总和
-- 列名as 别名，就是给列取了一个外号，在我们的结果显示这个外号， as也可以省略不写，效果一样
-- group by 列名表示按照那个列名分组，如果该列的值是相同的，就分到一个组
-- having ，必须是在group by后面，表示分组完成以后，再加过滤条件（和where相似）
-- 表示不等于
-- oeder by 列 表示按照该列的值排序，默认情况下是按照升序，也就是从小到大
-- oeder by 列 desc 表示按照该列的值降序，从大到小
-- order by 列 asc  表示按照该列的值排序，也就是和默认一样
select name as '名字',sum(score) as '总分' from user group by name having sum(score)=230 order by sum(score) desc;

select name as '名字',sum(score) as '总分' from user group by name having sum(score)=230 and name '文明' order by sum(score) desc;

-- 新建数据库

-- create database java5;

-- 新建表
create table java5.class_details(
	id int,
	userId int,
	is_aviable varchar(1),
	class_date date
);

-- 修改表
-- 增加列
alter table java5.book add printDate date;

-- 修改列的长度，从30改为了31
alter table java5.book modify name varchar(31);

-- 修改列的名字 change 后面第一个参数是原来的列名，接着跟新的列名
alter table java5.book change name bookName varchar(31); 
