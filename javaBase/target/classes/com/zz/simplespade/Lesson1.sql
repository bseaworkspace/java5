use java5;

-- select 表示查询
-- *表示所有列
-- from 表示后面跟的是表的名字
select * from user;

select name,pwd from user;

select * from user where name='玫瑰';

-- like 模糊查询 %表示零或者多个
select * from user where pwd like '4%';

select * from user where pwd like '%3';

select * from user where name like '%文%';

select * from user where pwd like '4%' and id>4;


-- 插入数据
insert into user(id,name,pwd,phoneNumber) values(8,'茶叶蛋1','153154','1541489744');

-- 删除数据
delete from user where id = 8;

update user set name='bsea' where id = 8;

-- 创建一张学生表 要求有：id，name，className，pwd，phoneNumber，date_join