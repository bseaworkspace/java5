use java5;


select * from user;


-- mysql 自带的函数 count（列的名字）表示计算总的记录条数，這里*表示任何列， 
select count(*) from user where name='文明';

select count(*) from user where phoneNumber='17765656323';


update user set score=90;


update user set score=49 where id=10;

-- avg(列的名字) 取的该列的平均值
select avg(score) from user;

-- min(列的名字) 取的该列的最小值
select min(score),name from user;


-- max(列的名字) 取的该列的最大值
select max(score) from user;

select min(score),max(score),name from user where phoneNumber='17765656323';

-- distinct 列名。   表示在结果集中，去掉值是重复的，只保留一条。
-- 比如这里，“文明”的值有四条，这里只会有一条显示在查询结果中。
select distinct name from user;

-- distinct 列名1，列名2.  只有列名1和列名2的值都相同的时候，才会认为是重复记录。重复记录，只显示一条。
select distinct name,score from user;






select * from user group by name having count(*)>2;

select * from user where phoneNumber='17765656323';

update user set category='英语';


-- 求 三课成绩总分大于230的人
-- sum(列) 表示 计算该列值的总和。
-- 列名  as 别名  ，就是给列取了一个外号，在我们的结果显示这个外号。 as也可以省掉 不写，效果一样。
-- group by 列名 表示按照那个列名的值分组，如果该列的值是相同的，就分到一个组。
-- having ,必须是在group by后面， 表示 分组完成以后，再加过滤条件（和 where相似）
-- <> 表示不等于
-- order by 列 ： 表示按照该列的值排序，默认情况下，是按照升序，也就是从小到大。

-- order by 列 desc： 表是按照该列的值，降序，从大到小显示。

-- order by 列 asc： 表是按照该列的值，是按照升序，也就是从小到大. 和不写是一样的效果。
select sum(score)   '总分',name as '名字' from user group by name having sum(score)>230;



select sum(score) as '总分',name as '名字' from user group by name having sum(score)>230 order by sum(score) desc;
select sum(score) as '总分',name as '名字' from user group by name having sum(score)>230 order by sum(score) asc;

select sum(score) as '总分',name as '名字' from user group by name having sum(score)>230 and name<>'文明' order by sum(score)  desc;

-- 新建数据库。

-- create database java5

-- 新建表

create table java5.class_details(
id int,
userId int,
is_avaiable varchar(1),
class_date date
);

-- 修改表
-- 增加列
alter table java5.book add  printDate date;

-- 修改列的长度，从30该成了31
alter table java5.book modify name varchar(31);

-- 修改列的名字   change 后面第一个参数是原来的列名，接着跟新的列名
alter table java5.book change name bookName varchar(31);








