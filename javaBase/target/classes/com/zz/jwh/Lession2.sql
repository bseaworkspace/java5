use java5;
select * from user;
--  自带的 count( 列名字) 表示计算总记录条数， * 表示任何列
select count(*) from user where name='文明';

select count(*) from user where phone='17721411770';
 update user set score =90;
 update `user` set score =59 where name='文明';
 
--  avg() 取平均值
 select avg(score) from user;
--  min() 最小值
 select min(score) from `user`;
--   max()最大值
 select max(score),name from `user`;
--  distinct 表示在结果集中 去掉重复的 只保留一条
 select distinct name from  user;
 
 select * distinct score,phone ;
 select * from  `user` group by name  having count(*)>2;
 update `user` set category='英语';
 
--  求三课 大于230
 select sum(score),name as '总分' from `user`  group by name having sum(score)>200  and name<>'bsea1 'order by sum(score)  desc ;
 
--  sum(列) 表示计算该列值的总和
-- 列名 as 别名 ，给列取个外号，在我们的结果显示这个外号     as也可以省略
-- group by 列名表示分组 如果该列值是相同的 就分到一个组里

--  having 必须在group by 表示分组完后的过滤条件
-- <>表示不等于
-- order by 列的值排序，默认情况下，按照升序 
--  desc 表示按照该列的值降序
-- asc 表示升序 和不写是一样的效果 
--  新建数据库 create datebase java5
-- 新建一张表 
create table java5.calss_details(
id int,
userid int,
is_avaiable varchar(1),
class_date date



);
-- 增加列
ALTER TABLE java5.book ADD printDate date NULL ;
-- 修改列的长度
alter table java5.book modify name varchar(31);
-- 修改列的名字 change 后面第一个参数是原来的列名 接着是新的列名
alter table java5.book change  name bookname  varchar(31);
