use java5;

select * from class_details;

select *from book;

select nvl(author,'wuming') from book;

select b.bookName,u.phoneNumber from user u, book b where u.name=b.author;

-- limit 2,2 表示从2号位置开始，取两条记录
select * from book  order by price desc limit 2,2;

-- limit 1，4 表示从1号位置开始，取4条记录（注意，起始位置是0号位）
select * from book  order by price desc limit 1,4;

select * from user;

select * from user where score<60;

select name from user where score<60 group by name having count(*)>=2;

select name from(select name from user where score<60 group by name having count(*)>=2)as t;

-- 把一个子查询的结果，在where中使用。下面语句意思是，查询出，作者得名字，存在于用户表的书的名字
select book.bookName from where author in (select name from user);

select name ,avg(score) from user group by name;

-- 把挂了两科以上的学生的平均分，和名字查出来
select name ,avg(score) from user where name in(select name from user where score<60 group by name having count(*)>=2) 
group by name;



select name,book.bookName 
from book left join user on book.author=user.name;

select name,book.bookName,class_details.is_avaiable from book 
left join user on book.author=user.name
left join class_details on user.id=class_details.id;



select name,book.bookName from book 
inner join user on book.author=user.name;
