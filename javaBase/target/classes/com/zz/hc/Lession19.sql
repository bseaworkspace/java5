use java5;

select * from user;



-- mysql 自带的函数count（列的名字），表示总的条数 *表示任何列
select count(*)from user where name='文明';

select count(*)from user where phoneNumber='15765565645';


update user set score=90;




update user set score=59 where id=8;

-- avg（列的名字）取得该列的平均值
select avg(score)from user;

-- min（列的名字）取得该列的平最小值
select min(score)from user;

-- max（列的名字）取得该列的最大值
select max(score)from user;


-- distinct列名。表示在结果集中，去掉值是重复的，只保留一条。
-- 比如这里 文明的值有四条，这里只会有一条显示在查询结果中。
-- distinct 列名1，列名2，只有在列名1和列名2都相同的时候，才会认为是重复记录，重复记录只显示一条。

select distinct name,score from user;

select * from user group by name having count(*)>2;

update user set category='英语';

-- sum（列）表示计算该列值的总和
-- 列名as 别名  ，就是给列取了一个外号，在我们的结果显示这个外号。as也可以省略。效果一样
-- group by 列名 表示按照那个列名的值分组，如果该列的值是相同的，就分到一个组。
-- having 必须是在group by后面，表示分组完成以后再加过滤条件（和where相似）
-- <> 表示不等于
-- oeder by 列表示按照该列的值排序，默认情况下是按照升序，也就是从小到大


-- oeder by列desc 表示按照该列的值，降序，从大到小显示
-- oeder by列asc 表示按照该列的值，升序，从小到大显示和不写是一样的效果
select sum(score)as '总分',name as '名字' from user group by name having sum(score)>230;

select sum(score)as '总分',name as '名字' from user group by name having sum(score)>230 order by sum(score)desc;
select sum(score)as '总分',name as '名字' from user group by name having sum(score)>230 order by sum(score)asc;


select sum(score)as '总分',name as '名字' from user group by name having sum(score)>230 and name <>'玫瑰' order by sum(score)desc;

-- 新建数据库
-- create database java5

-- 新建表
create table java5.class_details(
id int,
userId int,
is_avaiable varchar(1),
class_date date


);

-- 修改表
-- 增加列
alter table java5.book add printDate date;

-- 修改列的长度从30变成了31
alter table java5.book modify name varchar(31);

-- 修改列的名字change 后面第一个参数是原来的列名，接着跟了新的列名
alter table java5.book change name bookName varchar(31);


