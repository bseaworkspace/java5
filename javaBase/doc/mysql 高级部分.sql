use zztask0;

create TRIGGER user_trigger_bsea
AFTER
INSERT
on user for each row
BEGIN
	insert into user_team (t_id,u_id) values ('1',new.u_id);
END;


create index user_index_bsea on user(u_name,u_pwd,u_email);

-- 查询出，name重复的记录
select u_name,count(*) from user 
group by u_name having count(*)>1;

-- 删除重复记录，并且保留一条

select u_name,count(*),max(u_id) from user 
group by u_name having count(*)>1;


select * from user where u_name in (select u_name from user 
group by u_name having count(*)>1) and u_id not in (select max(u_id) from user 
group by u_name having count(*)>1);
-- 第一种方式
delete from user  where u_name 
in (select u_name from (select u_name from user  
group by u_name having count(*)>1) a) 
and  u_id
not in (select u_id from (select max(u_id) u_id from user  
group by u_name having count(*)>1) b);
-- 第二种方式
DELETE FROM user where u_id not in 
(select u_id from 
(select min(u_id) 
as u_id from user group by u_name) 
as b ) ;





