use java5;

-- select表示查询
-- *表示 所有列

-- from 表示后面跟的是表的名字
select * from user;


select name,pwd from user;

select * from user where name='玫瑰';

select * from user where id>5;

-- like 表示模糊查询 其中%表示0或者多
select * from user where pwd like '4%';


select * from user where phoneNumber like '177%';

select * from user where phoneNumber like '%3';

select * from user where name like '%文%';

-- where 后面如果需要多个过滤的查询条件，用 and 连接, 结果会越来越少，因为取交集
select * from user where phoneNumber like '177%' and id>5;

select * from user where phoneNumber like '177%' and id>5 and name='bsea';

-- where 后面如果需要多个过滤的查询条件，用 or 连接，结果会越来越多，因为取的是并集
select * from user where phoneNumber like '177%' or name='玫瑰';



-- 插入数据
insert into user(id,name,pwd,phoneNumber) values (8,'文明','23424','17721411770');
insert into user(id,name,pwd,phoneNumber) values (9,'文明','23424','17721411770');
insert into user(id,name,pwd,phoneNumber) values (10,'文明','23424','17721411770');

-- 删除数据
delete from user where name='bsea';

-- 修改
update user set name='bsea' where id=8;



-- 创建一张学生表，要求有：id,name,className,pwd,phoneNmuber, date_join,address
-- 插入9条数据，然后删除2条 








