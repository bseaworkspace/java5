use java5;

select * from class_details;

select * from book;





select b.bookName,u.phoneNumber from user u, book b where u.name=b.author;


-- limit 2,2 表示从2号位置开始，取两条记录
select * from book  order by price desc limit 2,2;

-- limit 1,4 表示从1号位置开始，取4条记录  （注意，起始位置是0号位）
select * from book  order by price desc limit 1,4;



select * from user;


select * from user where score<60;

select name, count(*) gk from user where score<60 group by name having gk>=2;


select name from user where score<60 group by name having count(*)>=2;


-- mysql子查询 1、where型子查询 （把内层查询结果当作外层查询的比较条件

-- 直接把一个查询的结果，当作数据表来用。
select name from (select name, count(*) gk from user where score<60 group by name having gk>=2) as t;

-- 把一个子的查询结果，在where中使用。 下面语句意思是， 查询出，作者的名字，存在于用户表的的书的名字
select bookName from book where author in (select name from user);


select name,avg(score) from user group by name ;

-- 把挂了两科以上的学生的平均分，和名字查出来

select name,avg(score) from user where name in 
(select name from user where score<60 group by name having count(*)>=2)
group by name ;

-- 1、左连接
--           以左表为准，去右表找数据，如果没有匹配的数据，则以null补空位，所以输出结果数>=左表原数据数

--            语法：select n1,n2,n3 from ta left join tb on ta.n1= ta.n2 [这里on后面的表达式，不一定为=，也可以>，<等算术、逻辑运算符]【连接完成后，可以当成一张新表来看待，运用where等查询】
select name ,bookName from book left join user  on book.author=user.name; 

select name ,bookName, class_details.is_avaiable as '是不是来上课了' from book
  left join user  on book.author=user.name
  left join class_details on user.id=class_details.userId  ;

select name ,bookName from book left join user  on book.author=user.name where user.name='黄琛'; 


-- 内连接
--             查询结果是左右连接的交集，【即左右连接的结果去除null项后的并集（去除了重复项）】
--             mysql目前还不支持 外连接（即左右连接结果的并集,不去除null项）out
--            语法：select n1,n2,n3 from ta inner join tb on ta.n1= ta.n2

select name ,bookName from book inner join user  on book.author=user.name; 












