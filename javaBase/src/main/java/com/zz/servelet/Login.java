package com.zz.servelet;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class Login
 * 背景知识： 
 * 1.
 * http 请求方式有：get,post，put,delete 等， 最长用的是get和post
 * 2. 本机的ip地址默认是127.0.0.1， 通常我们不直接写ip
 * 而是用localhost代替127.0.0.1
 * 
 * 3.在浏览器是直接输入地址的请求，都是Get请求。
 * 都会去执行servlet的doGet方法。
 */
//拦截地址
@WebServlet("/Login")
public class Login extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Login() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * servlet会默认两个方法，一个叫doGet, 一个叫doPost.
	 * 根据http请求的方式不同，如果Http请求的方式是GET，地址拦截以后，
	 * 就会执行doGet方法。 
	 * 
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String userName=request.getParameter("userName");
		String pwd=request.getParameter("pwd");
		String y=request.getParameter("yyyyy");
		System.out.println("页面上用户输入的用户名是："+userName+"  密码是："+pwd+"--y--"+y);
		String result="";
		if(userName.equals("张三")){
			
			result="login successful";
		}else{
			
			result="login fail";
		}
		
		response.getWriter().append(result);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}



