package com.zz.wsq;

import java.util.ArrayList;
import java.util.List;

public class Lession14 {
	public static void main(String[]args){
		int []a=new int[7];
		 a[0]=12;
		 a[1]=132;
		 a[2]=22;
		 a[3]=52;//52被移动了多少次
		 a[4]=72;
		 a[5]=925;
		 a[6]=552;
		 //length是属性，表示数组的长度，即数组包含多少个元素
		 int l=a.length;
		 //表示下标是3的位置的值
		 int b=a[3];
		 
		 
		 List list=new ArrayList();
		 //list常用方法
		 //size()方法，返回list的长度
		 int c=list.size();
		 //add输入参数是object，除了8大基本类型，都可以添加
		 list.add("adafafaagaggaga");
		 Lession14 l14=new Lession14();
		 list.add(l14);
		 list.add(1);
		 System.out.println(list.get(1));
		 //add(index,object)如果index已经有值就会被替换
		 list.add(1, 88);
		 list.add("wang");
		 System.out.println(list.get(1));
		 System.out.println(list.contains("wang"));
		 
		 
		 
		 
		 
		 
	}

}
