package com.zz.wsq;

public class Lession17 {
	// 判断连接符
	// ==
	// 1.==判断两个值是不是相等，适用范围，8个基本类型和String
	// 8个基本类型比的是，栈里面的值
	// 2.equals 方法，判断引用类型的两个变量是否相等
	// string方法比较特殊，==也是比较值，其它引用类型逼得是栈里面存的地址
	public static void main(String[] args) {
		int a = 13;
		int b = 12;
		int c = 13;
		if (a == c) {
			System.out.println("a=c");
		}

		String str1 = "abcd";
		String str2 = "abcde";
		String str3 = "abcd";
		if (str1 == str3) {
			System.out.println("str1=str3");
		}

		Person p1 = new Person();
		p1.idNumber = "1234";
		Person p2 = new Person();
		p2.idNumber = "1234";
		if (p1 == p2) {
			System.out.println("p1==p2");
		}else {
			System.out.println("equals p1！=p2");
		}
             //2.equals方法，判断引用类型的两个变量不是相等
		    //equals默认情况下，也是比较地址
		/*   public boolean equals(Object obj) {
        return (this == obj);
    }
		 * 
		 */
		   //是通过继承过来的
		if (p1.equals(p2)) {
			System.out.println(" equals p1==p2");
			
		}else {
			System.out.println("equals p1！=p2");
		}
	}
}
