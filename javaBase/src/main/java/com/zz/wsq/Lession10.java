package com.zz.wsq;
/*java面向对象的4个特点
 * 1.继承
 * 2.封装
 * 3.多态
 * 4.抽象
 * 
 */
public class Lession10 {
	  public static void main(String []args){
		  BussCar  bc=new BussCar();
		  bc.number="1096";
		  bc.color="白色";
		  bc.run(60);
		  bc.openDoor("前门");
		  Car c=new BussCar();
		  //
		//  c.run(100);
		/*
		 * 通过继承实现了，同一个方法，在运行的时候
		 * 根据输入参数是不同的子类，产生不一样的效果
		 * */		  
		  
		  
		  Lession10 l=new Lession10();
		  l.run(c, 100);
		  BussCar b=new BussCar();
		  b.number="1096";
		  l.run(b, 100);
	  }
	  public void run(Car c,int a){
            c.run(a);		  
	  }

}
