package com.zz.wsq;

public class Lesssion8 {
	public static void main(String[] args) {
		/**
		 * 数组 int []表示的是int类型的数组 new int[3]其中3表示数组的长度也就是这个数字最多能存3个数字
		 * 
		 * 
		 **/

		int[] a = new int[3];
		a[0] = 12;
		a[1] = 34;
		a[2] = 34;

		char[] s = new char[3];
		s[0] = 'a';
		s[1] = 'b';
		s[2] = 'c';

		for (int i = 0; i < a.length; i++) {
			System.out.println("a-->" + i + "-->" + a[i]);

		}

		int[][] b = new int[3][2];
		// 二维数组
		/*
		 * int [3][2]其中3表示第一维的数组长度是3 其中2表示第二维的数组的长度
		 * 也就是做这个二维数组可以存3个数组，每个数组可以存两个int类型的值
		 * 
		 */
		b[0][0] = 121;
		b[0][1] = 122;
		b[1][0] = 123;
		b[1][1] = 124;
		b[2][0] = 125;
		b[2][1] = 126;
		for (int i = 0; i < b.length; i++) {
			for (int j = 0; j < b[i].length; j++) {
				System.out.println("b-->" + i + "-->" + j + "-->" + b[i][j]);
			}

		}
		int c[][][]=new int[3][2][2];
		//有三个二维数组，二维数组里面包含两个一维数组；一维数组里面包含2个数
		c[0][0][0]=111;
		c[0][0][1]=112;
		c[0][1][0]=113;
		c[0][1][1]=114;
		c[1][0][0]=115;
		c[1][0][1]=116;
		c[1][1][0]=116;
		c[1][1][1]=117;
		c[2][0][0]=118;
		c[2][0][1]=119;
		c[2][1][0]=120;
		c[2][1][1]=121;
		for(int i=0;i<c.length;i++){
			for(int j=0;j<c[i].length;j++){
				for(int k=0;k<c[i][j].length;k++){
					System.out.println("c-->" + i + "-->" + j + "-->" +k + "-->"+ c[i][j][k]);
				}
			}
		}
		
		
		
		
		

	}

}
