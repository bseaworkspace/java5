package com.zz.wsq;

public interface Phone {
	//接口定义的对象是被final
	public void call(String phoneNumber);
    public boolean sendMessage(String content);
 }

