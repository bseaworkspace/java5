package com.zz.wsq;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Lession15 {
	public static void main(String[]args){
		Map map=new HashMap();
		map.put("test", 33333);
		map.put(888, 777);
		map.put(true, "sdfsfd");
		//map 是key，value的形式，map没有顺序，一个key只会对应一个value
		
		List list=new ArrayList();
		list.add("饮料");
		list.add("水果");
		list.add("玩具");
		map.put("全家便利店", list);
	
		
		
		System.out.println(map.get(888));
		System.out.println("test");
		System.out.println(true);
		System.out.println("全家便利店");
		
		//<String,Object>泛型，预先规定，这个map的key必须是String类型的，
		//值是Object类型，也就是可以随便放
		Map<String, Object> map2=new HashMap();
		map2.put("111", 555);
		//""+111把int类型的111转换成了string类型
		map2.put("全家便利店", list);
		map2.put("ttt", "coming");
		map2.put("map", map);
		for(String key:map2.keySet()){
			System.out.println(key+"----map2=="+map2.get(key));
			if(key=="list"){
				List tmp=(List) map2.get(key);
				List temp2=(List) tmp.get(3);
//				for(String s:temp2){
//					System.out.println(s);
//				}
			}
		}
		
		
	}

}
