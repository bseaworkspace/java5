package com.zz.wsq;

public class BussCar extends Car{
	//继承，子类继承父类的属性和方法
	/*
	 * extends是表示继承，后面跟的是父类的名字
	 * 1.java是单继承，只能继承一个父类
	 * 2.继承以后，子类会自动拥有弗雷德所有属性和方法
	 * 3.
	 * */
	String number;
	//override
	/*
	 * 1.必须是父类和子类之间，子类重写父类的方法
	 * 2.方法名必须一样
	 * 3.参数的个数和类型必须一样
	 * 4.返回的数据类型，也必须一致，否则会报编译错误
	 * */
	public void run(int b){
		System.out.println("这辆公交车"+number+"车以"+b+"公里每小时速度再跑");
		
	}
	
	public boolean openDoor(String way){
		System.out.println("车以"+way+"方法打开了门");
		return false;
		}
		
	public static void main(String[]args){
		
	}

}
