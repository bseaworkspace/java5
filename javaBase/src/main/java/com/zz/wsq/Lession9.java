package com.zz.wsq;

public class Lession9 {
		//java是一门面向对象的语言：
		/*两个人吃火锅
		 *在现实世界中的一些事物，可以转化为java代码描述
		 * 比如“哪里坐了一个穿红衣服的人”
		 * 这里我们如果可以用Java语言描述
		 * 类的名字是“人”
		 * 类有一个属性（成员变量）是红衣服
		 * 类的动作是坐
		 * 
		 * */
	
	public static void main(String[]args){
			//new 是关键字，表示要创建一个具体的对象
			//这里就是创建一个具体得人
			//new 后面跟的是构造方法，构造方法和类的名字是一样的1
			Person p=new Person();
			p.coth_color="red";
			
		
	}

}
