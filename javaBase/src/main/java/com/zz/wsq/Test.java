package com.zz.wsq;

public class Test {
	public static void main(String[]args){
		Lession16 l=new Lession16();
		//普通的成员变量，必须先new，才能被访问对象的属性
		System.out.println(l.name);
		//被static修饰的变量，叫静态变量，可以直接通过类的名字点出来使用，不需要new
		System.out.println(Lession16.classname);
		System.out.println(Lession16.IDNUM);
		Lession16.testStatic();
	}

}
