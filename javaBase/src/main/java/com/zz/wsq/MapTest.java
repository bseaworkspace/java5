package com.zz.wsq;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

public class MapTest {
	public static void main(String[]args){
		Map map=new HashMap();
		map.put("name", "value");//赋值
		map.get("name");//取值
		Map <String,Car> map1=new HashMap();//泛型，提前规定，这个map的key的String类型，value是Car类型
	    Car c1=new Car();
		map1.put("奔驰", c1);
		Car c2=new Car();
		map1.put("宝马", c2);
		Car c3=new Car();
		map1.put("奥迪", c3);
		/**
		 * 如果仅需要键（keys）后值（value）使用方法
		 * 如果你使用的语言版本低于java5
		 * 或是打算在遍历时删除entries，必须使用方法三
		 * 否则使用方法一
		 */
		//遍历map
		//第一种方法,也是最常用的方法
		for(Map.Entry<String, Car>e:map1.entrySet()){
			//entry是map中的每一个元素
			String key=e.getKey();
			Car c=e.getValue();
			System.out.println("遍历map方法——通过entrySet--"+key);
			System.out.println("遍历map方法——通过entrySet--"+c);
			
		}
		
		//第二种
		for(String str:map1.keySet()){
			System.out.println("遍历map方法——-通过entrySet-key--"+str);
			
		}
		for(Car c6:map1.values()){
			System.out.println("遍历map方法——通过entrySet-value--"+c6);
			
		} 
		
		//第三种
		Iterator<Map.Entry<String, Car>> entries=map1.entrySet().iterator();
		while(entries.hasNext()){
			Map.Entry<String, Car> e1= entries.next();
			System.out.println("遍历map方法——通过entrySet.iterator().hasNext()--"+e1.getKey()+e1.getValue());
		}
		
	  //第四种(尽量避免使用)
		for(String str:map1.keySet()){
			Car value=map1.get(str);
			System.out.println(value+str);
			
		}
		
		
		
		
		
	}

}
