package com.zz.wsq;

public class Lession13 {
	public static void main(String[] args) {
		int[] a = new int[7];
		a[0] = 12;
		a[1] = 132;
		a[2] = 22;
		a[3] = 52;// 52被移动了多少次
		a[4] = 72;
		a[5] = 925;
		a[6] = 552;
//选择排序：从所有的元素中选择一个最小的元素，放在0号位置
//比较排序和冒泡排序，比较次数是一样的，但是效率，比较排序比较高，因为交换次数少（不是绝对的，比如数组本来顺序是对的)
		//
		int count = 0;
		for (int i = 0; i < a.length; i++) {
			int biggestIndex = i;
			for (int j = i + 1; j < a.length; j++) {
				if (a[j] > a[biggestIndex]) {
					biggestIndex = j;
				}
			}
			int temp = a[i];
			a[i] = a[biggestIndex];
			a[biggestIndex] = temp;
			 if(a[i]==52||a[biggestIndex]==52){
			 count++;
			 }
		}
		// 最小下标的和i交换位置
		 System.out.println(count);

		for (int i : a) {
			System.out.println(i);
		}
	}

}
