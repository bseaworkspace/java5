package com.zz.wsq;

/*class 后面跟的是类的名字
 * interface后面跟的是接口名字
 * 
 * 
 * 
 */
public interface InterfaceTest {
	//接口里面的方法，是没有方法体的也就是没有{}
	//只是定义方法的名字，输入参数和返回类型
	public void test();
	
	

}
