package com.zz.jwh;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class MapTest {
/**
 * 如果只需要键（keys）或者值（values）使用方法二
 * 如果使用语言版本低于java5
 * 或者打算遍历时删除entries 必须是以哦那个方法三
 * 否则使用方法一 
 * 
 * 
 * 
 * @param args
 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
Map map=new HashMap();
//赋值
map.put("name", "value");
//取值
map.get("name");
//泛型 提前规定 这个map的    key必须是String类型    value似乎car类型
Map <String,car>map1=new HashMap();

car c1=new car();
map1.put("奔驰",c1 );
car c2=new car();
map1.put("宝马",c2 );
car c3=new car();
map1.put("奥迪",c3 );



//遍历map
//第一种 最常用

for (Map.Entry<String, car> e:map1.entrySet()){
	String key=e.getKey();
	car c=e.getValue();
	System.out.println(key+"");
}


//第二种  
for(String str:map1.keySet()){
	System.out.println(str);
	
}

for(car cc:map1.values()){
	System.out.println(cc.getInfo());
}

//第三种 
Iterator<Map.Entry<String, car>> E=map1.entrySet().iterator();
while(E.hasNext()){
	Map.Entry<String , car> e1=E.next();
	System.out.println(e1.getKey()+""+e1.getValue());
}



	}

	
	
}
