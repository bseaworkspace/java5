package com.zz.jwh.control;

import java.util.List;
import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.zz.jwh.model.User;

import jdk.nashorn.internal.ir.RuntimeNode.Request;

/**
 * 把一个普通的class 改成servlet
 * 
 * 1.继承httpServelt 2.重写doGet doPost 方法 3.加servlet节点 4.加servlet-mapping节点
 * 
 * 
 * @author 13040
 *
 */
public class UserController extends HttpServlet {
	protected void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		

			
			// jsp 就大内置对象
			/**
			 * 1request 
			 *2 response网页传回你会用户端
			 * 3session 与请求有会话期
			 * 4
			 * 5
			 * 
			 * servlet 中，包含了项目名字
			 * 
			 * 
			 * 
			 */
		
			//模拟dao层
			List<User> ls=new ArrayList<User>();
			User u1=new User();
			u1.setName("玫瑰1");
			u1.setId(1);
			u1.setPwd("111111");
			ls.add(u1);
			
			User u2=new User();
			u2.setName("玫瑰1");
			u2.setId(2);
			u2.setPwd("222222");
			ls.add(u2);
			
			User u3=new User();
			u3.setName("玫瑰3");
			u3.setId(3);
			u3.setPwd("333333");
			ls.add(u3);
			
			//结束模拟
			
		req.getSession().setAttribute("ls", ls);
			
			req.getRequestDispatcher("/jinwenhao/UserList1.jsp").forward(req, res);


	}

	protected void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		System.out.println(req.getParameter("name"));
		String result="Y";
		/**
		 * to do 
		 * 把数据更新到数据库
		 * 
		 * 
		 * 
		 * 
		 */
		//更新到数据库
		res.getWriter().append(result);
	}
}
