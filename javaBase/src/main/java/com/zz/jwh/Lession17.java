package com.zz.jwh;

public class Lession17 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		// 判断连接符
		// 1. == 判断两个值是不是相等 。 适用范围：8个基本类型和string
		/*
		 * ==8个基本类型 比较的是栈里面的值
		 * 
		 * String 方法比较特殊，==也是比较值 其他引用类型，比较的是栈里面的地址
		 */

		int a = 12;
		int b = 13;
		int c = 12;
		if (a == c) {
			System.out.println("a=c");
		}
		String str1 = "a";
		String str2 = "b";
		String str3 = "a";
		if (str1 == str3) {
			System.out.println("str1=str3");
		}
		person p1 = new person();
		p1.id = 123;
		person p2 = new person();
		p2.id = 123;

		if (p1.id == p2.id) {
			System.out.println("asd");
		}

		// 2. equals方法 判断引用类型的两个变量是不是相等
		// equals 默认情况下 也是比较地址，equals 是来自于Object
		// 通过继承来的

	}

}
