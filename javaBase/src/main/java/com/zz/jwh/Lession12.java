package com.zz.jwh;

import java.util.ArrayList;
import java.util.List;

public class Lession12 {

	public static void bubbling(int[] a) {
		int count = 0;
		for (int i = 0; i < a.length; i++) {
			for (int I = i + 1; I < a.length; I++) {
				if (a[i] < a[I]) {
					int temp = 0;
					temp = a[i];
					a[i] = a[I];
					a[I] = temp;
					if (a[i] == 52 || a[I] == 52) {
						count = count + 1;
					}
				}

			}
		}
		System.out.println(count);
	}

	public static void selection(int[] a) {
		int indexMax = 0;
		int count = 0;
		for (int i = 0; i < a.length; i++) {
			indexMax = i;
			for (int I = i + 1; I < a.length; I++) {
				if (a[indexMax] < a[I]) {
					indexMax = I;
				}
			}
			if (indexMax != i) {
				int temp = a[i];
				a[i] = a[indexMax];
				a[indexMax] = temp;
				if (a[i] == 52 || a[indexMax] == 52) {
					count = count + 1;
				}
			}
		}

		System.out.println(count);
	}
	
	/**选择排序：从所有元素中选择一个最小的元素，放在0号位置上
	 * 比较排序和冒泡排序，比较次数是一样的，但是效率，比较排序比较高，因为交换次数少（不是绝对的，比如数组本来就是有序的）
	 * 冒泡排序交换次数是0次 但选择排会是2
	 * 
	 * 
	 * 
	 * 
	 * 
	 * @param args
	 */

	public static void main(String[] args) {
		int[] a = new int[7];
		a[0] = 12;
		a[1] = 132;
		a[2] = 22;
		a[3] = 52;
		a[4] = 72;
		a[5] = 922;
		a[6] = 552;
		// bubbling(a);
		//selection(a);

		// for循环 遍历
		// for(int i=0;i<a.length;i++){
		// System.out.println(a[i]);
		// }

		// for循环的另外一种写法 for each写法。 第二个是参数 第一个参数是一个临时变量
		// 每次都把数组里面的元素值 赋值给第一个参数
		for (int X : a) {
			System.out.println(X);
		}

		
		//数组的常用方法
		int l=a.length; //length是数组的属性 即数组包含多少个元素
		
		int b=a[3];  //表示下表是3的位置上的值
		
		
		//List常用方法
		List list=new ArrayList();
		int c=list.size();//返回List的长度
		
		list.add("asdasd");//add输入参数是Object
		//add(index ,Object)
		//如果index已经有值了 就会替换
		list.add(1);
		list.add(1,123);
		//
	list.contains(123);
	
		String SS="asdasd";
//		SS.replace("s", "b");//原来的不动 会替换以后返回新的String
		System.out.println(SS.replace("s", "b"));
		
	}
}
