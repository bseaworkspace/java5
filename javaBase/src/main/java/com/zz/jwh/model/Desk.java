package com.zz.jwh.model;

public class Desk {
	private int id ;
	 private String is_avaiable ;
	 private int  customer_id  ;
	 private int  desk_id  ;
	 
	public int getDesk_id() {
		return desk_id;
	}
	public void setDesk_id(int desk_id) {
		this.desk_id = desk_id;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getIs_avaiable() {
		return is_avaiable;
	}
	public void setIs_avaiable(String is_avaiable) {
		this.is_avaiable = is_avaiable;
	}
	public int getCustomer_id() {
		return customer_id;
	}
	public void setCustomer_id(int customer_id) {
		this.customer_id = customer_id;
	}
	 
	 
}
