package com.zz.jwh.model;

public class Book {
	private int id;
	private String name;
	private  boolean is_brow;
	public boolean isIs_brow() {
		return is_brow;
	}
	public void setIs_brow(boolean is_brow) {
		this.is_brow = is_brow;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
}
