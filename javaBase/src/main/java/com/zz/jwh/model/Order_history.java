package com.zz.jwh.model;

import java.sql.Date;

public class Order_history {

private int id;
private int book_id;
private Date start_dt;
private Date end_dt;
private int user_id;
public int getId() {
	return id;
}
public void setId(int id) {
	this.id = id;
}
public int getBook_id() {
	return book_id;
}
public void setBook_id(int book_id) {
	this.book_id = book_id;
}
public Date getStart_dt() {
	return start_dt;
}
public void setStart_dt(Date start_dt) {
	this.start_dt = start_dt;
}
public Date getEnd_dt() {
	return end_dt;
}
public void setEnd_dt(Date end_dt) {
	this.end_dt = end_dt;
}
public int getUser_id() {
	return user_id;
}
public void setUser_id(int user_id) {
	this.user_id = user_id;
}

}
