package com.zz.jwh.model;

public class User {
private	int id;
private	String  name;
private	String pwd;
private	String phone;
private	float score;
private	String category;
private	float total_amt;
	public float getTotal_amt() {
	return total_amt;
}
public void setTotal_amt(float total_amt) {
	this.total_amt = total_amt;
}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getPwd() {
		return pwd;
	}
	public void setPwd(String pwd) {
		this.pwd = pwd;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public float getScore() {
		return score;
	}
	public void setScore(float score) {
		this.score = score;
	}
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}

	
	
	
}
