package com.zz.jwh.dao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.zz.jwh.model.User;
import com.zz.jwh.model.Book;

public class BookDao {
	BaseDao baseDao = new BaseDao();
	
	public List<Book> getBookByName(String name) {
		List<Book> ls = new ArrayList<Book>();
		Connection con = baseDao.getConnection();
		try {
			String sql = "select * from book where name= ?";
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setString(1, name);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				Book temp = new Book();
				temp.setId(rs.getInt("id"));
				temp.setName(rs.getString("name"));
				temp.setIs_brow(rs.getBoolean("is_brow"));
				ls.add(temp);
			}
			baseDao.closeQuery(rs, ps, con);

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return ls;
	}

	public List<Book> getBookByNameAndStatus(String name,String n) {
		List<Book> ls = new ArrayList<Book>();
		Connection con = baseDao.getConnection();
		try {
			String sql = "select * from book where name= ? and  is_brow=?";
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setString(1, name);
			ps.setString(2, n);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				Book temp = new Book();
				temp.setId(rs.getInt("id"));
				temp.setName(rs.getString("name"));
				temp.setIs_brow(rs.getBoolean("is_brow"));
				ls.add(temp);
			}
			baseDao.closeQuery(rs, ps, con);

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return ls;
	}
	
	public boolean borrowBook(String bookname,String  name){
		boolean flag=false;
		//判断有没有这本书
		List <Book> ls1=getBookByNameAndStatus(bookname,"N");
		UserDao udao=new UserDao();
		List<User> ls2=udao.getUserByName(name);
		
		
		if(ls1.size()>0&&ls2.size()>0){
			Connection con=baseDao.getConnection();
			String addsql="INSERT INTO java5.order_history(book_id, start_dt, user_id)VALUES( ?, ?, ?)";
			String updatesql="updata book det is_brow='Y' where id=? ";
			Book b=ls1.get(0);
			User u=ls2.get(0);
			
			try {
					PreparedStatement ps=con.prepareStatement(addsql);
					ps.setInt(1, b.getId());
					Date now = new Date(0); 
					ps.setDate(2, now);
					ps.setInt(3, u.getId());
					int count=ps.executeUpdate();
					if(count>0){
					ps=con.prepareStatement(updatesql);
					ps.setInt(1, b.getId());
					int count2=ps.executeUpdate();
					if(count2>0){
						flag=true;
					}
					
					}
						
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
					
		}
		
		//
		
		return flag;
	}
		
	public boolean returnBook(String bookname, String name) {
		boolean flag = false;
		// 判断有没有这本书
		List<Book> ls1 = getBookByNameAndStatus(bookname,"Y");
		UserDao udao = new UserDao();
		List<User> ls2 = udao.getUserByName(name);

		if (ls1.size() > 0 && ls2.size() > 0) {
			Connection con = baseDao.getConnection();
			String addsql = "update java5.order_history  set end_dt=? where book_id=? and user_id=?";
			String updatesql = "update book  is_brow='N' where id=? ";
			Book b = ls1.get(0);
			User u = ls2.get(0);

			try {
				PreparedStatement ps = con.prepareStatement(addsql);
				Date now = new Date(0);
				ps.setDate(1, now);
				ps.setInt(2, b.getId());
				int count = ps.executeUpdate();
				if (count > 0) {
					ps = con.prepareStatement(updatesql);
					ps.setInt(1, b.getId());
					int count2 = ps.executeUpdate();
					if (count2 > 0) {
						flag = true;
					}

				}

			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}

		//

		return flag;
	}
}
