package com.zz.jwh;

public class Lession8 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		//数组
		/*
		 * int[] 表示声明的是int类型的数组
		 * 
		 * new int[3] 表示数组的长度，这个数组最多存3个元素
		 * 
		 * 
		 * 
		 * 
		 */
		int[] a = new int[3];
		a[0] = 12;
		a[1] = 27;
		a[2] = 6666;

		char[] b = new char[3];
		b[0] = 'S';
		b[1] = 'C';
		b[2] = 'B';

		//二维数组
		/*int [3][2] 3表示第一维的数组长度是3 
		 *           2表示第二维数组的长度。
		 *           这个二维数组，可以存3个数组，每个数组可以存2个int值
		 * 
		 * 
		 * 
		 * 
		 * 
		 */
		
		int [][] c=new int [3][2];
		c[0][0]=100;
		c[0][1]=101;
		c[1][0]=110;
		c[1][1]=111;
		c[2][0]=120;
		c[2][1]=121;
		
		
		for(int i=0;i<a.length;i++){
			System.out.println("a-->"+i+"-->"+a[i]);
		}
		for(int I=0;I<c.length;I++)
			for(int i=0;i<c[I].length;i++){
				System.out.println("b-->"+I+" "+i+"-->"+c[I][i]);
			}
		
		int d[][][]=new int[1][2][3];
		d[0][0][0]=0;
		d[0][0][1]=1;
		d[0][0][2]=2;
		d[0][1][0]=10;
		d[0][1][1]=11;
		d[0][1][2]=12;
		for(int i=0;i<d.length;i++){
			for(int I=0;I<d[i].length;I++){
				for(int j=0;j<d[i][I].length;j++){
					System.out.println("d-->"+i+","+I+","+j+",-->"+d[i][I][j]);
				}
			}
			
		}
		
	}

}
