package com.zz.jwh;

import java.util.ArrayList;
import java.util.List;
import java.sql.*;

public class UserDao {
	BaseDao baseDao = new BaseDao();

	public List<User> getUserByName(String name) {
		List<User> ls = new ArrayList();
		Connection con = baseDao.getConnection();
		try {

			PreparedStatement ps = con.prepareStatement("select  *from user where name = '" + name + "'");
			ResultSet rs = ps.executeQuery();

			if (rs.next()) {
				User u = new User();
				u.setId(rs.getInt("Id"));
				u.setName(rs.getString("Name"));
				u.setPhone(rs.getString("Phone"));
				u.setScore(rs.getFloat("Score"));
				u.setPwd(rs.getString("pwd"));
				ls.add(u);

			}
			baseDao.closeQuery(rs, ps, con);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return ls;
	}
	
	//boolean 意思是 这个方法添加用户到数据库，是不是成功
	//如果成功 就返回true 失败返回 false
	/**
	 * 输入参数是User 是一个User 类型的形式参数
	 * 表示页面上用户输入的信息都   set 进去
	 * 然后方法里面就可以通过get方法 得到用户输入的值
	 * 
	 * 
	 * 
	 * 
	 * 
	 * @param u
	 * @return
	 */
	
  public boolean addUser (User u){
	  boolean flag=false;
	  /**
	   * 每一次 数据库交互
	   * 都必须完成jdbc连接的几个步骤
	   * 
	   * 1.加载驱动(Class.ForName(driver))  	     Class.forName("com.mysql.jdbc.Driver");
	   * 2.获取连接 DriverManager.getConnection   con = DriverManager.getConnection(con = DriverManager.getConnection(url, name, pwd), datebasename, pwd);
	   * 3.通过 connection 获取 Statement			 PreparedStatement ps=con.prepareStatement("select * from user");
	   * 4.通过 statement 执行sql        		 ps.execute();等
	   * 5.倒序关闭 所有链接                         		 rs.close() ps.close() con.close()
	   * 
	   * 
	   * 
	   */
	  Connection  con=baseDao.getConnection();
	
	  String sql ="insert into user(id,name ,pwd)values(?,?,?)";
	  try {
		  /**
		   *  ? 表示占位符  意思是这个地方会被输入参数代替
		   *  PreparedStatement的setString 方法
		   *  第一个参数位置 起始位置是从1开始
		   *  第二个参数的值 是sql语句中的值 
		   */
		PreparedStatement ps=con.prepareStatement(sql);
		
		ps.setInt(1, u.getId());
		ps.setString(2, u.getName());
		ps.setString(3, u.getPwd());
		if(ps.executeUpdate()>0){
			flag=true;
			
		}
		
		baseDao.closeUpdate(ps, con);
		
		
		
		
		
	} catch (SQLException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	  
	  
	  
	  return flag;
  }
	public void InsertIdName(int id, String name) {
		List<User> ls = new ArrayList();
		Connection con = baseDao.getConnection();
		boolean flag1 = false;
		boolean flag2 = false;
		try {
			PreparedStatement ps = con.prepareStatement("select * from user");
			ResultSet rs = ps.executeQuery();
			if (rs.next()) {
				if (rs.getString("name").equals(name)) {
					flag1 = true;
				}
				if (rs.getInt("Id") == id) {
					flag2 = true;
				}

			}
			if ((!flag1) && (!flag2)) {
				System.out.println("信息正确");

			} else {
				System.out.println("信息错误");
			}
			
			ps=con.prepareStatement("insert into user(id,name)values ('"+id+" ', '"+name+"'" );
			 ps.execute();
			 
			 baseDao.closeQuery(rs, ps, con);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		UserDao udao = new UserDao();
		List<User> ls = udao.getUserByName("玫瑰");
		boolean flag = false;
		if (ls.size() == 0) {
			System.out.println("用户名不存在");
		}
		String pwd = "123";

		for (User u1 : ls) {
			System.out.println(u1.getName() + u1.getId() + u1.getCategory() + u1.getPhone());
			if (u1.getPwd().equals(pwd)) {
				flag = true;
			}
			if (flag) {
				System.out.println("成功");
			} else {
				System.out.println("失败");
			}
		}
	}

}
