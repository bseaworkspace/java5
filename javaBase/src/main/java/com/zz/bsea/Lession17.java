package com.zz.bsea;

public class Lession17  {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		//判断连接符
		
		//1. == 判断两个值是不是相等。适用范围：8个基本类型和String
		/**
		 * == 8个基本类型，比较的是栈里面的值。
		 * String方法比较特殊，==也是比较值。
		 * 其他引用类型，比较的是 栈里面存的地址
		 * 
		 */
		
		int a=13;
		int b=12;
		int c=13;
		
		if(a==c){
			System.out.println("a=c");
		}
		
		String str1="abcd";
		String str2="abcde";
		String str3="abcd";
		
		if(str1==str3){
			System.out.println("str1=str3");
			
		}
		
		Persion p1=new Persion();
		p1.idNumber="1234";
		Persion p2=new Persion();
		p2.idNumber="1234";
		
		if(p1==p2){
			System.out.println("p1=p2");
		}else{
			System.out.println("p1！=p2");
			
		}
		
		
		
		//2.equals方法，判断引用类型的两个变量是不是相等。
		//equals默认情况下，也是比较地址 equals方法来自于Object
		/** Object的源码：
		 public boolean equals(Object obj) {
		        return (this == obj);
		    }
		    **/
		//是通过继承过来的。
		if(p1.equals(p2)){
			System.out.println("equals  p1=p2");
		}else{
			System.out.println("equals  p1！=p2");
			
		}
		
		
		

	}

}
