package com.zz.bsea.model;

public class Food_customer {
	private int food_id;
	private int customer_id;
	private float order_id;
	private int desk_id;
	
	
	public int getDesk_id() {
		return desk_id;
	}
	public void setDesk_id(int desk_id) {
		this.desk_id = desk_id;
	}
	public int getFood_id() {
		return food_id;
	}
	public void setFood_id(int food_id) {
		this.food_id = food_id;
	}
	public int getCustomer_id() {
		return customer_id;
	}
	public void setCustomer_id(int customer_id) {
		this.customer_id = customer_id;
	}
	public float getOrder_id() {
		return order_id;
	}
	public void setOrder_id(float order_id) {
		this.order_id = order_id;
	}
	
}
