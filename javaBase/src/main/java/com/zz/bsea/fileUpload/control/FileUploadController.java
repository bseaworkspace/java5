package com.zz.bsea.fileUpload.control;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

/**
 * Servlet implementation class FileUploadController
 */
public class FileUploadController extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public FileUploadController() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		System.out.print(request.getContextPath());
		DiskFileItemFactory factory=new DiskFileItemFactory();
		String imgName="";
		ServletFileUpload upload=new ServletFileUpload(factory);
		upload.setHeaderEncoding("utf-8");
		try {
			List<FileItem> ls= upload.parseRequest(request);
			System.out.println("----------"+ls.size());
			for(FileItem f:ls){
				boolean isFormField=f.isFormField();
				System.out.println("isFormField===="+isFormField);
				String fieldName=f.getFieldName();
				System.out.println("fieldName===="+fieldName);
				if(!isFormField){
					String fileName=f.getName();
					System.out.println("fileName===="+fileName);
					if(fileName.lastIndexOf("\\")!=-1){
						fileName=fileName.substring(fileName.lastIndexOf("\\"));
					}
					InputStream in=f.getInputStream();
					byte [] b=new byte[(int) f.getSize()];
					in.read(b);
					imgName=fileName;
					File file=new File("C:\\bsea\\wp\\java5\\zz_java5\\src\\main\\webapp\\bsea"+fileName);
					OutputStream out=new FileOutputStream(file);
					out.write(b);
					
					in.close();
					out.close();
				}
				
				
				
				
			}
		} catch (FileUploadException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		request.getSession().setAttribute("imgName", imgName);
		request.getRequestDispatcher("/bsea/showImage.jsp").forward(request, response);;
		
		
	}

}
