package com.zz.bsea;

import java.util.ArrayList;
import java.util.List;

public class Test {

	public static void main(String[] args) {
		
	    Lession16 l=new Lession16();
	    //普通的成员变量，必须要先new出对象，才能访问对象的属性
	    System.out.println(l.name);
	    //被static修饰的变量，叫静态变量，可以直接
	    //通过类的名字点出来使用，不需要new
	    System.out.println(Lession16.className);
	    System.out.println(l.className);
	    Lession16.className="Cary";
	    System.out.println(Lession16.className);
	    
	    System.out.println(Lession16.IDNUM);
	    //被final修饰的变量，不能再赋值 Lession16.IDNUM="234242";
	    
	    Lession16.testStatic();
	    
	}

}
