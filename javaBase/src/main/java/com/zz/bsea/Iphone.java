package com.zz.bsea;

/**
 * 实现一个接口，使用implements。
 * 
 * java中，一个类可以实现多个接口
 * 
 * 
 * @author jiyu
 *
 */
public class Iphone implements Phone, InterfaceTest {

	public void test() {
		// TODO Auto-generated method stub
		
	}

	public void call(String phoneNumber) {
		// TODO Auto-generated method stub
		 System.out.println("这是来自iphone的电话");
	}

	public boolean sendMessage(String content) {
		// TODO Auto-generated method stub
		System.out.println("这是来自iphone的短信");
		return true;
	}

	

}
