package com.zz.bsea;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class MapTest {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Map map=new HashMap();
		//赋值
		map.put("name", "value");
		//取值
		map.get("name");
		
		//泛型： 提前规定，这个map的key必须是String 类型， value是Car类型
		Map<String,Car> map1=new HashMap();
		Car c1=new Car();
		map1.put("奔驰", c1);
		Car c2=new Car();
		map1.put("宝马", c2);
		Car c3=new Car();
		map1.put("奥迪", c3);
		
		//遍历map
		/**
		 * 总结
如果仅需要键(keys)或值(values)使用方法二。
如果你使用的语言版本低于java 5，
或是打算在遍历时删除entries，必须使用方法三。
否则使用方法一(键值都要)。
		 * 
		 * 
		 * 
		 */
		
		//第一种，也是最常用的遍历方法
		for(Map.Entry<String, Car> e:map1.entrySet()){
			  String key=e.getKey();
			  Car c=e.getValue();
			  
			  System.out.println("遍历map方法一 通过entrySet--"+key);
			
		}
		
		
		//第二种。
		
		for(String str:map1.keySet()){
			 System.out.println("遍历map方法二 通过keySet-key-"+str);
			
		}
		
		for(Car c6:map1.values()){
			System.out.println("遍历map方法二 通过keySet-value-"+c6);
			
		}
		
		//第三种
		Iterator<Map.Entry<String, Car>> entries=map1.entrySet().iterator();
		
		while(entries.hasNext()){
			
			Map.Entry<String, Car> e1=entries.next();
			  System.out.println("遍历map方法三 通过entrySet--"+e1.getKey());
		}
		
		//第四种(尽量避免使用)
		/**
		 * 
作为方法一的替代，这个代码看上去更加干净；但实际上它相当慢且无效率。
因为从键取值是耗时的操作（与方法一相比，在不同的Map实现中该方法慢了20%~200%）。
如果你安装了FindBugs，它会做出检查并警告你关于哪些是低效率的遍历。所以尽量避免使用。
		 * 
		 * 
		 */
		
		
		for(String str:map1.keySet()){
			 System.out.println("遍历map方法四 通过keySet-key-"+str);
			 
			 Car value=map1.get(str);
			
		}
		
		
		
	}

}
