package com.zz.bsea.tool.excel;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

public class ExcelUtil {
	
	/**
	 * 只支持创建string类型的单元格
	 * @param wb
	 * @param row
	 * @param col  应该是你的cell单元格的位置也就是列号；
	 * @param align 应该是你的对齐方式；
	 * @param val 应该是你单元格里面要添加的值；
	 */
	public static void  cteateCell(HSSFWorkbook wb,HSSFRow row,int col,short align,String val){
		HSSFCell cell = row.createCell(col);
		//cell.setEncoding(HSSFCell.ENCODING_UTF_16);
		
		cell.setCellValue(val);
		HSSFCellStyle cellstyle = wb.createCellStyle();
		cellstyle.setAlignment(align);
		cell.setCellStyle(cellstyle);
	}

}
