package com.zz.bsea.tool.io;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;

import org.apache.poi.POIXMLDocument;
import org.apache.poi.POIXMLTextExtractor;
import org.apache.poi.hwpf.extractor.WordExtractor;
import org.apache.poi.openxml4j.opc.OPCPackage;
import org.apache.poi.xwpf.extractor.XWPFWordExtractor;
public class WordReader {

 /**
  * @param args
  */
 public static void main(String[] args) {
  // TODO Auto-generated method stub
	 readWord("C:\\tmp\\五交所需求文档.docx");
	 /**
        try{
         String text = WordReader.readDoc("C:\\tmp\\五交所需求文档.doc");
         System.out.println(text);
        }catch(Exception ex){
         ex.printStackTrace();
        }
        **/
 }
 
 public static StringBuffer readWord(String path) {
     String s = "";
     try {
         if(path.endsWith(".doc")) {
             InputStream is = new FileInputStream(new File(path));
             WordExtractor ex = new WordExtractor(is);
             s = ex.getText();
         }else if (path.endsWith("docx")) {
             OPCPackage opcPackage = POIXMLDocument.openPackage(path);
             POIXMLTextExtractor extractor = new XWPFWordExtractor(opcPackage);
             s = extractor.getText();
            // System.out.println("内容=="+s);
         }else {
             System.out.println("传入的word文件不正确:"+path);
         }

     } catch (Exception e) {
         e.printStackTrace();
     }
     StringBuffer bf = new StringBuffer(s);
     return bf;
 }
}
