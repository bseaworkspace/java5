package com.zz.bsea.tool.excel;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

/**
 * 方法重载： 1.同一个类里面  2.方法名字相同  3.参数列表不同（类型或者数量或者位置《参数的名字和方法返回类型无所谓，不参考》）
 * 
 * 
 * @author jiyu
 *
 */
public class Test4 {
	/**
	 * 只支持创建string类型的单元格
	 * @param wb
	 * @param row
	 * @param col  应该是你的cell单元格的位置也就是列号；
	 * @param align 应该是你的对齐方式；
	 * @param val 应该是你单元格里面要添加的值；
	 */
	private static void  cteateCell(HSSFWorkbook wb,HSSFRow row,int col,short align,String val){
		HSSFCell cell = row.createCell(col);
		//cell.setEncoding(HSSFCell.ENCODING_UTF_16);
		
		cell.setCellValue(val);
		HSSFCellStyle cellstyle = wb.createCellStyle();
		cellstyle.setAlignment(align);
		cell.setCellStyle(cellstyle);
	}
	private static String  cteateCell(HSSFRow row,HSSFWorkbook wb,short col,short align,String val){
		HSSFCell cell = row.createCell(col);
		//cell.setEncoding(HSSFCell.ENCODING_UTF_16);
		
		//cell.setCellValue(val);
		HSSFCellStyle cellstyle = wb.createCellStyle();
		cellstyle.setAlignment(align);
		cell.setCellStyle(cellstyle);
		return  "";
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		HSSFWorkbook wb=new HSSFWorkbook();
		
		HSSFSheet s1=wb.createSheet();
		//2表示冻结两列， 3表示冻结3行  ,如果要冻结第一行，（0，1）
		s1.createFreezePane(2, 3);
		
		HSSFRow r4=s1.createRow(4);
		
		cteateCell(wb,r4,3,HSSFCellStyle.ALIGN_CENTER,"测试封装方法");
		
		try {
			FileOutputStream out=new FileOutputStream("C:\\tmp\\t.xls");
			wb.write(out);
			out.close();
			
		} catch ( IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		

	}

}
