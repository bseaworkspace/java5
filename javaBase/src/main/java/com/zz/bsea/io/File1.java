package com.zz.bsea.io;

import java.io.File;
import java.io.IOException;

/**
 * 
 * @author bsea
 *
 */
public class File1 {
	
	/**
	 * 创建一个文件
	 * 
	 */
	
	public static void createFile(){
		//File f=new File("C:\\bsea\\tmp\\test.txt");
		File f=new File("C:"+File.separator+"bsea"+File.separator+"tmp"+File.separator+"test.txt");
		
		try {
			f.createNewFile();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	/**
	 * 删除一个文件
	 * 
	 */
	
	public static void deleteFile(){
		//File f=new File("C:\\bsea\\tmp\\test.txt");
		File f=new File("C:"+File.separator+"bsea"+File.separator+"tmp"+File.separator+"test.txt");
		if(f.exists()){
			f.delete();
		}else{
			System.out.println("文件不存在，不能完成删除");
			
		}
	}
	/**
	 * 创建一个文件夹
	 */
	
	public static void createFolder(){
		//File f=new File("C:\\bsea\\tmp\\test.txt");
		File f=new File("C:"+File.separator+"bsea"+File.separator+"tmp2");
		f.mkdir();
	}
	/**
	 * 一次创建多个文件夹
	 */
	
	public static void createFolders(){
		//File f=new File("C:\\bsea\\tmp\\test.txt");
		File f=new File("C:"+File.separator+"bsea"+File.separator+"tmp3"+File.separator+"tmp4");
		f.mkdirs();
	}

	/*
	 * 读取文件下，下面的文件。 不能读取子文件夹下面的文件
	 * 
	 */
	public static void listFile(){
		File f=new File("C:"+File.separator+"bsea"+File.separator+"zztask");
		File[] fs=f.listFiles();
		for(File f1:fs){
			System.out.println(f1);
			if(f1.isDirectory()){
				System.out.println(f1+" 这是一个文件夹");
				
			}
			
		}
	}
	/**
	 * 读取全部文件，包含子文件下面的文件
	 * 
	 */
	public static void listAllFile(File f){
		
		if(f!=null){
			if(f.isDirectory()){
				File[] fs=f.listFiles();
				for(File f1:fs){
					listAllFile(f1);//自己调用自己。递归算法
				}
				
			}else{
				
				System.out.println(f);
			}
			
		}else{
			
			System.out.println("文件为空");
		}
		
	}
	
	
	
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		createFile();
		//createFolders();
		//deleteFile();
		//listFile();
		//File f=new File("C:"+File.separator+"bsea"+File.separator+"zztask");
		//listAllFile(f);
	}

}
