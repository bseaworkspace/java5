package com.zz.bsea.io;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.Writer;

public class ReaderTemplate {
	/**
	 * 字节流 读取内容
	 * 
	 * FileInputStream
	 * 
	 */
	public static void read1(){
		File f=new File("C:"+File.separator+"bsea"+File.separator+"tmp"+File.separator+"test.txt");
		try {
			FileInputStream fs=new FileInputStream(f);
			byte[] b=new byte[(int)f.length()];
			fs.read(b);
			fs.close();
			System.out.println(new String(b));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static void read2(){
		File f=new File("C:"+File.separator+"bsea"+File.separator+"tmp"+File.separator+"test.txt");
		try {
			Reader r=new FileReader(f);
			char[] c=new char[(int)f.length()];
			r.read(c);
			r.close();
			System.out.println(new String(c));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	public static void read3(){
		File f=new File("C:"+File.separator+"bsea"+File.separator+"tmp"+File.separator+"test2.txt");
		try {
			FileInputStream fi=new FileInputStream(f);
			Reader r=new InputStreamReader(fi,"utf-8");
			BufferedReader br=new BufferedReader(r);
			while(br.ready()){
			String str=br.readLine();
			System.out.println(str);
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		//read1();
		//read2();
		read3();
	}

}
