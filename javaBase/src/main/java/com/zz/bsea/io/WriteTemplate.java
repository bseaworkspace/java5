package com.zz.bsea.io;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;

public class WriteTemplate {

	/**
	 * 字节流读取文件内容。
	 * 
	 * FileOutputStream
	 * 
	 * 
	 */
	public static void write1(){
		File f=new File("C:"+File.separator+"bsea"+File.separator+"tmp"+File.separator+"test.txt");
		try {
			FileOutputStream fs=new FileOutputStream(f);
			//  \r\n 会换行
			String str="hello java5 haha\r\nha1";
			byte[] b=str.getBytes();
			fs.write(b);
			fs.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/**
	 * 字符流，写入内容
	 * Writer
	 */
	public static void write2(){
		File f=new File("C:"+File.separator+"bsea"+File.separator+"tmp"+File.separator+"test.txt");
		try {
			Writer w=new FileWriter(f);
			String str="hello zz";
			w.write(str);
			w.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	/**
	 * 字符流，写入内容
	 * Writer
	 */
	public static void write3(){
		File f=new File("C:"+File.separator+"bsea"+File.separator+"tmp"+File.separator+"test.txt");
		try {
			//如果第二个参数是true，就不会覆盖，新内容会在后面追加。
			Writer w=new FileWriter(f,true);
			String str="2017";
			w.write(str);
			w.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		//write1();
		//write2();
		write3();
	}

}
