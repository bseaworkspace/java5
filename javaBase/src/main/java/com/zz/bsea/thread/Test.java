package com.zz.bsea.thread;

public class Test {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
			System.out.println("start");
			//count();
			long start=System.currentTimeMillis();
			Demo1 d1=new Demo1();
			Thread t1=new Thread(d1);
			t1.setName("demo1");
			t1.start();
			
			Demo3 d3=new Demo3();
			Thread t3=new Thread(d3);
			t3.setName("demo3");
			t3.start();
			long end=System.currentTimeMillis();
			
			Demo2 d2=new Demo2();
			d2.setName("demo2");
			d2.start();
			Demo4 d4=new Demo4();
			d4.setName("demo4");
			d4.start();
			
			System.out.println("当前线程的名字："+Thread.currentThread().getName());
			System.out.println("当前线程的getPriority："+Thread.currentThread().getPriority());
			//线程的优先等级，最高是10 ，最小是1
			System.out.println("当前线程的MAX_PRIORITY："+Thread.currentThread().MAX_PRIORITY);
			System.out.println("当前线程的MIN_PRIORITY："+Thread.currentThread().MIN_PRIORITY);
			System.out.println("消耗时间："+(end-start));
			
	}
	
	
	public static void count(){
		
		for(int i=1;i<101;i++){
			System.out.println(i);
		}
		
	}
	
	static String name="tt";
	public synchronized static String getName(){
		String threadName=Thread.currentThread().getName();
		System.out.println("getName 当前线程"+threadName);
		if("demo2".equals(threadName)){
			try {
				Thread.currentThread().wait();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		if("demo4".equals(threadName)){
			Thread.currentThread().notifyAll();
		}
		System.out.println("getName2 当前线程"+Thread.currentThread().getName());
		return name;
	}
	public synchronized static void setName(String name1){
		System.out.println("setName 当前线程"+Thread.currentThread().getName());
		name=name1;
	}

	

}
