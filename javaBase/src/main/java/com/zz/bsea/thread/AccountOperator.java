package com.zz.bsea.thread;

public class AccountOperator implements Runnable{

	 private Account account;
	   public AccountOperator(Account account) {
	      this.account = account;
	   }

	   public void run() {
	      synchronized (account) {
	    	  System.out.println(Thread.currentThread().getName() + "--start--:" + account.getBalance());
	    	  if("t4".equals(Thread.currentThread().getName())){
	    		  
	    		  account.notify();
	    	  }else{
	    		  try {
	  				account.wait();
	  			} catch (InterruptedException e) {
	  				// TODO Auto-generated catch block
	  				e.printStackTrace();
	  			}
	    		  
	    		  }
	    	
	         account.deposit(500);
	         account.withdraw(500);
	         System.out.println(Thread.currentThread().getName() + ":" + account.getBalance());
	      }
	   }

}
