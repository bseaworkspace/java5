package com.zz.bsea.thread;

public class AccountOperator2 implements Runnable{

	 private Account account;
	   public AccountOperator2(Account account) {
	      this.account = account;
	   }

	   public void run() {
	         account.deposit(500);
	         account.withdraw(500);
	         System.out.println(Thread.currentThread().getName() + ":" + account.getBalance());
	   }

}
