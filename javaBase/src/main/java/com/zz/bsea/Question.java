package com.zz.bsea;

public class Question {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println(computer(5));
	}
	
	/**
	 * 如果昨天是明天，今天是星期五，问今天是星期几？
	 */
	static String computer(int a){
		if(a>2){
			return "今天是星期"+(a-2);
		}else{
			return "今天是星期"+(a+5);
		}
	}
}
