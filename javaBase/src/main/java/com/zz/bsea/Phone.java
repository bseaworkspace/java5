package com.zz.bsea;

public interface Phone {
	
     public void call(String phoneNumber); 
     
     public boolean sendMessage(String content);
     
}
