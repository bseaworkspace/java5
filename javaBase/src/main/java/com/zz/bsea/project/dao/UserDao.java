package com.zz.bsea.project.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.zz.bsea.dao.BaseDao;
import com.zz.bsea.model.User;

public class UserDao {
	
	 BaseDao baseDao=new BaseDao();
	 
	 /**
	  * 分页中，
	  * 获取每页的记录。
	  * 
	  * 
	  * @param startIndex
	  * @param pageConut
	  * @return
	  */
	 public List<User> getPageRecords(int startIndex,int pageConut){
		 List<User> ls=new ArrayList();
		 String sql="select u_name,u_pwd,u_phone from user limit ?,? ";
		 Connection con=baseDao.getConnection();
		 try {
			PreparedStatement ps=con.prepareStatement(sql);
			ps.setInt(1, startIndex);
			ps.setInt(2, pageConut);
			ResultSet rs=ps.executeQuery();
		    while(rs.next()){
		    	User u=new User();
		    	u.setName(rs.getString("u_name"));
		    	u.setPhoneNumber(rs.getString("u_phone"));
		    	u.setPwd(rs.getString("u_pwd"));
		    	ls.add(u);
		    }
			baseDao.closeQuery(rs, ps, con);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		 
		 return ls;
		 
	 }
	 public int getTotalRecords(){
		 int totalCount=0;
		 String sql="select count(*) from user";
		 Connection con=baseDao.getConnection();
		 try {
			 PreparedStatement ps=con.prepareStatement(sql);
			 ResultSet rs=ps.executeQuery();
			 while(rs.next()){
				 totalCount=rs.getInt(1);
			 }
			 baseDao.closeQuery(rs, ps, con);
		 } catch (SQLException e) {
			 // TODO Auto-generated catch block
			 e.printStackTrace();
		 }
		 return totalCount;
	 }

	 public static void main(String[] args){
		 UserDao udao=new UserDao();
		 
		 System.out.println("总记录数: "+udao.getTotalRecords());
		 
		 int totalPages=0;
		 
		 if(udao.getTotalRecords()%4>0){
			 totalPages=udao.getTotalRecords()/4;
			 totalPages++;
			 
		 }else{
			 
			 totalPages=udao.getTotalRecords()/4;
		 }
		 
		 System.out.println("总页数: "+totalPages);
		 /**
		  * 
		  * 对于有些选择分支结构,可以使用简单的条件运算符来代替. 如:
if(a<b)
    min=a;
else
    min=b;
可以用下面的条件运算符来处理
min=(a<b)?a:b;

　　其中"(a<b)?a:b"是一个"条件表达式",它是这样执行的:　　如果a<b为真,则表达式取a值,否则取b值.
　　条件运算符由两个符号组成"?"和":", 要求有3个操作对象,所以也叫它三目运算符,它是C语言中唯一的三目运算符.
　　　　它的一般形式为:
　　　　　　　　表达式1?表达式2:表达式3;
　　以下是关于条件运算符的几点说明:
		  * 
		  */
		 
		 int totalPages2=udao.getTotalRecords()%4>0?(udao.getTotalRecords()/4)+1:udao.getTotalRecords()/4;
		 System.out.println("总页数2: "+totalPages2);
		 
		 int pageNumber=3;
		 int startIndex=(pageNumber-1)*4;
		 System.out.println("每页的起始位置: "+startIndex);
		 
	 }
}
