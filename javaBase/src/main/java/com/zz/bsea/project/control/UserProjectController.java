package com.zz.bsea.project.control;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.zz.bsea.model.User;
import com.zz.bsea.project.dao.UserDao;

/**
 * Servlet implementation class UserProjectController
 * @author bsea
 */
public class UserProjectController extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserProjectController() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String curentPage=request.getParameter("page");
		System.out.println("当前页="+curentPage);
		int curpage=1;
		if(curentPage!=null){
			//把String 转换成 int
			curpage=Integer.parseInt(curentPage);
		}
		UserDao udao=new UserDao();
		//获取页的记录
		 int startIndex=(curpage-1)*4;
		List<User> ls=udao.getPageRecords(startIndex, 4);
		request.setAttribute("records", ls);
		//获取总的页数
		int totalPages2=udao.getTotalRecords()%4>0?(udao.getTotalRecords()/4)+1:udao.getTotalRecords()/4;
		System.out.println("totalPages2--"+totalPages2);
		request.setAttribute("curpage", curpage);
		request.setAttribute("pageConut", totalPages2);
		request.getRequestDispatcher("/bsea/porjectUserList.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String name=request.getParameter("name");
		if(name.equals("1")){
			response.getWriter().append("Y");
			
		}else{
			
			response.getWriter().append("N");
		}

	}

}
