package com.zz.bsea.control;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.zz.bsea.dao.UserDao;
import com.zz.bsea.model.User;

/**
 * 把一个普通的class变成一个servlet.
 * 
 * 第一步： 继承httpServlet
 * 第二步： 重写doGet 和doPost方法。
 *  * 
 * @author jiyu
 *
 *
 *
 *http://localhost:8080/zz_java5/UserController
 */
public class UserController extends HttpServlet {

	protected void doGet(HttpServletRequest req,HttpServletResponse res) throws ServletException, IOException{
		String i=req.getParameter("number");
		System.out.println("userController-----"+i);
		
		
		/**
		 * 
		 * JSP共有以下9种基本内置组件（可与ASP的6种内部组件相对应）： 
			1、request 用户端请求，此请求会包含来自GET/POST请求的参数
			2、response 网页传回用户端的回应
			3、pageContext 网页的属性是在这里管理
			4、session 与请求有关的会话期
			5、application servlet 正在执行的内容
			6、out 用来传送回应的输出
			7、config servlet的构架部件
			8、page JSP网页本身
			9、exception 针对错误网页，未捕捉的例外
		 * 
		 * 
		 * 
		 */
		HttpSession session=req.getSession(true);
		//模拟dao层 开始
		List<User> ls=new ArrayList();
		
		User u1=new User();
		u1.setName("玫瑰");
		u1.setId(1);
		u1.setPhoneNumber("135424555");
		User u2=new User();
		u2.setName("玫瑰1");
		u2.setId(2);
		u2.setPhoneNumber("177424546");
		User u3=new User();
		u3.setName("玫瑰2");
		u3.setId(3);
		u3.setPhoneNumber("2342423453");
		User u4=new User();
		u4.setName("玫瑰3");
		u4.setId(4);
		u4.setPhoneNumber("234234");
		User u5=new User();
		u5.setName("玫瑰4");
		u5.setId(5);
		u5.setPhoneNumber("675765765");
		
		ls.add(u1);
		ls.add(u2);
		ls.add(u3);
		ls.add(u4);
		ls.add(u5);
		
		
		
		//模拟dao层 结束
		
		session.setAttribute("us", ls);
		/**
		 * servelt中， / 包含了项目的名字。
		 * 
		 * jsp中， / 没有包含项目名字。
		 * 
		 */
		
		req.getRequestDispatcher("/bsea/bootstrap/userList.jsp").forward(req, res);
		
	}
	protected void doPost(HttpServletRequest req,HttpServletResponse res) throws ServletException, IOException{
		 System.out.println(req.getParameter("name")+"----"+req.getParameter("phoneNumber")+" ---"+req.getParameter("id3"));
		 /**
		  * to do  
		  * 
		  *把上面得到的name和phonnumber 更新到数据库
		  * 
		  */
		 
		 //数据库更新成功就是Y， 更新失败就是N
		 String result="Y";
		 res.getWriter().append(result);
	}
	
}
