package com.zz.bsea.control;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.zz.bsea.model.User;
import net.sf.json.JSONArray;  
import net.sf.json.JSONObject;

/**
 * Servlet implementation class TestJson
 */
public class TestJson extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public TestJson() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		 response.setContentType("text/html");
	        System.out.println("into doPost");
	        
	        //用map来接收request发送过来的多维数组
	        Map map = request.getParameterMap();
	        Iterator<String> iter = map.keySet().iterator();
	        while (iter.hasNext()) {
	            String key = iter.next();
	            System.out.println("key=" + key );
	            String[] value =  (String[]) map.get(key);
	            System.out.print("value=");
	            for(String v:value){                
	                System.out.print(v + "  ");
	            }            
	            System.out.println();
	        }        
	        
	        String sName= request.getParameter("name");//得到ajax传递过来的paramater  
	        System.out.println(sName);

	        PrintWriter printWriter = response.getWriter();  
	        List list = new ArrayList();//传递List  
	        
	        Map m=new HashMap();//传递Map      
	        User u1=new User();  
	       u1.setName("n1");
	       u1.setPwd("123");
	        User u2=new User();  
	        u2.setName("n2");
		       u2.setPwd("1235555");   
	        list.add(u1); //添加User对象        
	        list.add(u2);     //添加User对象    
	        
	        m.put("u1", u1);  
	        m.put("u2", u2);     
	          
	        JSONArray jsonArray2 = JSONArray.fromObject( list );  
	        //把java数组转化成转化成json对象   
	        //JSONObject jsonObject =JSONObject.fromObject(m);//转化Map对象  
	        printWriter.print(jsonArray2);//返给ajax请求  
	        //printWriter.print(jsonObject);//返给ajax请求  
	        printWriter.flush();
	        printWriter.close();
	        System.out.println("finish");
	}

}
