package com.zz.bsea.control;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import com.zz.bsea.model.User;
import com.zz.bsea.tool.excel.ExcelUtil;

import net.sf.json.JSONArray;  
import net.sf.json.JSONObject;

/**
 * Servlet implementation class TestJson
 */
public class ExportExcelController extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ExportExcelController() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		 response.setContentType("text/html");
	        System.out.println("into doPost");
	        HSSFWorkbook wb=new HSSFWorkbook();
			
			HSSFSheet s1=wb.createSheet();
			
			 HSSFRow r0=s1.createRow(0);
			 ExcelUtil.cteateCell(wb,r0,0,HSSFCellStyle.ALIGN_CENTER,"prodName");
			 ExcelUtil.cteateCell(wb,r0,1,HSSFCellStyle.ALIGN_CENTER,"prodDT");
			 ExcelUtil.cteateCell(wb,r0,2,HSSFCellStyle.ALIGN_CENTER,"prodST");
	        //用map来接收request发送过来的多维数组
	        Map map = request.getParameterMap();
	        Iterator<String> iter = map.keySet().iterator();
	        int count=0;
	        int countrow=1;
	        HSSFRow r=null;
	        String fielname="";
	        while (iter.hasNext()) {
	            String key = iter.next();
	            System.out.println("key=" + key );
	            String colum1=key.substring(key.indexOf("][")+2,key.length()-1);
	            System.out.println("colum1=========="+colum1);
	            String[] value =  (String[]) map.get(key);
	            System.out.print("value=");
	            String val="";
	            for(String v:value){                
	                System.out.print(v + "  ");
	                val=v;
	            } 
	            if(count%3==0){
		            r=s1.createRow(countrow);
		            countrow++;
		            count=0;
	            }
				ExcelUtil.cteateCell(wb,r,count,HSSFCellStyle.ALIGN_CENTER,val);
				count++;
				
	            System.out.println();
	        }        
	        
	        
	        try {
	        	Date d=new Date();
	        	fielname=d.getTime()+".xls";
	        	String path = this.getServletContext().getRealPath("/bsea/resources/"+fielname);
				FileOutputStream out=new FileOutputStream(path);
				wb.write(out);
				out.flush();
				out.close();
				
			} catch ( IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	        request.setCharacterEncoding("utf-8");
			response.setCharacterEncoding("utf-8");
			 PrintWriter printWriter = response.getWriter();  
			 String path2 = this.getServletContext().getRealPath("/bsea/resources/"+fielname);
			printWriter.print(path2);
		    printWriter.flush();
		    printWriter.close();
	}
	
	
	public void download1(HttpServletResponse response,String fileName) throws IOException{  
        //获取所要下载文件的路径  
          String path = this.getServletContext().getRealPath("/bsea/resources/"+fileName);  
          System.out.println("path==="+path);
          String realPath = path.substring(path.lastIndexOf("\\")+1);  
          System.out.println("realPath==="+realPath);
         //告诉浏览器是以下载的方法获取到资源  
         //告诉浏览器以此种编码来解析URLEncoder.encode(realPath, "utf-8"))  
       response.setHeader("content-disposition","attachment; filename="+URLEncoder.encode(realPath, "utf-8"));  
        //获取到所下载的资源  
          FileInputStream fis = new FileInputStream(path);  
          int len = 0;  
           byte [] buf = new byte[1024];  
           while((len=fis.read(buf))!=-1){  
               response.getOutputStream().write(buf,0,len);  
           }  
      }

}
