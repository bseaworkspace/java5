package com.zz.bsea.control;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.zz.bsea.dao.UserDao;
import com.zz.bsea.model.User;

/**
 * Servlet implementation class LoginController
 */
public class LoginController extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public LoginController() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		request.setCharacterEncoding("utf-8");
		response.setCharacterEncoding("utf-8");
		
		String name=request.getParameter("username");
		String pwd=request.getParameter("pwd");
		UserDao userDao=new UserDao();
		List<User> ls=userDao.getUserByName(name);
		List<User> ls2=new ArrayList();
		String responseMessage="";
		/**
		 *   /开头的，就是绝对路径。else就是相对路径
		 * 
		 */
		String path="/bsea/login.jsp";
		//System.out.println("ls.get(0).getPwd()="+ls.get(0).getPwd());
		if(ls.size()==0){
			responseMessage="用户名不存在";
			request.setAttribute("rs", responseMessage);
		}else if(pwd.equals(ls.get(0).getPwd())){
			//ls2=userDao.getExamSummary();
			//整个系统的代码代码， 应该只有登录的时候，会把
			//用户的信息存在session里面。
			//其他地方都只能从 session.getAttribute("user")里面
			//得到user对象，来获取user的信息。
			User user=new User();
			user=ls.get(0);
			HttpSession session=request.getSession();
			session.setAttribute("user", user);
			//------------登录功能，必须完成user放到session的步骤。
			
			
			User u1=new User();
			u1.setName("testName1");
			u1.setPhoneNumber("2342432");
			u1.setTotal_amt(100);
			User u2=new User();
			u2.setName("testName1");
			u2.setPhoneNumber("2342432");
			u2.setTotal_amt(100);
			ls2.add(u1);
			ls2.add(u2);
			request.setAttribute("userList", ls2);
			responseMessage="登录成功";
			path="bsea/showExamSummary.jsp";
			
		}else{
			responseMessage="密码错误";
		}
		System.out.println("用户名="+name+" 密码是："+pwd);
		//设置response.setContentType指定 HTTP 响应的编码,同时指定了浏览器显示的编码. 
		response.setContentType("text/html;charset=utf-8");
		//response.getWriter().append(responseMessage);
		//servlet的请求转发的方式，实现页面跳转
		System.out.println("path==="+path+"--responseMessage---"+responseMessage);
		request.getRequestDispatcher(path).forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
