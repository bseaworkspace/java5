package com.zz.bsea.control;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class FormController
 */
public class FormController extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public FormController() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String name=request.getParameter("userName");
		String pwd=request.getParameter("pwd");
		String sex=request.getParameter("sex");
		String[] like=request.getParameterValues("like");
		String sel=request.getParameter("sel");
		String contents=request.getParameter("contents");
		System.out.println(name);
		System.out.println(pwd);
		System.out.println(sex);
		System.out.println(like[0]);
		System.out.println(like[1]);
		System.out.println(sel);
		System.out.println(contents);
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String userName=request.getParameter("username");
		System.out.println("username------"+userName);
	     Map<String, String> m1=new HashMap();
	     m1.put("id", "1");
	     m1.put("name", "teamNama1");
	     Map<String, String> m2=new HashMap();
	     m2.put("id", "2");
	     m2.put("name", "teamNama2");
	     List<Map> ls=new ArrayList();
	     ls.add(m1);
	     ls.add(m2);
	     String rs="";
	     for(Map map:ls){
	    	 
	    	 rs=rs+"#<option value='"+map.get("id")+"'>"+map.get("name")+"</option>";
	    	
	     }
	     System.out.println(rs);
		response.getWriter().append(rs);
	}

}
