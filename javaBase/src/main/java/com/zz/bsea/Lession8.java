package com.zz.bsea;

public class Lession8 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		//数组 
		/**
		 * int[] 表示声明的是int类型的数组
		 * 
		 * new int[3] 其中3表示数组的长度也就是这个数字最多能存3个元素。
		 * 
		 */
		int[] a=new int[3];
		a[0]=12;
		a[1]=34;
		a[2]=32;
		
		//二维数组
		/**
		 * int[3][2] 其中3表示第一维的数组长度是3， 
		 * 其中2表示第二维数组的长度。
		 * 也就是这个二维数组，可以存3个数组，
		 * 每个数组可以存2个int类型的值
		 * 
		 */
		int[][] b=new int[3][2];
		
		b[0][0]=121;
		b[0][1]=122;
		b[1][0]=123;
		b[1][1]=124;
		b[2][0]=125;
		b[2][1]=126;
		
		
		for(int i=0;i<a.length;i++){
			System.out.println("a-->"+i+"-->"+a[i]);
			
		}
		
		
		for(int i=0;i<b.length;i++){
			
			for(int j=0;j<b[i].length;j++){
				System.out.println("b-->"+i+"-->"+j+"-->"+b[i][j]);
				
			}
			
		}
		
		
		

	}

}
