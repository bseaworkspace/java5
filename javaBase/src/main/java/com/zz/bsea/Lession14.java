package com.zz.bsea;

import java.util.ArrayList;
import java.util.List;

public class Lession14 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
        int[] a=new int[7];
		
		a[0]=121;
		a[1]=132;
		a[2]=1231;
		a[3]=52;
		a[4]=72;
		a[5]=925;
		a[6]=552;
		
		//数组的常用方法
		
		
		//length属性， 表示数组的长度，即数组包含多少个元素。
		int l=a.length;
		//表是下标是3的位置上的值
		int b=a[3];
		
		
		List list=new ArrayList();
		
	
		
		//list常用方法
		
		//size()方法，返回list的长度。
		int c=list.size();
		
		//add输入参数是Object，什么都可以添加。
		list.add("sdfjsldfjs");
		Lession14 l14=new Lession14();
		list.add(l14);
		
		list.add(1);
		
		System.out.println(list.get(1));
		//add(index, object) 
		//如果index已经有值了，就会替换。
		list.add(1, 88);
		
		System.out.println(list.get(1));
		
		System.out.println(list.contains("sdfdfjs"));
		
		//String
		
		String sss="sdfsdf";
		//replace原来的string 不动， 会替换以后，返回新的string
		String t=sss.replace("s", "a");
		
		System.out.println(t);
		
		

	}

}
