package com.zz.bsea;

/**extends是表示继承，后面跟的是父类的名字
 * 
 * 1.java是单继承。只能继承个父类
 * 
 * 2.继承以后，子类会自动拥有父类的所有属性和方法
 * 
 * 
 * @author zizaitianyuan
 *
 */
public class BussCar extends Car {
	String no;
	static{
		System.out.println("3");
		
	}
	//因为子类的构造方法都会调用父类的构造方法。
	public BussCar(){
		super("c");
		System.out.println("4");
	}
	
	public BussCar(String name){
		System.out.println("6");
	}
	//方法的重写
	/**
	 * 1. 必须是父类和子类之间，子类重写父类的方法
	 * 
	 * 2. 方法名字必须一样。
	 * 
	 * 3. 参数的个数，和类型必须一样。
	 * 
	 * 4.返回的数据类型，也必须一样，否则会报编译错误。
	 * 
	 */
	public void run(int a){
		System.out.println("这辆公交车+"+no+"车以"+a+"公里每小时的速度在跑");
		
	}
	
	
	public boolean openDoor(String way,String a){
		System.out.println("车以"+way+"方法打开了门");
		return false;
	}
}
