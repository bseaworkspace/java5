package com.zz.bsea.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.zz.bsea.model.User;

public class IndexFillter implements Filter {

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
		// TODO Auto-generated method stub
		System.out.println("IndexFillter  ---init");
		
	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		// TODO Auto-generated method stub
		System.out.println("IndexFillter  ---doFilter");
		HttpServletRequest request1=(HttpServletRequest)request;
		HttpSession session=request1.getSession();
		User u=(User) session.getAttribute("user");
		System.out.println("getLocalPort"+request.getLocalPort());
		System.out.println("getContextPath"+request.getServletContext().getContextPath());
		System.out.println("getRemoteAddr"+request.getRemoteAddr());
		System.out.println("getServletPath"+request1.getServletPath());
		request.setCharacterEncoding("utf-8");
		response.setCharacterEncoding("utf-8");
		if(request1.getServletPath().indexOf("bsea")==-1&&!("/LoginControllerBsea".equals(request1.getServletPath()))&&u==null&&!("/FormController".equals(request1.getServletPath()))){
			request.getRequestDispatcher("/bsea/login.jsp").forward(request, response);
			
		}else{
			response.setContentType("text/html;charset=utf-8");
			//表示把请求，传递下去。如果有filter就
			//执行下一个filter，如果没有了filter就去找真正的web资源。
			chain.doFilter(request, response);
		}
		
		
	}

	@Override
	public void destroy() {
		// TODO Auto-generated method stub
		System.out.println("IndexFillter  ---destroy");
		
	}

}
