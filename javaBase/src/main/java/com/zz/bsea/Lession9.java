package com.zz.bsea;

public class Lession9 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		//java是一门面向对象的语言。
		/**
		 * 在现实世界中的一些事物，可以转化成java代码描述。
		 * 
		 * 比如“那里坐了一个穿红衣服的人”
		 * 这里我们如果用java语言描述，
		 * 类的名字是“人”
		 * 类有一个属性（成员变量）是“红衣服”
		 * 类的动作是“坐” 
		 *
		 * 
		 * 
		 */
		//new是一个关键字，表示要创建一个具体的对象。
		//这里，就是创建一个具体的人。
		//new后面跟的是构造方法,构造方法跟类的名字是一样的
		Persion p=new Persion();
		p.coth_color="red";
		
		/**
		 * 生产红色车的工厂
		 * 
		 * 名词： 车，工厂。---》类
		 * 动词：生产---》方法
		 * 形容词：红色--》属性（成员变量）
		 * 
		 */
	}

}


