package com.zz.bsea;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class UserDao {
	 BaseDao baseDao=new BaseDao();
	 
	 /**
	  * 
	  * public List<User> getUserByName(String name)
	  * 
	  * 
	  * @param name
	  * @return
	  */
	 public List<User> getUserByName(String name){
		 List<User> ls=new ArrayList();
		 //select * from user where name=name
		 /**
		  *  * 1.通过Class.forName加载驱动)
		 * 2.通过DriverManager 获取连接
		 * 3.通过Connection获取 Statement.
		 * 4.通过statement执行sql语句。
		 * 5.倒序关闭所有连接。
		  * 
		  * 
		  */
		 //1.通过Class.forName加载驱动) 2.通过DriverManager 获取连接
		 Connection con=baseDao.getConnection();
		 try {
			 //3.通过Connection获取 Statement.
			PreparedStatement ps=con.prepareStatement("select * from user where name='"+name+"'");
			//4.通过statement执行sql语句。
			ResultSet rs=ps.executeQuery();
			
			while(rs.next()){
				User u=new User();
				u.setId(rs.getInt("id"));
				u.setName(rs.getString("name"));
				u.setPhoneNumber(rs.getString("phoneNumber"));
				u.setScore(rs.getFloat("score"));
				u.setPwd(rs.getString("pwd"));
				ls.add(u);
				
			}
			//5.倒序关闭所有连接。
			baseDao.closeQuery(rs, ps, con);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		 return ls;
		 
	 }
	//boolean 意思是，这个方法添加用户到数据库，是不是成功，
	 //如果成功，就返回true,失败就返回false;
	 
	 /**
	  * 
	  * 输入参数是User类型的形参 u
	  * 表是，页面上用户输入的信息，都set到了這个u
	  * 然后，方法里面，就可以通过u的get方法，
	  * 得到用户输入的
	  * 值。
	  * 
	  * @param u
	  * @return
	  */
	public boolean addUser(User u){
		boolean flag=false;
		/**
		 * 每一次跟数据库打交道，就是
		 * 要进行数据库的查询，或者修改等
		 * 都必须完成jdbc连接的几个步骤。
		 * 
		 * 1.通过Class.forName加载驱动)
		 * 2.通过DriverManager 获取连接
		 * 3.通过Connection获取 Statement.
		 * 4.通过statement执行sql语句。
		 * 5.倒序关闭所有连接。
		 * 
		 * 
		 */
		//1.通过Class.forName加载驱动)  2.通过DriverManager 获取连接
		//baseDao.getConnection()封装了第一和第二步。
		Connection con=baseDao.getConnection();
		/**insert into user (id, name,pwd) values(?,?,?)
		 * ？表示占位符，意思是这个地方，
		 * 会被输入参数的值代替。
		 * 
		 * 
		 */
		String sql="insert into user (id, name,pwd) values(?,?,?)";
		try {
			// 3.通过Connection获取 Statement.
			PreparedStatement ps=con.prepareStatement(sql);
			ps.setInt(1, u.getId());
			/**
			 *
			 * 
			 * PreparedStatement的setString方法，
			 * 第一个参数是位置（下标），起始位置是从1开始。
			 * 第二个参数是输入参数的值，也就是真正sql语句
			 * 里面的值。
			 * 
			 * 
			 * ps.setString(2, u.getName())
			 * 
			 * 表示： 第二个？的地方，
			 * 我们要用u.getName()的值替换。
			 * 
			 * 
			 */
			ps.setString(2, u.getName());
			ps.setString(3, u.getPwd());
			// 4.通过statement执行sql语句。
			int r=ps.executeUpdate();
			if(r>0){
				flag=true;
			}
			System.out.println("addUser---"+flag);
			//5.倒序关闭所有连接。
			baseDao.closeUpdate(ps, con);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return flag;
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		UserDao udao=new UserDao();
		
		List<User> ls=udao.getUserByName("毛屹嘉");
		if(ls.size()==0){
			System.out.println("用户名不存在");
		}
		
		boolean flag=false;
		String pwd="111111";
		for(User u1:ls){
			
			System.out.println("userdao -->"+u1.getScore());
			if(pwd.equals(u1.getPwd())){
				flag=true;
				
			}
		}
		
		if(flag){
			System.out.println("登陆成功");
			
		}else{
			
			System.out.println("用户名或者密码错误");
		}

	}

}
