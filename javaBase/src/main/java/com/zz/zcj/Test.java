package com.zz.zcj;

public class Test {
	public static void main(String[] args) {
		//普通的成员变量必须要new出对象，才能访问对象的属性类
		maopao m=new maopao();
		System.out.println(m.name);
		maopao.name1="jack";
		System.out.println(maopao.name1);
		System.out.println(maopao.IDnum);
		
		maopao.test1();
		//final 被final修饰的变量不能再赋值
		Iphone iphone=new Iphone();
		iphone.call("17721411770");
		//判断连接符
		//1.==判断两个值是不是相等
		/*2。equals方法，判断引用类型的两个变量是否相等
		 * ==8个基本类型，比较的是栈里面的值
		 * String方法比较特殊，==也是比较值
		 * 其他引用类型，比较的是栈里面存的地址
		 */
	}
}
