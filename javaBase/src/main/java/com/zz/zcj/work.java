package com.zz.zcj;

public class work {

	private static final String way = null;

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		//java是一门面向对象的语言
		/**
		 * 在现实世界中的一些事物，可以转换成java代码描述
		 * 比如“那里坐了一个穿红衣服的人”
		 * 这里我们如果用java语言描述
		 * 类的名字是“人”
		 * 类有一个属性（成员变量）是“红衣服”
		 * 类的动作是“坐”
		 */
		
		//new是一个关键词，表示要创建一个具体的对象
		//这里就是创建一个具体的人
		//new后面跟的是构造方法，构造方法跟类的名字是一样的
		BusCar bc=new BusCar();
		bc.no="1096";
		bc.color="白色";
		bc.run(60);
		car c=new BusCar();
		work w=new work();
		BusCar b=new BusCar();
		b.no="1063";
		w.run(b, 100);
		
	}
	public void run(car c,int a){
		c.run(a);
	}

}
