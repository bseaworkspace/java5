package com.zz.zcj;

public interface Phone {
	public void call(String phoneNumber);
	public boolean sendMessage(String content);
	
}
