package com.zz.EDGE;

public class lessson18 {
	/**
	 * +	加法 - 相加运算符两侧的值	A + B 等于 30
-	减法 - 左操作数减去右操作数	A – B 等于 -10
*	乘法 - 相乘操作符两侧的值	A * B等于200
/	除法 - 左操作数除以右操作数	B / A等于2
％	取模 - 左操作数除以右操作数的余数	B%A等于0
++	自增: 操作数的值增加1	B++ 或 ++B 等于 21（区别详见下文）
--	自减: 操作数的值减少1	B-- 或 --B 等于 19（区别详见下文）

	 * 
	 */
	public static void computer(){
		System.out.println("---算术运算符---开始---");
		int a=20;
		int b=12;
		System.out.println("a+b="+(a+b));
		System.out.println("a-b="+(a-b));
		System.out.println("a*b="+(a*b));
		System.out.println("a/b="+(a/b));
		System.out.println("a%b="+(a%b));
		System.out.println("b%a="+(b%a));
		int c=78;
		System.out.println("c是不是偶数："+(c%2));
		
		System.out.println("a+=="+(a++));
		System.out.println("a--="+(a--));
		int d=a++;
		//先赋值在运算
		int g=++a;
		//先运算在赋值
		System.out.println("="+(a--));
		System.out.println("a--="+(a--));
		System.out.println("---算术运算符---结束---");
		
		
	}
	public static void booleanComputer(){
		/**
		 * 
		 * 
		 * 
		 */
		System.out.println("---结果一定是boolean--算术运算符---开始---");
		int a=100;
		int b=3;
		System.out.println("a==b	"+(a==b));
		System.out.println("a>b	"+(a>b));
		System.out.println("a<b	"+(a<b));
		System.out.println("a>=b	"+(a>=b));
		System.out.println("a,=b	"+(a<=b));
		System.out.println("a!=b	"+(a!=b));
		System.out.println("---算术运算符---结束---");
	}
	/**
	 * 下表列出了逻辑运算符的基本运算，假设布尔变量A为真，变量B为假
操作符	描述	例子
&&	称为逻辑与运算符。当且仅当两个操作数都为真，条件才为真。	（A && B）为假。
| |	称为逻辑或操作符。如果任何两个操作数任何一个为真，条件为真。	（A | | B）为真。
！	称为逻辑非运算符。用来反转操作数的逻辑状态。如果条件为true，则逻辑非运算符将得到false。	！（A && B）为真。

	 * 
	 * 
	 */
	public static void loginComputer(){
		System.out.println("--结果一定是boolean同时参与运算的运算也必须是boolean-逻辑运算符--");
		boolean a=true;
		boolean b=false;
		System.out.println("a="+a+ "b="+b);
		System.out.println("&&	称为逻辑与运算符。必须两边同时都是true才是true其他情况都是false");
		System.out.println("| |	称为逻辑或操作符 必须两边同时都是false才是false其他情况都是true");
		if(test2()&&test1()){
			System.out.println("test2()&&test1()=true");
		}else{
			System.out.println("test2()&&test1()=false");
		}
		
		if(test1()||test2()){
			System.out.println("test2()||test1()=true");
		}else{
			System.out.println("test2()||test1()=false");
		}
		System.out.println("!a"+(!a));
		System.out.println("!test1()"+(!test1()));
		System.out.println("---逻辑运算符---结束---");
	}
	
	public static boolean test1(){
		System.out.println("执行了test1，直接返回true");
		
		
		
		return true;
	}
	
	public static boolean test2(){
		System.out.println("执行了test2，直接返回false");
		
		
		
		return false;
	}
	
	public static void setingComputer(){
		/*
		 * 下面是Java语言支持的赋值运算符：
	操作符	描述	例子
	=	简单的赋值运算符，将右操作数的值赋给左侧操作数	C = A + B将把A + B得到的值赋给C
	+ =	加和赋值操作符，它把左操作数和右操作数相加赋值给左操作数	C + = A等价于C = C + A
	- =	减和赋值操作符，它把左操作数和右操作数相减赋值给左操作数	C - = A等价于C = C -
	 A
	* =	乘和赋值操作符，它把左操作数和右操作数相乘赋值给左操作数	C * = A等价于C = C * A
	/ =	除和赋值操作符，它把左操作数和右操作数相除赋值给左操作数	C / = A等价于C = C / A
	（％）=	取模和赋值操作符，它把左操作数和右操作数取模后赋值给左操作数	C％= A等价于C = C％A
	<< =	左移位赋值运算符	C << = 2等价于C = C << 2
	>> =	右移位赋值运算符	C >> = 2等价于C = C >> 2
	＆=	按位与赋值运算符	C＆= 2等价于C = C＆2
	^ =	按位异或赋值操作符	C ^ = 2等价于C = C ^ 2
	| =	按位或赋值操作符	C | = 2等价于C = C | 2

		 * 
		 * 
		 */
	}
	
	/**
	 * 
	 * 条件运算符也被称为三元运算符。该运算符有3个操作数，并且需要判断布尔表达式的值。该运算符的主要是决定哪个值应该赋值给变量。
variable x = (expression) ? value if true : value if false

	 * 
	 */
	public static void conditionalComputer(int b){
		System.out.println("---三元运算符---开始---");
		String result=(b%2==0)?"这是一个偶数":"这是一个奇数";
		System.out.println("result="+result);
		String result2=(b>90)?"优秀":(b>80)?"良好":(b>60)?"及格":"不及格";
		System.out.println("result2="+result2);
		System.out.println("---三元运算符---结束---");
	}
	
	public static void instanceOfComputer(){
		lessson18 t=new lessson18(); 
		boolean r=t instanceof lessson18;
		
		System.out.println("t的对象类型是不是lessson18  "+r);
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		computer();
		 booleanComputer();
		loginComputer();
		conditionalComputer(95);
		instanceOfComputer();
	}

}
