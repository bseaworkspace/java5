package com.zz.EDGE;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;

public class WriteTemplate {
/**
 * 字节流读取文件内容
 * 
 */
	public static void write1() {
		File f = new File("C:" + File.separator + "EDGE" + File.separator + "im" + File.separator + "test.txt");
		try {
			FileOutputStream fs=new FileOutputStream(f);
			// \r\n会换行
			String str="hello java5 h\r\nhh";
			byte[] b=str.getBytes();
			fs.write(b);
			fs.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/**
	 * 
	 * 字符流，写入内容
	 * Writer
	 */
	
	public static void write2(){
		File f = new File("C:" + File.separator + "EDGE" + File.separator + "im" + File.separator + "test.txt");
		try {
			Writer w=new FileWriter(f);
			String str="hello zz";
			w.write(str);
			w.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	public static void write3(){
		File f = new File("C:" + File.separator + "EDGE" + File.separator + "im" + File.separator + "test.txt");
		try {
			/**
			 * 如果第二个参数是true，就不会被覆盖，新内容就在后面被添加
			 * 
			 */
			Writer w=new FileWriter(f,true);
			String str="2017";
			w.write(str);
			w.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		//write1();
		//write2();
		write3();
	}

}
