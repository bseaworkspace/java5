package com.zz.EDGE;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class JDBCTest2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		 try {
		        //加载驱动
				Class.forName("com.mysql.jdbc.Driver");
		        //获取数据库连接  
			Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/java5", "root", "root");
                //获取statement
		        PreparedStatement ps=con.prepareStatement("update book set price=86 where id=1");
				// 执行sql以后，获取执行结果
		       ps.execute();
		       
		       //  关闭连接，必须从里到外关
		       //也就是先关resultSet，最后关connection
		      
		        if(ps!=null){
		        	ps.close();
		        }
		        if(con!=null){
		        	con.close();
		        }
		    
			} catch (ClassNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
				
				
				
	}

}
