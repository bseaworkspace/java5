package com.zz.EDGE;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class UserDao {
	BaseDao baseDao = new BaseDao();
/*
 * 
 * 
 * 
 */
	public List<User> getUserByname(String name) {
		List<User> ls = new ArrayList();
		
		Connection con = baseDao.getConnection();
		// select * from user where name =name
		try {
			PreparedStatement ps = con.prepareStatement("select * from user where u_name='" + name + "'");
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				User u = new User();
				u.setId(rs.getInt("u_id"));
				u.setName(rs.getString("u_name"));
				//u.setPhonenumber(rs.getString("phoneNumber"));
				//u.setScore(rs.getFloat("score"));
				u.setPwd(rs.getString("u_pwd"));
				ls.add(u);
			}
			
			baseDao.closequery(rs, ps, con);

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return ls;
	}

	// boolean意思是，这个方法添加用户到数据库，是不是成功
	// 如果成功，就返回true，失败就返回false;
	/*
	 * 输入参数是user类型的形参u 表示页面上用户输入的信息都set到了这个user中 然后方法里面就可以通过u的get方法 得到用户输入的值
	 * 
	 */
	public boolean addUser(User u) {
		boolean flag = false;
		/*
		 * 每一次跟数据库打交道，就是 要进行数据库的查询，或者修改等 都必须完成jdbc链接的几个步骤
		 * 
		 * 1.(通过Class.forName加载驱动) 2.(通过DriverManager获取链接)
		 * 3.通过Connection获取Statement。 4.通过Statement执行sql语句 5.倒序关闭所有连接。
		 * 
		 */
		// baseDao.getConnection()封装了第一和第二步。
		Connection con = baseDao.getConnection();
		/*
		 * "insert into user(id,name,pwd)values(?,?,?) ?表示占位符。意思是这个地方 会被输入的参数代替
		 * 占位符PreparedStatement
		 * 
		 * 
		 * 
		 */
		String sql = "insert into user(id,name,pwd)values(?,?,?)";
		try {
			// 3.通过Connection获取Statement。
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setInt(1, u.getId());
			/*
			 * 
			 * PreparedStatement的setString方法 第一个参数是位置(下标),起始位置从1开始
			 * 第一个参数是输入参数的值，也就是真正sql语句里面的值
			 * ps.setString(2, u.getName()); 表示第二个？的地方，我们要用u.getName()的值替换;
			 * 
			 */
			ps.setString(2, u.getName());
			ps.setString(3, u.getPwd());
			// 4.通过Statement执行sql语句
		int r=ps.executeUpdate();
		if(r>0){
			flag=true;
		}
			// 5.倒序关闭所有连接。
			baseDao.closeUpdate(ps, con);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return flag;
	}

	public static void main(String[] args) {
		UserDao udao = new UserDao();
		List<User> ls = udao.getUserByname("玫瑰");
		if (ls.size() == 0) {
			System.out.println("用户名不存在");
		}
		boolean flag = false;
		String pwd = "123";
		for (User u : ls) {
			System.out.println("userdao-->" + u.getScore());
			if (pwd.equals(u.getPwd())) {
				flag = true;
			}
		}
		if (flag) {
			System.out.println("登陆成功");
		} else {
			System.out.println("用户名或者密码错误");
		}
	}

}
