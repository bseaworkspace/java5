package com.zz.EDGE.control;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.zz.EDGE.dao.UserDao;
import com.zz.EDGE.model.User;

/**
 * Servlet implementation class LoginController
 */
public class LoginController extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public LoginController() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String name=request.getParameter("username");
		System.out.println("用户名2="+name);
		String pwd=request.getParameter("pwd");
		UserDao userDao=new UserDao();
		List<User>ls=userDao.getUserByname(name);
		
		
		
		String reponseMessage="";
		String path="edge/login.jsp";
		if(ls.size()==0){
			reponseMessage="用户名不存在";
		}else if(pwd.equals(ls.get(0).getPwd())){
			//ls2=userDao.getExamSummary(); 
			//整个系统的代码应该只有登陆的时候，会把用户的信息存在session里面
			//其他的地方都只能从session.getAttribute("user")里面
			//得到user对象，来获取user的信息
			User user=new User();
			user=ls.get(0);
			HttpSession session=request.getSession();
			session.setAttribute("user", user);
			//-----登录动能，必须完成user放到session的步骤。
			User u1=new User();
			u1.setName("testName1");
			u1.setPhonenumber("1212");
			u1.setTotal_amt(100);
			User u2=new User();
			u2.setName("testName1");
			u2.setPhonenumber("1212");
			u2.setTotal_amt(100);
			List<User> ls2=new ArrayList();
			ls2.add(u1);
			ls2.add(u2);
			
			
			System.out.println(ls2.get(0).getName());
			request.setAttribute("userList", ls2);
			reponseMessage="登陆成功";
			path="edge/showExamSummary.jsp";
		}else{
			reponseMessage="密码错误";
		}
		System.out.println("用户名="+name+"密码="+pwd);
		//设置response.setContentType指定 HTTP 响应的编码,同时指定了浏览器显示的编码. 
		response.setContentType("text/html;charset=utf-8");
		//response.getWriter().append(reponseMessage);
		//servlet的请求转发的方式，实现页面跳转forward
		request.getRequestDispatcher(path).forward(request, response);
	}

}
