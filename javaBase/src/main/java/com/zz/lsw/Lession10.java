package com.zz.lsw;
/*java面向对象的四个特点
 * 1.继承
 * 2.多态
 * 3.封装
 * 4.抽象 
 *
 * 
 * 
 * 
 */

public class Lession10 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		BussCar bc=new BussCar();
		
		bc.no="1096";
		bc.color ="白色";
		bc.run(60);
		
		Car c=new BussCar();
		
		//c.run(100);
		/*多态：通过继承实现了，同一个方法，在运行的时候
		 * 根据输入参数的不同的子类，产生不同的效果。
		 * 
		 */
		Lession10 l10=new Lession10();
		l10.run(c,100);

	}
	public void run(Car c,int a){
		c.run(a);
	}

}
