use java5;
-- select表示查询
-- *表示所有列
-- from 表示后面跟的是表的名字
select*from user;

select name,postword from user;

select*from user where name='玫瑰';

select*from user where id>5;
-- 模糊查询4开头
select*from user where postword like'%4%';
-- 模糊查询以3结尾的
select*from user where postword like'%3';
-- like表示模糊查询其中%表示零多个	
select*from us   where name like'%文%';
-- where 后面可以跟很多查询条件用and连接
select*from user where postword like'%4%'and id>5;
-- or取的是并集and是交集
select*from user where postword like'%4%'or id>5;

select*from user where postword like'%4%'and id>5 and name like'bsea';

-- 增加
insert into user(id,name,postword,phonenumber)values(8,'bsea1','23424','17721411770');
-- 删除
delete from user where name='bsea';
-- 修改
update user set name='bsea'



use java5;
-- (1).写一个SQL语句，查询选修了’计算机原理’的学生
-- (2).写一个SQL语句，查询’张三’同学选修了的课程名字
-- (3).写一个SQL语句，查询选修了3门课程的学生学号、姓名 和年龄
select student.sname from student where student.sno in (select sc.sno from sc where sc.cno in (select course.cno from course where course.cname='计算机原理'));
-- select sc.sno from sc where sc.cno=143;  
-- select course.cno from course where course.cname='计算机原理';
-- select student.sname from student where student.sno=1;
select course.cname from course where course.cno in (select sc.cno from sc where sc.sno in (select student.sno from student where student.sname='张三'));
select student.sno,student.sname,student.aselect student.sname from student where student.sno in (select sc.sno from sc where sc.cno in (select course.cno from course where course.cname='计算机原理'));ge from student,sc where student.sno=sc.sno group by student.sno having count(sc.cno)>=3;

 (select sno,count(sc.sno)counts from sc group by sc.sno ) c

use java5;

-- 创建一张计划表，包含计划的内容，计划事件预计完成的时间，计划需要的开销。 
-- 插入5条记录，然后删除其中两条。 再插入3条记录。 最后把，预计完成日期是今天以前的记录查询出来。 提供sql语句
-- 查看详情
select*from plantable;
insert into plantable(no,content,time,expenses)values(1,'学习java','2009-7-1',16000);
insert into plantable(no,content,time,expenses)values(2,'学习英语','2011-8-21',34000);
insert into plantable(no,content,time,expenses)values(3,'学习语文','2013-2-4',3200);
insert into plantable(no,content,time,expenses)values(4,'学习数学','2015-4-11',6000);
insert into plantable(no,content,time,expenses)values(5,'学习物理','2017-2-28',300);
delete from plantable where no=1;
delete from plantable where no=2;
insert into plantable(no,content,time,expenses)values(1,'学习java','2018-12-25',100);
insert into plantable(no,content,time,expenses)values(2,'学习音乐','2020-1-12',16000);
insert into plantable(no,content,time,expenses)values(6,'学习对象','2017-7-17',16000);
select*from plantable where time>'2017-7-17';

select distinct name from user;
-- distinct列名1列名2 只有列名1和列名2都相同时，才不会以为是重复记录，只显示一条
select distinct name,score from `user`;

select *fr om `user` group by name having count(*)>2; 

update user set category='英语';

-- 求3课成绩总分大于230的人
-- sum(列)表示计算改列值的总和
-- 列名后加as 别名，就是给列一个外号，在我们的结果显示这个外号 as也可以省掉 效果一样
-- group by 列名表示按照那个列名的值分组，如果该列的值是相同的，就分到一个分组
-- having ，必须在group by 后面表示分组完成以后，再加过滤条件（和where相似）


        
-- <>表示不等于
-- order by 列desc；表是按照该列的值，降序排序的
-- order by 列表示按照该列的值排序，默认情况下，是按照升序，也就从小到大
-- order by 列asc；表是按照该列的值，升序排序的 和不写一样的效果。
select *from user group by name having sum(score)>230;
select sum(score)as'总分',name as '名字'from user group by name having sum(score)>230;
select sum(score)as'总分',name as '名字'from user group by name having sum(score)>230 order by sum(score) desc;
select sum(score)as'总分',name as '名字'from user group by name having sum(score)>230 and name <>'bsea'order by sum(score) desc;
-- 新建数据库
-- create database java5;
-- 新建表
-- 
create table java5.class_details(
id int,
useId int,
is_avaiable varchar(1),
class_date date
);
-- 修改表
-- 增加列
alter table java5.book add printDate date;
-- 修改列的长度，从30改成31
alter table java5.book modify name varchar(31);
-- 修改列的名字，change，后面第一个参数是原来的列名后面是新的列名
alter table java5.book change name bookName varchar(31);

use student;
select * from computer;

select* from score;


-- 查询出三科成绩都大于80分的学生的名字
select name from computer where SQL2000>80 and net>80 and flash>80; 
-- 查询出总分大于600并且平均成绩大于90的学生（一共7科，每科满分100）
select 姓名 from score  group by 姓名 having sum(成绩)>600 and avg(成绩)>90;
-- 把学生的成绩字段的数据类型，从int改成float
ALTER TABLE student.score MODIFY COLUMN 成绩 float NULL ;

-- 今天我们班有5个同学，同时过生日。 用java描述

select 姓名 from score group by 姓名 having count(成绩>80)>3;

use `work`;
insert into Student(Sno,Sname,Ssex,Sbirthday,class)values(108, '曾华','男','1977-09-01', 95033);
insert into Student(Sno,Sname,Ssex,Sbirthday,class)values(105, '匡明','男','1975-10-02', 95031);
insert into Student(Sno,Sname,Ssex,Sbirthday,class)values(107, '王丽','女','1976-01-23', 95033);
insert into Student(Sno,Sname,Ssex,Sbirthday,class)values(101, '李军','男','1976-02-20', 95033);
insert into Student(Sno,Sname,Ssex,Sbirthday,class)values(109, '王芳','女','1975-02-10', 95031);
insert into Student(Sno,Sname,Ssex,Sbirthday,class)values(103, '陆君','男','1974-06-03', 95031);
alter table student add constraint pk_t primary key(Sno);



insert into Course(Cno,Cname,Tno)values(105, '计算机导论' ,825);
insert into Course(Cno,Cname,Tno)values(245, '操作系统 ',804);
insert into Course(Cno,Cname,Tno)values(166, '数字电路' ,856);
insert into Course(Cno,Cname,Tno)values(888, '高等数学' ,831);
alter table Course add constraint pk_t primary key(Cno);


insert into Score(sno,Cno,Degree)values(103, '3-245', 86);
insert into Score(sno,Cno,Degree)values(105, '3-245', 75);
insert into Score(sno,Cno,Degree)values(109, '3-245', 68);
insert into Score(sno,Cno,Degree)values(103, '3-105', 92);
insert into Score(sno,Cno,Degree)values(105, '3-105', 88);
insert into Score(sno,Cno,Degree)values(109, '3-105', 76);
insert into Score(sno,Cno,Degree)values(101, '3-105', 64);
insert into Score(sno,Cno,Degree)values(107, '3-105', 91);
insert into Score(sno,Cno,Degree)values(108, '3-105', 78);
insert into Score(sno,Cno,Degree)values(101, '6-166', 85);
insert into Score(sno,Cno,Degree)values(107, '6-166', 79);
insert into Score(sno,Cno,Degree)values(108, '6-166', 81);
alter table score add constraint pk_t primary key(Cno);
alter table score add constraint pk_t primary key(Sno);

insert into teacher(Tno,Tname,Tsex,Tbirthday,Prof,Depart)values(804,'李诚', '男', '1958-12-02', '副教授','计算机系');
insert into teacher(Tno,Tname,Tsex,Tbirthday,Prof,Depart)values(856,'张旭', '男', '1969-03-12', '讲师','电子工程系');
insert into teacher(Tno,Tname,Tsex,Tbirthday,Prof,Depart)values(825,'王萍', '女', '1972-05-05', '助教','计算机系');
insert into teacher(Tno,Tname,Tsex,Tbirthday,Prof,Depart)values(831,'刘冰', '女', '1977-08-14', '助教','电子工程系');


-- 1、?查询Student表中的所有记录的Sname、Ssex和Class列。
select Sname,Ssex,Class from student;
-- 2、?查询教师所有的单位即不重复的Depart列。
select distinct Depart from teacher;
-- 3、?查询Student表的所有记录。
select *from student;
-- 4、?查询Score表中成绩在60到80之间的所有记录
select * from score where `Degree` between 60 and 80;
-- 5、?查询Score表中成绩为85，86或88的记录
select * from score where `Degree`=85 or `Degree`=86 or `Degree`=88;
-- 6查询Student表中“95031”班或性别为“女”的同学记录
select * from student where Class='95031'or Ssex='女';
-- 7、?以Class降序查询Student表的所有记录
select *from student order by Class desc;
-- 8、?以Cno升序、Degree降序查询Score表的所有记录。
select* from score order by Cno asc , `Degree` desc;
-- 9、?查询“95031”班的学生人数
select count(*) from student where class='95031'; 
-- 10、查询每门课的平均成绩。
select Cname,avg(Degree) from score,course where score.Cno=course.Cno group by score.Cno;
-- 11 查询分数大于70，小于90的Sno列
select Sno from score where degree between 70 and 90;

use july23;

select nvl(author,'wuming')from book;

select book.bookName,user.phonenumber from book,`user` where `user`.userName=book.author;
-- limit 2,2 表示从2号位置开始，取2条记录；
select*from book  order by price desc limit 2,2;
-- limit 1,4 表从1号位置开始，取4条记录；（起始位置是0）

select*from book  order by price desc limit 1,4;

select* from `user` where score<60;

select  userName,count(*) from `user` where score<60 group by userName having count(*)>=2;

select  userName from `user` where score<60 group by userName having count(*)>=2;
-- 直接把一个查询结果，当做数据表用
select userName from (select  userName from `user` where score<60 group by userName having count(*)>=2)as t;
-- 把一个子的查询结果，在where中使用，下面语句意思是，查询出，作者的名字，存在于用户表的书的名字
select book.bookName from book where author in (select userName from `user`);
select  userName from `user` where score<60 group by userName having count(*)>=2;
select `user`.userName,avg(score)from `user` group by userName ; 
-- 挂了两科的以上学生的平均分和名字查出来
select `user`.userName,avg(score)from `user` 
where userName in(select  userName from `user` where score<60 group by userName having count(*)>=1) group by userName ;
select  userName from `user` where score<60 group by userName having count(*)>=1;

select author, book.bookName from book left   join `user` on book.author = user.userName;
select author, book.bookName from book left   join `user` on book.author = user.userName where user.userName='玫瑰';
select author, book.bookName from book left   join `user` on book.author = user.userName 
left join class_details on class_details.useId = user.id;
select * from book inner join user on book.author=userName;

  create database july23;

create table july23.student(
Id INT(10) not null UNIQUE PRIMARY key AUTO_INCREMENT COMMENT '学号',
Name VARCHAR(20) not null COMMENT '姓名',
Sex VARCHAR(4)  null COMMENT '性别',
Birth YEAR null COMMENT '出生年份',
Department VARCHAR(20) not null COMMENT '院系' ,
Address VARCHAR(50) null COMMENT '家庭住址' 
)charset utf8;

create table july23.Score(
Id INT(10) not null UNIQUE PRIMARY key AUTO_INCREMENT COMMENT '编号',
Stu_id INT(10) not null COMMENT '学号',
C_name  VARCHAR(20)  null COMMENT '课程名',
Grade  INT(10)  null COMMENT '分数'
)charset utf8;
use july23;
-- 格兰芬多、赫奇帕奇、拉文克劳、斯莱特林
select * from student;
insert into student( Id,Name,Sex,Birth,Department,Address) 
values ( '901','赵四','男',1949,'格兰芬多','伦敦');
insert into student values(null ,'王二','男',1988,'赫奇帕奇','抽象工作室');
INSERT INTO student VALUES(null,'张老大', '男',1985,'计算机系', '北京市海淀区');
insert into student values(null,'雪娟','女',1999,'斯莱特林','抽象工作室');
insert into student values(null,'陈毅','男',1988,'拉文克劳','抽象工作室');
insert into student values(null,'李四','男',1998,'联合国委员会','孤儿院');

insert into score values(null,901,'黑魔法防御',89);
insert into score values(null,901,'兰州拉面的制作',77);
insert into score values(null,901,'挖掘机操纵',82);
insert into score values(null,902,'黑魔法防御',39);
insert into score values(null,902,'兰州拉面的制作',69);
insert into score values(null,902,'挖掘机操纵',81);
insert into score values(null,903,'黑魔法防御',76);
insert into score values(null,903,'兰州拉面的制作',64);
insert into score values(null,903,'挖掘机操纵',41);
insert into score values(null,904,'黑魔法防御',88);
insert into score values(null,904,'兰州拉面的制作',90);
insert into score values(null,904,'挖掘机操纵',89);
insert into score values(null,905,'黑魔法防御',99);
insert into score values(null,905,'兰州拉面的制作',90);
insert into score values(null,905,'挖掘机操纵',89);
insert into score values(null,906,'黑魔法防御',69);
insert into score values(null,906,'兰州拉面的制作',23);
insert into score values(null,906,'挖掘机操纵',41);

-- 从student表中查询计算机系和英语系的学生的信息
select * from student where Department='计算机系' or Department='英语系';

-- 从student表查询所有学生的学号（id）、姓名（name）和院系（department）的信息
select Id,Name,Department from student

-- 从student表中查询年龄18~22岁的学生信息
select * from student where (2017-Birth) between 18 and 22;

-- 从student表中查询每个院系有多少人
select Department,count(*) from student group by Department ;

-- 从score表中查询每个科目的最高分
select C_name,max(Grade) from score group by C_name;

-- 查询李四的考试科目（c_name）和考试成绩（grade）
select score.C_name,score.Grade from score where Stu_id in (select id from student where Name='李四');

-- 用连接的方式查询所有学生的信息和考试信息
select * from student left join score on student.id=score.Stu_id;

-- 计算每个学生的总成绩
select Stu_id,sum(score.Grade) from score group by Stu_id; 

-- 算每个考试科目的平均成绩
select C_name,avg(score.Grade) from score group by C_name;

-- 查询计算机成绩低于95的学生信息
select * from student where Id in(select Stu_id from score where C_name='计算机'and Grade<95);

-- 查询同时参加计算机和英语考试的学生的信息
select * from student where Id in(select Stu_id from score where Stu_id in (select stu_id from score where c_name='计算机') and C_name='英语');

-- 将计算机考试成绩按从高到低进行排序
select score.Grade from score where score.C_name='计算机' order by Grade desc;
-- 从student表和score表中查询出学生的学号，然后合并查询结果
select student.id,score.Stu_id from student,score ;

-- 查询姓张或者姓王的同学的姓名、院系和考试科目及成绩
select Name ,Department,c_name,Grade from score,student 
where score.Stu_id=student.Id and (student.Name like '张%'or student.Name like '王%');

-- 查询都是湖南的学生的姓名、年龄、院系和考试科目及成绩
select Name,(2017-Birth), Department,c_name,Grade from score,student 
where score.Stu_id=student.Id and address='湖南';






