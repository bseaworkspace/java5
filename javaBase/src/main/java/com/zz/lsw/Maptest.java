package com.zz.lsw;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;



public class Maptest {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Map map=new HashMap();
		
		map.put("name", "value");
		
		map.put("name","value");
		
		map.get("name");
		//泛型，提前规定这个map的key必须是String类型，value必须是Car类型
		Map<String,Car> map1=new HashMap();
		Car c1=new Car();
		map1.put("奔驰", c1);
		Car c2=new Car();
		map1.put("宝马", c2);
		Car c3=new Car();
		map1.put("奥迪", c3);
		
		//遍历map  第一种方法也是最常用的遍历方法
		
		for(Map.Entry<String, Car> e:map1.entrySet()){
			String key=e.getKey();
			Car c=e.getValue();
			
			System.out.println("遍历map方法一通过entryset--"+key);
		}
		
		//第二种
		
		for(String str:map1.keySet()){
			System.out.println("通过map的第二种遍历方法通过.setkey"+str);
		}
		for(Car c6:map1.values()){
			System.out.println("通过map的第二种遍历方法通过.setvalues"+c6);
		}
		
		//第三种
		Iterator<Map.Entry<String,Car>> entries=map.entrySet().iterator();

		while(map1.entrySet().iterator().hasNext()){
			Map.Entry<String, Car> e1=entries.next();
			System.out.println(e1.getKey());
		}
		
		for(String str:map1.keySet()){
			System.out.print("通过map的第二种遍历方法通过key.set"+str);
			Car value=map1.get(str);
		}
		//第四种(尽量不用)
		/*总结
		 * 如果仅需要键key或者values使用方法
		 * 如果你使用的语言版本低于java5
		 * 或者打算在遍历时删除entries必须使用方法3
		 * 否则使用方法一（键值都要）
		 * 
		 * 
		 * 
		 */
	}

}
