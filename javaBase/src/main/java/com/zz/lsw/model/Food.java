package com.zz.lsw.model;

public class Food {
	private int id;
	private String name;
	private String category;
	private float price;
	private String is_avaiable;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public float getPrice() {
		return price;
	}
	public void setPrice(float price) {
		this.price = price;
	}
	public String getIs_avaiable() {
		return is_avaiable;
	}
	public void setIs_avaiable(String is_avaiable) {
		this.is_avaiable = is_avaiable;
	}

	

}
