package com.zz.lsw.model;

public class desk {

	private int id;
	private String is_avaible;
	private int customer_id;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getIs_avaible() {
		return is_avaible;
	}
	public void setIs_avaible(String is_avaible) {
		this.is_avaible = is_avaible;
	}
	public int getCustomer_id() {
		return customer_id;
	}
	public void setCustomer_id(int customer_id) {
		this.customer_id = customer_id;
	}
}
