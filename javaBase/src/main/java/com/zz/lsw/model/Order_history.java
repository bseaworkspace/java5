package com.zz.lsw.model;

import java.util.Date;

public class Order_history {

	private float id;
	private int book_id;
	private Date star_dt;
	private Date end_dt;
	private int user_id;
	public float getId() {
		return id;
	}
	public void setId(float id) {
		this.id = id;
	}
	public int getBook_id() {
		return book_id;
	}
	public void setBook_id(int book_id) {
		this.book_id = book_id;
	}
	public Date getStar_dt() {
		return star_dt;
	}
	public void setStar_dt(Date star_dt) {
		this.star_dt = star_dt;
	}
	public Date getEnd_dt() {
		return end_dt;
	}
	public void setEnd_dt(Date end_dt) {
		this.end_dt = end_dt;
	}
	public int getUser_id() {
		return user_id;
	}
	public void setUser_id(int user_id) {
		this.user_id = user_id;
	}
	
}
