package com.zz.lsw.model;

import java.sql.Date;

public class Order {
	private int id;
	private float sum_amt;
	private int customer_id;
	private Date create_dt;
	private int desk_id;
	private int status;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public float getSum_amt() {
		return sum_amt;
	}
	public void setSum_amt(float sum_amt) {
		this.sum_amt = sum_amt;
	}
	public int getCustomer_id() {
		return customer_id;
	}
	public void setCustomer_id(int customer_id) {
		this.customer_id = customer_id;
	}
	public Date getCreate_dt() {
		return create_dt;
	}
	public void setCreate_dt(Date create_dt) {
		this.create_dt = create_dt;
	}
	public int getDesk_id() {
		return desk_id;
	}
	public void setDesk_id(int desk_id) {
		this.desk_id = desk_id;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}

}
