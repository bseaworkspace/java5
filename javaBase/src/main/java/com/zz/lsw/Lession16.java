package com.zz.lsw;

public class Lession16 {

	public  String name="jacky";
	public static String className="java5";
	//被final修饰的变量，叫做常量，值不能再变，一般名字全部大写
	public final static String IDNUM="62492386498";
	public static void testStatic(){
		System.out.println("这是玫瑰");
	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		//八大数据类型byte,short,int,long,float,double,char,boolsen
		/*
		 * 基本数据类型是存储在栈里面
		 * 引用类型，值是存在堆里面，地址的值是存在栈里面的  
		 * 
		 */
		  //关键字：static 静态的
		//特征：被static修饰 的成员变量，或者方法，可以在
		//不需要new的情况下使用
		//普通的成员变量，必须要先new出对象，才能访问对象的属性
		//被static修饰的变量，叫做静态变量。可以直接
		//通过类的名字点出来用，不需要new
		Lession16.testStatic();
		//被final修饰的变量，不能再被赋值 Lession16.IDNUM="4983274592";
		  

		
		System.out.println(Lession16.IDNUM);
	}

}
