package com.zz.lsw;
/*
 * class后面跟的是类的名字
 * 
 * interface后面跟的是接口的名字
 * 
 * @author jiyu
 * 
 * 
 * 
 */
public interface InterFaceTest {
 
	//接口里面的方法，是没有方法体也就是没有{}
	//只是定义方法的名字，输入参数和返回类型
	public void test();
}
