package com.zz.lsw;

public class lsw1 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		//数组 
		/**
		 * int[] 表示声明的是int类型的数组
		 * 
		 * new int[3] 其中3表示数组的长度也就是这个数字最多能存3个元素。
		 * 
		 */
		int[] a=new int[3];
		a[0]=12;
		a[1]=34;
		a[2]=34;
		char[] b=new char[4];
		b[0]='a';
		b[1]='c';
		b[2]='w';
		b[3]='s';
		//int[3][2]其中3表示第一维的数组长度为3
		//2表示2维的长度为2
		//也就是这二维数组，可以存3个数组，每个数组可以存两个int类型的值
		//0号位这个
		int[][] c=new int[3][2];
		c[0][0]=12;
		c[0][1]=12;
		c[1][0]=12;
		c[1][1]=12;
		c[2][0]=12;
		c[2][1]=12;
		for(int i=0;i<a.length;i++){
			System.out.println("a-"+i+"-"+a[i]);
		}
		for(int i=0;i<c.length ;i++){
			for(int j=0;j<c[i].length ;j++){
				System.out.println("c-->"+i+"-->"+j+"-->"+c[i][j]);
			}
		}
		
		int[][][] e=new int[2][2][2];
		e[0][0][0]=1;
		e[0][0][1]=2;
		e[0][1][0]=3;
		e[0][1][1]=4;
		e[1][0][0]=5;
		e[1][0][1]=6;
		e[1][1][0]=7;
		e[1][1][1]=8;
		for(int i=0;i<e.length;i++){
			for(int j=0;j<e[i].length ;j++){
				for(int k=0;k<e[i][j].length ;k++){
					System.out.println("c-->"+i+"-->"+j+"-->"+k+"-->"+e[i][j][k]);	
				}
			}
		}
	}

}
