package com.zz.lsw;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class UserDao {
	BaseDao baseDao=new BaseDao();
	
	public List<User> getUserByName(String name){
		List<User>ls=new ArrayList();
		/*public List<User> getUserByName(String name)
		 * 
		 * 
		 * 
		 * 
		 * 第一第二步
		 */
		Connection con =baseDao.getConnection();
		try {
			//
			PreparedStatement ps=con.prepareStatement("select * from user where name='"+name+"'");
			ResultSet rs= con.prepareStatement("select * from user where name='"+name+"'").executeQuery();
			while(rs.next()){
				User u=new User();
				u.setId(rs.getInt("id"));
				u.setName(rs.getString("name"));
				u.setPhoneNumber(rs.getString("phoneNumber"));
				u.setScore(rs.getFloat("score"));
				u.setPwd(rs.getString("postword"));
				ls.add(u);
			}
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return ls;
	}
	//bollean意思是，这个方法添加用户到数据库，是不是成功，
	//如果成功，就返回true，失败就返回false；
	/*
	 * 
	 * 输入参数是User类型的，形参u
	 * 表是，页面上用户输入的信息，都set到了这个u
	 * 然后，方法里面，就可以通过u的get，得到用户输入的值
	 * 
	 * 
	 * 
	 * 
	 */

	public boolean addUser(User u){
		boolean flag=false;
		/*
		 * 每一次和数据库打交道，就是‘
		 * 要进行数据库的查询，或者修改等
		 * 都必须完成jdbc连接的几个步骤。
		 * 
		 * 1加载驱动（通过class。forName加载驱动）
		 * 2获取连接（通过DriverManager获取连接）
		 * 3通过Connection获取Statement.
		 * 4通过statement执行sql语句
		 * 5倒序关闭所有连接
		 * 
		 * 
		 */
		
		//baseDao.getConnection();封装了第一第二步。
	    Connection con=baseDao.getConnection();
	    /*"insert into user(id,name,pwd)values(?,?,?)";
	     * ？号表示占位符，意思是这个地方会被输入参数的值代替
	     */
	    String sql="insert into user(id,name,pwd)values(?,?,?)";
	   try{
		   //3通过Connection获取Statement.
	    PreparedStatement ps=con.prepareStatement(sql);
	    ps.setInt(1,u.getId());
	    /* PreparedStatement的setString方法
	     * 第一个参数的位置（下标），起始位置是从1 开始
	     * 第二 个参数是输入参数的值，也就是真正的sql语句里面的的值
	     * ps.setString(2,u.getName());
	     * 
	     * 表示第二个？的地方，我们要用u.getName（）的值替换。
	     * 
	     */
	    ps.setString(2,u.getName());
	    ps.setString(3,u.getPwd());
	    //4通过statement执行sql语句
	    int r=ps.executeUpdate();
	    if(r>0){
	    	flag=true;
	    }
	 
	    //5倒序关闭所有连接
	    baseDao.closeUpdate(ps, con);
	    }catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	    
	    
	    
	    
	    
		return flag;
	}

	public static void main(String[] args) {
		
		// TODO Auto-generated method stub

		UserDao udao=new UserDao();
		List<User> ls=udao.getUserByName("bsea");
		if(ls.size()==0){
			System.out.println("用户民不存在");
		}
		boolean flag=false;
		String pwd="123";
		for(User u1:ls){
			System.out.println("userdao---"+u1.getScore());
		    if(pwd.equals(u1.getPwd())){
		    	flag=true;
		    }
		    
		}
		if(flag){
			System.out.println("登录成功");
		}
		
		for(User u1:ls){
			System.out.println("userdao----"+u1.getScore());
		}
	}

}
