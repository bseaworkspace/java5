package com.zz.lsw;
/*extends表示继承，后面跟的是父亲的名字
 * 
 * 1,。java是单继承，只能继承个父类
 * 
 * 2.继承以后，子类会自动拥有父类的所有属性和方法
 * 
 * 
 * 
 */

public class BussCar extends Car{
	String no;
	//方法的重写
	/*必须是父类和子类之间，子类重写父亲的方法
	 * 
	 * 方法名字必须一样
	 * 
	 * 参数的个数，和类型必须一样
	 * 
	 * 返回的数据类型，也必须一致
	 * 
	 * (non-Javadoc)
	 * @see com.zz.lsw.Car#run(int)
	 */

	public void run(int b){
		System.out.println("这公交车"+no+"车以"+b+"公里每小时的速度在行驶");

	}
	public boolean openDoor(String way){
		System.out.println("车以"+way+"方式打开了门");
		return false;
	}
	
}
