package com.zz.lsw;

public interface Phone {

	public void call(long phonenumber) ;
	
	public boolean sendMessage(String content);
}
