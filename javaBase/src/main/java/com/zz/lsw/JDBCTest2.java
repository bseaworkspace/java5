package com.zz.lsw;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class JDBCTest2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String driver="com.mysql.jdbc.Driver";
		String database="java5";
		String userName="root";
		String pwd="";
		//localhost代表连接的是自己电脑的是本地数据库
		//如果需要连接远程的数据库，就改成远程电脑的ip地址。
		//port是我们安装数据库的时候，myini是写的，一般默认3306
		//database是我们自己创建的数据的名字
		String url="jdbc:mysql://localhost:3306/"+database;
		String sql="update book set price = 110 where id =1";
		
		try {
			//加载驱动
			Class.forName("com.mysql.jdbc.Driver");
			//通过驱动包建立连接
			Connection con=DriverManager.getConnection(url, userName, pwd);
			//执行aql语句
			PreparedStatement ps =con.prepareStatement(sql);
			
			ps.execute();
			
			
			//从里到外，依次关闭连接
		
			if(ps!=null){
				ps.close();
			}
			if(con!=null){
				con.close();
			}
		} catch (ClassNotFoundException  e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

}
