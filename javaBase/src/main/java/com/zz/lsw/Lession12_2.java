package com.zz.lsw;

public class Lession12_2 {
	public static void main(String[] args) {	
		int[] a=new int[7];
		a[0]=12;
		a[1]=132;
		a[2]=22;
		a[3]=52;//比较排序，移动多少次？
		a[4]=72;
		a[5]=925;
		a[6]=552;
		//选择排序，从所有的元素中选择一个最小的元素，放到0位置
		//比较排序和冒泡排序，比较次数是一样的，但是效率，比较排序比较高，因为交换
		//次数少（也不是绝对的,比如如果排序一开始就完成，冒泡排序的交换次数少，为零，选择排序为2）
		bjpx(a);
		
	}
	public static void bjpx(int arry[]){
		
		
		for(int i=0;i<arry.length;i++){
			int biggestIndex=i;
		for(int j=i+1;j<arry.length;j++){
			if(arry[j]>arry[biggestIndex]){
				biggestIndex=j;
			}
		}
		int temp=arry[i];
		arry[i]=arry[biggestIndex];
		arry[biggestIndex]=temp;
		
		}
		System.out.println(arry.clone());
		
	}

}
