package com.zz.lsw.dao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.zz.lsw.model.Book;
import com.zz.lsw.model.User;

public class BookDao {
	BaseDao baseDao=new BaseDao();
	
	public List<Book> getBookByName(String name){
		List<Book> ls=new ArrayList();
		Connection con=baseDao.getConnection();
		String sql="select *from book where bookName=?";
		
		try {
			PreparedStatement ps=con.prepareStatement(sql);
			ps.setString(1, name);
			ResultSet rs=ps.executeQuery();
			while(rs.next()){
				
				Book b=new Book();
				int id=rs.getInt("id");
				b.setId(id);
				b.setBookName(rs.getString("bookName"));
				b.setAuthor(rs.getString("author"));
				b.setCategory(rs.getString("Category"));
				b.setIs_brow(rs.getString("is_brow"));
				b.setPrintby(rs.getString("printby"));
				b.setPrintDate(rs.getDate("printDate"));
				b.setPrice(rs.getFloat("price"));
				ls.add(b);
			}
			baseDao.closeQuery(rs, ps, con);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		
		return ls;
		
		
	}
	public List<Book> getBookByNameAndStatus(String name,String status){
		List<Book> ls=new ArrayList();
		Connection con=baseDao.getConnection();
		String sql="select *from book where bookName=? and is_brow='?'";
		
		try {
			PreparedStatement ps=con.prepareStatement(sql);
			ps.setString(1, name);
			ps.setString(2, status);
			ResultSet rs=ps.executeQuery();
			while(rs.next()){
				
				Book b=new Book();
				int id=rs.getInt("id");
				b.setId(id);
				b.setBookName(rs.getString("bookName"));
				b.setAuthor(rs.getString("author"));
				b.setCategory(rs.getString("Category"));
				b.setIs_brow(rs.getString("is_brow"));
				b.setPrintby(rs.getString("printby"));
				b.setPrintDate(rs.getDate("printDate"));
				b.setPrice(rs.getFloat("price"));
				ls.add(b);
			}
			baseDao.closeQuery(rs, ps, con);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		
		return ls;
		
		
	}
	
	public List<Book> getBookByNameAndStatusAndBookid(String name,String status,int bookId){
		List<Book> ls=new ArrayList();
		Connection con=baseDao.getConnection();
		String sql="select *from book where bookName=? and is_brow='?' and id=?";
		
		try {
			PreparedStatement ps=con.prepareStatement(sql);
			ps.setString(1, name);
			ps.setString(2, status);
			ps.setInt(3, bookId);
			ResultSet rs=ps.executeQuery();
			while(rs.next()){
				
				Book b=new Book();
				int id=rs.getInt("id");
				b.setId(id);
				b.setBookName(rs.getString("bookName"));
				b.setAuthor(rs.getString("author"));
				b.setCategory(rs.getString("Category"));
				b.setIs_brow(rs.getString("is_brow"));
				b.setPrintby(rs.getString("printby"));
				b.setPrintDate(rs.getDate("printDate"));
				b.setPrice(rs.getFloat("price"));
				ls.add(b);
			}
			baseDao.closeQuery(rs, ps, con);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		
		return ls;
		
		
	}
	
	
	public boolean borrowBook(String bookName,String userName){
		boolean flag=false;
		List<Book> ls=getBookByNameAndStatus(bookName,"N");
		UserDao uDao=new UserDao();
		List<User> ls2=uDao.getUserByName(userName);
		if(ls.size()>0&&ls2.size()>0){
			Connection con=baseDao.getConnection();
			String addSql="INSERT INTO java5.order_history( book_id,user_id)VALUES(?,?)";
			String updateSql="update book set  is_brow = 'Y' where book_id=?";
			Book b=ls.get(0);
			User u=ls2.get(0);
			
			try {
				PreparedStatement ps = con.prepareStatement(addSql);
				ps.setInt(1, b.getId());
				ps.setInt(2, u.getId());
				int count = ps.executeUpdate();
				ps = con.prepareStatement(updateSql);
				ps.setInt(1, b.getId());
				int c2 = ps.executeUpdate();
				if (c2 > 0) {
					flag=true;

				}
				
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
		//判断这个书有没有。
		
		//借书
		
		
		
		
		return flag;
	}
	
	
	public boolean returnBook(String bookName,String userName,int bookId){
		boolean flag=false;
		List<Book> ls=getBookByNameAndStatusAndBookid(bookName,"Y",bookId);
		UserDao uDao=new UserDao();
		List<User> ls2=uDao.getUserByName(userName);
		if(ls.size()>0&&ls2.size()>0){
			Connection con=baseDao.getConnection();
			String addSql="update order_history set end_dt=? where book_id=? and user_id=?";
			String updateSql="update book set is_borw='N' where book_id=?";
			Book b=ls.get(0);
			User u=ls2.get(0);
			
			try {
				PreparedStatement ps = con.prepareStatement(addSql);
				ps.setInt(1, b.getId());
				ps.setInt(2, b.getId());
				ps.setInt(3, u.getId());
				int count = ps.executeUpdate();
				if(count>0){
				ps = con.prepareStatement(updateSql);
				ps.setInt(1, b.getId());
				int c2 = ps.executeUpdate();
				if (c2 > 0) {
					flag=true;
				}
			}
					
					
					
					
//					Connection con=baseDao.getConnection();
//		    		String addSql="update order_history set end_dt=? where book_id=? and user_id=?";
//		    		String updateSql="update book set is_borw='N' where book_id=?";
//		    		Book b=ls.get(0);
//		    		User u=ls2.get(0);
//
//	    			PreparedStatement ps=con.prepareStatement(addSql);
//	    			Date dt=new Date(0);
//	    			ps.setDate(1, dt);
//	    			ps.setInt(2, b.getId());
//	    			ps.setInt(3, u.getId());
//	    			int count=ps.executeUpdate();
//	    			if(count>0){
//	    				ps=con.prepareStatement(updateSql);
//	    				ps.setInt(1, b.getId());
//	    				int c2=ps.executeUpdate();
//	    				if(c2>0){
//	    					flag=true;
//	    			
	    		

				
				
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
		//判断这个书有没有。
		
		//借书
		
		
		
		
		return flag;
	}

}
