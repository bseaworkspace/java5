package com.zz.simplespade;

public interface Phone {
	
	public void call(int phoneNumber);
	
	public boolean sendMessage(String content);
	
}
