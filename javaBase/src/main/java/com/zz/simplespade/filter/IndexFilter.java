package com.zz.simplespade.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.zz.simplespade.model.User;

public class IndexFilter implements Filter {

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
		// TODO Auto-generated method stub
		System.out.println("IndexFilter  ---init");
	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		// TODO Auto-generated method stub
		System.out.println("IndexFilter  ---doFilter");
		request.setCharacterEncoding("utf-8");
		response.setCharacterEncoding("utf-8");
		response.setContentType("text/html;charset=utf-8");
		HttpSession session = ((HttpServletRequest) request).getSession();
		User u = (User) session.getAttribute("user");

		if (!(((HttpServletRequest) request).getServletPath().indexOf("simplespade")!=-1) && !("/LoginController".equals(((HttpServletRequest) request).getServletPath())) && u == null) {
			
			request.getRequestDispatcher("/simplespade/login.jsp").forward(request, response);

		} else {
			// 表示把请求传递下去
			// 执行下一个Filter，如果没有了Filter就去找真正的web资源
			chain.doFilter(request, response);
		}
	}

	@Override
	public void destroy() {
		// TODO Auto-generated method stub
		System.out.println("IndexFilter  ---destroy");
	}

}
