package com.zz.simplespade;

public class Person {
	String cloth_color;
	public void sit(){
		System.out.println("坐");
	}
	/**
	 * 1.方法的名字必须和类的名字一样
	 * 2.没有返回值，连void都不要写
	 * 
	 * 
	 */
	public Person(String cloth_color){
		this.cloth_color = cloth_color;
	}
	public static void main(String[]args){
		//new是一个关键字，表示要创建一个具体的对象
		//这里，就是创建一个具体的人
		//new后面跟得是构造方法，构造方法跟类的名字是一样的
		Person p = new Person("red");
		/*p.cloth_color ="red";*/
	}
}
