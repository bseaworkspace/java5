package com.zz.simplespade;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class Lesson15 {
	public static void main(String[] args) {
		Map map = new HashMap();
		map.put("test", 33333);
		map.put(888, 777);
		map.put(true, "sdfsfd");
		List list = new ArrayList();
		list.add("饮料");
		list.add("水果");
		list.add("玩具");
		list.add(list);

		map.put("全家便利店", list);
		// <String,Object> 泛形
		Map<String, Object> map2 = new HashMap<String, Object>();

		map2.put("1", 555);
		map2.put("全家便利店", list);
		map2.put("ttt", "coming ");

		for (String key:map2.keySet()) {

			System.out.println("map2===="+map2.get(key));
			if(key=="list"){
				List tmp = (List)map2.get(key);
				List temp2 = (List)tmp.get(3);
			}
		}

		System.out.println(map.get(888));
		System.out.println(map.get("test"));
		System.out.println(map.get(true));
		System.out.println(map.get("全家便利店"));
	}
}
