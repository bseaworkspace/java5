package com.zz.simplespade;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class JDBCTest {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String driver = "com.mysql.jdbc.Driver"; 
		String database = "java5";
		String userName = "root";
		String pwd = "root";
		String url = "jdbc:mysql://localhost:3306/"+database;
		String sql = "select * from user;";
		try {
			//加载驱动
			Class.forName(driver);
			//建立数据库连接
			Connection conn = DriverManager.getConnection(url, userName, pwd);
			
			PreparedStatement ps = conn.prepareStatement(sql);
			
			ResultSet rs = ps.executeQuery();
			
			while(rs.next()){
				
				int id = rs.getInt(1);
				String category = rs.getString("category");
				String name = rs.getString(2);
				
				System.out.println("id:"+id+"  name:"+name+"  科目："+category);
				
			}
			
			//关闭连接，先关resultset，最后关connection
			if(rs!=null){
				rs.close();
			}
			if(ps!=null){
				ps.close();
			}
			if(conn!=null){
				conn.close();
			}
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
