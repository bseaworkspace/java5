package com.zz.simplespade;

public class Factory {
	public Car produce(String color){
		Car car = new Car(color);
		return car;
	}
	public static void main(String[]args){
		Factory f = new Factory();
		f.produce("red");
	}
}
