package com.zz.simplespade;

public class Lesson16 {
	
	public static String className="java5";
	//被final修饰的变量叫常量，值不能再变，一般名字全部大写。
	public final static String IDNUM = "31495164111";
	
	//static 需要放在返回类型的前面
	public static void testStatic(){
		System.out.println("这是一个静态方法");
	}
	
	public static void main(String[]args){
		//八个基本数据类型byte,short,int,long,float,double,char,boolean
		/**
		 * 
		 * 基本数据类型是存储在栈里面
		 * 引用类型，值是存在堆里面，地址的值存在栈里面
		 * 
		 * 
		 * */
		//关键字:static 静态的
		//特征: 被static修饰的成员变量，或者方法 可以在不需要new的情况下使用
	}
}
