package com.zz.simplespade.dao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.zz.simplespade.model.Book;
import com.zz.simplespade.model.User;

public class BookDao {
	BaseDao baseDao = new BaseDao();

	public List<Book> getBookByName(String name) {
		List<Book> list = new ArrayList<Book>();
		try {
			Connection conn = baseDao.getConnection();
			String sql = "select * from book where name = ?";
			PreparedStatement ps = conn.prepareCall(sql);
			ps.setString(1, name);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				Book b = new Book();
				b.setId(rs.getInt("id"));
				b.setName(rs.getString("name"));
				b.setAuthor(rs.getString("author"));
				b.setCategory(rs.getString("category"));
				b.setPrintBy(rs.getString("printBy"));
				b.setPrintDate(rs.getDate("printDate"));
				b.setPrice(rs.getFloat("price"));
				b.setIs_brow(rs.getString("is_brow"));
				list.add(b);
			}
			baseDao.closeQuery(rs, ps, conn);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return list;
	}

	public boolean borrowBook(String bookName, String userName) {
		boolean flag = false;
		List<Book> ls = getBookByNameAndStatus(bookName,"N");
		UserDao userDao = new UserDao();
		List<User> ls2 = userDao.getUserByName(userName);
		// 判断这个数在不在
		if (ls.size() > 0 && ls2.size() > 0) {
			// 借书
			try {
				Connection conn = baseDao.getConnection();
				String addSql = "INSERT INTO order_history(book_id,user_id)VALUES(?,?)";
				String updateSql = "update book set is_brow = 'Y' where book_id = ?";
				Book b = ls.get(0);
				User u = ls2.get(0);
				PreparedStatement ps = conn.prepareStatement(addSql);
				ps.setInt(1, b.getId());
				ps.setInt(2, u.getId());
				if (ps.executeUpdate() > 0) {
					ps = conn.prepareStatement(updateSql);
					ps.setInt(1, b.getId());
					if (ps.executeUpdate() > 0) {
						flag = true;
					}
				}
				baseDao.closeUpdate(ps, conn);
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return flag;
	}
	
	public boolean returnBook(String bookName, String userName) {
		boolean flag = false;
		List<Book> ls = getBookByNameAndStatus(bookName, "Y");
		UserDao userDao = new UserDao();
		List<User> ls2 = userDao.getUserByName(userName);
		// 判断这个数在不在
		if (ls.size() > 0 && ls2.size() > 0) {
			// 借书
			try {
				Connection conn = baseDao.getConnection();
				String addSql = "update order_histort set end_dt = ? where book_id = ? and user_id = ?";
				String updateSql = "update book set is_brow = 'N' where book_id = ?";
				Book b = ls.get(0);
				User u = ls2.get(0);
				PreparedStatement ps = conn.prepareStatement(addSql);
				ps.setDate(1, new Date(0));
				ps.setInt(2, b.getId());
				ps.setInt(3, u.getId());
				if (ps.executeUpdate() > 0) {
					ps = conn.prepareStatement(updateSql);
					ps.setInt(1, b.getId());
					if (ps.executeUpdate() > 0) {
						flag = true;
					}
				}
				baseDao.closeUpdate(ps, conn);
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return flag;
	}

	public List<Book> getBookByNameAndStatus(String name,String status) {
		List<Book> list = new ArrayList<Book>();
		try {
			Connection conn = baseDao.getConnection();
			String sql = "select * from book where name = ? and is_brow = ?";
			PreparedStatement ps = conn.prepareCall(sql);
			ps.setString(1, name);
			ps.setString(2, status);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				Book b = new Book();
				b.setId(rs.getInt("id"));
				b.setName(rs.getString("name"));
				b.setAuthor(rs.getString("author"));
				b.setCategory(rs.getString("category"));
				b.setPrintBy(rs.getString("printBy"));
				b.setPrintDate(rs.getDate("printDate"));
				b.setPrice(rs.getFloat("price"));
				b.setIs_brow(rs.getString("is_brow"));
				list.add(b);
			}
			baseDao.closeQuery(rs, ps, conn);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return list;
	}
}
