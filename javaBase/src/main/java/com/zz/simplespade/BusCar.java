package com.zz.simplespade;
/* extends是表示继承，后面跟的是父类的名字
 * 
 * 1.java是单继承。只能继承一个父亲
 * 
 * 2.继承以后，子类会自动拥有父类的所有属性和方法
 * 
 */
public class BusCar extends Car{
	String no;
	//方法的重写
	/**
	 * 1.必须是父类和子类之间，子类重写父类的方法
	 * 
	 * 2.方法名字必须一样
	 * 
	 * 3.参数的格式和类型必须一样
	 * 
	 * 4.返回类型，也必须一致
	 */
	public void run(){
		
	}
	public boolean openDoor(String way){
		System.out.println("车以"+way+"方式打开了门");
		return false;
	}
	BusCar(String color){
		super(color);
		// TODO Auto-generated constructor stub
	}
	public BusCar() {
		// TODO Auto-generated constructor stub
	}
}
