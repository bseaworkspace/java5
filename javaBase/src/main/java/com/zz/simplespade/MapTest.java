package com.zz.simplespade;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

public class MapTest {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Map map = new HashMap();
		// 赋值
		map.put("name", "value");
		// 取值
		map.get("name");
		// 泛型，提前规定这个map的key必须是String类型，value必须是Car类型
		Map<String, Car> map1 = new HashMap();
		Car c1 = new Car();
		map1.put("奔驰", c1);
		Car c2 = new Car();
		map1.put("宝马", c2);
		Car c3 = new Car();
		map1.put("奥迪", c3);

		// 遍历map
		/**
		 * 总结 如果仅需要key或value使用方法二 如果你是用的语言版本低于java5或是打算在遍历时删除entries，必须使用方法三
		 * 否则使用方法一（key，value都要）
		 */
		// 第一种，使用map.entrySet()遍历，也是最常用的遍历
		for (Map.Entry<String, Car> entry : map1.entrySet()) {
			String key = entry.getKey();
			Car c = entry.getValue();
			System.out.println(key+c);
		}
		System.out.println();

		// 第二种
		for (String s : map1.keySet()) {
			System.out.println(s);
		}
		for (Car car : map1.values()) {
			System.out.println(car);
		}
		System.out.println();

		// 第三种，使用Iterator遍历
		Iterator<Map.Entry<String, Car>> it = map1.entrySet().iterator();
		while (it.hasNext()) {
			Map.Entry<String, Car> e = it.next();
			System.out.println(e.getKey() + e.getValue());
		}

	}

}
