package com.zz.simplespade.control;

import java.util.List;

import com.zz.bsea.dao.Food_customerDao;
import com.zz.simplespade.dao.FoodDao;
import com.zz.simplespade.dao.OrderDao;
import com.zz.simplespade.model.Food;
import com.zz.simplespade.model.Food_customer;
import com.zz.simplespade.model.Order;

public class OrderControl {
	 public boolean orderSubmit(List<Food_customer> list){
		boolean flag = false;
		//开始插入订单表order
		FoodDao fd = new FoodDao();
		float sum_amt = 0;
		int customer_id = 0;
		int desk_id = 0;
		for(Food_customer t:list){
			int food_id = t.getFood_id();
			Food f = fd.getFoodById(food_id);
			sum_amt += f.getPrice();
			customer_id = t.getCustomer_id();
			desk_id = t.getDesk_id();
		}
		OrderDao orderDao = new OrderDao();
		Order o = new Order();
		o.setSum_amt(sum_amt);
		o.setCoustomer_id(customer_id);
		o.setDesk_id(desk_id);
		float order_id = orderDao.add(o);
		//结束插入订单表order
		
		//开始插入中间表
		Food_customerDao food_customerDao=new Food_customerDao();
		for(Food_customer t:list){
			int food_id = t.getFood_id();
			t.setOrder_id(order_id);
			food_customerDao.add(food_id, order_id, customer_id);
		}
		
		
		return flag;
	 }
}
