package com.zz.simplespade.control;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.zz.simplespade.model.User;
/**
 * 把一个普通的Class变成一个Servlet
 * 
 * 第一步：继承HttpServlet
 * 
 * 第二步：重写doGet()和doPost()方法
 * */
public class UserController extends HttpServlet {

	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String i = req.getParameter("number");
		System.out.println("UserController----"+i);
		/**
		 * 
		 * JSP共有以下9种基本内置组件（可与ASP的6种内部组件相对应）： 
			1、request 用户端请求，此请求会包含来自GET/POST请求的参数
			2、response 网页传回用户端的回应
			3、pageContext 网页的属性是在这里管理
			4、session 与请求有关的会话期
			5、application servlet 正在执行的内容
			6、out 用来传送回应的输出
			7、config servlet的构架部件
			8、page JSP网页本身
			9、exception 针对错误网页，未捕捉的例外
		 * 
		 * 
		 * 
		 */
		HttpSession session = req.getSession(true);
		//模拟dao层 开始
		List<User> ls = new ArrayList();
		
		User u1 = new User();
		u1.setName("玫瑰1");
		u1.setId(1);
		u1.setPhoneNumber("15202164405");
		
		User u2 = new User();
		u2.setName("玫瑰2");
		u2.setId(2);
		u2.setPhoneNumber("1525464405");
		
		User u3 = new User();
		u3.setName("玫瑰3");
		u3.setId(3);
		u3.setPhoneNumber("15254164405");

		User u4 = new User();
		u4.setName("玫瑰4");
		u4.setId(4);
		u4.setPhoneNumber("1520456405");
		
		User u5 = new User();
		u5.setName("玫瑰5");
		u5.setId(5);
		u5.setPhoneNumber("15416445405");
		
		ls.add(u1);
		ls.add(u2);
		ls.add(u3);
		ls.add(u4);
		ls.add(u5);
		//结束
		session.setAttribute("us", ls);
		
		
		/**
		 * servlet中，/包含了项目名
		 * 
		 * jsp中，/不包含项目名
		 * 
		 * */
		//页面跳转
		req.getRequestDispatcher("/simplespade/bootstrap/userList.jsp").forward(req, resp);
	}

	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		/**
		 * 
		 * to do
		 * 
		 * 根据上面得到而name和phonenumber
		 * 
		 * */
		//数据库更新成功就是Y 更新失败就是N
		String result="Y";
		req.getParameter("u_id");
		req.getParameter("name");
		req.getParameter("phone");
		System.out.println(req.getParameter("u_id")+req.getParameter("name")+req.getParameter("phone"));
		resp.getWriter().append(result);
	}
}
