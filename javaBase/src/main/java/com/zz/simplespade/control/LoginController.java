package com.zz.simplespade.control;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.zz.simplespade.dao.UserDao;
import com.zz.simplespade.model.User;

/**
 * Servlet implementation class LoginServlet
 */
public class LoginController extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public LoginController() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		String name = request.getParameter("username");
		String pwd = request.getParameter("pwd");
		UserDao userDao = new UserDao();
		List<User> ls = userDao.getUserByName(name);
		//List<User> ls2;
		String path = "simplespade/login.jsp";
		if (ls.size() == 0) {
		} else if (pwd.equals(ls.get(0).getPwd())) {
			//ls2 = userDao.getExamSummary();
			//request.setAttribute("userList", ls2);
			
			/*整个系统的代码，应该只有登录的时候，会把
			用户的信息存在session里面。
			其他地方都只能从session.getAttribute("user")里面
			得到user对象，来获取user的信息。*/
			User user = new User();
			user = ls.get(0);
			HttpSession session = request.getSession();
			session.setAttribute("user", user);
			
			User u1 = new User();
			u1.setName("testNaem1");
			u1.setPhoneNumber("21568");
			u1.setTotal_amt(100);
			
			User u2 = new User();
			u2.setName("testNaem2");
			u2.setPhoneNumber("164846");
			u2.setTotal_amt(100);
			
			List<User> ls2 = new ArrayList<User>();
			ls2.add(u1);
			ls2.add(u2);
			
			request.setAttribute("userList", ls2);
			
			path = "simplespade/show.jsp";
		}
		System.out.println("用户名：" + name + "\n密码：" + pwd);
		// 设置response.setContentType指定 HTTP 响应的编码,同时指定了浏览器显示的编码. 
		response.setContentType("text/html;charset=utf-8");
		// response.getWriter().append(responseMessage);
		request.getRequestDispatcher(path).forward(request, response);
	}

}
