package com.zz.simplespade;

public class Lesson12 {
	//冒泡排序
	/**
	 * 从大到小排序
	 * 
	 * 1.两个for循环
	 * 
	 * 2.第一个for循环的初始值是0，第二个for循环的初始值是i+1
	 * 
	 * 3.只有i位置的值比j位置的值小的时候，才交换位置
	 * 
	 * 
	 */
	public void bubbleSort(int test[]){
		int count = 0;
		for(int i = 0;i<test.length;i++){
			for(int j= i+1;j<test.length;j++){
				if(test[i]<test[j]){
					int temp=0;
					temp = test[i];
					test[i]= test[j];
					test[j]=temp;
					if(test[i] == 52||test[j]==52){
						count++;
					}
				}
			}
		}
		System.out.println(count);
		
	}
	
	public static void main(String[]args){
		int []a = new int[7];
		a[0] = 121;
		a[1] = 132;
		a[2] = 221;
		a[3] = 52;
		a[4] = 72;
		a[5] = 925;
		a[6] = 552;
		Lesson12 l12 = new Lesson12();
		
		l12.bubbleSort(a);
		System.out.println();
		//for循环的另一种写法，for each写法，第二个参数是数组，第一个参数是一个临时变量
		//每次都把数组里面的元素值赋值给第一个参数，
		for(int i:a){
			System.out.println(i);
		}
	}
}
