package com.zz.simplespade.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;

public class FSReader {
	public static void read (File f){
		//File file = new File("C:" + File.separator + "tmp" + File.separator + "test.txt");
		try {
			FileInputStream fi = new FileInputStream(f);
			InputStreamReader r = new InputStreamReader(fi,"utf-8");
			BufferedReader br = new BufferedReader(r);
			while(br.ready()){
				br.readLine();
			}
			br.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
