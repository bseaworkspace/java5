package com.zz.simplespade.util;

import java.util.List;

import com.zz.simplespade.model.User;

public class SimpleSpadeUtil {
	public boolean isMatch(List<User> ls,String pwd){
		boolean flag = false;
		for(User u : ls){
			if(pwd==u.getPwd()){
				flag = true;
			}
		}
		
		return flag;
	}
}
