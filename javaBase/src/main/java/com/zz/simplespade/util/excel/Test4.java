package com.zz.simplespade.util.excel;

import java.io.FileOutputStream;
import java.io.IOException;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

public class Test4 {
	/**
	 * 方法重载
	 * 
	 * 1.同一个类里面
	 * 
	 * 2.方法名字相同
	 * 
	 * 3.参数列表不同（类型或者数量或者位置（参数名字无所谓））
	 * 
	 */
	private static void cteateCell(HSSFWorkbook wb, HSSFRow row, int col, short align, String val) {
		HSSFCell cell = row.createCell(col);
		cell.setCellValue(val);
		HSSFCellStyle cellstyle = wb.createCellStyle();
		cellstyle.setAlignment(align);
		cell.setCellStyle(cellstyle);
	}

	private static void cteateCell(HSSFRow row, HSSFWorkbook wb, int col, short align, String val) {
		HSSFCell cell = row.createCell(col);
		cell.setCellValue(val);
		HSSFCellStyle cellstyle = wb.createCellStyle();
		cellstyle.setAlignment(align);;
		cell.setCellStyle(cellstyle);
	}

	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub
		HSSFWorkbook wb = new HSSFWorkbook();
		
		HSSFSheet s1 = wb.createSheet();
		
		HSSFRow r1 = s1.createRow(4);
		
		cteateCell(wb, r1, 0, HSSFCellStyle.ALIGN_CENTER_SELECTION, "测试封装方法");
		
		FileOutputStream out = new FileOutputStream("C:\\java\\temp\\t.xls");
		
		wb.write(out);
		out.close();
		
	}

}
