package com.zz.simplespade.util.excel;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Date;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFDataFormat;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

public class Test3 {

	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub
		HSSFWorkbook wb = new HSSFWorkbook(); // 建立新HSSFWorkbook对象
		HSSFSheet sheet = wb.createSheet("成绩"); // 建立新的sheet对象
		HSSFRow row = sheet.createRow((short) 0);
		// 在sheet里创建一行，参数为行号（第一行，此处可想象成数组）
		HSSFCell cell = row.createCell(0);
		// 在row里建立新cell（单元格），参数为列号（第一列）
		cell.setCellValue(1); // 设置cell的整数类型的值
		row.createCell(1).setCellValue(1.2); // 设置cell浮点类型的值
		row.createCell(2).setCellValue("test"); // 设置cell字符类型的值
		row.createCell(3).setCellValue(true); // 设置cell布尔类型的值
		HSSFCellStyle cellStyle = wb.createCellStyle(); // 建立新的cell样式
		cellStyle.setDataFormat(HSSFDataFormat.getBuiltinFormat("m/d/yy h:mm"));
		// 设置cell样式为定制的日期格式
		HSSFCell dCell = row.createCell(4);
		dCell.setCellValue(new Date()); // 设置cell为日期类型的值
		dCell.setCellStyle(cellStyle); // 设置该cell日期的显示格式
		HSSFCell csCell = row.createCell(5);
		// csCell.setEncoding(HSSFCell.ENCODING_UTF_16);
		// 设置cell编码解决中文高位字节截断
		csCell.setCellValue("中文测试_Chinese Words Test"); // 设置中西文结合字符串
		row.createCell(6).setCellType(HSSFCell.CELL_TYPE_ERROR);
		// 建立错误cell

		// 开始新建第二行
		HSSFRow row1 = sheet.createRow(1);
		row1.createCell(0).setCellValue("姓名");
		row1.createCell(1).setCellValue("学号");
		row1.createCell(2).setCellValue("成绩");

		// 开始新建第三行
		HSSFRow row2 = sheet.createRow(2);
		row2.createCell(0).setCellValue("小明");
		row2.createCell(1).setCellValue("1");
		row2.createCell(2).setCellValue("98");

		FileOutputStream fileOut;
		try {
			fileOut = new FileOutputStream("c:\\java\\temp\\workbook.xls");
			wb.write(fileOut);
			fileOut.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
