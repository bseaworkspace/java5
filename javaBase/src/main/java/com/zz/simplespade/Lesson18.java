package com.zz.simplespade;

import com.sun.javafx.runtime.SystemProperties;

public class Lesson18 {

	public static void computer() {
		System.out.println("----算术运算符----");
		int a = 10;
		int b = 12;
		System.out.println("a+b=" + (a + b));
		System.out.println("a-b=" + (a - b));
		System.out.println("a*b=" + (a * b));
		System.out.println("a/b=" + (a / b));
		System.out.println("a%b=" + (a % b));
		System.out.println("b%a=" + (b % a));
		int c = 75;
		System.out.println("c是不是偶数 : " + (c % 2));
		System.out.println("a++=" + (a++));
		System.out.println("a--=" + (a--));
	}

	public static void logicComputer() {
		System.out.println("--结果一定是boolean同时参与运算的成员也必须是boolean--逻辑运算符--开始----");

		boolean a = true;
		boolean b = false;
		System.out.println("a=" + a + " b=" + b);
		System.out.println("逻辑与&&,必须两边同时是true结果才是true; --a&&b" + (a && b));
		System.out.println("逻辑与||,必须两边同时是false结果才是false; --a||b" + (a || b));

		if (test2() && test1()) {
			System.out.println("test2()&&test1()=true");
		}
		if (test1() || test2()) {
			System.out.println("test2()||test1()=true");
		}

		System.out.println("----逻辑运算符--结束----");
	}

	public static boolean test1() {
		System.out.println("执行了test1，直接返回结果true");
		return true;
	}

	public static boolean test2() {
		System.out.println("执行了test2，直接返回结果false");
		return false;
	}

	public static void setingComputer() {

	}

	public static void condtionalComputer(int b) {
		System.out.println("----三元运算--开始----");
		String result = (b % 2 == 0) ? "这是一个偶数" : "这是一个奇数";
		System.out.println("result" + result);
		String result2 = (b>90)?"优秀":(b>80)?"良好":(b>60)?"及格":"不及格";
		System.out.println("----三元运算--结束----");
	}
	
	public static void instanceOfComputer(){
		Lesson18 t = new Lesson18();
		boolean r = t instanceof Lesson18;
		
		System.out.println(r);
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		logicComputer();
	}

}
