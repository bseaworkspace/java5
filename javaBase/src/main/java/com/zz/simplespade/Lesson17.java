package com.zz.simplespade;

public class Lesson17 {
	public static void main(String[]args){
		//判断连接符
		
		//==
		
		//1.==判断两个值是不是相等。适用范围，8个基本类型和String
		
		//2.equals方法，判断引用类型的两个变量是不是相等。
		//equals默认情况下，也是比较地址
		/**
		 * == 8个基本类型，比较的是栈里面的值
		 * String方法比较特殊，==也是比较值
		 * 其他引用类型，比较的是栈里面存的地址
		 * */
		
		int a = 13;
		int b = 12;
		int c = 13;
		
		if(a==c){
			System.out.println("a=c");
		}
		
		String str1 = "abcd";
		String str2 = "abcde";
		String str3 = "abcd";
		if(str1==str3){
			System.out.println("str1=str3");
		}
		
	}
	
}
