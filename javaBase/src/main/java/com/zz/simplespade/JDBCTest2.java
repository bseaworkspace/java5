package com.zz.simplespade;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class JDBCTest2 {
	public static void main(String[]args){
		String driver = "com.mysql.jdbc.Driver"; 
		String database = "java5";
		String userName = "root";
		String pwd = "root";
		String url = "jdbc:mysql://localhost:3306/"+database;
		String sql = "update book set price = 86 where id = 1";
		try {
			//加载驱动
			Class.forName(driver);
			//建立数据库连接
			Connection conn = DriverManager.getConnection(url, userName, pwd);
			
			PreparedStatement ps = conn.prepareStatement(sql);
			
			ps.execute();
			//关闭连接，先关resultset，最后关connection
			if(ps!=null){
				ps.close();
			}
			if(conn!=null){
				conn.close();
			}
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
