package com.zz.simplespade;

/**
 * class后面跟的是类名
 * 
 * interface后面跟的是接口的名字
 * 
 * 
 * */
	
public interface InterfaceTest {
	//接口里面的方法，是没有方法体的，也就是没有{}
	//只是定义方法的名字，输入参数和返回类型
	public void test();
}
