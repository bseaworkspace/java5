package com.zz.simplespade.model;

import java.sql.Date;

public class Book {
	private int id; 
	private String name; 
	private String author; 
	private String category; 
	private String printBy; 
	private Date printDate; 
	private float price; 
	private String is_brow;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getAuthor() {
		return author;
	}
	public void setAuthor(String author) {
		this.author = author;
	}
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public String getPrintBy() {
		return printBy;
	}
	public void setPrintBy(String printBy) {
		this.printBy = printBy;
	}
	public Date getPrintDate() {
		return printDate;
	}
	public void setPrintDate(Date printDate) {
		this.printDate = printDate;
	}
	public float getPrice() {
		return price;
	}
	public void setPrice(float price) {
		this.price = price;
	}
	public String getIs_brow() {
		return is_brow;
	}
	public void setIs_brow(String is_brow) {
		this.is_brow = is_brow;
	}
	
}
