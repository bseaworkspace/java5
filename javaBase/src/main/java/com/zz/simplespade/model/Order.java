package com.zz.simplespade.model;

import java.sql.Date;

public class Order {
	private float id;
	private float sum_amt;
	private float customer_id;
	private Date creat_dt;
	private int desk_id;
	private int status;
	public float getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public float getSum_amt() {
		return sum_amt;
	}
	public void setSum_amt(float sum_amt) {
		this.sum_amt = sum_amt;
	}
	public float getCoustomer_id() {
		return customer_id;
	}
	public void setCoustomer_id(float customer_id) {
		this.customer_id = customer_id;
	}
	public Date getCreat_dt() {
		return creat_dt;
	}
	public void setCreat_dt(Date creat_dt) {
		this.creat_dt = creat_dt;
	}
	public int getDesk_id() {
		return desk_id;
	}
	public void setDesk_id(int desk_id) {
		this.desk_id = desk_id;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
}
