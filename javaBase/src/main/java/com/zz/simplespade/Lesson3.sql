use java5;

select * from class_details;

select * from book;

select nvl(author,'wuming') from book;

select b.name,u.phoneNumber from book b,`user` u  where b.author = u.name;

-- limit 1,4 表示从1号位置开始，取4条记录（注意起始位置是0号位）
select * from book order by price desc limit 1,4;

select name,count(*) gk from `user` where score<60 group by name having gk>=2;

-- 直接把一个查询的结果，当做数据表来使用
select name from(select name,count(*) gk from `user` where score<60 group by name having gk>=2) as t;

-- 把一个子的查询结果，在where中使用，下面语句意思是，书的表中，作者的名字，存在于用户表的书的名字
select book.name 
from book 
where author in (select `user`.name from `user`);

-- 把挂两科以上的学生的平均分和名字查出来
select name,avg(score) 
from `user` 
where name in (
	select name 
	from(
		select name,count(*) gk 
		from `user` 
		where score<60 
		group by name having gk>=2
	) 
	as t
) 
group b;


select book.name from book left join `user` on book.author = `user`.name;

select book.name from book 
left join `user` on book.author = `user`.name where `user`.name='黄琛';

select * from book inner join `user` on book.author = `user`.name;