package com.zz.simplespade;

public class Lesson09 {
	public static void main(String[]args){
		//java是一门面向对象的语言。
		/**
		 * 在现实生活中的一些事物，可以转换成java代码描述。
		 * 比如“哪里坐了一个穿红衣服的人”
		 * 这里我们如果用Java语言描述，
		 * 类名为“人”
		 * 类的一个属性（成员变量）是“红衣服”
		 * 类的一个动作（方法）是“坐”
		 * 
		 */
	}
}
