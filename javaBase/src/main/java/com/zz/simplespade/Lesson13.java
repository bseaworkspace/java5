package com.zz.simplespade;

public class Lesson13 {
	//选择排序:从所有的元素中选择一个最小的元素，放在0号位置
	//选择排序和冒泡排序，比较次数都是一样的，
	//但是效率，选择排序比较高，因为交换次数少（不是绝对，比如数组本来顺序就是对的），
	//冒泡排序交换次数为0，但是选择排序，会是2。
	/**
	 * 
	 * 
	 * 
	 * @param array
	 */
	public void selectSort(int array[]){
		int minIndex = 0;
		int temp = 0;
		int count = 0;
		for(int i =0;i<array.length;i++){
			minIndex = i;
			for(int j = i+1;j<array.length;j++){
				if(array[j]<array[minIndex]){
					minIndex = j;	
				}
			}
			if(minIndex!=i){
				temp = array[i];
				array[i]=array[minIndex];
				array[minIndex]=temp;
				if(array[i] == 52||array[minIndex]==52){
					count++;
				}
			}
		}
		System.out.println(count);
	}
	public static void main(String[]args){
		int []a = new int[7];
		a[0] = 12;
		a[1] = 132;
		a[2] = 22;
		a[3] = 52;
		a[4] = 72;
		a[5] = 925;
		a[6] = 552;
		Lesson13 l13 = new Lesson13();
		l13.selectSort(a);
		for(int i:a){
			System.out.println(i);
		}
	}
}
