package com.zz.simplespade;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class UserDao {
	BaseDao baseDao = new BaseDao();

	public List<User> getUserByName(String name) {
		List<User> ls = new ArrayList<User>();
		Connection conn = baseDao.getConnection();
		// select * from user where name = name;
		try {
			PreparedStatement ps = conn.prepareStatement("select * from user where name = '" + name + "'");
			ResultSet rs = ps.executeQuery();

			while (rs.next()) {
				User u = new User();
				u.setId(rs.getInt("id"));
				u.setName(rs.getString("name"));
				u.setPhoneNumber(rs.getString("phoneNumber"));
				u.setPwd(rs.getString("pwd"));
				u.setScore(rs.getFloat("score"));
				ls.add(u);
			}
			baseDao.closeQuery(rs, ps, conn);

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return ls;
	}

	// 自写代码
	/*
	 * public void setUserByName(User u) { UserDao ud = new UserDao();
	 * 
	 * if (ud.getUserByName(u.getName()).isEmpty()) { Connection conn =
	 * baseDao.getConnection(); PreparedStatement ps; try {
	 * 
	 * ps = conn.prepareStatement( "insert into user (id, name, pwd)values(" +
	 * u.getId() + "," +"'"+ u.getName() +"'"+ "," +"'"+ u.getPwd() +"'"+ ")");
	 * ps.execute(); baseDao.closeUpdate(ps, conn); } catch (SQLException e) {
	 * // TODO Auto-generated catch block e.printStackTrace(); }
	 * System.out.println("注册成功"); }else{ System.out.println("用户名已存在"); } }
	 */
	/**
	 * 输入参数是User类型的形式参数u
	 * 
	 * 表示页面上用户输入的信息，都set到了这个u中
	 * 
	 * 然后，方法里面，就可以通过u的get方法，得到用户输入的值
	 * 
	 * 
	 * 
	 */
	// 师傅代码
	public boolean addUser(User u) {
		boolean flag = false;
		/**
		 * 每一次和数据库打交道， 就是要进行数据库的查询或者修改等 都必须完成jdbc连接的几个步骤
		 * 
		 * 1.加载驱动(Class.forName()) 2.获取连接(DriverManager.getConnection())
		 * 3.获取PreparedStatement(conn.preparestatement()) 4.ps.execute();
		 * 5.倒序关闭连接
		 * 
		 */

		// baseDao.getConnection()封装了第一步和第二步
		Connection conn = baseDao.getConnection();
		/**
		 * ?是PreparedStatement特有的 ?表示占位符，意思是这个地方，会被输入参数代替
		 */
		String sql = "insert into user (id,name,pwd) values(?,?,?)";
		try {
			// 第三步
			PreparedStatement ps = conn.prepareStatement(sql);
			/**
			 * ps.setInt(1, u.getId());
			 * 
			 * PreparedStatement的set方法 第一个参数是位置（下标），起始位置是从1开始。
			 * 第二个参数是输入参数的值，也就是真正的sql语句里面的值。
			 * 
			 */
			ps.setInt(1, u.getId());
			ps.setString(2, u.getName());
			ps.setString(3, u.getPwd());
			// 第四步
			int r = ps.executeUpdate();
			if (r > 0) {
				flag = true;
			}
			// 第五步
			baseDao.closeUpdate(ps, conn);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return flag;
	}

	public static void main(String[] args) {
		UserDao udao = new UserDao();

		List<User> ls = udao.getUserByName("玫瑰");

		if (ls.size() == 0) {
			System.out.println("用户名不存在");
		}

		boolean flag = false;

		String pwd = "123";

		for (User u : ls) {
			System.out.println("userdao-->" + u.getScore());
			if (pwd.equals(u.getPwd())) {
				flag = true;
			}
		}
		if (flag) {
			System.out.println("登陆成功");

		}
		User u2 = new User();
		u2.setId(9);
		u2.setName("瓜皮");
		u2.setPwd("12345");
		udao.addUser(u2);
	}
}
