package com.zz.simplespade.io;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;

public class ReadTemplate {
	
	/**
	 * 字节流读取文件
	 * 
	 * FileInputStream
	 * 
	 * */
	public static void read1(){
		File file = new File("C:" + File.separator + "tmp" + File.separator + "test.txt");
		try {
			FileInputStream fs = new FileInputStream(file);
			byte b[] = new byte[(int)file.length()];
			fs.read(b);
			fs.close();
			System.out.println(new String(b));
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/**
	 * 字符流读取文件
	 * 
	 * FileReader
	 * 
	 * */
	public static void read2(){
		File file = new File("C:" + File.separator + "tmp" + File.separator + "test.txt");
		try {
			Reader r = new FileReader(file);
			char c[] = new char [(int)file.length()];
			int count = r.read(c);
			r.close();
			System.out.println(new String(c, 0, count));
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static void read3(){
		File file = new File("C:" + File.separator + "tmp" + File.separator + "test.txt");
		try {
			InputStreamReader r = new InputStreamReader(new FileInputStream(file),"utf-8");
			BufferedReader br = new BufferedReader(r);
			while(br.ready()){
				br.readLine();
			}
			br.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		read3();
	}

}
