package com.zz.simplespade.io;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;

public class WriteTemplate {
	
	/**
	 * 字节流读取文件内容
	 * 
	 * FileOutputStream
	 * 
	 * */
	public static void write1(){
		File file = new File("C:" + File.separator + "tmp" + File.separator + "test.txt");
		try {
			FileOutputStream fs = new FileOutputStream(file);
			String str = "hello \r\n java5";
			fs.write(str.getBytes());
			fs.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/**
	 * 字符流读取文件内容
	 * 
	 * FileWriter
	 * 
	 * */
	public static void write2(){
		File file = new File("C:" + File.separator + "tmp" + File.separator + "test.txt");
		try {
			Writer w = new FileWriter(file);
			String str = "hello zz";
			w.write(str);
			w.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static void write3(){
		File file = new File("C:" + File.separator + "tmp" + File.separator + "test.txt");
		try {
			//如果第二个参数是true，就不会覆盖，新内容会跟在后面增加
			Writer w = new FileWriter(file,true);
			String str = "2017";
			w.write(str);
			w.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		write3();
	}

}
