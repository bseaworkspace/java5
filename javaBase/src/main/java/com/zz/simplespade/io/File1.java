package com.zz.simplespade.io;

import java.io.File;
import java.io.IOException;

public class File1 {

	/**
	 * 创建一个txt文件
	 * 
	 * @author simpleSpade
	 * 
	 */

	public static void creatFile() {
		File file = new File("C:" + File.separator + "tmp" + File.separator + "test.txt");

		try {
			file.createNewFile();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * 创建一个文件夹
	 * 
	 * @author simpleSpade
	 */

	public static void creatFolder() {
		File file = new File("C:" + File.separator + "tmp");
		file.mkdir();

	}
	
	/**
	 * 创建多个文件夹
	 * 
	 * @author simpleSpade
	 */

	public static void creatFolders() {
		File file = new File("C:" + File.separator + "tmp3" + File.separator + "temp4");
		file.mkdirs();

	}


	/**
	 * 
	 * 删除一个txt文件
	 * 
	 * @author simpleSpade
	 * 
	 */
	public static void deleteFile() {
		File file = new File("C:" + File.separator + "tmp" + File.separator + "test.txt");
		if (file.exists()) {
			file.delete();
			System.out.println("删除文件成功！");
		}else{
			System.out.println("文件不存在！");
			
		}
	}
	
	/**
	 * 
	 * 读取文件夹下面的文件，不能读取子文件夹下面的文件
	 * 
	 * 
	 * */
	
	public static void listFile(){
		File f = new File("C:" + File.separator + "project01" + File.separator + "Login" );
		File [] fs = f.listFiles();
		for(File f1:fs){
			System.out.println(f1);
			if(f1.isDirectory()){
				System.out.println("这是一个文件夹");
			}
		}
			
	}
	
	/**
	 * 
	 * 读取全部文件，包含子文件下面的文件
	 * 
	 * */
	
	public static void listAllFile(File file){
		if(file!=null){
			if(file.isDirectory()){
				File [] fs = file.listFiles();
				for(File f1 : fs){
					listAllFile(f1);
				}
			}
			else{
				System.out.println(file);
			}
		}else{
			System.out.println("文件为空");
		}
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		listAllFile(new File("C:" + File.separator + "project01" + File.separator + "Login"));
	}

}
