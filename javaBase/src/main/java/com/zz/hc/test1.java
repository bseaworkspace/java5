package com.zz.hc;

import com.zz.hc.Factory;

public class test1 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		//数组
		/**
		 * int[]表示申明的是int类型的数组
		 * 
		 * new int[3]其中3表示数组的长度也就是这个数组最多能存3个元素。
		 * 
		 */
		Factory a=new Factory();
		a.create();
		System.out.println(a.create().color);
		
		
		int[] p=new int[3];
		p[0]=12;
		p[1]=23;
		p[2]=33;
		
		char[] b=new char[3];
		b[0]='z';
		b[1]='x';
		b[2]='c';
		
		
		//二维数组
		/**
		 * int[3][2]其中3表示第一维数组长度是3
		 * 其中2表示第二维数组的长度。
		 * 也就是这个二维数组，可以存3个数组，每个数组可以存2个int类型的值
		 * 
		 * 
		 */
		int[][] c=new int[3][2];
		
		c[0][0]=123;
		c[0][1]=124;
		c[1][0]=125;
		c[1][1]=126;
		c[2][0]=127;
		c[2][1]=128;
		
		
		for(int i=0;i<p.length;i++){
			System.out.println("a-->"+i+"-->"+p[i]);
		}
		for(int i=0;i<c.length;i++){
			for(int j=0;j<c[i].length;j++){
				System.out.println("c-->"+i+"-->"+j+"-->"+c[i][j]);
			}
			
			int[][][] d=new int[2][2][2];
			d[0][0][0]=121;
			d[0][0][1]=122;
			d[0][1][0]=123;
			d[0][1][1]=124;
			d[1][0][0]=125;
			d[1][0][1]=126;
			d[1][1][0]=127;
			d[1][1][1]=128;
			for(int f=0;f<d.length;f++){
				for(int j=0;j<d[f].length;j++){
					for(int k=0;k<d[f][j].length;k++)
						System.out.println("c-->"+f+"-->"+j+"-->"+k+"-->"+d[f][j][k]);	
				}
		}
		
		

	}

}
}
