package com.zz.hc.control;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.zz.hc.dao.UserDao;
import com.zz.hc.model.User;

/**
 * Servlet implementation class LoginController
 */
public class LoginController extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public LoginController() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String name=request.getParameter("userName");
		System.out.println("用户名2="+name);
		String pwd=request.getParameter("pwd");
		UserDao userDao=new UserDao();
		List<User>ls=userDao.getUserByName(name);
		String reponseMessage="";
		if(ls.size()==0){
			reponseMessage="用户名不存在";
		}else if(pwd.equals("ls.get(0).getPwd()")){
			reponseMessage="登陆成功";
		}else{
			reponseMessage="密码错误";
		}
		System.out.println("用户名="+name+"密码="+pwd);
		//设置response.setContentType指定 HTTP 响应的编码,同时指定了浏览器显示的编码. 
		response.setContentType("text/html;charset=utf-8");
		response.getWriter().append(reponseMessage);
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
