package com.zz.hc.control;

import java.util.List;

import com.zz.hc.dao.FoodDao;
import com.zz.hc.dao.Food_customerDao;
import com.zz.hc.dao.OrderDao;
import com.zz.hc.model.Food;
import com.zz.hc.model.Food_Customer;
import com.zz.hc.model.Order;

public class OrderControl {
	
	public boolean orderSubmit(List<Food_Customer> List){
		boolean flag =true;
		//--------开始插入订单表
		FoodDao foodDao=new FoodDao();
		float sum_amt=0;
		int customer_id=0;
		int desk_id=0;
		for(Food_Customer t:List){
			int food_id=t.getFood_id();
			Food food=foodDao.getFoodById(food_id);
//			计算整个订单的总价
			sum_amt=sum_amt+food.getPrice();
			customer_id=t.getCustomer_id();
			desk_id=t.getDesk_id();
		}
		OrderDao orderDao=new OrderDao();
		Order o=new Order();
		o.setSum_amt(sum_amt);
		o.setCustomer_id(customer_id);
		o.setDesk_id(desk_id);
		float order_id=orderDao.add(o);
		//--------结束插入订单表order---------
		//--------开始插入中间表Food_customer____
		Food_customerDao food_customerDao=new Food_customerDao();
		for(Food_Customer t:List){
			int food_id=t.getFood_id();
			t.setOrder_id(order_id);
			food_customerDao.add(t.getFood_id(), t.getOrder_id(), t.getDesk_id());
		}
		
		return flag;
		
	}
	
	
	public boolean updateStatus(Order o){
		OrderDao odao=new OrderDao();
		return odao.updateStatus(o);
		
		
	}
	
	

}
