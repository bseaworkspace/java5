package com.zz.hc.dao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.zz.hc.model.Book;
import com.zz.hc.model.User;

public class BookDao {
	BaseDao bd = new BaseDao();

	public List<Book> getBookByName(String name) {
		List<Book> ls = new ArrayList();
		Connection con = bd.getConnection();
		String sql = "select * from book where bookName=?";
		try {
			PreparedStatement ps = con.prepareCall(sql);
			ps.setString(1, name);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				Book bk = new Book();
				int id = rs.getInt("id");
				bk.setId(id);
				bk.setBookName(rs.getString("bookName"));
				bk.setAuthor(rs.getString("author"));
				bk.setCategory(rs.getString("category"));
				bk.setPrintBy(rs.getString("printBy"));
				bk.setPrintDate(rs.getDate("printDate"));
				bk.setPrice(rs.getFloat("price"));
				bk.setIs_brow(rs.getString("is_brow"));
				ls.add(bk);
			}
			bd.closeQuerey(rs, ps, con);

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return ls;

	}

	public List<Book> getBookByNameAndStatus(String name,String Status) {
		List<Book> ls = new ArrayList();
		Connection con = bd.getConnection();
		String sql = "select * from book where bookName=?and is_brow=?";
		try {
			PreparedStatement ps = con.prepareCall(sql);
			ps.setString(1, name);
			ps.setString(2, Status);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				Book bk = new Book();
				int id = rs.getInt("id");
				bk.setId(id);
				bk.setBookName(rs.getString("bookName"));
				bk.setAuthor(rs.getString("author"));
				bk.setCategory(rs.getString("category"));
				bk.setPrintBy(rs.getString("printBy"));
				bk.setPrintDate(rs.getDate("printDate"));
				bk.setPrice(rs.getFloat("price"));
				bk.setIs_brow(rs.getString("is_brow"));
				ls.add(bk);
			}
			bd.closeQuerey(rs, ps, con);

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return ls;

	}

	public boolean borrowBooks(String bookName, String userName) {
		boolean flag = false;
		// 判断这个书有没有
		List<Book> ls = getBookByNameAndStatus(bookName,"N");
		UserDao udao = new UserDao();
		List<User> ls2 = udao.getUserByName(userName);

		if (ls.size() > 0 && ls2.size() > 0) {
			// 借书
			Connection con = bd.getConnection();
			String addSql = "INSERT INTO java5.order_history(book_id,user_id)VALUES(?，?)";
			String updateSql = "update book set is_borw='Y'where book_id=?";
			Book b = ls.get(0);
			User u = ls2.get(0);

			try {
					PreparedStatement ps = con.prepareCall(addSql);
					ps.setInt(1, b.getId());
					ps.setInt(2, u.getId());
					int count = ps.executeUpdate();
					
					if (count > 0) {
						ps = con.prepareCall(updateSql);
						ps.setInt(1, b.getId());
						int c2 = ps.executeUpdate();
						if(c2>0){
							flag=true;
						}
	
					}

			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}

	

		return flag;

	}
	
//	public List<Book> SearchBook(String userName){
//		
//		
//		
//	}
	
	
	
	
	public boolean returnBooks(String bookName, String userName) {
		boolean flag = false;
		// 判断这个书有没有
		List<Book> ls = getBookByNameAndStatus(bookName,"Y");
		
		UserDao udao = new UserDao();
		List<User> ls2 = udao.getUserByName(userName);
		
		if (ls.size() > 0 && ls2.size() > 0) {
			// 借书
			Connection con = bd.getConnection();
			String addSql = "update java5.order_history set end_dt=? where book_id=?and user_id=?";
			String updateSql = "update book set is_borw='N'where book_id=?";
//			String ReturnSql = "";
			
			Book b = ls.get(0);
			
			User u = ls2.get(0);
			try {
				
				PreparedStatement ps=con.prepareStatement(addSql);
    			Date dt=new Date(0);
    			ps.setDate(1, dt);
    			ps.setInt(2, b.getId());
    			ps.setInt(3, u.getId());
    			int count=ps.executeUpdate();
    			if(count>0){
    				ps=con.prepareStatement(updateSql);
    				ps.setInt(1, b.getId());
    				int c2=ps.executeUpdate();
    				if(c2>0){
    					flag=true;
    				}
    			}
				
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
		
		
		
		return flag;
		
	}

}
