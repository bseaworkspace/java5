package com.zz.hc.dao;

import java.util.ArrayList;
import java.util.List;

import com.zz.hc.model.User;

import java.sql.*;
public class UserDao {
	BaseDao baseDao=new BaseDao();
	
//	public void insert(String name){
//		List<User> ls=new ArrayList();
//		Connection con=baseDao.getConnection();
//		try {
//			PreparedStatement ps=con.prepareStatement("select * from user where name='"+name+"'");
//			
//			ResultSet rs=ps.executeQuery();
//			
//			if(rs.next()){
//				if(rs.getString("name").equals(name)){
//					System.out.println("用户名已存在");
//				}
//				if(!rs.getString("name").equals(name)){
//					while(rs.next()){
//						User u=new User();
//						u.setId(rs.getInt("id"));
//						u.setName(rs.getString("name"))	;
//						u.setPhoneNumber(rs.getString("phoneNumber"));
//						u.setScore(rs.getFloat("score"));
//						u.setPwd(rs.getString("pwd"));
//						ls.add(u);
//						
//					}
//					
//					
//				}
//			}
//				
//					
//			
//		} catch (SQLException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//		
////		return ls;
//	
//		
//		
//		
//		
//	}
	
	//boolean 意思是，这个方法添加用户到数据库，是不是成功
	//如果成功，就返回true，失败就返回false；
	/**
	 * 输入参数是user类型的，形参u
	 * 表示页面上用户输入的信息，都set到了这个u
	 * 然后，方法里面，就可以通过u的get方法得到用户输入的值
	 * 
	 * 
	 * 
	 * @param u
	 * @return
	 */
	public boolean addUser(User u){
		boolean flag=false;
		/**
		 * 每一次和数据库打交道，就是要进行
		 * 数据库的查询，或者修改等
		 * 都必须完成jdbc连接的几个步骤
		 * 1.加载驱动（通过Class.ForName）
		 * 2.获取连接（通过DriverManager）
		 * 3.通过Connection获取Statement，
		 * 4.通过statement执行sql语句
		 * 5.倒序关闭所有连接。
		 */
		 //baseDao.getConnection()封装了第一步和第二步
		
		Connection con=baseDao.getConnection();
		/**
		 * insert into user (id,name,pwd)value(?,?,?)
		 * ？表示占位符，意思是这个地方必须会被
		 * 输入参数代替。
		 * 
		 */
		String sql="insert into user (id,name,pwd)value(?,?,?)";
		try {
			//3.通过Connection获取Statement，
			PreparedStatement ps=con.prepareStatement(sql);
			/**
			 * ps.setString(2, u.getName());
			 * PreparedStatement的setString方法，
			 * 第一个参数是位置（下标），起始位置是从1开始
			 * 第二个参数是输入参数的值，也就是真正sql语句里面的值
			 * 
			 * ps.setString(2, u.getName());
			 * 表示：第二个？的地方我们要用u.getName()的值替换。
			 * 
			 */
			ps.setInt(1, u.getId());
			ps.setString(2, u.getName());
			ps.setString(3, u.getPwd());
			//4.通过statement执行sql语句
			int r=ps.executeUpdate();
			if(r>0){
				flag=true;
			}
			//5.倒序关闭所有连接。
			baseDao.closeUpdate(ps, con);
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		
		return flag;
		
	}
	
	
	
	
	public List<User> getUserByName(String name){
		List<User> ls=new ArrayList();
		/**
		 * 
		 * 
		 * 
		 */
		
		
		Connection con=baseDao.getConnection();
		try {
			PreparedStatement ps=con.prepareStatement("select * from user where name='"+name+"'");
			ResultSet rs=ps.executeQuery();
					
			while(rs.next()){
				User u=new User();
				u.setId(rs.getInt("id"));
				u.setName(rs.getString("name"))	;
				u.setPhoneNumber(rs.getString("phoneNumber"));
				u.setScore(rs.getFloat("score"));
				u.setPwd(rs.getString("pwd"));
				ls.add(u);
				
			}
					
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return ls;
	}
	

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		UserDao udao=new UserDao();
		/**
		 * 
		 * 
		 */
		List<User> ls=udao.getUserByName("king");
		if(ls.size()==0){
			System.out.println("用户名不存在");
		}
		boolean flag=false;
		String pwd="13652";
		
		for(User u1:ls){
			System.out.println(u1.getScore());
			if(pwd.equals(u1.getPwd())){
				flag=true;
			}
		}
		if(flag){
			System.out.println("登陆成功");
		}else{
			
			System.out.println("用户名或密码错误");
		}
		

	}

}
