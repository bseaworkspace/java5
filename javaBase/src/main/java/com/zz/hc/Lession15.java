package com.zz.hc;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Lession15 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Map map=new HashMap();
		//<String,Object>泛型 预先规定，这个map的key必须是String类型的
	    //值是object类型，也就是值可以随便改
		Map<String,Object> map2=new HashMap();
		
		map.put("test", 33333);
		map.put(888, 777);
		map.put(true, "dgddg");
		
		List list=new ArrayList();
		list.add("饮料");
		list.add("水果");
		list.add("玩具");
		
		List list2=new ArrayList();
		list2.add("饮料");
		list2.add("水果");
		list2.add("玩具");
		list2.add(list);
		
		map.put("全家便利店", list);
		
		
		System.out.println(map.get(888));
		System.out.println(map.get(true));
		System.out.println(map.get("test"));
		System.out.println(map.get("全家便利店"));
		
//		for(int i=0;i<map.keySet().size();i++){
//			System.out.println(map.keySet());
//			
//		}
		//""+111把int类型的111转化成string类型
		map2.put(""+111, 5555);
		
		map2.put("全家便利店", list);
		
		map2.put("ttt","coming  ");
		
		for(String key:map2.keySet()){
			System.out.println(key+"map2="+map2.get(key));
			if(key=="list"){
				List tmp=(List)map2.get(key);
				List temp2= (List)tmp.get(3);
			}
//			for(String s:temp2){
//				System.out.println(s);
//			}
		}
		for(Object key:map.keySet()){
			System.out.println("map="+map.get(key));
			
		}

	}

}
