package com.zz.hc;

public class Lession12 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int[] a=new int[7];
		a[0]=12;
		a[1]=132;
		a[2]=22;
		a[3]=52;//使用冒泡排序，从大到小，问这个52一共移动了多少次？
		a[4]=72;
		a[5]=925;
		a[6]=552;
		//冒泡排序
		/**
		 * 从大到小排序
		 * 1.两个for循环
		 * 2.第一个for循环的初始值是0，第二个for循环的初始值是i+1
		 * 3.只有i位置的值比j位置的值小的时候，才交换位置。
		 * 
		 * 
		 */
		//1.两个for循环
		for(int i=0;i<a.length;i++){  
			//2.第一个for循环的初始值是0，第二个for循环的初始值是i+1
		    for(int j=i+1;j<a.length;j++){  
		    	//只有i位置的值比j位置的值小的时候，才交换位置。
		        if(a[i] < a[j]){  
		            int temp = a[i];  
		            a[i] = a[j];  
		            a[j] = temp;  
		     
		      }   
		}
	}
	//for循环的另外一种写法，for each写法。 第二个参数是数组，第一个参数是一个临时变量
	//每次都把数组里面的元素赋值给第一个参数。
	for(int t:a){
		System.out.println(t);
	}
		
	}

}
