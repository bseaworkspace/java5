package com.zz.hc;

public class Lession13 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int[] a=new int[7];
		a[0]=12;
		a[1]=132;
		a[2]=22;
		a[3]=52;//使用冒泡排序，从大到小，问这个52一共移动了多少次？
		a[4]=72;
		a[5]=925;
		a[6]=552;
		//选择排序：从所有的元素中选择一个最小的元素，放在0号位置。
		//比较排序和冒泡排序，比较次数是一样的，但是效率，比较排序比较高，因为交换次数少（不是绝对的，比如数组本来顺序就是对的）
		//冒泡排序交换次数是0，但是比较排序，会是2.
		int count=0;
		for(int i=0;i<a.length;i++){
			int maxindex = i;
			for(int j=i+1;j<a.length;j++){
				
				if(a[j]>a[maxindex]){
					maxindex=j;
				}
			}
			int temp=a[i];
			a[i]=a[maxindex];
			a[maxindex]=temp;
			if(a[i]==52||a[maxindex]==52){
				count++;	
			}
			
		}
		System.out.println(count);
		for(int t:a){
			System.out.println(t);
		}

	}

}
