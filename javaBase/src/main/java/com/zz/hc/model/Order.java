package com.zz.hc.model;

import java.sql.Date;

public class Order {
	private float id; 
	private float sum_amt; 
	private int customer_id; 
	private Date crate_dt; 
	private int desk_id; 
	private int status;
	public float getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public float getSum_amt() {
		return sum_amt;
	}
	public void setSum_amt(float sum_amt) {
		this.sum_amt = sum_amt;
	}
	public int getCustomer_id() {
		return customer_id;
	}
	public void setCustomer_id(int customer_id) {
		this.customer_id = customer_id;
	}
	public Date getCrate_dt() {
		return crate_dt;
	}
	public void setCrate_dt(Date crate_dt) {
		this.crate_dt = crate_dt;
	}
	public int getDesk_id() {
		return desk_id;
	}
	public void setDesk_id(int desk_id) {
		this.desk_id = desk_id;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}

}
