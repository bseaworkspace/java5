package com.zz.hc.model;

import java.sql.*;

public class Book {
	
	private int id;
	private String bookName; 
	private String author;
	private String category; 
	private String printBy; 
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getBookName() {
		return bookName;
	}
	public void setBookName(String bookName) {
		this.bookName = bookName;
	}
	public String getAuthor() {
		return author;
	}
	public void setAuthor(String author) {
		this.author = author;
	}
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public String getPrintBy() {
		return printBy;
	}
	public void setPrintBy(String printBy) {
		this.printBy = printBy;
	}
	public Date getPrintDate() {
		return printDate;
	}
	public void setPrintDate(Date printDate) {
		this.printDate = printDate;
	}
	public float getPrice() {
		return price;
	}
	public void setPrice(float price) {
		this.price = price;
	}
	public String getIs_brow() {
		return is_brow;
	}
	public void setIs_brow(String is_brow) {
		this.is_brow = is_brow;
	}
	private Date printDate; 
	private float price;
	private String is_brow;

}
