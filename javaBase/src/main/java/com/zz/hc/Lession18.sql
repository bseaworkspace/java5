use java5;


-- select表示查询
-- *表示所有列
-- from 表示后面的是表的名字
select * from user;

select name,pwd from user ;

-- 查询
select * from user where name='玫瑰';

select * from user where id>5;

-- like表示模糊查询其中%表示0或更多
select * from user where pwd like '4%';


-- where后面如果要多个过滤的查询条件，用and连接，结果会越来越少，因为取交集
select * from user where phoneNumber like '168%' and '%6' ;


-- where后面如果要多个过滤的查询条件，用or连接，结果会越来越多，因为取并集
select * from user where phoneNumber like '%5'or name='king';


select * from user where name like '%文%';






-- 插入数据
insert into user(id,name,pwd,phoneNumber) values (8,'king1','13652','13262215729');

-- 删除数据
delete from user where name='king';

-- 修改数据
update user set name='king' where id=8;




select * from student;

insert into student1(id,name,className,pwd,phoneNumber,date_join,address) values (1,'玫瑰1','1411','58652','13262215721',19960417,'橄榄路水华路1350');
insert into student1(id,name,className,pwd,phoneNumber,date_join,address) values (2,'玫瑰2','1411','58652','13262215722',19960417,'橄榄路水华路1350');
insert into student1(id,name,className,pwd,phoneNumber,date_join,address) values (3,'玫瑰3','1411','58652','13262215723',19960417,'橄榄路水华路1350');
insert into student1(id,name,className,pwd,phoneNumber,date_join,address) values (4,'玫瑰4','1411','58652','13262215724',19960417,'橄榄路水华路1350');
insert into student1(id,name,className,pwd,phoneNumber,date_join,address) values (5,'玫瑰5','1411','58652','13262215725',19960417,'橄榄路水华路1350');
insert into student1(id,name,className,pwd,phoneNumber,date_join,address) values (6,'玫瑰6','1411','58652','13262215726',19960417,'橄榄路水华路1350');
insert into student1(id,name,className,pwd,phoneNumber,date_join,address) values (7,'玫瑰7','1411','58652','13262215727',19960417,'橄榄路水华路1350');
insert into student1(id,name,className,pwd,phoneNumber,date_join,address) values (8,'玫瑰8','1411','58652','13262215728',19960417,'橄榄路水华路1350');
insert into student1(id,name,className,pwd,phoneNumber,date_join,address) values (9,'玫瑰9','1411','58652','13262215729',19960417,'橄榄路水华路1350');


