package com.zz.hc;

public class Lession16 {
	
	public String name="jacky";
	public static String className="java5";
	//被final修饰的变量，叫常量，值不能再变，一般名字全部大写
	public final static String IDNUM="642642685236";
	
	
	//static 需要放在返回类型的前面
	public  static void testStatic(){
		System.out.println("这是一个静态方法");
		
	}
	

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		
		//八个基本数据类型byte，short，int，long，float，double，char，boolean
		/**
		 * 
		 * 
		 * 基本数据类型存在栈里面的。
		 * 引用类型，值是存在堆里面，地址的值是存在栈里面
		 * 
		 * 
		 * 
		 * 
		 * 
		 */
		//关键字：static 静态的
		//特征：被static修饰的成员变量，或者方法，可以在
		//不需要new的情况下，使用。
		
		
		//final 被final修饰的变量，一旦赋值，就不能被改变。
		
		
		
		
		

	}

}
