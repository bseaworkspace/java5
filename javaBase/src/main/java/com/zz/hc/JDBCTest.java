package com.zz.hc;

import java.sql.*;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;

public class JDBCTest {
	
	

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		try {
			//加载驱动
			Class.forName("com.mysql.jdbc.Driver");
			//获取数据库连接
			Connection con=DriverManager.getConnection("jdbc:mysql://localhost:3306/java5", "root","warpten");
			
			//获取sql语句
			PreparedStatement ps= con.prepareStatement("select*from user");
			
			ResultSet rs=ps.executeQuery();
			while(rs.next()){
				int id=rs.getInt(1);
				String name=rs.getString(2);
				
				int id2=rs.getInt("id");
				
				System.out.println("id="+id+ "name="+name+ "id2="+id2);
				
			}
			//关闭连接必须从里到外关闭。也就是先关rs，再管ps，最后关con。
			if(rs!=null){
				rs.close();
			}
			if(ps!=null){
				ps.close();
			}
			if(con!=null){
				con.close();
			}
			
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
