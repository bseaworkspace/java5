package com.zz.hc;

/**
 * extends是表示继承，后面跟的是父类的名字
 * 
 * 1.java是单继承。只能继承一个父类
 * 
 * 2.继承以后，子类会自动拥有父类所有属性和方法
 * 
 * 
 * 
 * 
 * @author king
 *
 */



public class BussCar extends Car{
	
	String no;
	
	public void run(int a){
		
		
		//方法的重写
		/**
		 * 1.必须是父类和子类之间，子类重写父类的方法
		 * 
		 * 2.方法名字必须一样。
		 * 
		 * 3.参数的个数，和类型必须一样。
		 * 
		 * 4.返回的数据类型，也必须一样，否则会报编译错误。
		 */
		System.out.println("这辆公交车+"+no+"以每小时"+a+"公里的速度在跑");
	}
	
    public boolean openDoor(String way,String a){
		
		System.out.println("车以"+way+"方式打开了门");
		
		return false;
		
	}
	
	
	
	
	
	
}
