package com.zz.hc;
/**
 * class后面更的是类的名字
 * 
 * 
 * interface后面更的是接口的名字
 * 
 * 
 * 
 * @author king
 *
 */

public interface InterfaceTest {
	//接口里面的方法，是没有方法体也就是没有｛｝
	//只是定义方法的名字，输入参数和返回类型
	public void test();
	

}
