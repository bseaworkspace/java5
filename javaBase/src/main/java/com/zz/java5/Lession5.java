package com.zz.java5;

import java.util.ArrayList;
import java.util.List;

public class Lession5 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		for(int i=0;i<10;i++){
		
			for(int j=0;j<i;j++){
				//System.out.print  会在一行打印
				//System.out.println 会打印以后，马上换行
				System.out.print("#");
			}
			System.out.println("");
			
		}
		
		
		
		List<String> ls=new ArrayList();
		
		ls.add("hello");
		ls.add("你好五班");
		ls.add("大家好");
		
		
		for(String s: ls){
			System.out.println(s);
		}
		
		//ls.size() 返回list集合里面元素的总数
		for(int i=0;i<ls.size();i++){
			
			System.out.println("通过for循环来获取list里面元素的值       "+ls.get(i));
		}
		
	}

}

