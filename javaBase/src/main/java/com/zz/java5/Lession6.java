package com.zz.java5;

public class Lession6 {
	//类里面包含： 变量（属性），方法
	/**
	 * 变量分两种：1.成员变量   2.局部变量
	 * 
	 * @param args
	 */
	//声明的地方，不在任何一个方法里面的，叫成员变量
	int a=1;

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		//声明在一个方法里面的，叫局部变量
		int a=2;
		//如果局部变量的名字和成员变量的名字一样，在方法里面
		//局部变量的值会覆盖成员变量的值
		System.out.println("a="+a);
		
		
		Lession6 l6=new Lession6();
		l6.test();
		Lession7 l7=new Lession7();
		l7.test2();
		
		

	}
	
	public  void test(){
		
		System.out.println("from test a="+a);
	}

}
