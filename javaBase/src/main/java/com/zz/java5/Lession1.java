//package后面跟的是包的名字，这个是我们创建Java
//类的时候，自己定义的。
package com.zz.java5;


//public这个是修饰符，表示可以在整个项目中，访问到这个类
//class 表示后面跟的是“类”的名字
public class Lession1 {

	/**
	 * java的数据类型分两种。
	 * 《1》基本类型
	 * 《2》引用类型
	 * 
	 * @param args
	 * 
	 * -----------基本类型-----------
	 * java有8个基本类型
	 * 
	 * byte
	 * short
	 * char
	 * int
	 * boolean
	 * float
	 * long
	 * double
	 * 
	 */
	
	//byte类型是最小范围的java基本数据类型，占8位
	//范围是最大值是127（2^7-1），最小值是-128(-2^7)  ，整数
	byte a=127;
	
	
	//范围是最大值32767(2^15-1), 最小值是-32768（-2^15）,占16位，整数
	short b=1;
	
	//范围是，最大值2147485647（2^31-1）,最小值是-2147485648，占32位，整数
	int c=1;
	//范围是，最大值2^63-1，最小值是-2^63, 整数
	long d=1;
	
	//float是单精度，32位的浮点型
	float e=1.1f;
	
	//double是双精度，64位。
	double g=1.11;
	
	
	//boolean只可能是true, false其中的一个
	
	boolean f=true;
	
	//char是一个单位的unicode字符，最多能存一个中文字符
	char h='g';
	char y='国';
	
	
	
	
	
	//public这个是修饰符，表示可以在整个项目中，访问到这个类
	//main方法是一个特殊的Java方法，如果一个类有
	//main 方法，才可以右键-->Run as-->Application类
	//来运行main方法
public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println("hello java 5 班");
		
		//声明并且赋值一个变量
		//一共三部分
		//第一部分：数据类型（就是刚刚那8个类型里面选一个）
		//第二部分是变量的名字，这个是自己随便取的名字
		//第三部分是，等号和值，值是必须是第一部分中数据类型范围以内的
		int test1=32;
		int test2=30;
		
		int test3=test1+test2;
		int test4=test1-test2;
		int test5=test1*test2;
		int test6=test1/test2;
		double test7=test1/test2;
		
		System.out.println(test3);
		System.out.println(test4);
		System.out.println(test5);
		System.out.println(test6);
		System.out.println(test7);
		
		byte b1=100;
		//从小到大，类型可以自动的转换
		//把byte类型， 转换到 了int类型，是从小到大
		int i1=b1;
		
		//从大到小转换，不能自动转换，必须强制转换，也就是使用“（类型）”
		//把int类型的变量，转换成btye类型，是从大到小，需要强制转换
		b1=(byte)i1;
		
		
		//声明一个变量
		//数据类型， 变量名字（自己随便取）
		int value;
		
		
		//用等号，表示赋值
		value=111;
		
		System.out.println(value);
		
		

	}

}
