
package com.zz.java5;

public class Lession4 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		//for()   int i=0  声明了一个int类型的变量i 并且赋值为0
		//i<10 一个结果是true或者false的判断表达式。
		//i++ 相当于 i=i+1
		
		int j=8;
		//j=j+1;
		j++;
		System.out.println("j="+j);
		
		for(int i=0;i<10;i++){
			
			System.out.println(i);
			
		}
		
		//i+=2 相当于 i=i+2
	   for(int i=0;i<10;i+=2){
			
			//System.out.println("*");
			
		}
	   //i-- 相当于 i=i-1
	   for(int i=10;i>5;i--){
		   
		   System.out.println("*");
		   
	   }		

	}

}