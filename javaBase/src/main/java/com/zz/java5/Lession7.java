package com.zz.java5;

public class Lession7 {
	
	/*
	 * java的修饰符
	 *
	 *          同一个类	同一个包	不同包中的子类	任何地方
      private	可以访问	不可以		
                无修饰符	            可以访问	 可以访问		
     protected	可以访问	可以访问	可以访问	
     public 	可以访问	可以访问	可以访问	可以访问
	 */

	//只能是同一个类里调用
	private void test1(){
		
		System.out.println("test1 is running");
	}
	//只能是本类和同一个包下面的类，可以访问
	 void test2(){
		
		System.out.println("test2 is running");
	}
	
	public static void main(String[] args) {
		Lession7 l7=new Lession7();
		l7.test1();
	}
}


