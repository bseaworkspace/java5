package com.zz.java5;


//import 表示 导入下面的java类
import java.util.ArrayList;
import java.util.List;

public class Lession3 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		// * java 数据类型之“引用类型”
		
		//String是我们常用的引用类型
		String test="你好，string";
		//集合
		List ls=new ArrayList();
		
		int a=100;
		ls.add(a);
		
		int b=200;
		
		ls.add(b);
		int c=400;
		
		ls.add(c);
		
		System.out.println("0号位置的值是=="+ls.get(0));
		System.out.println("1号位置的值是=="+ls.get(1));
		System.out.println("2号位置的值是=="+ls.get(2));
		
		//自己创建的Java类
		Lession2 l2=new Lession2();
		
		l2.test();
		
		
		
		//if(一个判断的表达式，结果会是true或者false){ 如果小括号里面的判断结果是true,
		//就会执行大括号里面的代码}
		int g=2;
		int g2=11;
		if(g>g2){
			
			System.out.println("g>g2");
		}
		
		
		if(g>g2){
			
			System.out.println("g>g2");
		}else{
			
			System.out.println("g<g2");
		}
		
		int score=75;
		if(score>80){
			
		
			
			System.out.println("优秀");
		}else if(score>60){
			
			System.out.println("恭喜及格");
		}else{
			
			System.out.println("没有及格");
		}
		
		
		
		

	}

}
