package com.zzwsq_1.model;

import java.sql.Date;

public class Order {
	private float id;
	private	float sum_amt;
	private int customer_id;
	private	Date create_date;
	private	int desk_id;
	private	int state;
	public float getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public float getSum_amt() {
		return sum_amt;
	}
	public void setSum_amt(float sum_amt) {
		this.sum_amt = sum_amt;
	}
	public int getCustomer_id() {
		return customer_id;
	}
	public void setCustomer_id(int customer_id) {
		this.customer_id = customer_id;
	}
	public Date getCreate_date() {
		return create_date;
	}
	public void setCreate_date(Date create_date) {
		this.create_date = create_date;
	}
	public int getDesk_id() {
		return desk_id;
	}
	public void setDesk_id(int desk_id) {
		this.desk_id = desk_id;
	}
	public int getState() {
		return state;
	}
	public void setState(int state) {
		this.state = state;
	}

}
