package com.zzwsq_1;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class JDBCTest2 {
	public static void main(String[]args){
		   try {
			   //加载驱动
			Class.forName("com.mysql.jdbc.Driver");
			Connection con=DriverManager.getConnection("jdbc:mysql://localhost:3306/java5", "root", "1995wang");
		    //获取数据库连接
			PreparedStatement ps=con.prepareStatement("update book set price=86 where price=100;");
			//获取statement
		   
		   ps.execute();//execute 即可执行update也可以delete
		   
		    //关闭连接，必须从里到外关，也就是先关resultset 最后在关connection
//		    if(rs!=null){
//		    	rs.close();
//		    }
		    if(ps!=null){
		    	ps.close();
		    }if(con!=null){
		    con.close();
		    }
		    
		    
		    
		    
		   } catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}	
		
		
		
		}
	

}
