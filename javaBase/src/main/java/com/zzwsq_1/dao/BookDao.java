package com.zzwsq_1.dao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.zzwsq_1.model.Book;
import com.zzwsq_1.model.User;

public class BookDao {
	BaseDao basedao = new BaseDao();

	public List<Book> getBookByName(String name) {
		List<Book> ls = new ArrayList();
		Connection con = basedao.getConnection();
		String sql = "select *from book where name=?";
		try {
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setString(1, name);//设置占位符
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				Book b = new Book();
				int id = rs.getInt("id");
				b.setId(id);
				b.setName(rs.getString("name"));
				b.setAuthor(rs.getString("author"));
				b.setCategory(rs.getString("category"));
				b.setPrintBy(rs.getString("printBy"));
				b.setPrintDate(rs.getDate("printDate"));
				b.setPrice(rs.getFloat("price"));
				b.setPrintBy(rs.getString("printBy"));
				b.setPrintDate(rs.getDate("printDate"));
				ls.add(b);
			}
			basedao.closeQuery(rs, ps, con);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return ls;

	}

	public List<Book> getBookByNameStatue(String name,String statue) {
		List<Book> ls = new ArrayList();
		Connection con = basedao.getConnection();
		String sql = "select *from book where name=? and is_brow='N'";
		try {
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setString(1, name);
			ps.setString(2, statue);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				Book b = new Book();
				int id = rs.getInt("id");
				b.setId(id);
				b.setName(rs.getString("name"));
				b.setAuthor(rs.getString("author"));
				b.setCategory(rs.getString("category"));
				b.setPrintBy(rs.getString("printBy"));
				b.setPrintDate(rs.getDate("printDate"));
				b.setPrice(rs.getFloat("price"));
				b.setPrintBy(rs.getString("printBy"));
				b.setPrintDate(rs.getDate("printDate"));
				ls.add(b);
			}
			basedao.closeQuery(rs, ps, con);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return ls;

	}
	public boolean add(Book b){
		boolean flag=false;
		String sql="insert into book (name,author,price)value(?,?,?)";
		Connection con=basedao.getConnection();
		
		try {
			PreparedStatement ps=con.prepareStatement(sql);
			ps.setString(1,b.getName() );
			ps.setString(2, b.getAuthor());
			ps.setFloat(3, b.getPrice());
			ps.executeUpdate();
			basedao.closeUpdate(ps, con);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return flag;
	}
	
	
	
	

	public boolean borrowBook(String name, String userName) {
		boolean flag = false;
		// 判断这个书在不在
		List<Book> ls = getBookByNameStatue(name,"Y");
		UserDao userdao = new UserDao();
		List<User> ls2 = userdao.getUserByName(userName);
		if (ls.size() > 0&&ls2.size()>0) {

			// 借书
			Connection con = basedao.getConnection();
			String addSql = "INSERT INTO java5.order_history( book_id,  user_id)VALUES(?,?);";
			String updateSql = "update book set id_brow='y' where book_id=?";
			Book b = ls.get(0);//只有借书情况和书的名字
			User u = ls2.get(0);//只有借书人名
			try {
				PreparedStatement ps = con.prepareStatement(addSql);
				ps.executeUpdate();
				ps.setInt(1, b.getId());
				ps.setInt(2, u.getId());
				
				int count = ps.executeUpdate();
				if (count > 0) {
					ps = con.prepareStatement(updateSql);
					ps.setInt(1, b.getId());
					int c2 = ps.executeUpdate();
					if (c2 > 0) {
						flag = true;
					}

				}
				
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		return flag;
	}
	public boolean returnBook(String name, String userName) {
		boolean flag = false;
		// 判断这个书在不在
		List<Book> ls = getBookByNameStatue(name, "N");
		UserDao userdao = new UserDao();
		List<User> ls2 = userdao.getUserByName(userName);
		if (ls.size() > 0&&ls2.size()>0) {

			// 借书
			Connection con = basedao.getConnection();
			String addSql = "INSERT INTO java5.order_history( book_id,  user_id)VALUES(?,?);";
			String updateSql = "update book set id_brow='y' where book_id=?";
			Book b = ls.get(0);
			User u = ls2.get(0);
			try {
				PreparedStatement ps = con.prepareStatement(addSql);
			
				Date dt=new Date(0);
				ps.setDate(1, dt);
				ps.setInt(1, b.getId());
				ps.setInt(2, u.getId());
				int count = ps.executeUpdate();
				if (count > 0) {
					ps = con.prepareStatement(updateSql);
					ps.setInt(1, b.getId());
					int c2 = ps.executeUpdate();
					if (c2 > 0) {
						flag = true;
					}

				}
				basedao.closeUpdate(ps, con);
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		return flag;
	}
	
	public static void mian(String []args){
		BookDao bookdao1=new BookDao();
		bookdao1.borrowBook("spring", "");
	}
	
}
