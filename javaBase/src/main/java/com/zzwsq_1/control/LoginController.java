package com.zzwsq_1.control;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.zzwsq_1.dao.UserDao;
import com.zzwsq_1.model.User;

/**
 * Servlet implementation class LoginController
 */
public class LoginController extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public LoginController() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String name=request.getParameter("UserName");
		String pwd=request.getParameter("pwd");
		UserDao userdao=new UserDao();
		List<User> ls=userdao.getUserByName(name);
		List<User> ls2;
          String responseMessage=""; 	
          String path="wsq/login.jsp";
 		if(ls.size()==0){
 			 responseMessage="用户名不存在"; 
		}else if(pwd.equals(ls.get(0).getPassword())){


			ls2=userdao.getExamSunmmary();
			System.out.println(ls.get(0).getName());
			request.setAttribute("userlist", ls2);//这个方法相当于key value 形式
			
			
			responseMessage="登录成功";
			path="wsq/showExamSummary.jsp";
			
		}else{
			responseMessage="密码错误";
		}
		
		System.out.println("用户名:"+name+"密码:"+pwd);
		response.setContentType("text/html;charset=utf-8");
//		response.getWriter().append("Served at: ").append(responseMessage); //打印上面responseMessage的里面
		request.getRequestDispatcher(path).forward(request, response);
	//forward方法是请求转发，把servlet的请求转发实现页面跳转
	
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
		//dopost会调用doget方法，所以写在doget里面
	}

}
