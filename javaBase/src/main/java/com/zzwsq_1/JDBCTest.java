package com.zzwsq_1;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class JDBCTest {
	
	
	
	public static void main(String []args){
	   try {
		   //加载驱动
		Class.forName("com.mysql.jdbc.Driver");
		Connection con=DriverManager.getConnection("jdbc:mysql://localhost:3306/java5", "root", "1995wang");
	    //获取数据库连接
		PreparedStatement ps=con.prepareStatement("select*from user;");
		//获取statement
	    ResultSet rs=ps.executeQuery();//只能用于查询
	    //执行sql以后，获取执行结果
	    while(rs.next()){
	    int id=rs.getInt(1);
	    String name=rs.getString(2);
	    int id2=rs.getInt("id");
	    System.out.println("id="+ id + "name="+ name + "id2="+ id2 );
	    	
	    }
	    //关闭连接，必须从里到外关，也就是先关resultset 最后在关connection
	    if(rs!=null){
	    rs.close();
	    }
	    if(ps!=null){
	    	ps.close();
	    }if(con!=null){
	    con.close();
	    }
	    
	    
	    
	    
	   } catch (ClassNotFoundException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	} catch (SQLException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}	
	
	
	
	}

}
