package com.zzwsq_1;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class UserDao {
	BaseDao baseDao=new BaseDao();
	
	public List<User> getUserByName(String name){
		List<User> ls=new ArrayList();
		Connection con=baseDao.getConnection();
		try {
			PreparedStatement ps=con.prepareStatement("select *from user where name='" +name+"' ");
			ResultSet rs=ps.executeQuery();
			while(rs.next()){
				User u=new User();
				u.setId(rs.getInt("id"));
				u.setName(rs.getString("name"));
				u.setPhoneNumber(rs.getString("phoneNumber"));
				u.setScorr(rs.getFloat("scorr"));
				u.setPassword(rs.getString("password"));
				ls.add(u);
			}
			baseDao.closeQuery(rs, ps, con);
			
		
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return ls;
	}
	public boolean addUser(User u){
		// boolean 意思是，这个方法添加用户到数据库，是不是成功，
		//如果成功就返回true，失败返回false
		/*输入参数是一个user类型的，形参
		 * 表示页面上用户输入的信息都set到了这个user中
		 * 然后方法里面就可以通过u的get方法同过用户的值
		 * 
		 */
		boolean flag=false;
		Connection con=baseDao.getConnection();  //baseDao.getConnection()封装了第一步和第二步		
		/*每一次跟数据库打交道，就是要跟数据库的查询或者修改等
		 * 都必须完成jdbc连接的几个步骤
		 * 1.通过(Class.forName(""))获取加载驱动
		 * 2.通过(driverManager)获取连接
		 * 3.通过Connection 获取statement，
		 * 4.通过statement执行sql语句
		 * 5.倒序关闭所有连接
		 */
		//1.通过(Class.forName(""))获取加载驱动
		// * 2.通过(driverManager)获取连接
		String sql="insert into user(id,name,password) values(?,?,?)";
		/*insert into user(id,name,password) values(?,?,?)
		 * ? 表示占位符，意思是这个地方会被输入参数代替，set
		 * 
		 */
		try {
			PreparedStatement ps=con.prepareStatement(sql);
			//3.通过Connection 获取statement
			ps.setInt(1, u.getId());
            /*ps.setString(2,u.getName);
             * PreparedStatement的setString方法，
             * 第一个参数是位置(下标),起始位置是从1开始
             * 第二个参数是输入参数的值，也就是真正sql语句里面的值
             * 
             * ps.setString(2,u.getName)
             * 	表示第二个？的地方，我们要用u.getName()得值替代		
             */
			ps.setString(2, u.getName());
			ps.setString(3, u.getPassword());
			
			int r=ps.executeUpdate(); 
			if(r>0){
				flag=true;
			}
			//4.通过statement执行sql语句
			baseDao.closeUpdate(ps, con);
			//5.倒序关闭所有连接
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		return flag;
	}
	
	
	
	public static void main(String[]args){
		UserDao udao=new UserDao();
		List<User> ls=udao.getUserByName("玫瑰");
		if(ls.size()==0){
			System.out.println("用户名不存在");
		}
		boolean flag=false;
		String pwd="123";
		for(User u1:ls){
			System.out.println("userdao-->"+u1.getScorr());
			if(pwd.equals(u1.getPassword())){
				flag=true;
			}
		}
		if(flag){
			System.out.println("登陆成功");
		}else{
			System.out.println("用户名或密码错误");
		}
		
	}

}
