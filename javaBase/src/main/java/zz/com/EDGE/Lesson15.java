package zz.com.EDGE;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class Lesson15 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Map<Object,Object> map=new HashMap<Object,Object>();
		map.put("猫女", "dsfdg");
		map.put("ttt", "dsfdg");
		map.put("长江", "ios");
		for(Map.Entry<Object,Object>entry:map.entrySet()){
			System.out.println("Key="+entry.getKey()+",Value ="+entry.getValue());
	}

		//第二种方法
		Map<Object,Object> map1=new HashMap<Object,Object>();
		map1.put("猫女", "dsfdg");
		map1.put("ttt", "dsfdg");
		map1.put("长江", "ios");
		for(Object key:map.keySet()){
			System.out.println("Key="+key);
		}
		for(Object value:map.values()){
			System.out.println("Value ="+value);
		}
		//第三种方法
		Map<Object,Object> map2=new HashMap<Object,Object>();
		map2.put("猫女", "dsfdg");
		map2.put("ttt", "dsfdg");
		map2.put("长江", "ios");
		Iterator<Map.Entry<Object,Object>> entries=map.entrySet().iterator();
		while(entries.hasNext()){
			Map.Entry<Object,Object>entry=entries.next();
			System.out.println("key="+entry.getKey()+",Value="+entry.getValue());
		}
	}
}
