package zz.com.EDGE;
//4. 比较培训实现一个数组的从小到大排序。 
public class Lesson9 {
	public static void selection(int[] a) {
		int indexMax = 0;
	
		for (int i = 0; i < a.length; i++) {
			indexMax = i;
			for (int I = i + 1; I < a.length; I++) {
				if (a[indexMax] < a[I]) {
					indexMax = I;
				}
			}
			if (indexMax != i) {
				int temp = a[i];
				a[i] = a[indexMax];
				a[indexMax] = temp;

			}
		}

	
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int[] a = new int[7];
		a[0] = 127;
		a[1] = 2;
		a[2] = 122;
		a[3] = 85943;
		a[4] = 34235;
		a[5] = 922;
		a[6] = 552;
		selection(a);
		for (int X : a) {
			System.out.println(X);
		}

	}

}
