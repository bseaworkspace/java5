package zz.com.EDGE;
/**extends时表示继承，后面跟父类的名字；
 * 1.java是单继承，只能继承父类
 * 2.继承之后，子类会自动拥有父类的所有属性和方法
 * 
 * 
 * @author Administrator
 *
 */
public class Bus extends Car{
String no;

//方法的重写
/**
 * 1.必须是父类和子类之间，子类重写父类的方法
 * 2.方法名字一样
 * 3.参数的个数和类型必须一样。
 * 4.返回的数据类型也必须一致，否则会报编译错误。
 */


public void run(int x){
	System.out.println("这辆公交车+"+no+"车以"+x+"公里每小时的速度在跑");
}

public boolean openDoor(String way,String a){
	System.out.println("车以"+way+"方法打开了门");
	return true;
}
}
