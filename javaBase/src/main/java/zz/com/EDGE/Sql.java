package zz.com.EDGE;

public class Sql {
//	use java5;
//
//
//	--select表示查询
//	--*表示所有列
//	--from表示后面跟的是表的名字
//
//
//
//	select * from students;
//
//
//
//	select name,pwd from user;
//	--查询
//	select *from user where name='玫瑰';
//
//	select *from user where id>5;
//
//	--like表示模糊查询，其中%表示0或者多
//	select *from user where pwd like '4%';
//	--where如果需要多个过滤的查询条件,用or链接，结果会越来越多，因为取得是交集
//	select *from user where phonenumber like '177%';
//
//	select *from user where phonenumber like '%3';
//
//	select *from user where name like '%文%';
//	--where 后面如果
//	select *from user where phonenumber like '177'and id>4;
//	select *from user where phonenumber like '177'and id>5 and name='EDGE';
//	select *from user where phonenumber like '177'and id>5 or name='EDGE';
//
//
//	--插入数据
//	insert into user(id,name)values(8,'文');
//	insert into newtable(id,name,classname,pwd,phonenumber,data_join,address)values(001,'玫瑰','玫瑰3班','23123','12132312321',20170909,'第五大道');
//
//
//	--删除数据
//	delete from uesr where name='edge';
//
//	--修改数据
//	update user set name='edge' where id=8;
//
//
//	主键和外键
//	CREATE TABLE Student4.score (
//		sno varchar(20) NOT NULL,
//		cno varchar(20) NOT NULL,
//		`degree` DECIMAL NULL,
//		CONSTRAINT score_pk PRIMARY KEY (sno,cno),
//		CONSTRAINT score_student_fk FOREIGN KEY (sno) REFERENCES Student4.student(sno),
//		CONSTRAINT score_course_fk FOREIGN KEY (cno) REFERENCES Student4.course(cno)
//	)
//	ENGINE=InnoDB
//	DEFAULT CHARSET=utf8
//	COLLATE=utf8_general_ci;
//
//
//	limit
//	use java5;
//	select * from class_detail;
//	select * from book;
//	select nvl(author,'wuming')from book;
//	select b.bookname,u.phonenumber from user u,book b where u.name=b.anthor;
//	-- limit 2,2表示从2号位开始，取两条记录
//	select * from book  order by price desc limit 2,2;
//	-- limit1，4表示从1号位开始，取4条记录（注意，起始位置是0）
//	select * from book  order by price desc limit 1,4;
//
//
//	select* from user;
//
//	select name,score,category from user where score<60;
//	select name,count(*) gk from user where score<60 group by name having gk>=2;
//	select name from user where score<60 group by name having count(*)>=2;
//	-- 二、mysql子查询 1、where型子查询 （把内层查询结果当作外层查询的比较条件）
//
//	-- 直接把一个查询的结果当作数据表来用
//	select name from (select name,count(*) gk from user where score<60 group by name having gk>=2) as t;
//	-- 把一个子类的查询结果在where中使用下面语句的意思是查询出作者的名字存在与用户表的书的名字。
//	select bookname from book where anthor in (select name from user);
//	select name,avg(score)from user group by name;
//	-- 把挂了两科以上学生的平均分和名字查出来；
//	select name,avg(score) from user where name in (select name from user where score<60 group by name having count(*)>=2) group by name;
//
//	--  1、左连接
//	--            以左表为准，去右表找数据，如果没有匹配的数据，则以null补空位，所以输出结果数>=左表原数据数
//	-- 语法：select n1,n2,n3 from ta left join tb on ta.n1= ta.n2 [这里on后面的表达式，不一定为=，也可以>，<等算术、逻辑运算符]【连接完成后，可以当成一张新表来看待，运用where等查询】
//
//	select user.id,name,book.bookname from book left join user on book.anthor=user.name;
//	select name,bookname,is_avaiable as '是不是来上课了' from book
//	left join user on book.anthor=user.name
//	left join class_details on user.id=class_details.userId;
//
//	--  3、内连接
//	--            查询结果是左右连接的交集，【即左右连接的结果去除null项后的并集（去除了重复项）】
//	--            mysql目前还不支持 外连接（即左右连接结果的并集,不去除null项）out
//	--           语法：select n1,n2,n3 from ta inner join tb on ta.n1= ta.n2
//
//	select name,book.bookname from book inner join user on book.anthor=user.name;

}
