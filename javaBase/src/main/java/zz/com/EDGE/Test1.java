package zz.com.EDGE;

public class Test1 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Lesson16 t=new Lesson16();
		
//	普通成员变量必须要先new一个对象才能访问对象的属性
System.out.println(t.name);
//被static修饰的变量叫静态变量，可以直接通过类的名字点出来使用，不需要new
System.out.println(Lesson16.classname);
System.out.println(t.classname);
Lesson16.teststatic();
Lesson16.classname="cary";
System.out.println(Lesson16.classname);
System.out.println(Lesson16.IDNUM);
//被final修饰的变量已经不能被赋值Lesson16.
Lesson16.teststatic();
	}

}
