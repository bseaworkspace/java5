package zz.com.EDGE;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class MapTest {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
//		1.1.题目：请把学生名与考试分数录入到Map中，并按分数显示前三名成绩学员的名字
Map map=new HashMap();
map.put("name", "value");
//赋值
map.get("name");
//取值
Map<String,Car>map1=new HashMap();
//泛型提前规定这个map的key必须是String类型，value是car型
Car c1=new Car();
map1.put("奔驰",c1 );
Car c2=new Car();
map1.put("宝马",c2 );
Car c3=new Car();
map1.put("奥迪",c3 );
//遍历map

//第一种也是最常用的遍历方法。
/*总结
 * 如果仅需要键(key)或者值（values）使用方法2
 * 如果你使用的语言版本低于java5
 * 或者打算在遍历时删除entries，必须使用方法3
 * 否则使用方法1(键值都要）;
 * 
 */
for(Map.Entry<String, Car>e:map1.entrySet()){
	String key=e.getKey();
	Car c=e.getValue();
	
	System.out.println("遍历map方法通过entrySet--"+key);
}
//第二种
for(String str:map1.keySet()){
	System.out.println("遍历map方法通过keySet-key--"+str);
}
for(Car c6:map1.values()){
	System.out.println("遍历map方法通过keySet-value-"+c6);
}
//第三种while当true是一直循环
Iterator<Map.Entry<String, Car>> entries=map1.entrySet().iterator();

while(entries.hasNext()){
	Map.Entry<String, Car>e1=entries.next();
	System.out.println("遍历map方法三通过entrySet--"+e1.getKey());
}
//第四种（尽量避免使用）
for(String str:map1.keySet()){
	System.out.println("遍历map方法四通过keySet-key--"+str);
	Car value=map1.get(str);
}




map.put("玫瑰", 98);
map.put("小米", 88);
map.put("华为", 88);
map.put("兰花", 98);
map.put("小毛", 48);
List a=new ArrayList();
List b=new ArrayList();

for(Object key:map.keySet()){
a.add(map.get(key));
b.add(key);
}
System.out.println(a);
System.out.println(b);
	}

}
