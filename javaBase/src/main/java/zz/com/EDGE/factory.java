package zz.com.EDGE;

public class factory {
//	返回类型是car
//	方法的输入参数(类型 参数的名字)
	
/**
 * 生产红色车的工厂
 * 名词：工厂 车-->类
 * 动词：生产-->方法
 * 形容词：红色-->属性（成员变量）
 * @param color
 */
	public Car create(){
		Car car=new Car();
		car.color="红色";
		return car;
				
	}



}
