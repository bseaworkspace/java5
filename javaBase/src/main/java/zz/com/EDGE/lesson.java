package zz.com.EDGE;
/**
 * java面向对象的四个特点：
 *1.继承
 *2.多态
 *3.封装
 *4.抽象
 *
 *
 *
 * @author Administrator
 *
 */
public class lesson {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Bus bc=new Bus();
		bc.no="1096";
		bc.color="白色";
		bc.run(60);
		
		
		Car c=new Bus();
		c.run(100);	
//		a11.run(100);
		/**
		 * 多态，通过继承实现了同一个方法，在运行的时候
		 * 根据输入参数是不同的子类，产生不一样的效果
		 */
		lesson a11=new lesson();
		a11.run(c,100);
		Bus g=new Bus();
		g.no="1023";
		a11.run(g,100);
		
		
	}
public void run(Car c,int a){
	c.run(a);	
}
}
