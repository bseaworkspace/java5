package zz.com.EDGE;

public class Lesson16 {
public static String name="jacky";
public static String classname="java5";
//被final修饰的变量叫常量，值不能再变，一般名字全大写
public final static String IDNUM="2331";
//static需要放在void之前
public static void teststatic(){
	System.out.println("这是一个静态方法");
	
}
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		//八大基本数据类型 byte ，short，int，long，float，double，插入，boolean。
		/*	
		 * 基本类型是存在栈里面，读取速度更快
		 * 引用类型:值是存在堆里，物理空间大读取速度慢，地址的值是存在栈里面。
		 * 
		 * 
		 * 
		 */
		//	关键字：static  静态的
		//		特征：被static修饰的成员变量或者方法可以在
		//		不需要new的情况下使用
		//		final被final修饰的变量一旦赋值，不能再改变
	}

}
