package zz.com.EDGE;

public class lesson1 {
public static void main(String[]args){
		// int[]表示声明的是int类型的数组
//new int[3]3表示数组的长度，这个数组最多能存3个数
		int[] a = new int[3];
		a[0] = 11;
		a[1] = 15;
		a[2] = 10;

		char[] b = new char[2];
		b[0] = 'r';
		b[1] = 'o';
//		int[3][2]其中3表示第一维数组长度是3，
//		其中2表示第二位数组的长度。
//		也就是这个二维数组可以存3个数组，每个数组可以存两个int类型的值。
		
		int[][]c=new int[3][2];
		c[0][0]=12;
		c[0][1]=34;
		c[1][0]=78;
		c[1][1]=89;
		c[2][0]=120;
		c[2][1]=12;
		
		for(int i=0;i<a.length;i++){
			System.out.println("a-->"+i+"--> "+a[i]);
		}
		for(int i=0;i<c.length;i++){
			for(int j=0;j<c[i].length;j++){
				System.out.println("c-->"+i+"-->"+j+"--> "+c[i][j]);
			}
			
		}
		int[][][]d=new int[2][2][2];
		d[0][0][0]=57;
		d[0][0][1]=77;
		d[0][1][0]=87;
		d[0][1][1]=97;
		d[1][0][0]=7;
		d[1][0][1]=37;
		d[1][1][0]=127;
		d[1][1][1]=570;
		for(int i=0;i<d.length;i++){
			System.out.println("a-->"+i+"--> "+a[i]);
		}
		for(int i=0;i<d.length;i++){
			for(int j=0;j<d[i].length;j++){
				for(int k=0;k<d[i][j].length;k++){
				System.out.println("d-->"+i+"-->"+j+"-->"+k+"--> "+d[i][j][k]);
			}
			}
		}
	}
}
