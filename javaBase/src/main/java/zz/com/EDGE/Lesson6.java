package zz.com.EDGE;

public class Lesson6 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
int[]a=new int[7];
a[0]=12;
a[1]=132;
a[2]=22;
a[3]=52;//使用冒泡排序，从大到小，问这个52移动多少次
a[4]=72;
a[5]=925;
a[6]=552;
//冒泡排序
/**
 * 从大到小排序
 * 
 * 1.两个for循环
 * 
 * 2.第一个for循环的初始值是0，第二个for循环的初始值是i+1
 * 
 * 3.只有i位置的值比j位置的值小的时候，才交换位置。
 */
//1.两个for循环
 
int count=0;
for (int i = 0; i < a.length; i++) {
	for (int I = i + 1; I < a.length; I++) {
		if (a[i] < a[I]) {
			int temp = a[i];
			a[i] = a[I];
			a[I] = temp;
			if(a[i]==52||a[I]==52){
				count++;
		}
	}

	
}
	System.out.println(a[i]);
}
//新式写法 for循环的另一种写法，for each写法，第二个参数是数组，第一个参数是一个临时变量
//每次都把数组里面的元素值赋给第一个参数。
//for(int t:a){
//	System.out.println(t);
//}
}
	
}
