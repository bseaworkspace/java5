<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
    <%@ page import="com.zz.jwh.model.User" %>
    <%@ page import="java.util.List" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<%List<User> ls=(List<User>)request.getSession().getAttribute("ls"); %>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>成员管理</title>
<link rel="stylesheet" href="jinwenhao/css/bootstrap.min.css">  
</head>
<body>
<%=ls.get(1).getName() %>
<table class="table table-hover table-bordered">
	<caption>设置成员信息</caption>
   <thead>
      <tr>
         <th>编号</th>
         <th>成员名称</th>
		   <th>成员电话</th>
			  <th>管理操作</th>
      </tr>
   </thead>
   <tbody>
   <%for(User U:ls){   %>
	   
      <tr id="<%=U.getId() %>_1">
      <td><%=U.getId() %></td>
         <td><%=U.getName() %></td>
         <td><%=U.getPhone() %></td>
		 <td><a onclick="edit1('<%=U.getId() %>_1','<%=U.getId() %>_2')">修改</a>&nbsp<a>身份设置</a>&nbsp <a>移除</a> </td>
			
      </tr>
	    
 <tr id="<%=U.getId() %>_2" style="display:none">
       <td><%=U.getId() %></td>
         <td><input type="text" id='<%=U.getId() %>name'></td>
         <td><input type="text"  id='<%=U.getId() %>phone'></td>
			  <td><a onclick="ok(<%=U.getId()%>)">确认</a>&nbsp<a>取消</a></td>
			
      </tr>
	   
	   
	   
 <%	 
   }
   %>
   </tbody>
</table>
<script type="text/javascript" src="/zz_java5/jquery/jquery-3.2.1.min.js"></script>
<script>
function edit1(id1,id2){
     document.getElementById(id1).style.display="none";
	 document.getElementById(id2).style.display="table-row";
}
function ok(id){
     var username=$("#"+id+"name").val();
     alert(username);
     var userphone=$("#"+id+"phone").val();
     $.post("/zz_java5/test",{name:username ,phone :userphone, id2: id},
     function(data){
    	 alert("Data Loaded: " + data);
    	 if(data.equals("Y")){
    		   document.getElementById(id+"_1").style.display="table-row";
    			 document.getElementById(id+"_2").style.display="none";
    	 }
     }
     
     );
}

</script>
</body>
</html>