<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@page import="java.util.List"%>
<%@page import="com.zzwsq_1.model.User"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<head>
<meta charset="utf-8">
<title>Bootstrap 实例 - 基本的表格</title>
<link rel="stylesheet" href="/zz_java5/wsq/css/bootstrap.min.css">
<script src="https://cdn.bootcss.com/jquery/2.1.1/jquery.min.js"></script>
<script
	src="https://cdn.bootcss.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
</head>
</head>
<body>
	<%
		List<User> userList = (List<User>) request.getSession().getAttribute("userlist");
	%>
	<table class="table table-hover table-bordered">
		<caption>设置成员信息</caption>
		<thead>
			<tr>
				<th>编号</th>
				<th>成员名称</th>
				<th>成员电话</th>
				<th>管理操作</th>
			</tr>
		</thead>
		<tbody>
			<%
				for (User u : userList) {
			%>

			<tr id="<%=u.getId()%>_1">
				<td><%=u.getId()%></td>
				<td id="<%=u.getId()%>_tdName"><%=u.getName()%></td>
				<td id="<%=u.getId()%>_tdPhone"><%=u.getPhoneNumber()%></td>

				<td><a onclick="edit('<%=u.getId()%>_1','<%=u.getId()%>_2')">修改</a>&nbsp<a>身份设置</a>&nbsp
					<a>移除</a></td>

			</tr>

			<tr id="<%=u.getId()%>_2" style="display: none">
				<td><%=u.getId()%></td>
				<td><input id="<%=u.getId()%>name" type="text"></td>
				<td><input id="<%=u.getId()%>phone" type="text"></td>
				<td><a onclick="update1('<%=u.getId()%>')">确认</a>&nbsp<a>取消</a></td>

			</tr>
			<%
				}
			%>
		</tbody>
	</table>
	
	<script>
	
		function edit(id1, id2) {
			document.getElementById(id1).style.display = "none";
			document.getElementById(id2).style.display = "table-row";
		}

			//	alert($("#3_tdName").val());
			//alert($("#3_tdName").text());
			//	alert($("#3_tdName").html());
			/*       
			url,[date]，[callback] ，[type]
			
			只有url是必填的，其他都是选填的
			利用jquery实现ajax call
			url:发送请求地址  -->对应是：web.xml的url.pattern  拦截地址
			date:待发送key/value   
			列如   下面这个key即使是String 他也不需要用引号
			$.post("zz_java5/UserController", {
				name : userName,
				phoneNumber : phone,
				id3 : id
			},
			callback:发送成功是回调函数    下面是function 方法
				type:返回内容格式，xml,html，script，json
				
				<2>ajax 是异步执行，局部刷新
				异步执行 是不相干的东西，是已经先放好的
				
				   什么是 AJAX ？
				    AJAX = 异步 JavaScript 和 XML。
				    AJAX 是一种用于创建快速动态网页的技术。
				    通过在后台与服务器进行少量数据交换，AJAX 
				    可以使网页实现异步更新。
				    这意味着可以在不重新加载整个网页的情况下，
				    对网页的某部分进行更新。
				    传统的网页（不使用 AJAX）如果需要更新内容，
				    必需重载整个网页面。
				    有很多使用 AJAX 的应用程序案例：
				    新浪微博、Google 地图、开心网等等。
				
				<3> 局部刷新比喻：一辆部件坏了或者老化了，我们不需要把整个车子拆散，全部重新组装
				只是把那个部件更换或者升级
				  就是把前端界面上改的值，传到数据库中，把数据库原来的值覆盖掉，在改的地方 
			 */
		function update1(id) {

			var userName = $("#" + id + "name").val();
			//这是取值
			var phone = $("#" + id + "phone").val();

			$.post("/zz_java5/UserController",{name : userName , phoneNumber : phone,id3 : id},
							//这是传值过程 key:value
	     
	             	function(data) {
								alert(data);
								if (data == 'Y') {
									$("#" + id + "_tdName").text(userName);
									//# 查询id 地址
									$("#" + id + "_tdPhone").text(phone);
									document.getElementById(id + "_2").style.display = "none";
									document.getElementById(id + "_1").style.display = "table-row";
								}
							});

		}
	</script>
</body>
</html>