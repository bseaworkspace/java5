<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>自在天原</title>
</head>
<body>
<h1>登录</h1>

<!-- 写一些注释,xml里面单引号和双引号是一样的 -->

<form action="/zz_java5/LoginController">
<!-- action="这个是servlet"的拦截地址 -->
<!-- 如果在servlet端如果想要获取用户输入的值，就必须对输入框设置name属性
，这样的话在servlet端，就可以吧name属性的值，
可以当做key来获取用户输入的值 -->
用户名:<input name="UserName" type="text">
<br>
密码:&nbsp&nbsp&nbsp&nbsp<input name="pwd" type="password">
<br>
<input type="submit" value="登录">  
</form>
</body>
</html>