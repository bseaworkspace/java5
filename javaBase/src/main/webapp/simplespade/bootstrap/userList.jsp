<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ page import="java.util.List"%>
<%@ page import="com.zz.simplespade.model.User"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link rel="stylesheet"
	href="/zz_java5/simplespade/bootstrap/css/bootstrap.min.css">
<script src="https://cdn.bootcss.com/jquery/2.1.1/jquery.min.js"></script>
<script
	src="https://cdn.bootcss.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>成员管理</title>
</head>
<body>
	<%
		List<User> ls = (List<User>) request.getSession().getAttribute("us");
	%>

	<table class="table table-hover table-bordered">
		<caption>设置成员信息</caption>
		<thead>
			<tr>
				<th>编号</th>
				<th>成员名称</th>
				<th>成员电话</th>
				<th>管理操作</th>
			</tr>
		</thead>
		<tbody>
			<%
				for (User u : ls) {
			%>
			<tr id="<%=u.getId()%>_1" style="display: table-row">
				<td><%=u.getId()%></td>
				<td id="<%=u.getId()%>tdname"><%=u.getName()%></td>
				<td id="<%=u.getId()%>tdphone"><%=u.getPhoneNumber()%></td>
				<td><a onclick="edit('<%=u.getId()%>_1','<%=u.getId()%>_2')">修改</a>&nbsp;<a>身份设置</a>&nbsp;<a>移除</a></td>
			</tr>
			<tr id="<%=u.getId()%>_2" style="display: none">
				<td><%=u.getId()%></td>
				<td><input id="<%=u.getId()%>name" type="text"></td>
				<td><input id="<%=u.getId()%>phone" type="text"></td>
				<td><a onclick="update(<%=u.getId()%>)">确认</a>&nbsp;<a>取消</a></td>
			</tr>
			<%
				}
			%>
		</tbody>
	</table>
	<script>
function edit(id1,id2){
	document.getElementById(id1).style.display="none";
	document.getElementById(id2).style.display="table-row";
}

function update(id){
	var userName=$("#"+id+"name").val();
	var phoneNumber=$("#"+id+"phone").val();
	//alert($("#3_tdName").val());
	//alert($("#3_tdName").text());
	//alert($("#3_tdName").html());
	/*
	jQuery.post(url, [data], [callback], [type])
	    利用jquery实现ajax call.
	    url:发送请求地址。
	    data:待发送 Key/value 参数。
	    callback:发送成功时回调函数。
	    type:返回内容格式，xml, html, script, json, text, _default
	    《1》jQuery和$是一样的，表示要使用jquery的方法
	    《2》
	    什么是 AJAX ？
	    AJAX = 异步 JavaScript 和 XML。
	    AJAX 是一种用于创建快速动态网页的技术。
	    通过在后台与服务器进行少量数据交换，AJAX 
	    可以使网页实现异步更新。
	    这意味着可以在不重新加载整个网页的情况下，
	    对网页的某部分进行更新。
	    传统的网页（不使用 AJAX）如果需要更新内容，
	    必需重载整个网页面。
	    有很多使用 AJAX 的应用程序案例：
	    新浪微博、Google 地图、开心网等等。
	    
	  《3》 局部刷新比喻： 一辆车子，
	                                某个部件坏了或者老化了，
	                 我们不需要，把整个车子拆散了，
	                 全部重新组装。
	        只是把那个部件更换或者升级。
	 《4》
	 
	 jQuery.post(url, [data], [callback], [type])
	    利用jquery实现ajax call.
	    url:发送请求地址。
	    data:待发送 Key/value 参数。
	    	key的字符串不需要双引号
	    	value的字符串一定要双引号
	    callback:发送成功时回调函数。
	    	servlet执行完以后的方法
	    	回调方法
	    type:返回内容格式，xml, html, script, json, text, _default
	          只有url是必填，其他都是选填。
	  */
	$.post("/zz_java5/SSUsController",{name:userName,phone:phoneNumber,u_id:id},
		function(data){
		alert(data);
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
			
			if(data=='Y'){
			$("#"+id+"tdname").text(userName);
			$("#"+id+"tdphone").text(phoneNumber);
			document.getElementById(id+"_1").style.display="table-row";
			document.getElementById(id+"_2").style.display="none";
			}
		}
	);
}
</script>
</body>
</html>