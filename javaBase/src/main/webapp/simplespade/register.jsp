<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 5.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Insert title here</title>
</head>
<body>
<!-- onsubmit="return checkSubmit()"
如果checkSubmit()返回的是false，就可以阻止提交，
也就是不会去执行servlet了。
 -->
	<form action="" onsubmit="return checkSubmit()">
		<p>
			&nbsp;&nbsp;&nbsp;&nbsp;用户名：<input type="text" id="name" name="username"
				onblur="over()">
		</p>
		<p>
			身高：<input type="text" id="height" onkeyup="checkHeight()">cm
		</p>
		<p>
			性别：
			<select id="sex" onchange="checkSex()">
			<option value="N"></option>
			<option value="man">男</option>
			<option value="woman">女</option>
			</select>
		</p>
		<img alt="" id="maning" src="man.png" width="90px" height="47px" style=display:none>
		<img alt="" id="womaning" src="woman.png" width="90px" height="45px" style=display:none>
		<p>
			&nbsp;&nbsp;&nbsp;&nbsp;请输入密码：<input type="password" id="pwd1" name="pwd">
		</p>

		<p>
			再次输入密码：<input type="password" id="pwd2" onblur="checkPassword()">
		</p>

		<p>
			<input id="submitbtn" type="submit" value="提交" disabled="disabled">
		</p>
	</form>
	<script type="text/javascript">
		function checkPassword() {
			var pwd1;
			var pwd2;

			pwd1 = document.getElementById("pwd1").value;
			pwd2 = document.getElementById("pwd2").value;
			if (pwd1 != pwd2) {

				alert("两次密码输入不一致");

			}
		}
		function over() {
			var name = document.getElementById("name").value;
			if (name.length > 15) {
				alert("用户名过长");
			}else{
				document.getElementById("submitbtn").disabled="";
			}
		}
		function checkHeight(){
			var h=document.getElementById("height").value;
			var correctList="1234567890";
			var flag=false;
			var arr=h.split("");
			
			for(var i=0;i<arr.length;i++){
				if(correctList.indexOf(arr[i])==-1){
					
					alert("身高只能是数字");
				}
				
			}
		}
		function checkSex(){
			var sexVal = document.getElementById("sex").value;
			if(sexVal=="man"){
				document.getElementById("maning").style.display="inline";
				document.getElementById("womaning").style.display="none";
			}else if(sexVal=="woman"){
				document.getElementById("womaning").style.display="inline";
				document.getElementById("maning").style.display="none";
			}else {
				document.getElementById("womaning").style.display="none";
				document.getElementById("maning").style.display="none";
			}
		}
		function checkSubmit(){
			alter("onsubmit");
			
		}
	</script>

</body>
</html>