<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>自在天原</title>
</head>
<body>
<!-- 这个是注释  -->
<h1>登录</h1>
<!--  action=这个是servlet的拦截地址 -->
<form action="/zz_java5/LoginController">
<!-- 如果在servlet端想要获取用户输入的值，就必须对输入框设置name的属性
这样的话，在servlet段，就可以把name属性的值当作key来获取用户输入的值 -->
用户名：<input name="username"type="text">
<br>
密码&nbsp&nbsp&nbsp&nbsp：<input name="pwd"type="password">
<br>
<input type="submit"value="登录">

</form>
</body>
</html>