<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Insert title here</title>
</head>
<body>
<nav class="navbar navbar-default" role="navigation">
	<div class="container-fluid">
	<div class="navbar-header">
		<a class="navbar-brand" href="#">菜鸟教程</a>
	</div>
	<div>
		<ul class="nav navbar-nav">
			<li class="active"><a href="#" onclick="showPage1()">showpage1</a></li>
			<li><a href="#" onclick="showPage2()">showpage2</a></li>
			<li class="dropdown">
				<a href="#" class="dropdown-toggle" data-toggle="dropdown">
					Java
					<b class="caret"></b>
				</a>
				<ul class="dropdown-menu">
					<li><a href="#" onclick="showPage1()">showpage1</a></li>
					<li><a href="#" onclick="showPage2()">showpage2</a></li>
					<li><a href="#">Jasper Report</a></li>
					<li class="divider"></li>
					<li><a href="#">分离的链接</a></li>
					<li class="divider"></li>
					<li><a href="#">另一个分离的链接</a></li>
				</ul>
			</li>
		</ul>
	</div>
	</div>
</nav>

<div id="page1">
<jsp:include page="projectDemoInclude1.jsp"></jsp:include>
</div>
<div id="page2">
<jsp:include page="projectDemoInclude2.jsp"></jsp:include>
</div>

<script type="text/javascript">

function showPage1(){
	$("#page1").show();
	$("#page2").hide();
}
function showPage2(){
	$("#page2").show();
	$("#page1").hide();
}


</script>

</body>
</html>