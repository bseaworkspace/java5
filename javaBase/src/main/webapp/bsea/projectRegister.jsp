<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link rel="stylesheet" href="https://cdn.bootcss.com/bootstrap/3.3.7/css/bootstrap.min.css">  
	<script src="https://cdn.bootcss.com/jquery/2.1.1/jquery.min.js"></script>
	<script src="https://cdn.bootcss.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<title>Insert title here</title>
</head>
<body>
<form class="form-horizontal" role="form">
	<div class="form-group">
		<label for="firstname" class="col-sm-2 control-label">名字</label>
		<div class="col-sm-5">
			<input type="text" class="form-control" id="firstname" 
				   placeholder="请输入名字" onblur="onblurName()">
		</div>
		<div class="col-sm-5"></div>
	</div>
	<div class="form-group">
		<label for="lastname" class="col-sm-2 control-label">姓</label>
		<div class="col-sm-5">
			<input type="text" class="form-control" id="lastname" 
				   placeholder="请输入姓">
		</div>
		<div class="col-sm-5"></div>
	</div>
	<div class="form-group">
		<div class="col-sm-offset-2 col-sm-10">
			<div class="checkbox">
				<label>
					<input type="checkbox"> 请记住我
				</label>
			</div>
		</div>
	</div>
	<div class="form-group">
		<div class="col-sm-offset-2 col-sm-10">
			<button type="submit" class="btn btn-default">登录</button>
		</div>
	</div>
</form>
<script type="text/javascript">
function onblurName(){
	var name2=$("#firstname").val();
	
	$.post("/zz_java5/UserProjectController", { name: name2},
	          function(data){
			   if(data=="N"){}else{
				   alert("用户已经存在");
				   
			   }
	          "src/main/webapp/bsea/projectRegister.jsp"
	          });
	
	
}

</script>
</body>
</html>