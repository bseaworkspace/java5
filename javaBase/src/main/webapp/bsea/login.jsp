<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<script src="https://cdn.bootcss.com/jquery/2.1.1/jquery.min.js"></script>
<title>自在天原</title>
</head>
<body>
<!-- 這个是注释 -->
<h1>登录</h1>
<!--  action=”这个是servlet的拦截地址“ -->
<form action="/zz_java5/LoginControllerBsea" method="post">
<!--  如果在servlet端，想要获取用户输入的值，就
必须对输入框设置name属性， 
这样的话，在servlet端，就可以把name属性的值
当作key来获取用户输入的值
 -->
用户名：<input name="username" id="username" type="text" onblur="onblurUserName()">
<br>
密码：&nbsp&nbsp&nbsp&nbsp<input name="pwd" type="password">
<br>
团队：&nbsp&nbsp&nbsp&nbsp<select name="team" id="team"><option id="blankoption"> </option></select>
<br>
<div id="v_container" style="width: 200px;height: 50px;"></div>
		<input type="text" id="code_input" value="" placeholder="请输入验证码"/>
		<button id="my_button">验证</button>

<input type="submit" value="登录">

</form>

<script src="/zz_java5/bsea/js/gVerify.js"></script>
	<script>
		$("#v_container").hide();
		$("#code_input").hide();
		$("#my_button").hide();
	    function showVerifyCode(){
			var verifyCode = new GVerify("v_container");
	    }

		document.getElementById("my_button").onclick = function(){
			var res = verifyCode.validate(document.getElementById("code_input").value);
			if(res){
				alert("验证正确");
			}else{
				alert("验证码错误");
			}
		}
	</script>
	
<script type="text/javascript">
function onblurUserName(){
	var userName=$("#username").val();
	$.post("/zz_java5/FormController",{"username":userName},function(data){
		// alert(data);
		 var ops=data.split("#");
		 for(var i=0;i<ops.length;i++){
			 
			 $("#team").append(ops[i]);
		 }
	});
	
}

</script>

<%
String rs=(String)request.getAttribute("rs");
String isShowVerifyCode="N";
if(rs!=null||rs!=""){
	isShowVerifyCode="Y";
}
%>
<script>
var message='<%=rs%>';
var isShowVerifyCode='<%=isShowVerifyCode%>';

if(isShowVerifyCode=="Y"){
	alert(message);
	$("#v_container").show();
	$("#code_input").show();
	$("#my_button").show();
	showVerifyCode();
	
}



</script>
</body>
</html>