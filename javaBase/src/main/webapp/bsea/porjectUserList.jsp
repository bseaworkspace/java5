<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%@ page import="java.util.List" %>
<%@ page import="com.zz.bsea.model.User" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<title>分页例子</title>
	<link rel="stylesheet" href="https://cdn.bootcss.com/bootstrap/3.3.7/css/bootstrap.min.css">  
	<script src="https://cdn.bootcss.com/jquery/2.1.1/jquery.min.js"></script>
	<script src="https://cdn.bootcss.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>

<%
List<User> ls=(List<User>)request.getAttribute("records");
int totalPages=(int)request.getAttribute("pageConut");

int currentPage=(int)request.getAttribute("curpage");;

%>
<div class="table-responsive">
	<table class="table">
		<caption>用户列表</caption>
		<thead>
			<tr>
				<th>用户名</th>
				<th>用户电话</th>
				<th>密码</th>
			</tr>
		</thead>
		<tbody>
		<%
		for(User u:ls){
		%>	
		<tr>
				<td><%=u.getName() %></td>
				<td><%=u.getPhoneNumber()%></td>
				<td><%=u.getPwd() %></td>
			</tr>
		
		<% 	
		}
		
		%>
			
			
		</tbody>
</table>
</div>  

<div class="container">
	<div class="row">
		<div class="col-sm-3"></div>
		<div class="col-sm-6">
			<ul class="pagination">
			<!--  currentPage是从后台传过来的，表示当前用户看到的页码-->
				<%if(currentPage!=1){%>
				<li><a href="/zz_java5/UserProjectController?page=<%=currentPage-1%>">&laquo;</a></li>
				<%}
				//count 用来标记， 当前显示了多少页（目前我们设计是，一次最多显示5页）
				int count=0;
				//totalPages 从后台传过来的，表示整个数据，经过计算以后，总的页数。
				for(int i=1;i<totalPages+1;i++){
					//这个表示，需要显示页码的起始位置。
					int startPage=currentPage-2;
					//这个表示，需要显示页码的末尾位置。
					int endPage=currentPage+2;
					if((startPage<3&&count<5)||(startPage<=i&&i<=endPage&&count<5)){
						count++;
						if(currentPage==i){
							%>	
							<li class="active"><a href="/zz_java5/UserProjectController?page=<%=i%>"><%=i %></a></li>
							
						<%		
						}else{
							%>	
							<li><a href="/zz_java5/UserProjectController?page=<%=i%>"><%=i %></a></li>
							
							
						<%	
						}
					}
				}
				
				
				if(currentPage!=totalPages){
				%>
				
				<li><a href="/zz_java5/UserProjectController?page=<%=currentPage+1%>">&raquo;</a></li>
				<%} %>
			</ul>
		</div>
		<div class="col-sm-3"></div>
	</div>
</div>
</body>
</html>