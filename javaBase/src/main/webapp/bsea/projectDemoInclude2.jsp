<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Insert title here</title>
<link rel="stylesheet" href="https://cdn.bootcss.com/bootstrap/3.3.7/css/bootstrap.min.css">  
	<script src="https://cdn.bootcss.com/jquery/2.1.1/jquery.min.js"></script>
	<script src="https://cdn.bootcss.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>

<table class="table">
	<caption>page2</caption>
	<thead>
		<tr>
			<th>产品</th>
			<th>付款日期</th>
			<th>状态</th>
		</tr>
	</thead>
	<tbody>
		<tr class="active">
			<td>产品1</td>
			<td>23/11/2013</td>
			<td>待发货</td>
		</tr>
		<tr class="success">
			<td>产品2</td>
			<td>10/11/2013</td>
			<td>发货中</td>
		</tr>
		<tr  class="warning">
			<td>产品3</td>
			<td>20/10/2013</td>
			<td>待确认</td>
		</tr>
		<tr  class="danger">
			<td>产品4</td>
			<td>20/10/2013</td>
			<td>已退货</td>
		</tr>
	</tbody>
</table>

</body>
</html>