function edit(id1,id2){
     document.getElementById(id1).style.display="none";
	 document.getElementById(id2).style.display="table-row";
}

function update2(id){
	var userName=$("#"+id+"name").val();
	var phone=$("#"+id+"phone").val();
	//alert($("#3_tdName").val());
	//alert($("#3_tdName").text());
	//alert($("#3_tdName").html());
	/**
	
	    
	    
	    《1》jQuery和$是一样的，表示要使用jquery的方法
	    《2》
	    什么是 AJAX ？
	    AJAX = 异步 JavaScript 和 XML。
	    AJAX 是一种用于创建快速动态网页的技术。
	    通过在后台与服务器进行少量数据交换，AJAX 
	    可以使网页实现异步更新。
	    这意味着可以在不重新加载整个网页的情况下，
	    对网页的某部分进行更新。
	    传统的网页（不使用 AJAX）如果需要更新内容，
	    必需重载整个网页面。
	    有很多使用 AJAX 的应用程序案例：
	    新浪微博、Google 地图、开心网等等。
	    
	    <3> 局部刷新比喻： 一辆车子，
	                                某个部件坏了或者老化了，
	                 我们不需要，把整个车子拆散了，
	                 全部重新组装。
	        只是把那个部件更换或者升级。
	        
	        
	  《4》
	  
	  jQuery.post(url, [data], [callback], [type])
	    利用jquery实现ajax call.
	    url:发送请求地址。（对应：web.xml的url-pattern）
	    data:待发送 Key/value 参数。
	    callback:发送成功时回调函数。
	    type:返回内容格式，xml, html, script, json, text, _default。
	     只有url是必填的，其他的都是选填。   
	                  
	**/
	$.post("/zz_java5/UserController", { name: userName, phoneNumber: phone,id3:id },
	          function(data){
			   if(data=='Y'){
				   $("#"+id+"_tdName").text(userName);
				   $("#"+id+"_tdPhone").text(phone);
				   document.getElementById(id+"_2").style.display="none";
				   document.getElementById(id+"_1").style.display="table-row";
			   }

	 });
	
}