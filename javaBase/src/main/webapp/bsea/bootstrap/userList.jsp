<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%@ page import="java.util.List" %>
<%@ page import="com.zz.bsea.model.User" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<link rel="stylesheet" href="/zz_java5/bsea/bootstrap/css/bootstrap.min.css">  
	<script src="https://cdn.bootcss.com/jquery/2.1.1/jquery.min.js"></script>
	<script src="https://cdn.bootcss.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="/zz_java5/bsea/bootstrap/js/userList.js"></script>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<title>成员管理</title>
</head>
<body>

<%
List<User> userList=(List)request.getSession().getAttribute("us");

%>
<table class="table table-hover table-bordered">
	<caption>设置成员信息</caption>
   <thead>
      <tr>
         <th>编号</th>
         <th>成员名称</th>
		   <th>成员电话</th>
			  <th>管理操作</th>
      </tr>
   </thead>
   <tbody>
   		<%
   		for(User u: userList){
   		%>	
   		<tr id="<%=u.getId() %>_1">
	         <td><%=u.getId() %></td>
	         <td id="<%=u.getId() %>_tdName"><%=u.getName() %></td>
	         <td id="<%=u.getId() %>_tdPhone"><%=u.getPhoneNumber() %></td>
			 <td><a onclick="edit('<%=u.getId() %>_1','<%=u.getId()%>_2')">修改</a>&nbsp<a>身份设置</a>&nbsp <a>移除</a> </td>	
        </tr>
		<tr id="<%=u.getId()%>_2" style="display:none">
	          <td><%=u.getId() %></td>
			  <td><input id="<%=u.getId()%>name" type="text"> </td>
			  <td><input id="<%=u.getId()%>phone" type="text"></td>
			  <td><a onclick="update2('<%=u.getId()%>')">确认</a>&nbsp<a>取消</a></td>
	     </tr>
   		<%}%>
   </tbody>
</table>

</body>
</html>