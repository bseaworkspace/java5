<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>   
    <script type="text/javascript" src="jQuery/jquery-1.9.1.min.js"></script>
    <script type="text/javascript">  
       function test2(){  
       
          //用这种方式组装的json数组是正确的  
          var testArray = new Array();
          for(var i = 0; i < 5; i++) {
              var tempArray = new Array();  
              for(var j = 0; j < 10; j++){
                tempArray.push(i*j);
              }
              testArray.push(tempArray);
          }
          alert("tempArray=" + tempArray);
          
          //用这种方式组装的json数组是正确的
          var jsonArr = new Array();             
          for ( var j = 0; j < 3; j++) {                 
            var jsonObj = {};                  
            jsonObj["gradeId"]=1;                 
            jsonObj["gradeName"]=2;                 
            jsonObj["level"]=3;                 
            jsonObj["boundary"]=4;                 
            jsonObj["status"]=5;                 
            jsonArr.push(jsonObj)             
          }
          alert("jsonArr=" + jsonArr);            
          
          // 用这种方式组装的json数组是错误的，接收侧只能接收到整个的这个字符串            
          var employees='[';
          for ( var j = 0; j < 5; j++) {                          
            employees+= '{';                             
            employees+="name:";                            
            employees+="zhang";
            employees+=",";
            employees+="old:";                            
            employees+=30 + j;
            employees +='}';
            if(j!=4){                             
                employees+=','
            };                         
          } 
          employees+=']';
          alert("employees=" + employees);
          
           $.ajax({
                type:"POST", //请求方式  
                url:"./testJson", //请求路径  
                cache: false,     
                data:{//传参  
                    "name":"zhang3",
                    "testArray":testArray,    
                    "students": //用这种方式传递多维数组也是正确的
                        [
                            {"name":"jackson","age":100},
                            {"name":"michael","age":51}
                        ],
                    "employees":employees,
                    "jsonArr":jsonArr    
                },
                dataType: 'json',   //返回值类型  
                success:function(json){        
                    alert(json[0].username+" " + json[0].password);    //弹出返回过来的List对象  
                    alert(json[1].username+" " + json[1].password);    //弹出返回过来的List对象
                }  
            });  
      }  
    </script>  
  </head>  
    
  <body>  
    <input type="button" name="b" value="TestJson" onclick="test2()"/>
  </body>  

</html>