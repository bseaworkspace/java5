package com.zz.simpleSpade.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.zz.simpleSpade.dao.UserDao;
import com.zz.simpleSpade.model.User;

@RestController
public class UserDataController {
	
	@Autowired
	UserDao userDao;

	@PostMapping("getUserData")
	public List<User> getUserData(){
		List<User> ls = userDao.ListUser();
		return ls;
	}
	
}
