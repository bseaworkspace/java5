package com.zz.simpleSpade.controller;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.zz.simpleSpade.dao.LoginDao;
import com.zz.simpleSpade.model.User;



@RestController
public class LoginController {
	@Autowired
	LoginDao lDao;
	
	@RequestMapping(value="login",method=RequestMethod.POST)
	public Map<String,String> login(@RequestParam("name") String userName,@RequestParam("pwd") String pwd,HttpServletRequest request){
		System.out.println("userName"+userName+" ---pwd---"+pwd);
		User u=lDao.getUserByNamePWD(userName, pwd);
		Map<String,String> m = new HashMap<String,String>();
		String result="N";
		if(u!=null){
			result="Y";
			HttpSession session=request.getSession();
			session.setAttribute("user", u);
		}
		m.put("result", result);
		return m;
	}
	//@RequestMapping(value="login",method=RequestMethod.POST) 和
	//@PostMapping("login") 效果是一样的。只接收post请求
	/*@PostMapping("login")
	public String login2(){
		
		return "登录成功";
		
	}*/
}
