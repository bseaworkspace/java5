package com.zz.simpleSpade.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.zz.simpleSpade.model.User;




@Service("loginDao")
public class LoginDao {
	
	@Autowired
	BaseDao baseDao;
	
	public User getUserByNamePWD(String name, String pwd){
		User u=null;
		
		
		Connection con=baseDao.getConnection();
		String sql="select u_id,u_pwd,u_name from user where u_pwd=? and u_name=?";
		try {
			PreparedStatement ps=con.prepareStatement(sql);
			ps.setString(1, pwd);
			ps.setString(2, name);
			ResultSet rs=ps.executeQuery();
			while(rs.next()){
				u=new User();
				u.setU_id(rs.getInt("u_id"));
				u.setU_name(rs.getString("u_name"));
				u.setU_pwd(rs.getString("u_pwd"));
			}
			baseDao.closeQuery(rs, ps, con);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
			
		return u;
	}

}
