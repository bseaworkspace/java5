package com.zz.simpleSpade.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import com.zz.simpleSpade.model.User;

@Repository("userDao")
public class UserDao {
	
	@Autowired
	BaseDao baseDao;
	
	public List<User> ListUser(){
		List<User> ls = new ArrayList<User>();
		
		Connection con = baseDao.getConnection();
		String sql = "select * from user;";
		try {
			PreparedStatement ps = con.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();
			while(rs.next()){
				User u = new User();
				u.setU_name(rs.getString("u_name"));
				u.setU_nickname(rs.getString("u_nickname"));
				u.setU_pwd(rs.getString("u_pwd"));
				ls.add(u);
			}
			baseDao.closeQuery(rs, ps, con);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return ls;
	}

}
