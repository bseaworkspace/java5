package com.zz.simpleSpade.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Created by jiyu on 2017/10/25.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class Shop_Product {
    private String name;
    private int price;
    private int count;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }
}
