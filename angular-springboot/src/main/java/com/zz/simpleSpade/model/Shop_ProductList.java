package com.zz.simpleSpade.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.List;

/**
 * Created by jiyu on 2017/10/25.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class Shop_ProductList {
    List<Shop_Product> productList;

    public List<Shop_Product> getProductList() {
        return productList;
    }

    public void setProductList(List<Shop_Product> productList) {
        this.productList = productList;
    }
}
