package com.zz.lsw.model;

import java.util.List;


public class Shop_ProductList {

	 List<Shop_Product> productList;

	public List<Shop_Product> getProductList() {
		return productList;
	}

	public void setProductList(List<Shop_Product> productList) {
		this.productList = productList;
	}

	
}
