package com.zz.lsw.model;

import java.util.Date;

public class Product {
	private String name;
	private String status;
	private String parDate;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getParDate() {
		return parDate;
	}
	public void setParDate(String parDate) {
		this.parDate = parDate;
	}
	

}
