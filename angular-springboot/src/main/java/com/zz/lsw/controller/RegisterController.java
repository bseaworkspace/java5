package com.zz.lsw.controller;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.zz.lsw.dao.LoginDao;
import com.zz.lsw.dao.ReisterDao;
import com.zz.lsw.model.User;

@RestController
public class RegisterController{
	@RequestMapping(value="register",method=RequestMethod.POST)
	public String register(@RequestParam("name")String userName,@RequestParam("pwd")String pwd ,HttpServletRequest request){
		String result="N";
		Pattern patPunc=Pattern.compile("[`~!@#$^&*()=|{}':;',\\[\\].<>/?~！@#￥……&*（）——|{}【】‘；：”“'。，、？]$");
		Matcher matcher=patPunc.matcher(userName);
		Matcher matcher1=patPunc.matcher(pwd);
		if(!matcher.find()&&!matcher1.find()){
		LoginDao ld=new LoginDao();
		User u=ld.getUserByNamePWD(userName, pwd);
		if(u==null){
			ReisterDao rd=new ReisterDao();
			rd.addUser(userName,pwd);
			result="Y";
		}
		
	}else{
		result="X";
	}
		return result;
		}
}
