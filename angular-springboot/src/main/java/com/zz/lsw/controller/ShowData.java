package com.zz.lsw.controller;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.zz.lsw.model.Product;
import com.zz.lsw.model.User;

@RestController
public class ShowData {
	@RequestMapping("getData")
	public List<Product> getDate(HttpServletRequest request){
		List<Product> ls=new ArrayList();
		User u=(User) request.getSession().getAttribute("user");
		System.out.println("userName>>>>>>>>>>>>>>>"+u.getName());
		SimpleDateFormat bartDateFormat =new SimpleDateFormat("mm-dd-yyyy");
		Product p1=new  Product();
		Date Date=new Date();
		p1.setName("p1");
		p1.setStatus("发货中");
		p1.setParDate(bartDateFormat.format(Date));
		Product p2=new  Product();
		p2.setName("p2");
		p2.setStatus("毛屹嘉傻逼");
		p2.setParDate(bartDateFormat.format(Date));
		Product p3=new  Product();
		p3.setName("p3");
		p3.setStatus("等待");
		p3.setParDate(bartDateFormat.format(Date));
		ls.add(p1);
		ls.add(p2);
		ls.add(p3);
		return ls;
	}
}
