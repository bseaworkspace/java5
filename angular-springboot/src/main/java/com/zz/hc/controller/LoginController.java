package com.zz.hc.controller;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.zz.hc.dao.LoginDao;
import com.zz.hc.model.User;

@RestController
public class LoginController {
    @Autowired()
    LoginDao loginDao;
	
	@RequestMapping(value="login",method=RequestMethod.POST)
	public Map login(@RequestParam("name") String userName,@RequestParam("password") String pwd,HttpServletRequest request){
		System.out.println("username==="+userName+"pwd==="+pwd);
		Map m=new HashMap();
		User u=loginDao.getUserByNamePwd(userName, pwd);
		String result="N";
		if(u!=null){
			result="Y";
			request.getSession().setAttribute("user", u);
		}
		 
		HttpSession session =request.getSession();
		 session.setAttribute("userName", userName);
		m.put("rs", result);
		return m;
	}
	//@RequestMapping(value="login",method=RequestMethod.POST)
	//@PostMapping("login")效果是一样的
//	@PostMapping("login")
//	public String login2(){
//		return "登陆成功";
//	}
}
