package com.zz.hc.controller;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.zz.hc.model.Product;
import com.zz.hc.model.User;

@RestController
public class ShowDataController {

	@RequestMapping("getData")
	public List<Product> GetData(HttpServletRequest request){
		 List<Product> ls=new ArrayList();
		User u=(User) request.getSession().getAttribute("user");
		System.out.println("======"+u.getName());
		SimpleDateFormat sdf=new SimpleDateFormat("MM-dd-yyyy");
		Date dt=new Date();
		Product p1=new Product();
		p1.setName("p1");
		p1.setPayDate(sdf.format(dt));
		p1.setStatuc("代发货");
		
		Product p2=new Product();
		p2.setName("p2");
		p2.setPayDate(sdf.format(dt));
		p2.setStatuc("代发货");
		
		Product p3=new Product();
		p3.setName("p3");
		p3.setPayDate(sdf.format(dt));
		p3.setStatuc("代发货");
		
		Product p4=new Product();
		p4.setName("p4");
		p4.setPayDate(sdf.format(dt));
		p4.setStatuc("代发货");
		ls.add(p1);
		ls.add(p2);
		ls.add(p3);
		ls.add(p4);
		return ls;
	}
}
