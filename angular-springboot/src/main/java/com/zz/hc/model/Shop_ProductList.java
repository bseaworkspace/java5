package com.zz.hc.model;

import java.util.List;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import com.zz.hc.model.Shop_Product;

@JsonIgnoreProperties(ignoreUnknown=true)
public class Shop_ProductList {
	List<Shop_Product> productList;

	public List<Shop_Product> getProductList() {
		return productList;
	}

	public void setProductList(List<Shop_Product> productList) {
		this.productList = productList;
	}

}
