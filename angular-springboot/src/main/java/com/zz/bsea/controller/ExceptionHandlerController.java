package com.zz.bsea.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

@ControllerAdvice
public class ExceptionHandlerController {
	
	@ExceptionHandler(value=Exception.class)
	//@ResponseBody
	public String defaultHandler(HttpServletRequest req,Exception e){
		
		e.printStackTrace();
		System.out.println("coming to defaultHandler");
		
		return "/bsea/pageDemo1.html";
		
	}

}
