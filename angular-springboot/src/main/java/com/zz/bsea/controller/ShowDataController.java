package com.zz.bsea.controller;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.zz.bsea.mode.Product;
import com.zz.bsea.mode.User;

@RestController
public class ShowDataController {
	
	@RequestMapping("getData")
	public List<Product> getData(HttpServletRequest request){
		List<Product> ls=new ArrayList();
		User u=(User) request.getSession().getAttribute("user");
		System.out.println("ShowDataController---->userName="+u.getName());
		 SimpleDateFormat bartDateFormat = new SimpleDateFormat("MM-dd-yyyy");
		 Date date= new Date();
		Product p1=new Product();
		p1.setName("p1");
		p1.setPayDate(bartDateFormat.format(date));
		p1.setStatuc("待发货");
		Product p2=new Product();
		p2.setName("p2");
		p2.setPayDate(bartDateFormat.format(date));
		p2.setStatuc("发货中");
		Product p3=new Product();
		p3.setName("p3");
		p3.setPayDate(bartDateFormat.format(date));
		p3.setStatuc("待确认");
		
		ls.add(p1);
		ls.add(p2);
		ls.add(p3);
		
		
		return ls;
		
	}

}
