package com.zz.bsea.controller;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.zz.bsea.dao.user.LoginDao;
import com.zz.bsea.mode.User;

@RestController
public class LoginController {
	@Autowired
	LoginDao lDao;
	
	@RequestMapping(value="login",method=RequestMethod.POST)
	public Map login(@RequestParam("name") String userName,@RequestParam("password") String pwd,HttpServletRequest request){
		System.out.println("userName"+userName+" ---pwd---"+pwd);
		User u=lDao.getUserByNamePWD(userName, pwd);
		Map m=new HashMap();
		String result="N";
		if(u!=null){
			result="Y";
			HttpSession session=request.getSession();
			session.setAttribute("user", u);
		}
		
		m.put("rs", result);
		return m;
	}

}
