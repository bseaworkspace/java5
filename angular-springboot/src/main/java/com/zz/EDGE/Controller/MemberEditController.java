package com.zz.EDGE.Controller;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.zz.EDGE.dao.LoginDao;
import com.zz.EDGE.dao.RegisterDao;
import com.zz.EDGE.dao.UserDao;
import com.zz.EDGE.model.User;

@RestController
public class MemberEditController {
	@Autowired()
	RegisterDao register;
	@Autowired()
	UserDao ud;
	// 代替了LoginDao udao=new LoginDao();

	@RequestMapping(value = "MemberEdit", method = RequestMethod.GET)
	public Map receive() {
		Map m = new HashMap();
		int u_id = 30;
		User u = ud.getUserByNamePWD(30);
		System.out.println(u.getU_nickname());
		System.out.println(u.getU_phone());
		System.out.println(u.getU_email());
		m.put("rs", u);
		return m;
	}

	@RequestMapping(value = "Memberedit", method = RequestMethod.POST)
	public Map login(@RequestParam("username") String userName, @RequestParam("email") String email,
			@RequestParam("phonenumber") String phone) {
		System.out.println("userName" + userName + "--phone--" + phone + "--email--" + email);
		Map m = new HashMap();
		String result = "N";
		int u_id = 30;

		if (ud.updateUser(userName, email, phone, 30)) {
			result = "Y";
		}

		m.put("rs", result);
		User u = ud.getUserByNamePWD(30);
		m.put("u", u);
		System.out.println(result);
		return m;

	}

}
