package com.zz.EDGE.Controller;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.zz.EDGE.dao.LoginDao;
import com.zz.EDGE.dao.RegisterDao;
import com.zz.EDGE.model.User;

@RestController
public class RegisterController {
	@Autowired()
	RegisterDao register;
	@Autowired()
	LoginDao ld;
	//代替了LoginDao udao=new LoginDao();
	
	@RequestMapping(value="register",method=RequestMethod.GET)
	public Map login(@RequestParam("name") String userName,@RequestParam("password") String pwd,@RequestParam("email") String email, @RequestParam("phone") String phone, HttpServletRequest request){
		System.out.println("userName"+userName+"--pwd--"+pwd);		
		Map m=new HashMap();
		String result="N";
		User u=ld.getUserByNamePWD(userName, pwd);
		if(u==null){
		if(register.AddUser(userName, pwd, email, phone)){
			result="Y";		
		}
		}
		m.put("rs", result);
		return m;
		}
	//@RequestMapping(value="login",method=RequestMethod.POST)和
	//@PostMapping("login")效果一样只接受post请求
	//@PostMapping("login")
	/**public String login2(){
		return "登陆成功";
	}*/
	}
