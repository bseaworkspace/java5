package com.zz.EDGE.Controller;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.zz.EDGE.dao.LoginDao;
import com.zz.EDGE.model.User;

@RestController
public class LoginController {
	@Autowired()
	LoginDao loginDao;
	//代替了LoginDao udao=new LoginDao();
	
	@RequestMapping(value="login",method=RequestMethod.POST)
	public Map login(@RequestParam("name") String userName,@RequestParam("password") String pwd,HttpServletRequest request){
		System.out.println("userName"+userName+"--pwd--"+pwd);
		//LoginDao udao=new LoginDao();
		User u=loginDao.getUserByNamePWD(userName, pwd);
		Map m=new HashMap();
		String result="N";
		if(u!=null){
			result="Y";
		HttpSession session=request.getSession();
		session.setAttribute("userName", userName);
		}
		m.put("rs", result);
		return m;
		}
	//@RequestMapping(value="login",method=RequestMethod.POST)和
	//@PostMapping("login")效果一样只接受post请求
	//@PostMapping("login")
	/**public String login2(){
		return "登陆成功";
	}*/
	}
