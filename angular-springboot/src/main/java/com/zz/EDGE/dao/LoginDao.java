package com.zz.EDGE.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.zz.EDGE.model.User;


@Service("loginDao")
public class LoginDao {
	@Autowired
	BaseDao baseDao;
		public User getUserByNamePWD(String name,String pwd){
			User u=null;
			//BaseDao bd=new BaseDao();
			Connection con=baseDao.getConnection();
			try {
			String sql="select u_id,u_name,u_pwd from user where u_pwd=? and u_name=?";
				PreparedStatement ps=con.prepareStatement(sql);
				ps.setString(1, pwd);
				ps.setString(2, name);
				ResultSet rs=ps.executeQuery();
				if(rs.next()){
					u=new User();
					u.setU_id(rs.getInt("u_id"));
					u.setPwd(rs.getString("u_pwd"));
					u.setName(rs.getString("u_name"));				
				}
				baseDao.closeQuery(rs, ps, con);
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return u;
		}
}
