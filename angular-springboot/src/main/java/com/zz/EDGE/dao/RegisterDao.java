package com.zz.EDGE.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.assertj.core.api.BDDAssertions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.zz.EDGE.model.User;
@Service("registerDao")
public class RegisterDao {
	@Autowired
	BaseDao bd;
	public List<String> getUser(){
		List<String> ls=new ArrayList<String>();
		String name1="";
		Connection con=bd.getConnection();
		try {
		String sql="select u_name from user ";
			PreparedStatement ps=con.prepareStatement(sql);
			ResultSet rs=ps.executeQuery();
			while(rs.next()){
				name1=new String();
				name1=rs.getString("u_name");
				ls.add(name1);				
			}
			bd.closeQuery(rs, ps, con);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return ls;
	}
	public boolean  AddUser(String name,String pwd,String email,String phone){
		boolean flag=false;
		Connection con=bd.getConnection();
		try {
		String sql="insert into `user`(u_name,u_pwd,u_email,u_phone)values(?,?,?,?)";
			PreparedStatement ps=con.prepareStatement(sql);
			ps.setString(1, name);
			ps.setString(2, pwd);
			ps.setString(3, email);
			ps.setString(4, phone);
			if(ps.executeUpdate()>0){
				flag=true;
			}
			bd.closeUpdate(ps, con);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return flag;
	}
}
