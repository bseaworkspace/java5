package com.zz.EDGE.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.zz.EDGE.model.User;

@Service("ud")
public class UserDao {
	@Autowired
	BaseDao bd;
	public boolean updateUser(String name,String email,String phone,int u_id){
		boolean flag=false;
		Connection con=bd.getConnection();
		try {
		String sql="update user set u_nickname=?,u_email=? , u_phone=? where u_id=?";
			PreparedStatement ps=con.prepareStatement(sql);
			ps.setString(1, name);
			ps.setString(2, email);
			ps.setString(3, phone);
			ps.setInt(4, u_id);
			if(ps.executeUpdate()>0){
				flag=true;
			}
			bd.closeUpdate(ps, con);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return flag;
	}
	public User getUserByNamePWD(int u_id){
		User u=null;
		//BaseDao bd=new BaseDao();
		Connection con=bd.getConnection();
		try {
		String sql="select u_nickname,u_email,u_phone from user where u_id=?";
			PreparedStatement ps=con.prepareStatement(sql);
			ps.setInt(1, u_id);
			ResultSet rs=ps.executeQuery();
			if(rs.next()){
				u=new User();
				u.setU_nickname(rs.getString("u_nickname"));
				u.setU_email(rs.getString("u_email"));
				u.setU_phone(rs.getString("u_phone"));
				u.setU_id(u_id);
			}
			bd.closeQuery(rs, ps, con);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return u;
	}
}
