package com.zz.EDGE.model;

import java.util.List;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown=true)
public class Shop_ProductList {
	List<Shop_product> productList;

	public List<Shop_product> getProductList() {
		return productList;
	}

	public void setProductList(List<Shop_product> productList) {
		this.productList = productList;
	}
	

}