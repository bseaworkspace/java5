package com.zz.EDGE.model;

public class User {
private int u_id;
private String pwd;
private String name;
private String u_phone;
private String u_email;
private String u_nickname;
private String sex;
public String getSex() {
	return sex;
}
public void setSex(String sex) {
	this.sex = sex;
}
public String getU_nickname() {
	return u_nickname;
}
public void setU_nickname(String u_nickname) {
	this.u_nickname = u_nickname;
}
public String getU_phone() {
	return u_phone;
}
public void setU_phone(String u_phone) {
	this.u_phone = u_phone;
}
public String getU_email() {
	return u_email;
}
public void setU_email(String u_email) {
	this.u_email = u_email;
}
public int getU_id() {
	return u_id;
}
public void setU_id(int u_id) {
	this.u_id = u_id;
}
public String getPwd() {
	return pwd;
}
public void setPwd(String pwd) {
	this.pwd = pwd;
}
public String getName() {
	return name;
}
public void setName(String name) {
	this.name = name;
}
}
