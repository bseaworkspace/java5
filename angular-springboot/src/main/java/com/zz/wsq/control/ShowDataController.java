package com.zz.wsq.control;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.zz.wsq.model.Product;



@RestController
public class ShowDataController {
	
	@RequestMapping("getData")
	public List<Product> getData(HttpServletRequest request){
		List<Product> ls=new ArrayList();
		String name=(String) request.getSession().getAttribute("userName");
		System.out.println("ShowDataController---->userName="+name);
		Product p1=new Product();
		SimpleDateFormat bartDateFormat=new SimpleDateFormat("MM-dd-yy");
		Date date=new Date();
		p1.setName("p1");
		p1.setParDate(bartDateFormat.format(date));
		p1.setStatus("待发货");
		Product p2=new Product();
		p2.setName("p2");
		p2.setParDate(bartDateFormat.format(date));
		p2.setStatus("发货中");
		Product p3=new Product();
		p3.setName("p3");
		p3.setParDate(bartDateFormat.format(date));
		p3.setStatus("待确认");
		
		ls.add(p1);
		ls.add(p2);
		ls.add(p3);
		
		
		return ls;
		
	}

}
