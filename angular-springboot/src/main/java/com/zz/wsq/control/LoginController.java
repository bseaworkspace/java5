package com.zz.wsq.control;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.zz.wsq.dao.user.LoginDao;
import com.zz.wsq.model.User;

@RestController
public class LoginController {
	@Autowired  
		LoginDao loginDao; // 代替了  LoginDao ld=new LoginDao();
	  
	
	@RequestMapping(value="login",method=RequestMethod.POST)
	public Map login(@RequestParam("name") String userName,@RequestParam("pwd") String pwd,HttpServletRequest request){
		System.out.println("userName"+userName+" ---pwd---"+pwd);
		//LoginDao ld=new LoginDao();
		User u=loginDao.getUserByNamePwd(userName,pwd);
		Map m=new HashMap();
		String result="N";
		if(u!=null){
			result="Y";
			HttpSession session=request.getSession();
			session.setAttribute("userName", userName);
		}
		m.put("rs", result);
		return m;
	
	}
	// @RequestMapping(value="login",method=RequestMethod.POST) 和@PostMapping("login")是一样的
 /*	@PostMapping("login")
	public String login2(){
		return "登录成功";
	}
       */
}
