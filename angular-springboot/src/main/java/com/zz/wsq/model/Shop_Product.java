package com.zz.wsq.model;


public class Shop_Product {
	private String name;
	private Integer price;
	private Integer count;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getPrice() {
		return price;
	}
	public void setPrice(Integer price) {
		this.price = price;
	}
	public int getCount() {
		return count;
	}
	public void setCount(Integer count) {
		this.count = count;
	}
	

}
