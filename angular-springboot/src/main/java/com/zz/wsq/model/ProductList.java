package com.zz.wsq.model;

import java.util.List;


public class ProductList {
	public List<Shop_Product> getProductList() {
		return productList;
	}

	public void setProductList(List<Shop_Product> productList) {
		this.productList = productList;
	}

	List<Shop_Product> productList;

}
