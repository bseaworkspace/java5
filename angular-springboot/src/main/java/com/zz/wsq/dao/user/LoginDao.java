package com.zz.wsq.dao.user;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.zz.wsq.dao.BaseDao;
import com.zz.wsq.model.User;
@Service("loginDao") //loginDao 它是参数可以随便定义
public class LoginDao {
	//BaseDao basedao=new BaseDao();
	@Autowired
	BaseDao basedao;
	public User getUserByNamePwd(String name,String pwd){
		User u=null;
		Connection con=basedao.getConnection();
		String sql="select *from user where  name=? and pwd=?";
		try {
			PreparedStatement ps=con.prepareStatement(sql);			
		
			ps.setString(1, name);
			ps.setString(2, pwd);
			ResultSet rs=ps.executeQuery();
			while(rs.next()){
				u=new User();
			    u.setId(rs.getInt("u_id"));
				u.setPwd(rs.getString("pwd"));
				u.setName(rs.getString("name"));
				u.setPhoneNumber(rs.getString("phoneNumber"));
				u.setEmail(rs.getString("email"));
				
			}basedao.closeQuery(rs, ps, con);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return u;
	}
	public User getUserByName(String name){
		User u=null;
		Connection con=basedao.getConnection();
		String sql="select *from user where  name=? ";
		try {
			PreparedStatement ps=con.prepareStatement(sql);			
		
			ps.setString(1, name);
			
			ResultSet rs=ps.executeQuery();
			while(rs.next()){
				u=new User();
			    u.setId(rs.getInt("u_id"));
				u.setPwd(rs.getString("pwd"));
				u.setName(rs.getString("name"));
				u.setPhoneNumber(rs.getString("phoneNumber"));
				u.setEmail(rs.getString("email"));
				
			}basedao.closeQuery(rs, ps, con);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return u;
	}
	
	
	public boolean add(User u){
		boolean flag=false;
		Connection con=basedao.getConnection();
		String sql="insert into user (name,pwd) value(?,?)";
		try {
			PreparedStatement ps=con.prepareStatement(sql);
			ps.setString(1, u.getName());
			ps.setString(2, u.getPwd());
			if(ps.executeUpdate()>0){
				flag=true;
			}basedao.closeUpdate(ps, con);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return flag;
	}
	
}
