package com.zz.jwh.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.zz.jwh.model.Product;
import com.zz.jwh.model.User;

@RestController
public class ShowData {
	@RequestMapping("getData")
	public List<User> getDate(HttpServletRequest request){
		List<User> ls=new ArrayList();
		String name=(String) request.getSession().getAttribute("userName");
		System.out.println("userName>>>>>>>>>>>>>>>"+name);
		User u1=new  User();
		u1.setName("u1");
		u1.setPhoneNumber("18723132126");
		u1.setId(1);
		User u2=new  User();
		u2.setName("u2");
		u2.setPhoneNumber("13632132124");
		u2.setId(2);
		User u3=new  User();
		u3.setName("u3");
		u3.setPhoneNumber("18745645656");
		u3.setId(3);
		ls.add(u1);
		ls.add(u2);
		ls.add(u3);
		
		
		return ls;
	}
	
	@GetMapping("updata")
	public Map updata(@RequestParam("name") String name,@RequestParam("phoneNumber") String phoneNumber,@RequestParam("id") int id){
		System.out.println("name---------"+name);
		System.out.println("pn-----------"+phoneNumber);
		System.out.println("id-----------"+id);
		Map m=new HashMap();
		m.put("rs", "Y");
		return m;
	
	}
}
