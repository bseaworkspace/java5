package com.zz.jwh.model;

import java.util.Date;

public class Product {
	private String name;
	private String status;
	private Date parDate;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public Date getParDate() {
		return parDate;
	}
	public void setParDate(Date parDate) {
		this.parDate = parDate;
	}

}
