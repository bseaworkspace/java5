package com.zz.jwh.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.zz.jwh.BaseDao.BaseDao;
import com.zz.jwh.model.User;
@Service("loginDao")
public class LoginDao {
	@Autowired
	BaseDao basedao;
	public User getUserByNamePwd(String name,String pwd){
		User u=null;
		
		Connection con=basedao.getConnection();
		String sql="select u_id,pwd,name from user where pwd=? and name=?";
		try {
			PreparedStatement ps=con.prepareStatement(sql);
			ps.setString(1, pwd);
			ps.setString(2, name);
			ResultSet rs=ps.executeQuery();
			while(rs.next()){
				u=new User();
				u.setId(rs.getInt("u_id"));
				u.setName(rs.getString("name"));
				u.setPwd(rs.getString("pwd"));
			}basedao.closeQuery(rs, ps, con);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return u;
		
		
	}
	public User getUserByName(String name){
		User u=null;
		BaseDao bd=new BaseDao();
		Connection con=bd.getConnection();
		String sql="select u_id,pwd,name from user where  name=?";
		try {
			PreparedStatement ps=con.prepareStatement(sql);
			ps.setString(1, name);
			ResultSet rs=ps.executeQuery();
			while(rs.next()){
				u=new User();
				u.setId(rs.getInt("u_id"));
				u.setName(rs.getString("name"));
				u.setPwd(rs.getString("pwd"));
			}bd.closeQuery(rs, ps, con);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return u;
		
		
	}

}
