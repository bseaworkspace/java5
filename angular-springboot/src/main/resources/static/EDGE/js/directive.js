app.directive("aaaa",function(){
	//js中{}表示一个对象，{key:value,key2:value2....}
return {
		restrict:"E",
		template:'<ol class="breadcrumb">	<li><a href="#">Home</a></li>	<li><a href="#">2013</a></li>	<li class="active">十一月</li></ol>'
		
}

})

app.directive("bbbb",function(){
	//js中{}表示一个对象，{key:value,key2:value2....}
return {
		restrict:"A",
		template:'<div class="table-responsive">	<table class="table">		<caption>响应式表格布局</caption>		<thead>			<tr>				<th>产品</th>				<th>付款日期</th>				<th>状态</th>			</tr>		</thead>		<tbody>			<tr>				<td>产品1</td>				<td>23/11/2013</td>				<td>待发货</td>			</tr>			<tr>				<td>产品2</td>				<td>10/11/2013</td>				<td>发货中</td>			</tr>			<tr>				<td>产品3</td>				<td>20/10/2013</td>				<td>待确认</td>			</tr>			<tr>				<td>产品4</td>				<td>20/10/2013</td>				<td>已退货</td>			</tr>		</tbody></table></div>  	'
}

})
app.directive("cccc",function(){
	//js中{}表示一个对象，{key:value,key2:value2....}
return {
		restrict:"C",
		template:'<div class="progress">    <div class="progress-bar progress-bar-success" role="progressbar"         aria-valuenow="60" aria-valuemin="0" aria-valuemax="100"         style="width: 90%;">        <span class="sr-only">90% 完成（成功）</span>    </div></div>	'
}

})
