var app=angular.module('ngRouteExample', ['ngRoute'])
app.config(function ($routeProvider) {
    $routeProvider.
    when('/home', {
        templateUrl: 'angular3.html',
        controller: 'testController'
    }).
    when('/about', {
        templateUrl: 'embedded.about.html',
        controller: 'AboutController'
    }).
    otherwise({
        redirectTo: '/home'
    });
});