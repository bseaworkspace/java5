var app=angular.module("myapp",[]);
app.controller("myctr",function($scope,$http){
	//模拟数据，开发中，需要从后台拿到object list
		$http({
		    method: 'GET',
		    url: '/ng-sp/toPage',
			params:{'page':1}
		}).then(function successCallback(response) {
			$scope.students=response.data.rs;
			$scope.totalPage=response.data.rs1;
			$scope.pageRows=response.data.rs2;
			$scope.displayPages=response.data.rs3;
			$scope.currentPage=response.data.rs4;
			$scope.pageIndex=response.data.rs5;
		    }, function errorCallback(response) {		       
		});
	
	
	/*//一页显示三行
	$scope.pageRows=3;
	//当前页
	$scope.currentPage=1;
	console.log("8%3="+(8%3));
	console.log("8/3="+(8/3));
	console.log("parseInt(8/3)="+parseInt(8/3));	
	alert($scope.students.length);
	
	//$scope.totalPages=parseInt($scope.totalPages);
	
	
	
	
	
	console.log("总页数="+$scope.totalPages);
	$scope.pageIndex=[];
	//页码，demo里面，最多是2个页码
	$scope.displayPages=2;*/
		
		
		
	/*for(var i=1;i<$scope.currentPage+$scope.displayPages;i++){
		$scope.pageIndex.push(i);
	}*/
	$scope.toPage=function(index){
		$scope.currentPage=index;
		//把当前页传到后台， 然后 更新数据， 也就是
		//把新的数据赋值给$scope.students
		$http({
			    method: 'GET',
			    url: '/ng-sp/toPage',
				params:{'page':index}
			}).then(function successCallback(response) {
				$scope.students=response.data.rs;
				$scope.totalPage=response.data.rs1;
				$scope.pageRows=response.data.rs2;
				$scope.displayPages=response.data.rs3;
				$scope.currentPage=response.data.rs4;
				$scope.pageIndex=response.data.rs5;
			    }, function errorCallback(response) {		       
			});
	}
	
	
	$scope.pro=false;
	$scope.next=false;
	if(	$scope.totalPage>2){
		$scope.next=true;
	}
	
	$scope.nextPage=function(){
		$scope.currentPage=$scope.pageIndex[$scope.displayPages-1]+1;
		//alert($scope.currentPage);
		$scope.pageIndex=[];
		var maxPage=0;
		if($scope.currentPage<0){
			$scope.currentPage=1;
			$scope.next=true;
		}
		if($scope.totalPages<=$scope.currentPage+$scope.displayPages){
			maxPage=$scope.totalPages+1;
			$scope.next=false;
		}else{
			maxPage=$scope.currentPage+$scope.displayPages;			
		}
		for(var i=$scope.currentPage;i<maxPage;i++){
			$scope.pageIndex.push(i);
		}
		
	}
	
	$scope.preousPage=function(){
		$scope.currentPage=$scope.pageIndex[0]-$scope.displayPages;
		//alert($scope.currentPage);
		if($scope.currentPage<0){
			$scope.currentPage=1;
			$scope.pro=false;
		}
		if($scope.currentPage<=$scope.totalPages-$scope.displayPages){
			$scope.next=true;
		}
		$scope.pageIndex=[];
		for(var i=$scope.currentPage;i<$scope.currentPage+$scope.displayPages;i++){
			$scope.pageIndex.push(i);
		}
	}
	
	$scope.$watch("currentPage",function(){
		if($scope.currentPage>$scope.displayPages){
			$scope.pro=true;
		}
		/**
		$scope.totalIndexPage=($scope.totalPages%$scope.displayPages)==0?$scope.totalPages/$scope.displayPages:$scope.totalPages/$scope.displayPages+1
		$scope.totalIndexPage=parseInt($scope.totalIndexPage);
		if($scope.currentPage>$scope.totalIndexPage*$scope.displayPages){
			$scope.next=false;
		}
		**/
		
	});
	
	
	
	
})