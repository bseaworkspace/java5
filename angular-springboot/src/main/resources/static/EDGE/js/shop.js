var app=angular.module("myapp",[]);
app.controller("myctr",function($scope,$http,$interval){
	$scope.incomplete=true;
	$scope.time=false;
	$scope.start=true;
	$scope.products=[
		{selected:false,name:"洗衣机",price:3000,count:1},
		{selected:false,name:"电视机",price:6000,count:1},
		{selected:false,name:"缝纫机",price:2000,count:1},
		{selected:false,name:"油烟机",price:4000,count:1}
		
	];
	$scope.totalAMT=0;
	$scope.add=function(){
		$scope.computertotalamount();
	}
	$scope.changecount=function(){
		$scope.computertotalamount();
	}
	$scope.computertotalamount=function(){
		$scope.totalAMT=0;
		angular.forEach($scope.products,function(p){
			if(p.selected){				
				$scope.totalAMT=$scope.totalAMT+p.price*p.count;
			}
		})
	}
	$scope.pay=function(){
		selectedproductList=[];
		angular.forEach($scope.products,function(p){
			if(p.selected){
				selectedproductList.push(p);
			}
		})
		/*productList=JSON.stringify({
			"productList":selectedproductList
		})*/
		$http.post('/ng-sp/pay',{"productList":selectedproductList}).then(function successCallback(response) {
			console.log(response.data);
			    }, function errorCallback(response) {
			       
			});
	}
	
	$scope.addsubmit=function(){
		var prod={
			name:$scope.addName,
			price:$scope.addPrice,
			count:$scope.addnNumber
		};
		/*angular.forEach($scope.products,function(p){
			if(p.selected){
				selectedproductList.push(p);
			}
		})*/
		/*productList=JSON.stringify({
			"productList":selectedproductList
		})*/
		$http.post('/ng-sp/addProduct',prod).then(function successCallback(response) {
			console.log(response.data);
			    }, function errorCallback(response) {
			       
			});
	}
	
	$scope.addproduct=function(){
		$("#myModal").modal('show');
		
	}
	
	$scope.timeleft=1000;
	$scope.examtime=2000*60;
	$interval(function(){
		if($scope.timeleft>0){
			$scope.timeleft=$scope.timeleft-1000;
		}else{
			$interval.cancel;
		}
	},1000)
	
	$scope.$watch('timeleft',function(){$scope.test();});
		
	
	
	$scope.test=function(){
		if($scope.timeleft>0){			
			$scope.incomplete=true;
			$scope.time=false;
			$scope.start=true;
		}
		if($scope.timeleft==0){
			$scope.incomplete=false;
			$scope.time=true;
			$scope.start=false;
		}
	}
	
});