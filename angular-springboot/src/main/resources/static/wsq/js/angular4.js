var app=angular.module("myapp",[])
app.controller("myctr",function($scope){
	/*$scope 用于 myctr包的div 里面的ng-model
	ngularJS 应用组成如下：
	View(视图), 即 HTML。
	Model(模型), 当前视图中可用的数据。
	Controller(控制器), 即 JavaScript 函数，可以添加或修改属性。
	scope 是模型。
	scope 是一个 JavaScript 对象，带有属性和方法，这些属性和方法可以在视图和控制器中使用。*/
	
	$scope.firstname="这个是默认的名字"
	
})