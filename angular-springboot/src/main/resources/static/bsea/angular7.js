var app=angular.module("myapp",[])
/**
 * 
 * 当在控制器中添加 $scope 对象时，视图 (HTML) 可以获取了这些属性。
视图中，你不需要添加 $scope 前缀, 只需要添加属性名即可，如： {{carname}}。

Scope 概述
AngularJS 应用组成如下：
View(视图), 即 HTML。
Model(模型), 当前视图中可用的数据。
Controller(控制器), 即 JavaScript 函数，可以添加或修改属性。 
scope 是模型。
scope 是一个 JavaScript 对象，带有属性和方法，这些属性和方法可以在视图和控制器中使用。
 */
app.controller("myctr",function($scope){
	$scope.firstname="sdfGD"
	
})
