var app=angular.module('ngRouteExample', ['ngRoute']);
app.config(function ($routeProvider) {
    $routeProvider.
    when('/home', {
        templateUrl: 'angular11.html',
        controller: testController($scope)
        
    }).
    when('/about', {
        templateUrl: 'embedded.about.html',
        controller: 'AboutController'
    }).
    otherwise({
        redirectTo: '/home'
    });
});