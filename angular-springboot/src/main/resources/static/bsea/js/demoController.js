demoApp.controller("myctr",function($scope,productService){
	$scope.products=[
		{selected:false,name:"洗衣机",price:3000,count:1},
		{selected:false,name:"电视机",price:6000,count:1},
		{selected:false,name:"缝纫机",price:2000,count:1},
		{selected:false,name:"油烟机",price:4000,count:1}
	]
	$scope.totalAMT=0;
	
	$scope.add=function(){
		$scope.computtotalAMT();
	}
	$scope.changeCount=function(){
		$scope.computtotalAMT();
	}
	$scope.computtotalAMT=function(){
		$scope.totalAMT=0;
		angular.forEach($scope.products,function(p){
			if(p.selected){
				$scope.totalAMT=$scope.totalAMT+p.price*p.count;
			}
			
		})
	}
	
	$scope.pay=function(){
		var productList=[];
		angular.forEach($scope.products,function(p){
			if(p.selected){
				productList.push(p);
			}
		})
		
		productService.passProducts(productList).then(function successCallback(response) {
			        // 请求成功执行代码
				console.log(response.data);
	    }, function errorCallback(response) {
	        // 请求失败执行代码
		});
	}
	$scope.addSubmit=function(){
			var prod={
				name:$scope.addName,	
				price:$scope.addPrice,	
				count:$scope.addCount	
			};
			
			productService.add(prod).then(function successCallback(response) {
				        // 请求成功执行代码
				console.log(response.data);
				    }, function errorCallback(response) {
					        // 请求失败执行代码
				});
		
	}
	$scope.addProd=function(){
		$('#myModal').modal('show')
		
	}
	
});