var app=angular.module("loginApp",[])
app.controller("loginController",function($scope,$http){
	
	$scope.onClickLogin=function(){
		alert("用户名是："+$scope.name)
		alert("密码："+$scope.pwd)
		// 简单的 GET 请求，可以改为 POST
		$http({
		    method: 'POST',
		    url: '/ng-sp/login',
		    params:{'name':$scope.name,'password':$scope.pwd}
		}).then(function successCallback(response) {
		        // 请求成功执行代码
				var rs=response.data.rs;
				if("Y"==rs){
	          		window.location.href="/ng-sp/bsea/showData.html";
	          	}else{
	          		$scope.errorMsg="用户名或者密码错误，请重新输入";
	          		$('#myModal').modal('show')
	          	}
		    }, function errorCallback(response) {
		        // 请求失败执行代码
		});
		
	}
})