var app=angular.module("myapp",[]);
app.controller("myctr",function($scope,$http){
    $scope.products=[
        {selected:false,name:"洗衣机",price:3000,count:1},
        {selected:false,name:"电视机",price:6000,count:1},
        {selected:false,name:"缝纫机",price:2000,count:1},
        {selected:false,name:"油烟机",price:4000,count:1}
    ];

    $scope.totalAMT=0;

    $scope.add = function(){

        $scope.totalAMT=0;
        angular.forEach($scope.products,function (p) {

            if(p.selected){
                $scope.totalAMT = $scope.totalAMT+p.price*p.count;
            }
        })

    }
    
    $scope.changeCount = function(a){
    	alert(a)
        $scope.totalAMT=0;
        angular.forEach($scope.products,function (p) {

            if(p.selected){
                $scope.totalAMT = $scope.totalAMT+p.price*p.count;
            }
        })

    }
    $scope.pay=function(){
		selectedproductList=[];
		angular.forEach($scope.products,function(p){
			if(p.selected){
				selectedproductList.push(p);
			}
		})
		productList=JSON.stringify({
			"productList":selectedproductList
		})
		
		$http.post('/ng-sp/pay',productList).then(function successCallback(response) {
			        // 请求成功执行代码
				alert(response.data.rs);
	    }, function errorCallback(response) {
	        // 请求失败执行代码
	});
		
	}
    $scope.addsumit=function(){
		
		var Product={
				name:$scope.addName,
				count:$scope.addCount,
				price:$scope.addPrice
		
		};
		console.log(Product)
		
		
		$http.post('/ng-sp/addProduct',Product).then(function successCallback(response) {
			        // 请求成功执行代码
				alert(response.data.rs);
	    }, function errorCallback(response) {
	        // 请求失败执行代码
	});
		
	}
    $scope.addProd=function(){
    	$("#myModal").modal("show");
    }
})