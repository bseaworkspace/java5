var app = angular.module("myApp", []);
app.controller("myController", function($scope,$http,postObject,productService) {

	$scope.products = [

	{
		selected : false,
		name : "牙签",
		price : 5,
		count : 1
	}, {
		selected : false,
		name : "牙膏",
		price : 20,
		count : 1
	}, {
		selected : false,
		name : "电视机",
		price : 6666,
		count : 1
	}, {
		selected : false,
		name : "油烟机",
		price : 5555,
		count : 1
	}, {
		selected : false,
		name : "洗衣机",
		price : 4444,
		count : 1
	},

	];

	$scope.totalAMT = 0;
	$scope.add = function() {
		computerAMT();

	};
	$scope.changeNum = function() {
		computerAMT();
	};

	function computerAMT() {
		$scope.totalAMT = 0;
		angular.forEach($scope.products, function(p) {
			if (p.selected) {
				$scope.totalAMT = $scope.totalAMT + p.count * p.price;
			}

		});
	}
	$scope.addP=function(){
		
		$('#myModal').modal('show');
	}
	
	$scope.pushAddp=function(){
 
		var product={
				name:$scope.addName,
				price:$scope.addPrice,
				count:$scope.addCount
		}
	productService.pushAddpService(product)
	.then(function success(response){
		alert(response.data.rs);
		}
	,function error(response){
		
	});
 		//postObject.pushAddp(product);
	}
	$scope.pay=function(){
		 var postObjectList=[];
		angular.forEach($scope.products, function(p) {
			if (p.selected) {
				postObjectList.push(p);		
				}

		});
	/*	postObjectList=JSON.stringify({
			"postObjectList":selectedproductList
		})*/
	/*	
		$http.post("/ng-sp/postObject",{postObjectList})
		.then(function successCallback(response){
			console.log(response.data);
		},function errorCallback(response){
			
		});*/
		postObject.postObject(postObjectList);
		
		
	}

});

//factory
app.factory('postObject', function($http, $q) {
	var factory = {};
	factory.postObject = function(postObjectList) {
		var defer = $q.defer();
		$http({
			url : "/ng-sp/postObject",
			method : "POST",
			data :postObjectList
			
		}).success(function(data) {
			defer.resolve(data);
		}).error(function(data, status, headers, config) {
			// defer.resolve(data);
			defer.reject(data);
			
		});
		return defer.promise;
	};
	
	
	factory.pushAddp=function(product){
		var defer=$q.defer();
		var text=1;
		$http({
		url:"/ng-sp/pushAddp",
		method:"POST",
		data:{product,text}
		}
		).success(function(data){
			defer.resolve(data);
		}).error(function(data,status,heads,config){
			defer.reject(data);
		});
		
		return defer.promise;
	};
 
	return factory;

});

//service 
app.service("productService",function($http){
	this.pushAddpService=function(product){
		
		return $http.post("/ng-sp/pushAddp",product);
	}
	this.postObjectService=function(postObjectList){
		return $http.post("/ng-sp/postObject",postObjectList);
	}
})


