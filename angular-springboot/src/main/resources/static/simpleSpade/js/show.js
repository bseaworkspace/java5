/**
 * Created by jiyu on 2017/10/25.
 */
var app=angular.module("myapp",[]);
app.controller("myctrl",function($scope,$http){
    $scope.products=[
        {selected:false,name:"洗衣机",price:3000,count:1},
        {selected:false,name:"电视机",price:6000,count:1},
        {selected:false,name:"缝纫机",price:2000,count:1},
        {selected:false,name:"油烟机",price:4000,count:1}
    ];

    $scope.totalAMT=0;

    computtotalAMT = function(){
        $scope.totalAMT=0;
        angular.forEach($scope.products,function (p) {

            if(p.selected){
                $scope.totalAMT = $scope.totalAMT+p.price*p.count;
            }
        })
    }

    $scope.add = function(){

        computtotalAMT();

    }

    $scope.changeCount = function(){

        computtotalAMT();

    }

    $scope.pay = function(){
        selectedproductList = [];
        angular.forEach($scope.products,function(p){
            if(p.selected){
                selectedproductList.push(p);
            }
        })
        productList=JSON.stringify({
            "productList":selectedproductList
        })

        $http.post('/ng-sp/pay',productList).then(function successCallback(){

        },function error(){

        })
    }
})