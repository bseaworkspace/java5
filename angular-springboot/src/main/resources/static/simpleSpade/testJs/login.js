var app = angular.module("loginApp", [])

app.controller("loginController", function($scope,$http) {

	$scope.onclickLogin = function() {
		alert("用户名是：" + $scope.name)
		alert("密码是：" + $scope.pwd)

		$http({
			method : 'POST',
			url : '/ng-sp/login',
			params  : {name: $scope.name,pwd : $scope.pwd}
		}).then(function successCallback(response) {
			// 请求成功执行代码
			alert(response.data.result)
			var rs = response.data.result;
			if("Y"==rs){
				window.location.href="/ng-sp/simpleSpade/showData.html"
			}else{
				$scope.msgTitle = "登录失败";
				$scope.errorMsg = "用户名或者密码错误，请重新输入";
				$("#myModal").modal("show")
				
			}
		}, function errorCallback(response) {
			// 请求失败执行代码
		});
	}

})