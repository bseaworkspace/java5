package com.zz.wsq.redis.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.zz.wsq.redis.mode.User;

@RestController
public class UserController {
	@Autowired
	private StringRedisTemplate stringRedisTemplate;
	@Autowired
	private RedisTemplate<String, User> redisTemplate;
	@RequestMapping("/login/{name}/{pwd}")
	public String login(@PathVariable("name")String name,@PathVariable("pwd")String pwd){
		//第一步根据用户名和密码到mysql数据库拿到user对象，并且存到session
		//多了一步，同时把user对象存到redis里面
		User user=new User("wsq",18);
		redisTemplate.opsForValue().set(user.getUsername(), user);
		return "123";
		
	}
	@RequestMapping("showUser/{name}")
	public String showUser(@PathVariable("name")String name){
		//第一步从session里面去拿u
		//如果session里面没有
		long age=redisTemplate.opsForValue().get(name).getAge().longValue();
		return age+"";
		//基本类型转String+""
	}

}
