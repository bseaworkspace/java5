package com.zz.wsq.redis.mode;

import java.io.Serializable;

import javax.annotation.Generated;

import org.hibernate.annotations.Entity;
import org.springframework.data.annotation.Id;

@Entity
public class Dog implements Serializable{
	private static final long serialVersionUID= 1L;
	
	@Id@Generated(value = { "" })
	private int id;
	private String name;
	private String pwd;
	private String skil;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getPwd() {
		return pwd;
	}
	public void setPwd(String pwd) {
		this.pwd = pwd;
	}
	public String getSkil() {
		return skil;
	}
	public void setSkil(String skil) {
		this.skil = skil;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
}
