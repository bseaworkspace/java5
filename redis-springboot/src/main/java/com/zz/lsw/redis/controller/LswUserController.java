package com.zz.lsw.redis.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.zz.lsw.redis.model.User;

@RestController
public class LswUserController {
	@Autowired
	private StringRedisTemplate stringRedisTemplate;
	
	@Autowired
	private RedisTemplate<String, User> redisTemplate;
	
	@RequestMapping("/login/{name}/{pwd}")
	public String login(@PathVariable("name") String name,@PathVariable("pwd") String pwd){
		//第一步
		//多一步，把User对象存在redis
		User user=new User("lsw", 21);
		redisTemplate.opsForValue().set(user.getUsername(), user);
		return "登陆成功";
	}
	
	@RequestMapping("/showUser/{name}")
	public String showUser(@PathVariable("name") String name){
		//第一步
		//多一步，把User对象存在redis
		User user=redisTemplate.opsForValue().get(name);
		redisTemplate.opsForValue().set(user.getUsername(), user);
		return user.getAge().toString();
	}
}
