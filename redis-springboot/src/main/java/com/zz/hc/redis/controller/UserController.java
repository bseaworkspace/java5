package com.zz.hc.redis.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.zz.hc.redis.mode.User;

@RestController
public class UserController {
	
	@Autowired
	private RedisTemplate<String, User> redisTemplate;
	
	@RequestMapping("login/{name}/{pwd}")
	public String login(@PathVariable("name")String name,@PathVariable("pwd")String pwd){
		//第一步根据用户名和密码,到mysql拿到user对象,并存到session里
		//多了一个步骤,把user对象存到redis
		User user =new User("wsq",60);
		redisTemplate.opsForValue().set(user.getUsername(), user);
		
		
		return "登录成功";
	}
	@RequestMapping("/showuser/{name}")
	public String showuser(@PathVariable("name")String name){
		//第一步去session里拿u
		//如果session里没有
		User user=redisTemplate.opsForValue().get(name);
	
		
		return user.getAge().toString();
	}
	

}
