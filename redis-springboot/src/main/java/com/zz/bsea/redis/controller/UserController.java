package com.zz.bsea.redis.controller;

import org.junit.Assert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.zz.bsea.redis.mode.User;

@RestController
public class UserController {
	@Autowired
	private RedisTemplate<String, User> redisTemplate;
	
	@Autowired
	private StringRedisTemplate stringRedisTemplate;
	
	@RequestMapping("/login/{name}/{pwd}")
	public String login(@PathVariable("name") String name,@PathVariable("pwd") String pwd){
		//第一步  根据用户名和密码，到mysql数据库 拿到user对象，并且存到session.
		//多了一步骤。 把user对象存到redis
		User user = new User("wsq", 18);
		stringRedisTemplate.opsForValue().set("aaa", "111");
		redisTemplate.opsForValue().set(user.getUsername(), user);
		return "登录成功";
	}
	@RequestMapping("/showUser/{name}")
	public String showUser(@PathVariable("name") String name){
		//第一步 从session里面去拿 u
		//如果session里面没有,
		//System.out.println("showUser===="+redisTemplate.opsForValue().get("wsq").getAge().longValue());
		//System.out.println("myj===="+redisTemplate.opsForValue().get("myj").getAge().longValue());
		return stringRedisTemplate.opsForValue().get("aaa");
	}

}
