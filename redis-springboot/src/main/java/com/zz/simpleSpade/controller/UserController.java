package com.zz.simpleSpade.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.zz.simpleSpade.redis.model.User;



@RestController
public class UserController {
	@Autowired
	private RedisTemplate<String, User> redisTemplate;

	@RequestMapping("/login/{name}/{pwd}")
	public String login(@PathVariable("name") String name, @PathVariable("pwd") String pwd) {
		// 第一步 根据用户名和密码到mysql数据库拿到user对象，并且存到session
		// 多了一步。把user对象存到redis
		User user = new User("wsq", 18);
		redisTemplate.opsForValue().set(user.getUsername(), user);

		return "登陆成功";

	}
	
	@RequestMapping("/showUser/{name}")
	public String showUser(@PathVariable("name") String name) {
		// 第一步 根据用户名和密码到mysql数据库拿到user对象，并且存到session
		// 多了一步。把user对象存到redis
		User user = redisTemplate.opsForValue().get(name);

		return user.getAge().toString();

	}

}
