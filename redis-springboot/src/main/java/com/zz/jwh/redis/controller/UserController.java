package com.zz.jwh.redis.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.zz.jwh.redis.model.User;

@RestController
public class UserController {

	@Autowired
	private RedisTemplate<String, User> redisTemplate;
	
	@RequestMapping("/login/{name}/{pwd}")
	public String login(@PathVariable("name")String name, @PathVariable("pwd") String pwd){
		//第一步
		/**
		 * 根据用户名的密码 到mysql数据库拿到user对象  并存到session
		 * 
		 */
		//第二部
		/**
		 * 把 user 对象 存到redis中
		 * 
		 */
		

		User user = new User("wsq1", 18);
		redisTemplate.opsForValue().set(user.getUsername(), user);
		
		return "登陆成功 name---"+name+"pwd-----"+pwd;
		
	}
	@RequestMapping("/show/{name}")
	public String show(@PathVariable("name")String name){
		//第一步
		/**
		 * 第一步从session 中拿 u
		 * 
		 */
		//第二部
		/**
		 * 把 user 对象 存到redis中
		 * 
		 */
		System.out.println("nam--------"+name);
		
		System.out.println(redisTemplate.opsForValue().get("surperman1").getAge().longValue());
		return "登陆成功 name---";
		
	}
}
