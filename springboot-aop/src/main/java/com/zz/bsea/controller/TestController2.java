package com.zz.bsea.controller;

import org.apache.log4j.Logger;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TestController2 {
	
	@RequestMapping("hello333")
	public String helloSpringBoot222(@RequestParam("name") String name){
		
		return  "hello SpringBoot"+name;
	}

}
