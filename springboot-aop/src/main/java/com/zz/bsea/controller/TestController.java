package com.zz.bsea.controller;

import org.apache.log4j.Logger;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TestController {
	
	private Logger log=Logger.getLogger(getClass());
	
	@RequestMapping("hello")
	public String helloSpringBoot(){
		log.debug("this is from Log4j debug");
		log.info("this is from Log4j Info");
		log.warn("this is from Log4j warn");
		log.error("this is from Log4j error");
		return  "hello SpringBoot";
	}
	@RequestMapping(value="helloGet",method=RequestMethod.GET)
	public String helloSpringBoot2(){
		
		return  "hello SpringBoot";
	}
	@RequestMapping(value="helloPost",method=RequestMethod.POST)
	public String helloSpringBoot3(){
		
		return  "hello SpringBoot";
	}

}
