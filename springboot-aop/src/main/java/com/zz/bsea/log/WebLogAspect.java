package com.zz.bsea.log;

import java.util.Arrays;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

@Aspect
@Order(5)
@Component
public class WebLogAspect {
	
	Logger logger=Logger.getLogger(getClass());
	
	@Pointcut("execution(* com.zz.bsea.controller..*.*(..))")
	public void webLog(){
		
	}
	
	@Before("webLog()")
	public void doBefore(JoinPoint jp){
		
		ServletRequestAttributes ab=(ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
		HttpServletRequest request=ab.getRequest();
		
		logger.info("URL:---"+request.getRequestURI().toString());
		logger.info("Http method--"+request.getMethod());
		logger.info("ip---"+request.getRemoteAddr());
		logger.info("class method---"+jp.getSignature().getDeclaringTypeName());
		logger.info("args---"+Arrays.toString(jp.getArgs()));
		
	}
	
	@AfterReturning(returning="ret",pointcut="webLog()")
	public void afterReturning(Object ret){
		logger.info("返回值:---"+ret);
	}

}
